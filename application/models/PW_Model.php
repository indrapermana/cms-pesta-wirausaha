<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class PW_Model extends CI_Model {

  function __construct(){
    parent::__construct();
  }

  // Login
  public function login($user, $pass){
    $mpass = $this->user_pass($pass);
    // die($mpass);
    $sql = "select id, nama, email, userlevel, userstatus from t_user 
      where username = ? and passwd = ? and userstatus = 1";
    $query = $this->db->query($sql,array($user,$mpass));
    return $query;
  }

  // Password Login
  function user_pass($pass) {
    return md5("p35T4$$".$pass."##w1R4u5^#4");
  }

  /* ------------ User Start ----------- */
  function get_user(){
    $sql = "select * from t_user";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_user_by_id($id){
    $sql = "select * from t_user where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function check_email($email){
    $sql = "select * from t_user where email = ? and userlevel = 2";
    $query = $this->db->query($sql, array($email));
    return $query->result();
  }

  //activate user account
  function verifyEmailID($key){
    $data = array('user_status' => 1);
    $this->db->where('kode_aktivasi', $key);
    return $this->db->update('t_user', $data);
  }

  function user_insert($username, $passwd, $nama, $email, $telepon){
    // return $this->db->insert('t_user', $data)->insert_id();
    $passwd = $this->user_pass($passwd);
    $sql = "insert into t_user (id, username, passwd, nama, email, telepon, reg_date, kode_aktivasi, userlevel, userstatus) values (null, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($username, $passwd, $nama, $email, $telepon, date('Y-m-d H:i:s'), substr(md5($email), 0, 10), 2, 0));
  }
  /* ------------ User End ----------- */

  // Get Data Kwitansi by Id
  function get_kwitansi_by_id($id) {
    $sql = "select * from t_kwitansi where id = ? ";
    $query = $this->db->query($sql,array($id));
    return $query->result_array(); 
  }

  /* ------------ Rekening Mutasi Start ----------- */
  function get_rekening($event_id){
    $sql = "select * from t_rekening where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_rekening_by_id($id){
    $sql = "select * from t_rekening where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function rekening_insert($event_id, $kode, $nama, $nomor, $bank){
    $sql = "insert into t_rekening (event_id, kode, nama, nomor, bank) values (?, ?, ?, ?, ?)";
    $this->db->query($sql, array($event_id, $kode, $nama, $nomor, $bank));
  }

  function rekening_update($id, $kode, $nama, $nomor, $bank){
    $sql = "update t_rekening set kode=?, nama=?, nomor=?, bank=? where id=?";
    $this->db->query($sql, array($id, $kode, $nama, $nomor, $bank));
  }

  function rekening_delete($id){
    $sql = "delete from t_rekening where id = ?";
    $this->db->query($sql, array($id));
  }

  /* ------------ Rekening End ----------- */ 

  /* ------------ Rekening Mutasi Start ----------- */ 
  // Get Data Bank
  function get_bank($kode) {
    $sql = "select id, nama, nomor, bank from t_rekening where kode = ?";
    $query = $this->db->query($sql,array($kode));
    return $query->result_array();
  }

  // Get Data Transaksi Rekening
  function get_trans_rekening($tipetrans) { 
    $sql = "select id, tanggal, no_referensi, deskripsi, debet, kredit, saldo, tipe_trans, keterangan, settle_code from t_rekening_mutasi where  tipe_trans = ? order by tanggal "; 
    $query = $this->db->query($sql,array($tipetrans));
    return $query->result_array(); 
  }	

  // Update Mutasi Rekening Settlecode
  function set_rekenening_settlecode($mutasiid,$settlecode) {
    $sql = "update t_rekening_mutasi set settle_code = ? where id = ? ";
    $this->db->query($sql,array($settlecode,$mutasiid));
  }

  // Get Data Rekening
  function get_rekening_mutasi($rekeningid,$periode="") { 
    $xparam = array();
    $sql = "select id, tanggal, no_referensi, deskripsi, debet, kredit, saldo, tipe_trans, keterangan from t_rekening_mutasi where rekening_id = ? ";
    $xparam[] = $rekeningid;
    if ($periode!="") {
      $sql .= "and date_format(tanggal,'%m%y') = ? ";
      $xparam[] = $periode;
    }
    $sql .= "order by tanggal "; 
    $query = $this->db->query($sql,$xparam);
    return $query->result_array(); 
  }

  // Get Data Rekapa Rekening
  function get_rekap_rekening($rekeningid) { 
    $sql = "select date_format(tanggal,'%m%y') as periode, tipe_trans, sum(kredit) as nilai from t_rekening_mutasi where rekening_id = ? group by date_format(tanggal,'%m%y'), tipe_trans "; 
    $query = $this->db->query($sql,array($rekeningid)); 
    return $query->result_array(); 
  }	

  // Check Rekening
  function cek_rekening_mutasi($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo) {
    $sql = "select id from t_rekening_mutasi where rekening_id = ? and tanggal = ? and no_referensi = ? and deskripsi = ? and debet = ? and kredit = ? and saldo = ?";
    $query = $this->db->query($sql,array($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo));
    return $query->result_array(); 
  }

  // Get Rekening by Id
  function get_rekening_mutasi_byid($rekid) {
    $sql = "select id, tanggal, no_referensi, deskripsi, debet, kredit, saldo, tipe_trans, keterangan from t_rekening_mutasi where id = ?";
    $query = $this->db->query($sql,array($rekid));
    return $query->result_array();			
  }

  // Insert Data Rekening
  function rekening_mutasi_insert($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo) {
    $sql = "insert into t_rekening_mutasi (rekening_id,tanggal,no_referensi,deskripsi,debet,kredit,saldo,sync_status) values (?,?,?,?,?,?,?,1) ";
    $this->db->query($sql,array($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo));
    
    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->result_array();			
  } 

  // Syncron Rekening
  function rekening_sync($rekmutid) {
    $sql = "update t_rekening_mutasi set sync_status = 1 where id = ? ";
    $this->db->query($sql,array($rekmutid));
  }

  // Update Data Mutasi
  function upd_mutasi($rekid,$tipetrans,$keterangan) {
    $sql = "update t_rekening_mutasi set tipe_trans = ?, keterangan = ? where id = ?";
    $this->db->query($sql,array($tipetrans,$keterangan,$rekid));
  }
  /* ------------- Rekening End ------------- */ 

  /* ------------- Tiket Start ------------- */ 
  // Update Data Bayar Tiket
  function upd_tiket_bayar($tiketid,$bayarid,$tanggal) {
    $sql = "update t_tiket set id_bayar = ?, tgl_bayar = ? where id = ?";
    $this->db->query($sql,array($bayarid,$tanggal,$tiketid));
  }

  // Get Count tiket By Event
  function get_totaltiket($event_id) {
    $sql = "select * from t_tiket where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();		
  }

  function get_totaltiket_by_status($event_id, $status){
    $sql = "select a.nama, a.email, a.usaha, a.nilai, a.tgl_bayar, b.nama as type_tiket, a.invoice, a.no_hp from t_tiket a inner join t_layanan b on a.tipe_tiket=b.id where a.event_id = ? and a.status_bayar = ?";
    $query = $this->db->query($sql, array($event_id, $status));
    return $query->result_array();
  }

  function get_tiket_user_registration($event_id){
    $sql = "select a.nama, a.email, a.usaha, a.nilai, a.tgl_bayar, b.nama as type_tiket, a.invoice, a.no_hp from t_tiket a inner join t_layanan b on a.tipe_tiket=b.id where a.event_id = ? and a.id_bayar!='0'";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_tiket_by_invoice($invoice){
  	$this->db->from('t_tiket');
    $this->db->where('invoice', $invoice);
    $query = $this->db->get();
    return $query->result();
	}

  // Get Data Group Tiket
  function get_grouptiket() {
    $sql = "select id, invoice, affid, tanggal, nama, kontak, no_hp, email, nilai, nilai_lunas, status_bayar, jml_tiket, jml_terjual, n_nilai, n_pw, n_hadiah from t_tiketgroup_pw2019 order by nama ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  // Get Data Transaksi Group Tiket
  function get_grouptiket_trans($grouptiketid) {
    $sql = "select id, tanggal, jml_tiket, nilai_bayar, catatan from t_tiketgroup_trans where tiketgroup_id = ? order by tanggal ";
    $query = $this->db->query($sql,array($grouptiketid));
    return $query->result_array();
  }

  // Insert Data Group tiket
  function grouptiket_insert($groupid,$tiket,$bayar,$catatan) {
    $sql = "insert into t_tiketgroup_trans (tiketgroup_id,tanggal,jml_tiket,nilai_bayar,catatan) values (?,now(),?,?,?) ";
    $this->db->query($sql,array($groupid,$tiket,$bayar,$catatan)); 
  }

  // Get Data Group Tiket By Id
  function grouptiket_by_id($id) {
    $sql = "select invoice, affid, tanggal, nama, kontak, no_hp, email, nilai, nilai_lunas, status_bayar, jml_tiket, jml_terjual, n_nilai, n_pw, n_hadiah from t_tiketgroup_pw2019 where id = ? ";
    $query = $this->db->query($sql,array($id)); 
    return $query->result_array();
  }

  // Get Data Target Tiket
  function get_targettiket() {
    $sql = "select id, nilai from t_param_pw2019 where param = 'TIKET' ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  // Update Data Target Tiket
  function upd_targettiket($nilai) {
    $sql = "update t_param_pw2019 set nilai = ? where param = 'TIKET' ";
    $this->db->query($sql,array($nilai));
  }

  // Get Data Rekap Tiket
  function get_rekaptiket($event_id) {
    $sql = "select sum(jml_tiket) as jml_tiket, sum(n_nilai) as nilai, sum(n_pw) as pw, sum(n_hadiah) as hadiah, sum(n_daerah) as daerah, sum(n_affiliate) as affiliate, sum(n_kode) as kode from t_tiket where kode_bayar = 1 and jml_tiket = 1 and event_id = ? ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();		
  }

  // Get Data Tiket
  function get_datatiket($event_id) {
    $sql = "select id, invoice, tanggal, tipe_tiket, nama, usaha, no_hp, email, affid, daerah, kode_bayar, n_nilai, n_pw, n_daerah, n_affiliate, n_kode from t_tiket where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  // Get Data Tiket Unsettle / Belum bayar
  function get_datatiket_unsettle($event_id) {
    $sql = "select id, invoice, tanggal, tipe_tiket, nama, usaha, no_hp, email, affid, daerah, kode_bayar, n_nilai, n_pw, n_daerah, n_affiliate, n_kode from t_tiket where id_bayar = 0 and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  // Get Data Tiket By Id
  function get_tiket_byid($tiketid) {
    $sql = "select id, invoice, tanggal, nama, no_hp, email, usaha, n_nilai,aksi_bayar,approver,id_bayar from t_tiket where id = ?";
    $query = $this->db->query($sql, array($tiketid));
    return $query->result_array();
  }

  // Get Data Daerah Tiket
  function get_tiket_daerah($event_id) {
    $sql = "select daerah, sum(jml_tiket) as jml_tiket, sum(n_daerah) as nilai from t_tiket where status_bayar = 1  and event_id = ? group by daerah order by jml_tiket desc ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  // Get Data Affiliate / Anggota Reveral
  function get_tiket_affiliate($event_id) {
    $sql = "select affid, count(*) as jml_tiket, sum(n_affiliate) as nilai from t_tiket where status_bayar = 1 and event_id = ? group by affid order by nilai desc ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  // Get Data Kode Affiliate / Anggota Reveral
  function get_kode_affiliate() {
    $sql = "select id, affid, nama, usaha, daerah from t_aff_pw2019";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_tiket_by_affid($id, $limit=""){
    $this->db->select("a.id, b.nama as nama_event, a.nama, a.usaha, a.no_hp, c.nama as nama_layanan, a.affid, a.nilai, a.status_bayar, c.komisi, c.harga, a.jumlah_tiket");
    $this->db->from('t_tiket a');
    $this->db->join("t_event b", 'a.event_id=b.id');
    $this->db->join("t_layanan c", "c.event_id=b.id");
    $this->db->where('a.affid', $id);
    $this->db->order_by('a.id','DESC');
    if($limit!=""){
      $this->db->limit($limit);
    }
    $query = $this->db->get();
    return $query->result();
  }

  // Get Data Wilayah Berdasarkan Jumlah Anggota
  function get_tiket_wilayah() {
    $sql = "select id, daerah, wilayah, jml_anggota, tiket_online, tiket_group_affid from t_tiket_wilayah order by jml_anggota desc"; 
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_tiket_unpaid_date($tanggal, $event_id) {
    $sql = "select id, invoice, tanggal, nama, no_hp, usaha, email, nilai, n_nilai FROM t_tiket where dtanggal > ?  and kode_bayar = 0 and event_id = ?";
    $query = $this->db->query($sql,array($tanggal, $event_id));
    return $query->result_array(); 		
  }

  function get_tiket_unpaid_sebelum_date($tanggal, $event_id) {
    $sql = "select id, invoice, tanggal, nama, no_hp, usaha, email, nilai, n_nilai FROM t_tiket where date(dtanggal) < ? and dtanggal <> '' and kode_bayar = 0 and event_id = ?";
    $query = $this->db->query($sql,array($tanggal, $event_id));
    return $query->result_array(); 		
  }

  function get_tiket_unpaid_sehari($event_id) {
    $sql = "select id, invoice, tanggal, nama, no_hp, usaha, email, nilai, n_nilai FROM t_tiket where dtanggal > timestampadd(day, -1, now()) and kode_bayar = 0 and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array(); 		
  }

  function get_tikethp_bayar($nohp, $event_id) {
    $sql = "select id from t_tiket where no_hp = ? and kode_bayar = 1 and event_id = ?";
    $query = $this->db->query($sql,array($nohp, $event_id));
    return $query->result_array();
  }

  function get_tiket_gaffid_aktif($event_id) {
    $sql = "select id, invoice, nama, no_hp, usaha, daerah, gaffid FROM t_tiket where gaffid > 0 and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array(); 		
  }

  function get_tiket_aktif($event_id) {
    $sql = "select id, invoice, tanggal, nama, no_hp, usaha, daerah, gaffid, n_nilai,aksi_bayar,approver,id_bayar FROM t_tiket where tipe_tiket in ('Early Bird','Regular','Regular,') and kode_bayar = 1 and jml_tiket = 1 and event_id = ? order by tanggal desc";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_tiket_by_gaffid($id){
    $sql = "select * from t_tiket where gaffid = ?";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function tiket_insert($data){
		//var_dump($data);
    $this->db->insert('t_tiket', $data);
		$insert_id = $this->db->insert_id();
  	return  $insert_id;
  }
  /* ------------- Tiket End ------------- */

  /* ------------- Tiket User Start ------------- */
  function get_tiket_user(){
    $sql = "select * from t_user_tiket";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_tiket_user_by_id($id){
    $sql = "select * from t_user_tiket where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }

  function get_tiket_user_by_userid($id){
    $sql = "select * from t_user_tiket where user_id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }

  function get_tiket_user_by_tiket_and_userid($tiket_id, $user_id){
    $sql = "select * from t_peserta where tiket_id = ? and user_id = ?";
    $query = $this->db->query($sql, array($tiket_id, $user_id));
    return $query->result();
  }

  function get_tiket_user_by_tiket_and_affid($tiket_id, $affid){
    $sql = "select * from t_peserta where tiket_id = ? and affid = ?";
    $query = $this->db->query($sql, array($tiket_id, $affid));
    return $query->result();
  }

  function tiket_user_insert($data){
    $this->db->insert('t_user_tiket', $data);
  }

  /* ------------- Tiket Pesrta End ------------- */

  /* ------------- Peserta Start ------------- */
  function get_peserta(){
    $sql = "select * from t_peserta";
    $query = $this->db->query($sql);
    return $query->result();
  }

  function get_peserta_by_id($id){
    $sql = "select * from t_peserta where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result();
  }

  function get_peserta_by_gaffid($id){
    $sql = "select * from t_peserta where gaffid = ? order by id desc";
    $query = $this->db->query($sql, array($id));
    return $query->row();
  }

  function get_peserta_by_event_and_gaffid($event_id, $gaffid){
    $sql = "select * from t_peserta where event_id = ? and gaffid = ?";
    $query = $this->db->query($sql, array($event_id, $gaffid));
    return $query->result();
  }

  function get_peserta_by_event_and_affid($event_id, $affid){
    $sql = "select * from t_peserta where event_id = ? and affid = ?";
    $query = $this->db->query($sql, array($event_id, $affid));
    return $query->result();
  }

  function peserta_insert($data){
    $this->db->insert('t_peserta', $data);
  }
  /* ------------- Peserta End ------------- */

  /* ------------- Expo Start ------------- */
  // Get Data Expo Booth 
  function get_expobooth() {
    $sql = "select a.id, a.no_booth, a.tipe_booth, c.nama as nama_booth, a.status_booth, b.status_pesan, a.booked_date, a.booked_by, b.nama as booked_nama, a.nilai_booth, b.bayar1 from t_booth_pw2019 a 
            left outer join t_expo_pw2019 b on a.booked_by = b.id
            left outer join t_tipebooth_pw2019 c on a.tipe_booth = c.id
            where tipe_booth in (1,2,3,8) 
            order by a.tipe_booth, a.no_booth";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_expobooth2() {
    $sql = "select a.id, a.map_type, a.no_booth, a.tipe_booth, c.nama as nama_booth, a.status_booth, a.booked_date, a.booked_by, b.nama as booked_nama, b.usaha as booked_usaha, a.usaha as booked_usaha2, b.status_pesan, a.nilai_booth 
            from t_booth_pw2019 a 
            left outer join t_expo_pw2019 b on a.booked_by = b.id
            left outer join t_tipebooth_pw2019 c on a.tipe_booth = c.id 
            where tipe_booth in (1,2,3,4,8) 
            order by a.no_booth";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_dataexpo1($event_id) {
    $sql = "select id, nama, usaha, email, no_hp, status_pesan from t_expo_pw2019 where status_pesan in (2,3,4,6) and event_id = ? order by status_pesan, nama";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();		
  }

  function get_dataexpo2() { 
    $sql = "select id, booked_by, no_booth, tipe_booth, map_type from t_booth_pw2019 where status_booth = 1 order by no_booth ";
    $query = $this->db->query($sql);
    return $query->result_array();		
  }

  function get_dataexpo3() { 
    $sql = "select id, booked_by, no_booth, tipe_booth, status_booth, map_type, nilai_booth from t_booth_pw2019 order by no_booth ";
    $query = $this->db->query($sql);
    return $query->result_array();		
  }

  function get_expo_aktif($event_id) {
    $sql = "select id, invoice, tanggal, nama, usaha, email, no_hp, nobooth, nilai_bayar from t_expo_pw2019 where status_pesan = 4 and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();		
  }

  function get_tipebooth($event_id) {
    $sql = "select id, nama, harga from t_tipebooth_pw2019 where id in (1,2,3) and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_dataexpo($event_id) {
    $sql = "select id, tanggal, invoice, kode_pesan, nama, usaha, email, no_hp, status_pesan, premium, reguler, kuliner, kuliner2, nobooth, nilai_bayar, bayar1, bayar2 from t_expo_pw2019 where event_id = ? order by tanggal desc";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_expo_invoice($event_id) {
    $sql = "select invoice+1 as invoice from t_expo_pw2019 where event_id = ? order by invoice desc limit 1";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array(); 
  }

  function expo_invoice($id,$invoice,$kodepesan) { 
    $sql = "update t_expo_pw2019 set invoice = ?, kode_pesan = ? where id = ? ";
    $this->db->query($sql,array($invoice,$kodepesan,$id));
  }

  function get_dataexpo_wl($event_id, $status="0") {
    $xparam = array(); 
    $sql = "select id, tanggal, nama, usaha, email, no_hp, minat, premium, reguler, kuliner, kuliner2, status_pesan, catatan, team_fu, status_fu, last_update, nilai_bayar, bayar1 from t_expo_pw2019 where event_id = ?"; $xparam[] = $event_id;
    if ($status!="0") {
      $sql .= " and status_pesan = ? "; $xparam[] = $status;
    }
    $sql .= "order by tanggal desc";
    $query = $this->db->query($sql,$xparam);
    return $query->result_array();  
  }

  function get_dataexpo_byid($expoid) {
    $sql = "select tanggal, invoice, kode_pesan, date_format(tanggal,'%e %b %Y') as stanggal, date_format(tanggal+1,'%e %b %Y %H:%m') as expdate, nama, usaha, email, no_hp, minat, catatan, status_pesan, premium, reguler, kuliner, kuliner2, nilai_bayar, bayar1, bayar2, team_fu, nobooth, last_update from t_expo_pw2019 where id = ? ";
    $query = $this->db->query($sql,array($expoid));
    return $query->result_array(); 
  }

  function get_dataexpo_tbh($event_id) {
    $sql = "select nama, usaha, email, no_hp, premium, reguler, kuliner, kuliner2, tbh_penjaga, tbh_daya, tbh_meja, tbh_kursi, tbh_tiket, tbh_tv, tbh_lainnya, tbh_penanggungjawab, tbh_status, tbh_nilai from t_expo_pw2019 where tbh_penjaga > 0 and event_id = ? order by usaha";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array(); 
  }

  function get_dataexpo_tiket($event_id) {
    $sql = "select a.id, a.nama as nama_tiket, a.email as email_tiket, a.hp, a.status_tiket, a.no_tiket, a.nilai, b.nama, b.usaha,  b.premium, b.reguler, b.kuliner, b.kuliner2 from t_expo_tiket a inner join t_expo_pw2019 b on a.expo_id = b.id where b.event_id = ? order by b.id, a.id ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function expo_pesan($event_id,$invoice,$kodepesan,$nama,$usaha,$email,$hp,$premium,$reguler,$kuliner,$kuliner2, $nilai) {
    $sql = "insert into t_expo_pw2019 (tanggal,event_id,invoice,kode_pesan,nama,usaha,email,no_hp,premium,reguler,kuliner,kuliner2, nilai_bayar,status_pesan) values (now(),?,?,?,?,?,?,?,?,?,?,?,?,2) ";
    $this->db->query($sql,array($event_id,$invoice,$kodepesan,$nama,$usaha,$email,$hp,$premium,$reguler,$kuliner,$kuliner2, $nilai));

    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->result_array();			
  }

  function expo_nilaibayar($expoid,$nobooth,$nilai,$kodebayar,$nilai1,$nilai2) {
    $sql = "update t_expo_pw2019 set nobooth = ?, nilai_bayar = ?, kode_bayar = ?, bayar1 = ?, bayar2 = ? where id = ?";
    $this->db->query($sql,array($nobooth,$nilai,$kodebayar,$nilai1,$nilai2,$expoid));
  }

  function expo_waitinglist($event_id,$invoice,$kodepesan,$nama,$usaha,$email,$hp,$minat,$catatan) {
    $sql = "insert into t_expo_pw2019 (tanggal,event_id,invoice,kode_pesan,nama,usaha,email,no_hp,minat,catatan,status_pesan) values (now(),?,?,?,?,?,?,?,?,?,1) ";
    $this->db->query($sql,array($event_id,$invoice,$kodepesan,$nama,$usaha,$email,$hp,$minat,$catatan));

    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->result_array();			
  }

  function expo_status($expoid,$status) { 
    $sql = "update t_expo_pw2019 set status_pesan = ? where id = ? ";
    $this->db->query($sql,array($status,$expoid));
  }

  function expo_cancel($expoid) { 
    $sql = "insert into t_booth_pw2019_bck (tanggal,no_booth,booked_date,booked_by,nilai_booth,nama,usaha) select now(), no_booth,booked_date,booked_by,nilai_booth,nama,usaha from t_booth_pw2019 where booked_by = ?";
    $this->db->query($sql,array($expoid));

    $sql = "update t_booth_pw2019 set status_booth = 0, booked_by = 0, nama = '', usaha = '' where booked_by = ? ";
    $this->db->query($sql,array($expoid)); 
  }

  function expo_booth($expoid,$nobooth,$premium,$reguler,$kuliner1,$kuliner2,$nilai) {
    $sql = "update t_expo_pw2019 set nobooth = ?, premium = ?, reguler = ?, kuliner = ?, kuliner2 = ?, nilai_bayar = ? where id = ? "; 
    $this->db->query($sql,array($nobooth,$premium,$reguler,$kuliner1,$kuliner2,$nilai,$expoid));
  }

  function exponote_list($expoid) {
    $sql = "select a.id, a.user_id, b.nama, a.tanggal, a.catatan from t_expo_note_pw2019 a left outer join t_user b on a.user_id = b.id where a.expo_id = ? ";
    $query = $this->db->query($sql,array($expoid));
    return $query->result_array();
  }

  function exponote_insert($expoid,$userid,$catatan,$status,$nobooth) {
    $sql = "insert into t_expo_note_pw2019 (expo_id,user_id,tanggal,catatan,status_pesan,nobooth) values (?,?,now(),?,?,?) "; 
    $this->db->query($sql,array($expoid,$userid,$catatan,$status,$nobooth)); 
  }

  function booth_booked($nobooth,$booker,$nilai) {
    $sql = "update t_booth_pw2019 set status_booth = 1, booked_date = now(), booked_by = ?, nilai_booth = ? where no_booth = ? ";
    $this->db->query($sql,array($booker,$nilai,$nobooth));
  }

  function booth_id($nobooth) { 
    $sql = "select a.id, a.status_booth, a.tipe_booth, b.nama as nama_booth, b.harga, a.booked_by from t_booth_pw2019 a inner join t_tipebooth_pw2019 b on a.tipe_booth = b.id where a.no_booth = ? "; 
    $query = $this->db->query($sql,array($nobooth));
    return $query->result_array();		
  }

  function booth_pos($nobooth,$posx,$posy) {
    $sql = "update t_booth_pw2019 set map_x = ?, map_y = ? where no_booth = ? ";
    $this->db->query($sql,array($posx,$posy,$nobooth));
  }

  function booth_tipe($maptype) { 
    $sql = "select id, no_booth, tipe_booth, map_x, map_y, status_booth from t_booth_pw2019 where map_type = ? ";
    $query = $this->db->query($sql,array($maptype));
    return $query->result_array();		
  }

  function booth_expo($expoid) { 
    $sql = "select a.id, a.status_booth, a.tipe_booth, b.nama as nama_booth, a.no_booth, a.nilai_booth, b.harga, a.booked_by from t_booth_pw2019 a 
        inner join t_tipebooth_pw2019 b on a.tipe_booth = b.id where a.booked_by = ? ";
    $query = $this->db->query($sql,array($expoid));
    return $query->result_array();
  }

  function kodeexpo_id($kode) {
    $sql = "select id from t_expo_kodebayar where kode = ? ";
    $query = $this->db->query($sql,array($kode));
    return $query->result_array();		
  }

  function kodeexpo_insert($kode) {
    $sql = "insert into t_expo_kodebayar (kode,status_kode) values (?,0) ";
    $this->db->query($sql,array($kode));
  }

  function kodeexpo_get() {
    $kode = 0;
    $sql = "select id, kode from t_expo_kodebayar where status_kode = 0 limit 1 ";
    $query = $this->db->query($sql,array($kode));
    $data_kode = $query->result_array();
    if (sizeof($data_kode)>0) {
      $kode_id = $data_kode[0]["id"];
      $kode = $data_kode[0]["kode"];
      
      $sql = "update t_expo_kodebayar set status_kode = 1, tanggal = now() where id = ? ";
      $this->db->query($sql,array($kode_id)); 
    }
    return $kode;
  }
  /* ------------- Expo End ------------- */

  /* ------------- Budget Start ------------- */
  // Get Data Budget
  function get_budget($event_id) {
    $sql = "select id, parent_id, kode_budget, item_budget, keterangan, budget1, budget2, budget3, realisasi from t_budget where event_id = ? order by urutan ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();		
  }

  // Get Data Budget By Id
  function get_budget_byid($budgetid) {
    $sql = "select parent_id, kode_budget, item_budget, keterangan, budget1, budget2, budget3 from t_budget where id = ? ";
    $query = $this->db->query($sql,array($budgetid));
    return $query->result_array();		
  }

  // Insert Data Budget
  function budget_insert($event_id, $induk,$kode,$item,$keterangan) {
    $sql = "insert into t_budget (event_id, parent_id,kode_budget,item_budget,keterangan) values (?,?,?,?,?) ";
    $this->db->query($sql,array($event_id,$induk,$kode,$item,$keterangan));
  }

  // Update Data Budget
  function budget_update($budgetid,$induk,$kode,$item,$keterangan) {
    $sql = "update t_budget set parent_id = ?, kode_budget = ?, item_budget = ?, keterangan = ? where id = ? ";
    $this->db->query($sql,array($induk,$kode,$item,$keterangan,$budgetid));
  }

  // Delete Data Budget
  function budget_delete($budgetid) {
    $sql = "delete from t_budget where id = ? ";
    $this->db->query($sql,array($budgetid));
  }

  // Get Data Item Budget
  function get_budgetitem() {
    $sql = "select id, budget_id, jenis_budget, jml, harga, ppn, jml_hari, (harga + if (ppn>0,round(harga*10/100),0))*jml*jml_hari as total from t_budget_item ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  // Get Data Item Budget By Id Item
  function get_budgetitem_byid($id) {
    $sql = "select budget_id, jenis_budget, jml, harga, ppn, jml_hari from t_budget_item where id = ? ";
    $query = $this->db->query($sql,array($id));
    return $query->result_array();		
  }

  // Get Data Item Budget By Id Budget
  function get_budgetitem_bybudgetid($budgetid) {
    $sql = "select id, jenis_budget, jml, harga, ppn, jml_hari from t_budget_item where budget_id = ? ";
    $query = $this->db->query($sql,array($budgetid));
    return $query->result_array();		
  }

  // Insert Data Item Budget
  function budgetitem_insert($budgetid,$jenis,$jumlah,$harga,$ppn,$jmlhari) {
    $sql = "insert into t_budget_item (budget_id,jenis_budget,jml,harga,ppn,jml_hari) values (?,?,?,?,?,?) ";
    $this->db->query($sql,array($budgetid,$jenis,$jumlah,$harga,$ppn,$jmlhari));

    $xppn = 0; 
    if ($ppn=="1") { $xppn = $harga * 0.1; }
    $total1 = ($harga + $xppn) * $jumlah;
    $total2 = $total1 * $jmlhari;

    $xkolom = "budget1";
    if ($jenis=="2") { $xkolom = "budget2"; }
    if ($jenis=="3") { $xkolom = "budget3"; }
    $sql = "update t_budget set ".$xkolom." = ? where id = ? ";
    $this->db->query($sql,array($total2,$budgetid));
  }

  // Update Data Item Budget
  function budgetitem_update($itemid,$jenis,$jumlah,$harga,$ppn,$jmlhari) {
    $sql = "update t_budget_item  set jenis_budget = ?, jml = ?, harga = ?, ppn = ?, jml_hari = ? where id = ? ";
    $this->db->query($sql,array($jenis,$jumlah,$harga,$ppn,$jmlhari,$itemid));

    $budgetid = 0;
    $sql = "select budget_id, jenis_budget, jml, harga, ppn, jml_hari from t_budget_item where id = ? ";
    $query = $this->db->query($sql,array($itemid));
    $data_budgetitem = $query->result_array();
    if (sizeof($data_budgetitem)>0) { $budgetid = $data_budgetitem[0]["budget_id"]; }

    $xppn = 0;
    if ($ppn=="1") { $xppn = $harga * 0.1; }
    $total1 = ($harga + $xppn) * $jumlah;
    $total2 = $total1 * $jmlhari;

    $xkolom = "budget1";
    if ($jenis=="2") { $xkolom = "budget2"; }
    if ($jenis=="3") { $xkolom = "budget3"; }
    $sql = "update t_budget set ".$xkolom." = ? where id = ? ";
    $this->db->query($sql,array($total2,$budgetid)); 
  }

  // Delete Data Item Budget
  function budgetitem_delete($id) {
    $sql = "delete from t_budget_item where id = ? ";
    $this->db->query($sql,array($id));

    $budgetid = 0; $jenis = 0;
    $sql = "select budget_id, jenis_budget, jml, harga, ppn, jml_hari from t_budget_item where id = ? ";
    $query = $this->db->query($sql,array($id));
    $data_budgetitem = $query->result_array();
    if (sizeof($data_budgetitem)>0) { 
      $budgetid = $data_budgetitem[0]["budget_id"]; 
      $jenis = $data_budgetitem[0]["jenis_budget"]; 
    }

    $xkolom = "budget1";
    if ($jenis=="2") { $xkolom = "budget2"; }
    if ($jenis=="3") { $xkolom = "budget3"; }
    $sql = "update t_budget set ".$xkolom." = 0 where id = ? ";
    $this->db->query($sql,array($budgetid)); 
  }
  /* ------------- Budget End ------------- */

  /* ------------- Hotel Start ------------- */
  function get_rekaphotel($event_id) {
    $sql = "select jenis_kamar, status_bayar, sum(jml_kamar) as jumlah, sum(nilai) as nilai from t_hotel where status_bayar in (0,1,2) and event_id = ? group by jenis_kamar, status_bayar";
    $query = $this->db->query($sql, array($event_id)); 
    return $query->result_array();
  }

  function get_hotel($event_id, $status="") {
    $xparam = array();
    $sql = "select id, tanggal, kode_booking, jenis_kamar, tgl_cekin, tgl_cekout, room_type, breakfast, nama_lengkap, email, nohp, no_tiket, asal_tda, no_ktp, file_ktp, keterangan, jml_kamar, nilai, status_bayar from t_hotel where event_id = ? "; $xparam[] = $event_id;
    if ($status!="") {
      $sql .= " and status_bayar = ? "; $xparam[] = $status;
    }
    $sql .= "order by tanggal desc";
    $query = $this->db->query($sql,$xparam);
    return $query->result_array();  
  }

  function get_hotel_aktif($event_id) {
    $sql = "select id, id as invoice, tanggal, nama_lengkap as nama, email, nohp as no_hp, jenis_kamar, asal_tda as usaha, nilai from t_hotel where status_bayar = 1 and event_id = ?";
    $query = $this->db->query($sql,array($event_id));
    return $query->result_array();  
  }

  function get_hotel_blass($event_id) {
    $sql = "select tanggal, jenis_kamar, kode_booking, tgl_cekin, tgl_cekout, nama_lengkap, nohp, jml_kamar, nilai, status_bayar from t_hotel where status_bayar in (0,1) and event_id = ? ";
    $query = $this->db->query($sql,array($event_id));
    return $query->result_array(); 
  }

  function get_hotel_list($event_id) {
    $sql = "select kode_booking, jenis_kamar, tgl_cekin, tgl_cekout, nama_lengkap, email, nohp, no_ktp, asal_tda, jml_kamar, numpax, room_type, breakfast, nilai, status_bayar, keterangan from t_hotel where status_bayar in (1,3,4,6) and event_id = ? order by tgl_cekin, tgl_cekout, room_type, nama_lengkap ";
    $query = $this->db->query($sql,array($event_id));
    return $query->result_array(); 
  }

  function get_hotelemail_blass($event_id, $limit=10) {
    $sql = "select id, nama_lengkap, email from t_hotel where status_bayar in (1,3,4,6) and email_kirim = 0 and event_id = ? limit ? ";
    $query = $this->db->query($sql,array($event_id,$limit));
    return $query->result_array(); 
  }

  function hotelemail_kirim($id) {
    $sql = "update t_hotel set email_kirim = 1 where id = ? "; 
    $this->db->query($sql,array($id));
  }

  function get_hotel_rekap($event_id, $status) {
    $sql = "select id, tanggal, kode_booking, jenis_kamar, tgl_cekin, tgl_cekout, numpax, room_type, breakfast, nama_lengkap, email, nohp, no_tiket, asal_tda, no_ktp, file_ktp, keterangan, keterangan2, jml_kamar, nilai, nilai_bf, status_bayar from t_hotel where event_id = ? and status_bayar = ? order by jenis_kamar, tgl_cekin, nama_lengkap";
    $query = $this->db->query($sql,array($event_id, $status));
    return $query->result_array();  
  }

  function get_hotel_rekap2($event_id) {
    $sql = "select jenis_kamar, tgl_cekin, tgl_cekout, numpax, room_type, jml_kamar, status_bayar from t_hotel where event_id = ? status_bayar in (0,1,3,4,6) ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();  
  }

  function get_hotel_rekap3($event_id) {
    $sql = "select tgl_cekin, tgl_cekout, room_type, status_bayar, sum(jml_kamar) as jumlah from t_hotel where event_id = ? and status_bayar in (0,1,3,4,6) group by tgl_cekin, tgl_cekout, room_type, status_bayar order by tgl_cekin, tgl_cekout, room_type ";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array(); 
  }

  function get_hotel_byid($hotelid) { 
    $sql = "select tanggal, jenis_kamar, kode_booking, tgl_cekin, tgl_cekout, date_format(tgl_cekin,'%e %b %Y') as cekin, date_format(tgl_cekout,'%e %b %Y') as cekout, numpax, room_type, breakfast, nama_lengkap, email, nohp, no_tiket, asal_tda, no_ktp, file_ktp, keterangan, keterangan2, jml_kamar, nilai, status_bayar, metoda_bayar, np_txid, np_resultCode, np_resultMsg, np_resultText, np_authNo, np_creditcard, np_expdate, np_bankcode, np_vanum, np_vavaliddate, np_vavalidtime from t_hotel where id = ? ";
    $query = $this->db->query($sql, array($hotelid));
    return $query->result_array(); 
  }

  function get_hotel_by_kode($kode){
    $sql = "select id, tanggal, jenis_kamar, date_format(tgl_cekin,'%e %b %Y') as cekin, date_format(tgl_cekout,'%e %b %Y') as cekout, room_type, breakfast, nama_lengkap, email, nohp, no_tiket, asal_tda, no_ktp,file_ktp,keterangan,jml_kamar, metoda_bayar,nilai, status_bayar from t_hotel where kode_booking = ?";
    $query = $this->db->query($sql, array($kode));
    return $query->result_array();
  }

  function get_hotel_by_no_kode(){
    $sql = "select id, nama_lengkap, jenis_kamar from t_hotel where kode_booking = '' ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function cek_hotel_kode($kode) {
		$sql = "select id from t_hotel where kode_booking = ? ";
		$query = $this->db->query($sql,array($kode));
		return $query->result_array();
	}

  function hotel_insert($event_id, $jenis,$kode,$cekin,$cekout,$nama,$email,$hp,$notiket,$asal,$ktp,$fktp,$keterangan,$jumlah,$nilai,$status,$metoda=0) {
		$sql = "insert into t_hotel (event_id, tanggal,jenis_kamar ,kode_booking,tgl_cekin, tgl_cekout,nama_lengkap,email,nohp,no_tiket,asal_tda,no_ktp,file_ktp,keterangan,jml_kamar,nilai,status_bayar,metoda_bayar) values (?,now(),?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?) ";
		$this->db->query($sql,array($event_id,$jenis,$kode,$cekin,$cekout,$nama,$email,$hp,$notiket,$asal,$ktp,$fktp,$keterangan,$jumlah,$nilai,$status,$metoda));

		$sql = "select last_insert_id() as id";
		$query = $this->db->query($sql);
		return $query->result_array();
	}

  function hotel_update($hotelid,$status,$jenis,$cekin,$cekout,$numpax,$roomtype,$jumlah,$keterangan,$keterangan2) { 
    $sql = "update t_hotel set status_bayar = ?, jenis_kamar = ?, tgl_cekin = ?, tgl_cekout = ?, numpax = ?, room_type = ?, jml_kamar = ?, keterangan = ?, keterangan2 = ? where id = ? ";
    $this->db->query($sql,array($status,$jenis,$cekin,$cekout,$numpax,$roomtype,$jumlah,$keterangan,$keterangan2,$hotelid));
  }

  function hotel_np1($id,$trxid) {
		$sql = "update t_hotel set np_txid = ? where id = ?";
		$this->db->query($sql,array($trxid,$id));
  }
  
  function hotel_np2($id,$creditcard,$expdate,$status) {
		$sql = "update t_hotel_test set np_creditcard = ?, np_expdate = ?, np_status = ? where id = ?";
		$this->db->query($sql,array($creditcard,$expdate,$status,$id));
	}

	function hotel_np3($id,$bankcode,$vaccnum,$validdate,$validtime,$status) {
		$sql = "update t_hotel_test set np_bankcode = ?, np_vanum = ?, np_vavaliddate = ?, np_vavalidtime = ?, np_status = ? where id = ?";
		$this->db->query($sql,array($bankcode,$vaccnum,$validdate,$validtime,$status,$id));
	}

  function hotel_track($kode) {
		$sql = "update t_hotel set email_status = 1 where kode_booking = ?";
		$this->db->query($sql,array($kode)); 
  }

  function hotel_npcode($id,$code,$msg,$txt) {
		$sql = "update t_hotel_test set np_resultCode = ?, np_resultMsg = ?, np_resultText = ? where id = ?";
		$this->db->query($sql,array($code,$msg,$txt,$id));
	}	

  function get_kamar_hotel_byhotelid($hotelid) { 
    $sql = "select id, no_kamar, tipe_kamar, nama, telepon, cekin, cekout, hal from t_hotel_kamar where hotel_id = ? ";
    $query = $this->db->query($sql,array($hotelid));
    return $query->result_array(); 
  }

  function get_kamar_hotel_byid($id) { 
    $sql = "select no_kamar, tipe_kamar, nama, telepon, cekin, cekout, hal from t_hotel_kamar where id = ? ";
    $query = $this->db->query($sql,array($id));
    return $query->result_array();
  }

  function kamarhotel_insert($hotelid,$nokamar,$tipekamar,$nama,$telepon,$cekin,$cekout,$hal) {
    $sql = "insert into t_hotel_kamar (hotel_id,no_kamar,tipe_kamar,nama,telepon,cekin,cekout,hal) values (?,?,?,?,?,?,?,?) ";
    $this->db->query($sql,array($hotelid,$nokamar,$tipekamar,$nama,$telepon,$cekin,$cekout,$hal));
  }

  function kamarhotel_update($id,$nokamar,$tipekamar,$nama,$telepon,$cekin,$cekout,$hal) {
    $sql = "update t_hotel_kamar set no_kamar = ?, tipe_kamar = ?, nama = ?, telepon = ?, cekin = ?, cekout = ?, hal = ? where id = ?";
    $this->db->query($sql,array($nokamar,$tipekamar,$nama,$telepon,$cekin,$cekout,$hal,$id));
  }

  function kamarhotel_delete($id) {
    $sql = "delete from t_hotel_kamar where id = ?";
    $this->db->query($sql,array($id));
  }

  function jml_book_hotel(){
    $sql = "select sum(jml_kamar) as jumlah from t_hotel where status_bayar in (0,1)";
    $query = $this->db->query($sql);
    return $query->row();
  }

  function hotelkode_get() {
		$kode = 0;
		$sql = "select id, kode from t_hotel_kode where status_kode = 0 limit 1";
		$query = $this->db->query($sql);
		$data_kode = $query->result_array();

    if (sizeof($data_kode)>0) {
			$kode_id = $data_kode[0]["id"];
			$kode = $data_kode[0]["kode"];

			$sql = "update t_hotel_kode set status_kode = 1 where id = ? ";
			$this->db->query($sql,array($kode_id)); 
		}

    return $kode;
  }
  
  function hotelkode_update($id,$kode,$cekin,$cekout) {
		$sql = "update t_hotel set kode_booking = ?, tgl_cekin = ?,tgl_cekout = ? where id = ? ";
		$this->db->query($sql,array($kode,$cekin,$cekout,$id));
	}

  /* ------------- Hotel End ------------- */

  function get_gaffid($event_id,$nama,$usaha,$daerah) {
    $sql = "select affid from t_aff_pw2019 where event_id = ? nama = ? and usaha = ? and daerah = ?";
    $query = $this->db->query($sql,array($event_id,$nama,$usaha,$daerah));
    return $query->result_array(); 		
  }

  function update_gaffid($id,$gaffid) {
    $sql = "update t_tiket set gaffid = ? where id = ?";
    $this->db->query($sql,array($gaffid,$id));
  }

  function email_log($emailgroup,$kode,$nama,$pengirim,$tujuan,$subject,$pesan,$status) {
    $sql = "insert into t_email_log (email_group,tanggal,kode,nama,pengirim,tujuan,subject,pesan,status_email) values (?,now(),?,?,?,?,?,?,?)";
    $this->db->query($sql,array($emailgroup,$kode,$nama,$pengirim,$tujuan,$subject,$pesan,$status));
  }

  /* ---------------- Nicepay Start ----------------- */ 
  function get_nicepay_pay() {
    $sql = "select id, tanggal, no_invoice, id_trans, jenis_trans, nama_lengkap, email, nohp, note_trans, metoda_bayar, nilai, payfee, total, status_bayar, np_bankcode, np_vanum, np_vavaliddate, np_vavalidtime from t_nicepay_sajah where jenis_trans < 10 order by tanggal desc ";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_nicepay_jenis($jenis) {
    $sql = "select id, tanggal, no_invoice, id_trans, jenis_trans, nama_lengkap, email, nohp, note_trans, metoda_bayar, nilai, payfee, total, status_bayar, np_bankcode, np_vanum, np_vavaliddate, np_vavalidtime from t_nicepay_sajah where jenis_trans = ? order by tanggal desc ";
    $query = $this->db->query($sql,array($jenis));
    return $query->result_array();
  }

  function nicepay_hapus($id) {
    $sql = "delete from t_nicepay_sajah where id = ?";
    $this->db->query($sql,array($id));
  }

  function nice_log($ngroup,$metoda,$dataget,$datapost) {
		$sql = "insert into t_nicepay_log (tanggal,ngroup,metoda,data_get,data_post) values (now(),?,?,?,?) ";
		$this->db->query($sql,array($ngroup,$metoda,$dataget,$datapost));
	}
  /* ---------------- Nicepay End ----------------- */ 

  /* ---------------- Berita Start ----------------- */ 
  function get_berita($event_id){
    $sql = "select * from t_berita where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_berita_byid($id){
    $sql = "select * from t_berita where id=?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function berita_insert($event_id, $tanggal, $judul, $gambar, $berita){
    $sql = "insert into t_berita (id, event_id, tanggal, judul, gambar, berita) values (null, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($event_id, $tanggal, $judul, $gambar, $berita));
  }

  function berita_update_gambar($id, $gambar){
    $sql = "update t_berita set gambar = ? where id = ?";
    $this->db->query($sql, array($gambar, $id));
  }

  function berita_update($id, $tanggal, $judul, $gambar, $berita){
    $sql = "update t_berita set tanggal=?, judul=?, gambar=?, berita=? where id=?";
    $this->db->query($sql, array($tanggal, $judul, $gambar, $berita, $id));
  }

  function berita_delete($id){
    $sql = "delete from t_berita where id=?";
    $this->db->query($sql, array($id));
  }
  /* ---------------- Berita End ----------------- */ 

  /* ---------------- Kategori Start ----------------- */ 
  function get_kategori(){
    $sql = "select * from t_kategori";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_kategori_by_slug($slug){
    $sql = "select * from t_kategori where slug = ?";
    $query = $this->db->query($sql, array($slug));
    return $query->result_array();
  }

  function get_kategori_byid($id){
    $sql = "select * from t_kategori where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function kategori_insert($nama, $slug){
    $sql = "insert into t_kategori (id, nama, slug) values (null, ?, ?, ?)";
    $this->db->query($sql, array($nama, $slug));
  }

  function kategori_update($id, $nama, $slug){
    $sql = "update t_kategori set nama = ?, slug = ? where id = ?";
    $this->db->query($sql, array($nama, $slug, $id));
  }

  function kategori_delete($id){
    $sql = "delete from t_kategori where id=?";
    $this->db->query($sql, array($id));
  }
  /* ---------------- Kategori End ----------------- */ 

  /* ---------------- Event Start ----------------- */ 
  function get_event() {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id order by a.tgl_mulai desc";
    $query = $this->db->query($sql);
    return $query->result_array();		
  }

  function get_event_limit($limit) {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.status_event = 1 order by a.tgl_mulai desc limit ?";
    $query = $this->db->query($sql, array($limit));
    return $query->result_array();		
  }

  function get_event_by_name($name){
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.nama like '%".$name."%' order by a.tgl_mulai desc";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_event_by_tanggal($tanggal, $filter){
    $this->db->select("a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon");
    $this->db->from("t_event a");
    $this->db->join("t_user b", "a.user_id=b.id", "inner");
    $this->db->join("t_kategori c", "a.kategori_id = c.id", "inner");
    $this->db->join("t_wilayah d", "a.wilayah_id = d.id", "inner");

    if($filter == "today"){
      $this->db->where("DATE_FORMAT(a.tgl_mulai, '%Y-%m-%d') = '".$tanggal."'");
    } elseif($filter == "tomorrow") {
      $this->db->where("DATE_FORMAT(DATE_ADD(a.tgl_mulai, INTERVAL 1 DAY), '%Y-%m-%d') = '".$tanggal."'");
    } else {
      $this->db->where_in("DATE_FORMAT(a.tgl_mulai, '%Y-%m-%d')", $tanggal);
    }
    $this->db->order_by("a.tgl_mulai", "desc");
    return $this->db->get()->result_array();
  }

  function get_event_by_status($status, $limit=0) {
    $sql = "select a.id, a.user_id, b.nama as nama_user, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama as nama_event, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.status_event in (".$status.") order by a.tgl_mulai desc";
    if($limit > 0){
      $sql .= " limit ".$limit;
    }
    $query = $this->db->query($sql);
    return $query->result_array();		
  }

  function get_event_by_wilayah($wilayah_id, $limit = ""){
    $sql = "select a.id, a.user_id, b.nama as nama_user, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama as nama_event, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where d.id = ? and a.status_event = 1 order by a.tgl_mulai desc";
    if($limit!=""){
      $sql .= " limit ".$limit;
    }
    $query = $this->db->query($sql, array($wilayah_id));
    return $query->result_array();
  }

  function get_event_by_kategori($kategori_id, $limit = ""){
    $sql = "select a.id, a.user_id, b.nama as nama_user, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama as nama_event, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where c.id = ? and a.status_event = 1 order by a.tgl_mulai desc";
    if($limit!=""){
      $sql .= " limit ".$limit;
    }
    $query = $this->db->query($sql, array($kategori_id));
    return $query->result_array();
  }

  function get_event_by_kategori_and_wilayah($kategori_id, $wilayah_id, $limit = ""){
    $sql = "select a.id, a.user_id, b.nama as nama_user, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama as nama_event, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where c.id = ? and d.id = ? and a.status_event = 1 order by a.tgl_mulai desc";
    if($limit!=""){
      $sql .= " limit ".$limit;
    }
    $query = $this->db->query($sql, array($kategori_id, $wilayah_id));
    return $query->result_array();
  }

  function get_event_by_id($id) {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.status_event, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap, a.lok_lat, a.lok_long, a.materi_event, a.update_date from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.id = ? order by a.tgl_mulai desc";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();		
  }

  function get_event_by_tahun_slug($tahun, $slug) {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.status_event, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon, a.lokasi_lengkap, a.lok_lat, a.lok_long, a.materi_event, a.update_date from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.tahun = ? and a.slug = ? order by a.tgl_mulai desc";
    $query = $this->db->query($sql, array($tahun, $slug));
    return $query->row();		
  }

  function get_event_by_user_id($userid) {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.user_id = ? order by a.tgl_mulai desc";
    $query = $this->db->query($sql,array($userid));
    return $query->result_array();		
  }

  function get_event_by_user_id_and_status($userid, $status) {
    $sql = "select a.id, a.user_id, b.nama, a.kategori_id, c.nama as kategori_nama, a.wilayah_id, d.wilayah as wilayah_nama, a.nama, a.slug, a.tahun, a.gambar, a.lokasi, a.tgl_mulai, a.tgl_selesai, a.flag_commingsoon from t_event a inner join t_user b on a.user_id=b.id inner join t_kategori c on a.kategori_id = c.id inner join t_wilayah d on a.wilayah_id = d.id where a.user_id = ? and a.status_event = ? order by a.tgl_mulai desc";
    $query = $this->db->query($sql,array($userid, $status));
    return $query->result_array();		
  }
  
  function event_insert($user_id, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $gambar, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long){
    $sql = "insert into t_event (user_id, kategori_id, tahun, wilayah_id, nama, slug, status_event, gambar, lokasi, lokasi_lengkap, tgl_mulai, tgl_selesai, materi_event, lok_lat, lok_long) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($user_id, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $gambar, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long));
  }

  function event_update($id, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long){
    $sql = "update t_event set kategori_id=?, tahun=?, wilayah_id=?, nama=?, slug=?, status_event=?, lokasi=?, lokasi_lengkap=?, tgl_mulai=?, tgl_selesai=?, materi_event=?, lok_lat=?, lok_long=? where id=?";
    $this->db->query($sql, array($kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long, $id));
  }

  function event_update_gambar($id, $gambar){
    $sql = "update t_event set gambar = ? where id = ?";
    $this->db->query($sql, array($gambar, $id));
  }

  function event_update_status($id, $status){
    $sql = "update t_event status_event = ? where id = ?";
    $this->db->query($sql, array($status, $id));
  }

  function event_delete($id){
    $sql = "delete from t_event where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ---------------- Event End ----------------- */ 

  /* ---------------- Wilayah Start ----------------- */ 
  function get_wilayah($object=0){
    $sql = "select * from t_wilayah order by wilayah asc";
    $query = $this->db->query($sql);
    if($object){
      return $query->result();
    }else{
      return $query->result_array();
    }
  }

  function get_wilayah_by_id($id){
    $sql = "select * from t_wilayah where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function get_wilayah_by_wilayah($wilayah){
    $sql = "select * from t_wilayah where wilayah = ?";
    $query = $this->db->query($sql, array($wilayah));
    return $query->result_array();
  }

  function get_wilayah_by_slug($slug){
    $sql = "select * from t_wilayah where slug = ?";
    $query = $this->db->query($sql, array($slug));
    return $query->result_array();
  }

  function get_wilayah_by_induk_wilayah($wilayah_induk_id){
    $sql = "select a.id, a.wilayah, a.slug, a.gambar, a.wilayah_induk_id, b.wilayah_induk, b.slug as slug_wilayah_induk from t_wilayah a inner join t_wilayah_induk b on a.wilayah_induk_id=b.id where b.id = ?";
    $query = $this->db->query($sql, array($wilayah_induk_id));
    return $query->result_array();
  }

  function wilayah_insert($wilayah, $slug, $gambar, $wilayah_induk_id){
    $sql = "insert into t_wilayah (id, wilayah, slug, gambar, wilayah_induk_id) values (null, ?, ?, ?, ?)";
    $query = $this->db->query($sql, array($wilayah, $slug, $gambar, $wilayah_induk_id));
  }

  function wilayah_update($id, $wilayah, $slug, $gambar, $wilayah_induk_id){
    $sql = "update t_wilayah set wilayah = ?, slug = ?, gambar = ?, wilayah_induk_id = ? where id = ?";
    $query = $this->db->query($sql, array($wilayah, $slug, $gambar, $wilayah_induk_id, $id));
  }
  /* ---------------- Wilayah End ----------------- */ 

  /* ---------------- Wilayah Induk End ----------------- */
  function get_wilayah_induk($object=0){
    $sql = "select * from t_wilayah_induk order by wilayah_induk asc";
    $query = $this->db->query($sql);
    if($object){
      return $query->result();
    }else{
      return $query->result_array();
    }
  }

  function get_wilayah_induk_by_id($wilayah_induk_id){
    $sql = "select * from t_wilayah_induk where id = ?";
    $query = $this->db->query($sql, array($wilayah_induk_id));
    return $query->result_array();
  }

  function get_wilayah_induk_by_wilayah_induk($wilayah_induk){
    $sql = "select * from t_wilayah_induk where wilayah_induk = ?";
    $query = $this->db->query($sql, array($wilayah_induk));
    return $query->result_array();
  }
  
  function wilayah_induk_insert($wilayah_induk, $slug){
    $sql = "insert into t_wilayah_induk (id, wialayah_induk, slug) values(null, ?, ?)";
    $query = $this->db->query($sql, array($wilayah_induk, $slug));

    $sql = "select last_insert_id() as id";
    $query = $this->db->query($sql);
    return $query->row()->id;
  }

  function wilayah_induk_update($id, $wilayah_induk, $slug){
    $sql = "update t_wilayah_induk set wialayah_induk = ?, slug = ? where id = ?";
    $query = $this->db->query($sql, array($wilayah_induk, $slug, $id));
  }

  function wilayah_induk_delete($id){
    $sql = "delete from t_wilayah_induk where id = ?";
    $query = $this->db->query($sql, array($id));
  }
  /* ---------------- Wilayah Induk End ----------------- */

  /* ---------------- Kupon Start ----------------- */ 
  function get_kupon(){
    $sql = "select a.id, a.kode, a.type, a.diskon, a.komisi, a.status, a.user_id, b.nama, a.tgl_buat, a.tgl_exp from t_kupon a inner join t_user b on a.user_id=b.id";
    $query = $this->db->query($sql);
    return $query->result_array();
  }

  function get_kupon_by_id($id){
    $sql = "select kode, type, diskon, komisi, status, tgl_buat, tgl_exp from t_kupon where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function check_kupon($tanggal){
    $sql = "select id from t_kupon where status = '1' and ? <= tgl_exp";
    $query = $this->db->query($sql, array($tanggal));
    return $query->num_rows();
  }

  function check_kupon_regurer($tanggal){
    $sql = "select id from t_kupon where status = '1' and type = '1' and tgl_exp = ?";
    $query = $this->db->query($sql, array($tanggal));
    return $query->num_rows();
  }

  function check_kupon_mahasiswa($tanggal){
    $sql = "select id from t_kupon where status = '1' and type = '2' and tgl_exp = ?";
    $query = $this->db->query($sql, array($tanggal));
    return $query->num_rows();
  }

  function check_kupon_kode($kode){
    $sql = "select * from t_kupon where kode = ? and status = '1'";
    $query = $this->db->query($sql, array($kode));
    return $query->result_array();
  }

  function kupon_insert($kode, $type, $diskon, $komisi, $status, $user_id, $tgl_buat, $tgl_exp){
    $sql = "insert into t_kupon (id, kode, type, diskon, komisi, status, user_id, tgl_buat, tgl_exp) values (null,?,?,?,?,?,?,?,?)";
    $this->db->query($sql, array($kode, $type, $diskon, $komisi, $status, $user_id, $tgl_buat, $tgl_exp));
  }

  function kupon_update($id, $kode, $type, $diskon, $komisi, $status, $tgl_buat, $tgl_exp){
    $sql = "update t_kupon set kode=?, type=?, diskon=?, komisi=?, status=?, tgl_buat=?, tgl_exp=? where id=?";
    $this->db->query($sql, array($kode, $type, $diskon, $komisi, $status, $tgl_buat, $tgl_exp, $id));
  }

  function kupon_delete($id){
    $sql = "delete from t_kupon where id = ?";
    $this->db->query($sql, array($id));
  }
  /* ---------------- Kupon End ----------------- */ 

  function get_layanan($event_id){
    $sql = "select * from t_layanan where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result_array();
  }

  function get_layanan_tiket($event_id){
    $sql = "select * from t_layanan where status = 1 and event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result();
  }

  function get_layanan_by_id($id){
    $sql = "select * from t_layanan where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function get_layanan_by_nama($nama_layanan, $event_id){
    $sql = "select * from t_layanan where nama = ? and event_id = ? ";
    $query = $this->db->query($sql, array($nama_layanan, $event_id));
    return $query->result_array();
  }

  function layanan_insert($event_id, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah){
    $sql = "insert into t_layanan (event_id, nama, harga, status, waktu, komisi, daerah, hadiah, deskripsi, url, reseller, harga_fix, jumlah) values (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";
    $this->db->query($sql, array($event_id, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah));
  }

  function layanan_update($id, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah){
    $sql = "update t_layanan set nama = ?, harga = ?, status = ?, waktu = ?, komisi = ?, daerah = ?, hadiah = ?, deskripsi = ?, url = ?, reseller = ?, harga_fix = ?, jumlah=? where id=?";
    // print_r(array($nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah, $id));die;
    $this->db->query($sql, array($nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah, $id));
  }

  function layanan_delete($id){
    $sql = "delete from t_layanan where id = ? ";
    $this->db->query($sql, array($id));
  }

  /*----------------------------- Sponsor Start -------------------------------*/

  function get_sponsor($event_id){
    $sql = "select * from t_spoin_pw2019 where event_id = ?";
    $query = $this->db->query($sql, array($event_id));
    return $query->result();
  }

  function get_sponsor_by_status($event_id, $status){
    $sql = "select * from t_spoin_pw2019 where event_id = ? and np_status = ?";
    $query = $this->db->query($sql, array($event_id, $status));
    return $query->result();
  }

  function get_sponsor_by_id($id){
    $sql = "select * from t_spoin_pw2019 where id = ?";
    $query = $this->db->query($sql, array($id));
    return $query->result_array();
  }

  function sponsor_insert($event_id,$invoice,$nama,$usaha,$email,$hp,$asal,$jenis,$status,$catatan,$nilai,$metode) {
		$sql = "insert into t_spoin_pw2019 (event_id,tanggal,invoice,nama,usaha,email,no_hp,asal_tda,jenis_paket,status_pesan,catatan,nilai_bayar,metode_bayar) values (?,now(),?,?,?,?,?,?,?,?,?,?,?)";
		$this->db->query($sql,array($event_id,$invoice,$nama,$usaha,$email,$hp,$asal,$jenis,$status,$catatan,$nilai,$metode));

		$sql = "select last_insert_id() as id";
		$query = $this->db->query($sql);
		return $query->result_array();		
	}

	function sponsor_np1($id,$trxid,$request,$response) {
		$sql = "update t_spoin_pw2019 set np_txid = ?, np_requestOrder = ?, np_responseOrder = ? where id = ?";
		$this->db->query($sql,array($trxid,$request,$response,$id));
	}

	function sponsor_np2($id,$creditcard,$expdate,$status) {
		$sql = "update t_spoin_pw2019 set np_creditcard = ?, np_expdate = ?, np_status = ? where id = ?";
		$this->db->query($sql,array($creditcard,$expdate,$status,$id));
	}

	function sponsor_np3($id,$bankcode,$vaccnum,$validdate,$validtime,$status) {
		$sql = "update t_spoin_pw2019 set np_bankcode = ?, np_vanum = ?, np_vavaliddate = ?, np_vavalidtime = ?, np_status = ? where id = ?";
		$this->db->query($sql,array($bankcode,$vaccnum,$validdate,$validtime,$status,$id));
	}

	function sponsor_npcode($id,$code,$msg,$request,$response) {
		$sql = "update t_spoin_pw2019 set np_resultCode = ?, np_resultMsg = ?, np_requestOnePass = ?, np_responseOnePass = ?  where id = ?";
		$this->db->query($sql,array($code,$msg,$request,$response,$id));
	}

	function sponsor_status($id,$status) {
		$sql = "update t_spoin_pw2019 set status_pesan = ? where id = ?";
		$this->db->query($sql,array($status,$id)); 
	}

  /*----------------------------- Sponsor End ---------------------------------*/

  /*----------------------------- Dashboard Peserta Start -------------------------------*/
  public function tanggal(){
		//$tanggal = array();
		// for each day in the month
		for($i = 0; $i <=  date('t'); $i++){
      // add the date to the dates array
      $dates[] = date('') . "" . date('') . "" . str_pad($i, 2, '0', STR_PAD_LEFT);
		}

		return $dates;
	}

	public function daudaily($role,$profileid){
    $jumlah = [];
		for($i = 0; $i <=  date('t'); $i++){
      // add the date to the dates array
      $tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
      $this->db->select_sum('jml_tiket'); 
      $this->db->from('t_tiket');
      //$this->db->where('status', '1');
      $this->db->where('date(tanggal)', $tanggal);
			//$this->db->where('year(tgl_transaksi)', $tahun);
      $query = $this->db->get();
      $test = $query->result();
      $jumlah[] = $test[0]->jml_tiket;
      if ($jumlah[$i] === NULL){
        $jumlah[$i] = '0';
      }
		}
		
		return $jumlah;
	}

	public function nrudaily($role,$profileid){
    $jumlah = [];
		for($i = 0; $i <=  date('t'); $i++){
      // add the date to the dates array
      $tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
      $this->db->select('ID, count(*) as jumlah'); 
      $this->db->from('t_user');
      $this->db->where('userlevel', $role);
      $this->db->where('date(reg_date)', $tanggal);
      //$this->db->where('year(tgl_transaksi)', $tahun);
      $query = $this->db->get();
      $test = $query->result();
      $jumlah[] = $test[0]->jumlah;
      if ($jumlah[$i] === NULL){
        $jumlah[$i] = '0';
      }
		}
		
		return $jumlah;
	}

	public function revdaily($role,$profileid){
    $jumlah = [];
		for($i = 0; $i <=  date('t'); $i++){
      // add the date to the dates array
      $tanggal = date('Y') . "-" . date('m') . "-" . str_pad($i, 2, '0', STR_PAD_LEFT);
      $this->db->select_sum('nilai');
      $this->db->from('t_tiket');
      if ($role == '2') {
        $this->db->where('affid', $profileid);
      }
      $this->db->where('status_bayar', '1');
      $this->db->where('date(tanggal)', $tanggal);
			//$this->db->where('year(tgl_transaksi)', $tahun);
      $query = $this->db->get();
      $test = $query->result();
      $jumlah[] = $test[0]->nilai / 1000;
      if ($jumlah[$i] === NULL){
        $jumlah[$i] = '0';
      }
		}
		
		return $jumlah;
  }
  
  public function pendapatanbulanini($role,$profileid){
		$status = 1;
		$bulan = date('m');
		$tahun = date('Y');
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
    $this->db->from('t_tiket');
    if ($role == '2') {
      $this->db->where('affid', $profileid);
    }
    $this->db->where('status_bayar', $status);
		$this->db->where('month(tgl_bayar)', $bulan);
		$this->db->where('year(tgl_bayar)', $tahun);
    $query = $this->db->get();
    return $query->result();
  }

  public function pendapatanbulan($bln,$thn){
		$status = 1;
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
    $this->db->from('t_tiket');
    if ($role == '2') {
      $this->db->where('affid', $profileid);
    }
    $this->db->where('status_bayar', $status);
		$this->db->where('month(tgl_bayar)', $bln);
		$this->db->where('year(tgl_bayar)', $thn);
    $query = $this->db->get();
    return $query->result();
  }

  public function pendapatanbulanlalu($role,$profileid){
		$status = 1;
		$bulan = date('Y-m',strtotime('-1 month'));
		$bulannya = strtotime($bulan);
		$bulanfix = date('m',$bulannya);
		$tahun = date('Y',$bulannya);
		//var_dump($bulan);
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select('nilai');
    $this->db->from('t_tiket');
    if ($role == '2') {
      $this->db->where('affid', $profileid);
    }
    $this->db->where('status_bayar', $status);
		$this->db->where('month(tgl_bayar)', $bulanfix);
		$this->db->where('year(tgl_bayar)', $tahun);
    $query = $this->db->get();
    return $query->result();
    }

  public function totalpendapatan($role,$profileid){
		$status = 1;
		//$this->db->select_sum('jumlah');
		// $this->db->select('jumlah');
		//$bulan = date('m',strtotime('-1 months'));
		//$tahun = date('Y');
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
		if ($role == '2') {
      $this->db->where('affid', $profileid);
    }
    $this->db->from('t_tiket');
    $this->db->where('status_bayar', $status);
    $query = $this->db->get();
    return $query->result();
    //var_dump($query);
  }

  public function totalpoin($role,$profileid){
    $status = 1;
    $this->db->select("SUM(b.komisi) as nilai");
    $this->db->from("t_tiket a");
    $this->db->join("t_layanan b", "a.tipe_tiket=b.id");
    
    if($role == '2'){
      $this->db->where("a.affid", $profileid);
    }
    $this->db->where("a.status_bayar", $status);
		//$bulan = date('m',strtotime('-1 months'));
		//$tahun = date('Y');
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		// $this->db->select('SUM(tbl_membertetra.komisi) as jumlah');
    // $this->db->from('tbl_membertetra');
    //$tipe = array('regular','vip');
    // if ($role == '0') {
    //   $this->db->where('ID', $profileid);        	
    // }
    //$this->db->where_in('layanan', $tipe);
    //$this->db->where('status', $status);
		//$this->db->where('month(tgl_transaksi)', $bulan);
		//$this->db->where('year(tgl_transaksi)', $tahun);
    $query = $this->db->get();
    return $query->result();
  }

public function totalpoincair($role,$profileid){
		$status = 1;
		//$bulan = date('m',strtotime('-1 months'));
		//$tahun = date('Y');
        //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select('SUM(tbl_membertetra.komisiditarik) as jumlahcair');
        $this->db->from('tbl_membertetra');
        //$tipe = array('regular','vip');
        if ($role == '0') {
        	$this->db->where('ID', $profileid);        	
        }
        //$this->db->where_in('layanan', $tipe);
        //$this->db->where('status', $status);
		//$this->db->where('month(tgl_transaksi)', $bulan);
		//$this->db->where('year(tgl_transaksi)', $tahun);
        $query = $this->db->get();
        return $query->result();
}
public function transaksibulanini($role,$profileid){
		$status = 1;
		$bulan = date('m');
		$tahun = date('Y');
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
    $this->db->from('t_tiket');
    $this->db->where('status_bayar', $status);
		$this->db->where('month(tgl_bayar)', $bulan);
		$this->db->where('year(tgl_bayar)', $tahun);
    $query = $this->db->get();
    return $query->result();
    }

public function transaksihariini($role,$profileid){
		//$status = 1;
		//$tanggal = date('Y-m-d');
		$hari = date('d');
		$bulan = date('m');
		$tahun = date('Y');
		//$tahun = date('Y');
        //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		//$this->db->select('SUM(tbl_transaksi.jumlah) as jumlah');
        $this->db->from('t_tiket');
        $this->db->where('status_bayar', '1');
        if ($role == '0') {
        	$this->db->where('affid', $profileid);
        }
		//$this->db->where('DATE(tgl_transaksi)', $tanggal);
		$this->db->where('day(tgl_bayar)', $hari);
		$this->db->where('month(tgl_bayar)', $bulan);
		$this->db->where('year(tgl_bayar)', $tahun);
        $query = $this->db->get();
        return $query->result();
}

public function totalhariini($role,$profileid){
		$status = 1;
		$hari = date('d');
		$bulan = date('m');
		$tahun = date('Y');
		//$bulan = date('m',strtotime('-1 months'));
		//$tahun = date('Y');
        //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
        $this->db->from('t_tiket');
        $this->db->where('status_bayar', $status);
        if ($role == '0') {
        	$this->db->where('affid', $profileid);
        }
        $this->db->where('day(tgl_bayar)', $hari);
		$this->db->where('month(tgl_bayar)', $bulan);
		$this->db->where('year(tgl_bayar)', $tahun);
		//$this->db->where('month(tgl_transaksi)', $bulan);
		//$this->db->where('year(tgl_transaksi)', $tahun);
        $query = $this->db->get();
        return $query->result();
    }
public function transaksikemaren($role,$profileid){
		$status = 1;
		$tanggal = date('Y-m-d',strtotime('-1 days'));
		//$tahun = date('Y');
        //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select_sum('nilai');
        $this->db->from('t_tiket');
        $this->db->where('status_bayar', $status);
        if ($role == '0') {
        	$this->db->where('affis', $profileid);
        }
		$this->db->where('date(tgl_bayar)', $tanggal);
        $query = $this->db->get();
        return $query->result();
    }
public function tiketindent($role,$profileid){
		//$status = 1;
		//$tanggal = date('Y-m-d',strtotime('-1 days'));
		//$tahun = date('Y');
        //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
		$this->db->select('SUM(tbl_membertetra.tiket) as tiket');
        $this->db->from('tbl_membertetra');
       // $this->db->where('status', $status);
        if ($role == '0') {
        	$this->db->where('ID', $profileid);
        }
		//$this->db->where('date(tgl_transaksi)', $tanggal);
        $query = $this->db->get();
        return $query->result();
    }
  public function totaltiket($role,$profileid){
    $this->db->select("a.id, a.gaffid as user_id, a.affid, a.nama as nama_layanan, a.nama, a.usaha, a.no_hp, a.email, a.status_bayar, a.daerah ");
    $this->db->from("t_tiket a");
    $this->db->join("t_layanan b", "a.tipe_tiket=b.id");
    if($role=="2"){
      $this->db->where("a.affid", $profileid);
    }
    $this->db->where("a.status_bayar", '1');
        // $table1 = 'tbl_peserta';
        // $table2 = 'tbl_membertetra';
        // $this->db->select(''.$table1.'.id,'.$table1.'.uid,'.$table1.'.affid,'.$table1.'.tipe,'.$table1.'.nama,'.$table1.'.usaha, '.$table1.'.hp, '.$table1.'.email,'.$table1.'.status,'.$table1.'.sent,'.$table1.'.qrimage,'.$table2.'.ID, '.$table2.'.daerah');

        // $this->db->from(''.$table1.'',''.$table2.'');
        // $this->db->join(''.$table2.'', ''.$table2.'.ID='.$table1.'.uid' );
        // if ($role == '0') {
        // 	$this->db->where(''.$table1.'.affid', $profileid);
        // }
        // $this->db->where(''.$table1.'.status','');
        $query = $this->db->get();
        return $query->result();
  }

  public function transaksiterakhir($role,$profileid){
    $today =  date('Y-m-d');
    $date1 = date('Y-m-d',strtotime('-1 month'));
    $table1 = 'tbl_transaksi';
    $table2 = 'tbl_membertetra';
    $this->db->select(''.$table1.'.id,'.$table1.'.invoice,'.$table1.'.keterangan,'.$table1.'.kodediskon,'.$table1.'.profileid, '.$table1.'.status, '.$table1.'.tgl_transaksi,'.$table1.'.tipe,'.$table1.'.member, '.$table1.'.affiliate, '.$table1.'.layanan,'.$table1.'.jumlah, '.$table2.'.ID, '.$table2.'.display_name');
    $this->db->from(''.$table1.'',''.$table2.'');
    $this->db->join(''.$table2.'', ''.$table2.'.ID='.$table1.'.profileid', ''.$table2.'.ID='.$table1.'.affiliate as affiliate' );
    //$this->db->join(''.$table2.'', ''.$table2.'.ID='.$table1.'.profileid', ''.$table2.'.ID='.$table1.'.affiliate as affiliate', ''.$table2.'.display_name='.$table1.'.affiliate as namaaff' );	
    $this->db->where('date(tgl_transaksi) >=',$date1);
    $this->db->where('date(tgl_transaksi) <=',$today);
    if ($role == '0') {
          $this->db->where(''.$table1.'.affiliate', $profileid);
        }
    $this->db->order_by(''.$table1.'.id','DESC');
    $this->db->limit('5');	
    $query = $this->db->get();
    $data = $query->result();
    //$insert_id = $this->db->insert_id();
    //var_dump($data);
      return  $data;
  }
  public function transaksibulan($bln,$thn){
    $status = 1;
    //$this->db->select('MONTH(tgl_transaksi)='.$bulan.',  YEAR(tgl_transaksi)='.$tahun.'');
    //$this->db->select('SUM(tbl_transaksi.jumlah) as jumlah');
    $this->db->from('t_tiket');
    $this->db->where('status_bayar', $status);
    $this->db->where('month(tgl_bayar)', $bln);
    $this->db->where('year(tgl_bayar)', $thn);
    $query = $this->db->get();
    return $query->result();
  }

  public function leaderboard(){
    
  }
  /*----------------------------- Dashboard Peserta End ---------------------------------*/

}