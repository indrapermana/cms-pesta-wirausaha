<?php
class Template{
  protected $_ci;
  
  function __construct(){
    $this->_ci = &get_instance();
  }

  function frontend($content, $data = NULL){
    $data['header'] = $this->_ci->load->view('frontend/template/header', $data, TRUE);
    $data['content'] = $this->_ci->load->view('frontend/'.$content, $data, TRUE);
    $this->_ci->load->view('frontend/template/index', $data);
  }
	
  function backend($content, $data = NULL, $sidebar=true){
    $data['header'] = $this->_ci->load->view('backend/template/header', $data, TRUE);
    
    if($sidebar){
      $data['sidebar'] = $this->_ci->load->view('backend/template/sidebar', $data, TRUE);
    }
    
    $data['content'] = $this->_ci->load->view('backend/'.$content, $data, TRUE);
    $this->_ci->load->view('backend/template/index', $data);
  }
	
  function login($content, $data = NULL){
    $data['content'] = $this->_ci->load->view('login/'.$content, $data, TRUE);
    $this->_ci->load->view('login/index', $data);
  }
}

