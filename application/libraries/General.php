<?php defined('BASEPATH') OR exit('No direct script access allowed');

class General {

  public function session_check(){
    $CI =& get_instance();
    $CI->user_id    = $CI->session->userdata('id');
    $CI->user_nama  = $CI->session->userdata('nama');
    $CI->user_level = $CI->session->userdata('ulevel');
    $CI->peserta_id = $CI->session->userdata('pesertaid');
    $CI->event_id   = $CI->session->userdata('eventid');

    if ($CI->user_id=="") {
      redirect(site_url("login/logout")); 
    }

    // if($CI->event_id==""){
    //   redirect("kegiatan/manage");
    // }
  }
		
}