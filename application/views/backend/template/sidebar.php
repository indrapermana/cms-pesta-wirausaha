          <!--- Divider -->
          <div id="sidebar-menu">
            <ul>

              <li class="text-muted menu-title">Navigation</li>

              <li class="">
                <a href="<?= base_url("dashboard"); ?>" class="waves-effect"><i class="ti-bar-chart"></i> Dashboard</a>
              </li>
              
              <?php if($this->user_level == "1") { ?>
              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-desktop"></i> <span> Portal </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                  <!-- <li><a href="<?= base_url("kegiatan/manage")?>">Kegiatan</a></li> -->
                  <li><a href="<?= base_url("portal/berita")?>">Berita</a></li>
                  <li><a href="<?= base_url("portal/kategori")?>">Kategori</a></li>
                </ul>
              </li>

              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-money"></i> <span> Keuangan </span> <span class="menu-arrow"></span> </a>
                <ul class="list-unstyled">
                  <li><a href="<?= base_url("keuangan/anggaran")?>">Anggaran</a></li>
                  <li><a href="<?= base_url("keuangan/realisasi")?>">Realisasi</a></li>
                  <li><a href="<?= base_url("keuangan/nicepay")?>">Nicepay</a></li>
                  <li><a href="<?= base_url("keuangan/nicepaytiket")?>">Nicepay Tiket</a></li>
                  <li><a href="<?= base_url("keuangan/kupon")?>">Kupon</a></li>
                  <li><a href="<?= base_url("keuangan/layanan")?>">Layanan</a></li>
                </ul>
              </li>

              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-bookmark"></i><span> Tiket,Expo,Sponsor </span> <span class="menu-arrow"></span> </a>
                <ul class="list-unstyled">
                  <li><a href="<?= base_url("sales")?>">Rekap</a></li>
                  <li><a href="<?= base_url("sales/tiketonline")?>">Tiket Online</a></li>
                  <li><a href="<?= base_url("sales/tiketgroup")?>">Tiket Group</a></li>
                  <li><a href="<?= base_url("sales/distribusitiket")?>">Distribusi Tiket</a></li>
                  <li><a href="<?= base_url("sales/jnstiket")?>">Jenis Tiket</a></li>
                  <li><a href="<?= base_url("sales/expo")?>">Expo</a></li>
                  <li><a href="<?= base_url("sales/expowl")?>">Follow Up Expo</a></li>
                  <li><a href="<?= base_url("sales/itemtambahan")?>">Item Tambahan</a></li>
                  <li><a href="<?= base_url("sales/expotiket")?>">Expo Tiket PWN</a></li>
                  <li><a href="<?= base_url("sales/sponsor")?>">Sponsor</a></li>
                  <li><a href="<?= base_url("sales/hotel")?>">Hotel</a></li>
                  <li><a href="<?= base_url("sales/hotelrekap")?>">Hotel Rekap</a></li>
                  <li><a href="<?= base_url("sales/gift")?>">Gift</a></li>
                </ul>
              </li>

              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class="ti-email"></i> <span> Email </span> <span class="menu-arrow"></span> </a>
                <ul class="list-unstyled">
                  <li><a href="<?= base_url("email/kirim")?>">Pengiriman Email</a></li>
                </ul>
              </li>

              <li class="has_sub">
                <a href="javascript:void(0);" class="waves-effect"><i class=" ti-credit-card"></i><span> Rekening Bank </span> <span class="menu-arrow"></span></a>
                <ul class="list-unstyled">
                  <li><a href="<?= base_url("bank")?>">Rekening BSM</a></li>
                  <li><a href="<?= base_url("bank/upload")?>">Upload Data BSM</a></li>
                  <li><a href="<?= base_url("bank/rekap")?>">Rekap Rekening BSM</a></li>
                </ul>
              </li>
              
              <?php } elseif($this->user_level == "2") { ?>
              <li class="">
                <a href="<?= base_url("ajak"); ?>" class="waves-effect"><i class="ti-user"></i> Ajak Teman</a>
              </li>
              <li class="">
                <a href="<?= base_url("ajak/peserta"); ?>" class="waves-effect"><i class="ti-ticket"></i> Tiket</a>
              </li>
              <li class="">
                <a href="<?= base_url("ajak/penjualan"); ?>" class="waves-effect"><i class=" ti-pencil-alt"></i> Pejualan Tiket</a>
              </li>
              <li class="">
                <a href="<?= base_url("pencairan"); ?>" class="waves-effect"><i class="ti-money"></i> Pencairan Komisi</a>
              </li>
              <?php } ?>

              <li class="">
                <a href="<?= base_url("login/logout")?>" class="waves-effect"><i class="ti-power-off"></i> Logout</a>
              </li>
            </ul>
            <div class="clearfix"></div>
          </div>
          <div class="clearfix"></div>