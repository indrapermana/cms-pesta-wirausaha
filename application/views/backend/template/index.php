<?php
defined('BASEPATH') OR exit('No direct script access allowed');
$msg = "";
$titles = "";
$type = "";
if($this->input->cookie('msg_success') != "" OR $this->input->cookie('msg_error') != "" OR $this->input->cookie('msg_warning')){
	if($this->input->cookie('msg_success') != ""){
		$msg = $this->input->cookie('msg_success');
		$titles = "Berhasil";
		$type = "success";
	}elseif($this->input->cookie('msg_warning')){
		$msg = $this->input->cookie('msg_warning');
		$titles = "Peringatan";
		$type = "warning";
	}else{
		$msg = $this->input->cookie('msg_error');
		$titles = "Gagal";
		$type = "error";
	}
}

?>

<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="<?= base_url("assets/backend/images/logo-pw.png"); ?>">

    <title>CMS - Pesta Wirausaha</title>

    <!-- Plugins Css-->
    <link href="<?= base_url("assets/backend/plugins/bootstrap-tagsinput/css/bootstrap-tagsinput.css"); ?>" rel="stylesheet" />
    <link href="<?= base_url("assets/backend/plugins/switchery/css/switchery.min.css"); ?>" rel="stylesheet" />
    <link href="<?= base_url("assets/backend/plugins/select2/css/select2.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css"); ?>" rel="stylesheet">
    
    <!-- DataTables -->
    <link href="<?= base_url("assets/backend/plugins/datatables/jquery.dataTables.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/buttons.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/fixedHeader.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/responsive.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/scroller.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/dataTables.colVis.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/dataTables.bootstrap.min.css"); ?>" rel="stylesheet" type="text/css"/>
    <link href="<?= base_url("assets/backend/plugins/datatables/fixedColumns.dataTables.min.css"); ?>" rel="stylesheet" type="text/css"/>

    <!--Morris Chart CSS -->
    <link href="<?= base_url("assets/backend/plugins/morris/morris.css");?>" rel="stylesheet">

    <!-- Captcha -->
    <!-- <link type="text/css" rel="Stylesheet" href="<?php //echo CaptchaUrls::LayoutStylesheetUrl() ?>" /> -->

    <!-- Master Css -->
    <link href="<?= base_url("assets/backend/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/core.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/components.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/icons.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/pages.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/responsive.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/style.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= base_url("assets/backend/js/modernizr.min.js"); ?>"></script>

  </head>
  <body class="fixed-left">

    <div id="wrapper">
      <!-- Top Bar Start -->
      <div class="topbar">
        <?= $header; ?>
      </div>
      <!-- Top Bar End -->

      <?php if($this->event_id!="" && $this->user_level=="1"){ ?>
        <!-- Top My Event Start -->
      <div class="myevent">
        <div class="row event">
          <div class="col-lg-8 col-md-8 col-sm-12 title-event">
            <div class="hidden-xs hidden-sm">
              <p class="event-draft">DRAFT</p>
              <h3 class="event-title">
                <?= $event[0]['nama']; ?>
              </h3>
              <p class="event-times">
                <?= $event[0]['lokasi']." | ".$event[0]['lokasi_lengkap']; ?>
              </p>
              <p class="event-times">
                <?= set_datetime($event[0]['tgl_mulai'])." - ".set_datetime($event[0]['tgl_selesai']); ?>
              </p>
            </div>
            <div class="hidden-md hidden-lg text-center">
              <p class="event-draft">DRAFT</p>
              <h2 class="event-title">
                <?= $event[0]['nama']; ?>
              </h2>
              <p class="event-times">
                <?= set_datetime($event[0]['tgl_mulai'])." - ".set_datetime($event[0]['tgl_selesai']); ?>
              </p>
            </div>
          </div>

          <div class="col-lg-4 col-md-4 col-sm-12">
            <div class="hidden-xs hidden-sm">
              <div class="event-btn pull-right">
                <a href="<?= base_url("event/detail/".$event[0]['tahun']."/".$event[0]['slug']); ?>" class="btn btn-custom btn-white btn-md w-md m-r-10" target="blank">
                  <i class="fa fa-eye"></i>&nbsp; Tinjau
                </a>
                <a href="#" class="btn btn-custom btn-primary btn-md w-md <?= ($event[0]['status_event']=="1")? "disabled" : "";?>"><i class="fa fa-check"></i>&nbsp; Publish</a>
              </div>
              <p class="pull-right last-event-update">Terakhir disimpan pada <?= set_datetime($event[0]['update_date']); ?></p>
            </div>
            <div class="hidden-md hidden-lg">
              <div class="event-btn text-center">
                <a href="<?= base_url("event/detail/".$event[0]['tahun']."/".$event[0]['slug']); ?>" class="btn btn-custom btn-white btn-md w-md m-r-10" target="blank">
                  <i class="fa fa-eye"></i>&nbsp; Tinjau
                </a>
                <a href="#" class="btn btn-custom btn-primary btn-md w-md <?= ($event[0]['status_event']=="1")? "disabled" : "";?>"><i class="fa fa-check"></i>&nbsp; Publish</a>
              </div>
            </div>
          </div>
        </div>

        <!-- Menu Manage Event -->
        <!-- <div class="row"> -->
          <!-- <div class="col-xs-12" style="padding:0px;"> -->
            <nav class="navbar navbar-inverse">
              <div class="container-fluid">
                <ul class="nav navbar-nav">
                  <li class="nav-event <?= (isset($edit_event))? "active" : ""; ?>"><a href="<?= base_url("kegiatan/manage/ubah/".$event[0]['id']); ?>">Ubah</a></li>
                  <li class="nav-event <?= (isset($manage_event))? "active" : ""; ?>"><a href="<?= base_url("dashboard"); ?>">Manage</a></li>
                </ul>
              </div>
            </nav>
          <!-- </div> -->
        <!-- </div> -->

      </div>
      <!-- Top My Event End -->
      <?php } ?>

      <?php 
      if(isset($sidebar)){ 
        if($this->user_level!="1"){
          $style = "top: 60px; position: fixed;";
        }else{
          $style = "";
        } 
      ?>
      
      <!-- ========== Left Sidebar Start ========== -->
      <div class="left side-menu" style="<?= $style; ?>">
        <div class="sidebar-inner slimscrollleft">
          <?= $sidebar; ?>
        </div>
      </div>
      <!-- Left Sidebar End -->
      <?php } ?>
      
      <!-- ============================================================== -->
			<!-- Start right Content here -->
			<!-- ============================================================== -->
      <?php
      $style="";
      if(isset($sidebar)){ 
        $class = "content-page";
        if($this->user_level!="1"){
          $style = "margin-top: 60px;";
        }
      }else{
        $class = "content-page-no-sidebar";
      }
      ?>
			<div class="<?= $class; ?>">
				<!-- Start content -->
				<div class="content" style="<?= $style; ?>">
					<div class="container">
            <?= $content; ?>
          </div> <!-- container -->
        </div> <!-- content -->
        <footer class="footer">
          © 2019. Pesta Wirausaha.
        </footer>
      </div>
      <!-- ============================================================== -->
      <!-- End Right content here -->
      <!-- ============================================================== -->

    </div>

    <script>
      var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?= base_url("assets/backend/js/jquery.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/bootstrap.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/detect.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/fastclick.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.slimscroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.blockUI.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/waves.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/wow.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.nicescroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.scrollTo.min.js");?>"></script>

    <script src="<?= base_url("assets/backend/plugins/bootstrap-tagsinput/js/bootstrap-tagsinput.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/switchery/js/switchery.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/select2/js/select2.min.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("assets/backend/plugins/bootstrap-filestyle/js/bootstrap-filestyle.min.js"); ?>" type="text/javascript"></script>

    <script src="<?= base_url("assets/backend/plugins/autocomplete/jquery.mockjax.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("assets/backend/plugins/autocomplete/jquery.autocomplete.min.js"); ?>" type="text/javascript"></script>
    <script src="<?= base_url("assets/backend/plugins/autocomplete/countries.js"); ?>"  type="text/javascript"></script>
    <script src="<?= base_url("assets/backend/pages/autocomplete.js"); ?>" type="text/javascript"></script>

    <script src="<?= base_url("assets/backend/plugins/moment/moment.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/moment/moment-id.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/notifyjs/js/notify.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/notifications/notify-metro.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/morris/morris.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/raphael/raphael-min.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/jquery-knob/jquery.knob.js"); ?>"></script>
    <!-- Theme JS files -->
    <script src="<?= base_url("assets/backend/plugins/visualization/echarts/echarts.js"); ?>"  type="text/javascript"></script>

    <script>
    <?php if($this->user_level=="2" && isset($dashboard_peserta)) { ?>
      
      /* ------------------------------------------------------------------------------
       *
       *  # Echarts - lines and areas
       *
       *  Lines and areas chart configurations
       *
       *  Version: 1.0
       *  Latest update: August 1, 2015
       *
       * ---------------------------------------------------------------------------- */

      $(function() {
          // Set paths
          // -----------------------------
          require.config({
              paths: {
                  echarts: 'http://localhost/pesta_wirausaha/assets/backend/plugins/visualization/echarts'
              }
          });

          // Configuration
          // ------------------------------

          require(
              [
                  'echarts',
                  'echarts/theme/limitless',
                  'echarts/chart/bar',
                  'echarts/chart/line'
              ],


              // Charts setup
              function (ec, limitless) {


                  // Initialize charts
                  // ------------------------------

                  var basic_lines = ec.init(document.getElementById('basic_lines'), limitless);
                  //var stacked_lines = ec.init(document.getElementById('stacked_lines'), limitless);
                  //var inverted_axes = ec.init(document.getElementById('inverted_axes'), limitless);
                  //var line_point = ec.init(document.getElementById('line_point'), limitless);
                  //var basic_area = ec.init(document.getElementById('basic_area'), limitless);
                  //var stacked_area = ec.init(document.getElementById('stacked_area'), limitless);
                  //var reversed_value = ec.init(document.getElementById('reversed_value'), limitless);



                  // Charts setup
                  // ------------------------------

                  //
                  // Basic lines options
                  //

                  basic_lines_options = {

                      // Setup grid
                      grid: {
                          x: 40,
                          x2: 40,
                          y: 35,
                          y2: 25
                      },

                      // Add tooltip
                      tooltip: {
                          trigger: 'axis'
                      },

                      // Add legend
                      legend: {
                          data: ['Pendapatan', 'Tiket', 'Pendaftar']
                      },

                      // Add custom colors
                      color: ['#66BB6A', '#EF5350', '#88BB98','#074fc1'],

                      // Enable drag recalculate
                      calculable: true,

                      // Horizontal axis
                      xAxis: [{
                          type: 'category',
                          boundaryGap: false,
                          //data: ['Mon', 'Tue', 'Wed', 'Thu', 'Fri', 'Sat', 'Sun']
                          data : ["00","01","02","03","04","05","06","07","08","09","10","11","12","13","14","15","16","17","18","19","20","21","22","23","24","25","26","27","28","29","30"]                        }],

                      // Vertical axis
                      yAxis: [{
                          type: 'value',
                          axisLabel: {
                              formatter: '{value}'
                          }
                      }],

                      // Add series
                      series: [
                          {
                              name: 'Pendapatan',
                              type: 'line',
                              data: [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                              markLine: {
                                  data: [{
                                      type: 'average',
                                      name: 'Average'
                                  }]
                              }
                          },
                          // {
                          //     name: 'Tiket',
                          //     type: 'line',
                          //     data: ["0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
                          //     markLine: {
                          //         data: [{
                          //             type: 'average',
                          //             name: 'Average'
                          //         }]
                          //     }
                          // },
                          // {
                          //     name: 'Pendaftar',
                          //     type: 'bar',
                          //     data: ["0","0","0","0","0","0","0","0","0","0","1","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0","0"],
                          //     markLine: {
                          //         data: [{
                          //             type: 'average',
                          //             name: 'Average'
                          //         }]
                          //     }
                          //  },
                          // {
                          //     name: 'Ask Analyst',
                          //     type: 'bar',
                          //     data: ,
                          //     markLine: {
                          //         data: [{
                          //             type: 'average',
                          //             name: 'Average'
                          //         }]
                          //     }
                          // }
                      ]
                  };


                  
                  // Apply options
                  // ------------------------------

                  basic_lines.setOption(basic_lines_options);



                  // Resize charts
                  // ------------------------------

                  window.onresize = function () {
                      setTimeout(function () {
                          basic_lines.resize();
                          // stacked_lines.resize();
                          // inverted_axes.resize();
                          // line_point.resize();
                          // basic_area.resize();
                          // stacked_area.resize();
                          // reversed_value.resize();
                      }, 200);
                  }
              }
          );
      });
    <?php } ?>
    
    </script>

    <script src="<?= base_url("assets/backend/js/jquery.core.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.app.js");?>"></script>

    <script src="<?= base_url("assets/backend/plugins/datatables/jquery.dataTables.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.bootstrap.js"); ?>"></script>

    <script src="<?= base_url("assets/backend/plugins/datatables/dataTables.buttons.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.bootstrap.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/jszip.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/pdfmake.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/vfs_fonts.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.html5.min.js"); ?>"></script>
    <script src="<?= base_url("assets/backend/plugins/datatables/buttons.print.min.js"); ?>"></script>


    <!--form validation init-->
    <script src="<?= base_url("assets/backend/plugins/tinymce/tinymce.min.js"); ?>"></script>

    <!-- google maps api -->
    <!-- <script scr="https://maps.google.com/maps/api/js?sensor=true"></script> -->
    <!-- main file -->
    <!-- <script src="<?= base_url("assets/backend/plugins/gmaps/gmaps.min.js"); ?>"></script> -->

    <script src="<?= base_url("assets/backend/js/script.js"); ?>" type="text/javascript"></script>
    <script>
      // alert(<?= $type ?>);
      <?php if($this->input->cookie('msg_success') != "" OR $this->input->cookie('msg_error') != "" OR $this->input->cookie('msg_warning') != ""){?>
        msg("<?= $type; ?>", "<?= $type; ?>", "<?= $msg; ?>");
      <?php } ?>

      function msg(type, title, msg){
        $.Notification.notify(type,"top right",title, msg);
        <?php
        if($this->input->cookie('msg_success')) delete_cookie('msg_success');
				if($this->input->cookie('msg_error')) delete_cookie('msg_error');
				if($this->input->cookie('msg_warning')) delete_cookie('msg_warning');
        if($this->input->cookie('data')) delete_cookie('data');
        ?>
      }
    </script>

  </body>
</html>