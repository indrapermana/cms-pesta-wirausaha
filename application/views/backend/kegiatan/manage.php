<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Kegiatan</h4>
      <p class="text-muted page-title-alt">Informasi PW</p>
      <!-- <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <li class="active">
          Kegiatan
        </li>
      </ol> -->
    </div>
  </div>
</div>

<?php if($aksi==""){ ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>List Kegiatan</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("kegiatan/manage/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <ul class="nav nav-tabs tabs">
        <li class="active tab">
          <a href="#publish" data-toggle="tab" aria-expanded="false">
            <span class="visible-xs"><i class="fa fa-home"></i></span>
            <span class="hidden-xs">terbit</span>
          </a>
        </li>
        <li class="tab"> 
          <a href="#darf" data-toggle="tab" aria-expanded="false">
            <span class="visible-xs"><i class="fa fa-user"></i></span>
            <span class="hidden-xs">Darf</span>
          </a>
        </li>
        <li class="tab"> 
          <a href="#past" data-toggle="tab" aria-expanded="true">
            <span class="visible-xs"><i class="fa fa-envelope-o"></i></span>
            <span class="hidden-xs">Kadaluarsa</span>
          </a>
        </li>
      </ul>

      <div class="tab-content">
        <div class="table-responsive tab-pane active" id="publish">
          <table class="table table-hover">
            <?php
              if(sizeof($data_event_publish)>0){
                for($i=0; $i<sizeof($data_event_publish); $i++){
                  $id = $data_event_publish[$i]['id'];
                  $gambar = $data_event_publish[$i]['gambar'];
                  $judul = $data_event_publish[$i]['nama_event'];
                  $tanggal = $data_event_publish[$i]['tgl_mulai'];
                  $tahun = $data_event_publish[$i]['tahun'];

                  if($gambar!=""){
                    $img = "/upload/event/".$tahun."/".$gambar;
                  }else{
                    $img = "/upload/img3.jpg";
                  }
            ?>
              <tr>
                <td style="width: 10%;"><img src="<?= base_url($img); ?>"width="150"></td>
                <td>
                  <h3><a href="<?= base_url("kegiatan/manages/".$id); ?>"><?= $judul; ?></a></h3>
                  <?= set_datetime($tanggal); ?><br><br>
                  <a href="<?= base_url("kegiatan/manages/".$id); ?>">
                    <i class="fa fa-cog"></i> Manage
                  </a>
                  &nbsp;&nbsp;
                  <!-- <a href="#" class="btn btn-primary">
                    <i class="fa fa-pencil"></i> Edit
                  </a> -->
                  <a href="#">
                    <i class="fa fa-eye"></i> View
                  </a>
                </td>
              </tr>
            <?php
                }
              }
            ?>
          </table>
        </div>

        <div class="table-responsive tab-pane active" id="darf">
          <table class="table table-hover">
            <?php
              if(sizeof($data_event_non_publish)>0){
                for($i=0; $i<sizeof($data_event_non_publish); $i++){
                  $id = $data_event_non_publish[$i]['id'];
                  $gambar = $data_event_non_publish[$i]['gambar'];
                  $judul = $data_event_non_publish[$i]['nama_event'];
                  $tanggal = $data_event_non_publish[$i]['tgl_mulai'];
                  $tahun = $data_event_non_publish[$i]['tahun'];

                  if($gambar!=""){
                    $img = "/upload/event/".$tahun."/".$gambar;
                  }else{
                    $img = "/upload/big/img3.jpg";
                  }
            ?>
              <tr>
                <td style="width: 10%;"><img src="<?= base_url($img); ?>"width="150"></td>
                <td>
                  <h3><a href="<?= base_url("kegiatan/manages/".$id); ?>"><?= $judul; ?></a></h3>
                  <?= set_datetime($tanggal); ?><br><br>
                  <a href="<?= base_url("kegiatan/manages/".$id); ?>">
                    <i class="fa fa-cog"></i> Manage
                  </a>
                  &nbsp;&nbsp;
                  <a href="<?= base_url("kegiatan/manage/ubah/".$id); ?>">
                    <i class="fa fa-pencil"></i> Edit
                  </a>
                  &nbsp;&nbsp;
                  <a href="#">
                    <i class="fa fa-eye"></i> View
                  </a>
                </td>
              </tr>
            <?php
                }
              }
            ?>
          </table>
        </div>

        <div class="table-responsive tab-pane active" id="past">
          <table class="table table-hover">
            <?php
              if(sizeof($data_event_past)>0){
                for($i=0; $i<sizeof($data_event_past); $i++){
                  $id = $data_event_past[$i]['id'];
                  $gambar = $data_event_past[$i]['gambar'];
                  $judul = $data_event_past[$i]['nama_event'];
                  $tanggal = $data_event_past[$i]['tgl_mulai'];
                  $tahun = $data_event_past[$i]['tahun'];

                  if($gambar!=""){
                    $img = "/upload/event/".$tahun."/".$gambar;
                  }else{
                    $img = "/upload/img3.jpg";
                  }
            ?>
              <tr>
                <td style="width: 10%;"><img src="<?= base_url($img); ?>"width="150"></td>
                <td>
                  <h3><a href="<?= base_url("kegiatan/manages/".$id); ?>"><?= $judul; ?></a></h3>
                  <?= set_datetime($tanggal); ?><br><br>
                  <a href="<?= base_url("kegiatan/manages/".$id); ?>">
                    <i class="fa fa-cog"></i> Manage
                  </a>
                  &nbsp;&nbsp;
                  <!-- <a href="#" class="btn btn-primary">
                    <i class="fa fa-pencil"></i> Edit
                  </a> -->
                  <a href="#">
                    <i class="fa fa-eye"></i> View
                  </a>
                </td>
              </tr>
            <?php
                }
              }
            ?>
          </table>
        </div>
      </div>
    </div>
  </div>
</div>

<?php } else {
  $judul = ""; $slug = ""; $gambar = "";  $materi_event = ""; $kategori = ""; 
  $tahun = date('Y'); $wilayah = ""; $status = "0"; $lokasi = ""; $lokasi_lengkap = "";
  $tgl_mulai = ""; $tgl_selesai = ""; $lok_lat = ""; $lok_long = "";

  if (sizeof($data_event)>0) {
    $kategori = $data_event[0]["kategori_id"];
    $tahun = $data_event[0]["tahun"];
    $wilayah = $data_event[0]["wilayah_id"];
    $judul = $data_event[0]["nama"];
    $slug = $data_event[0]["slug"];
    $status = $data_event[0]["status_event"];
    $gambar = $data_event[0]["gambar"];
    $lokasi = $data_event[0]["lokasi"];
    $lokasi_lengkap = $data_event[0]["lokasi_lengkap"];
    $tgl_mulai = $data_event[0]["tgl_mulai"];
    $tgl_selesai = $data_event[0]["tgl_selesai"];
    $materi_event = $data_event[0]["materi_event"];
    $lok_lat = $data_event[0]["lok_lat"];
    $lok_long = $data_event[0]["lok_long"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Kegiatan"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Kegiatan"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Kegiatan"; }
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?= $xjudul; ?></b></h4>
      <hr>

      <form class="form-horizontal" action="<?= base_url('kegiatan/manage/'.$aksi."/".$eventid); ?>" method="post" enctype="multipart/form-data" >
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Judul</label>
          <div class="col-sm-6">
            <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
          </div>
        </div>
        <!-- <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Slug</label>
          <div class="col-sm-10">
            <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
          </div>
        </div> -->
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Tahun</label>
          <div class="col-sm-6">
            <input type="text" name="tahun" placeholder="Tahun" class="form-control" value="<?= $tahun; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Kategori</label>
          <div class="col-sm-6">
            <select name="kategori" class="form-control select2">
              <option value="">Pilih Kategori</option>
              <?php
              for($i=0; $i<sizeof($data_kategori); $i++){
                $id = $data_kategori[$i]['id'];
                $nama_kategori = $data_kategori[$i]['nama'];
                $select = ($kategori==$id)? "selected" : "";
              ?>
              <option value="<?= $id; ?>" <?= $select; ?>><?= $nama_kategori; ?></option>
              <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Wilayah</label>
          <div class="col-sm-6">
            <select name="wilayah" class="form-control select2">
              <option value="">Pilih Wilayah</option>
              <?php
              for($i=0; $i<sizeof($data_wilayah); $i++){
                $id = $data_wilayah[$i]['id'];
                $nama_wilayah = $data_wilayah[$i]['wilayah'];
                $select = ($wilayah==$id)? "selected" : "";
              ?>
              <option value="<?= $id; ?>" <?= $select; ?>><?= $nama_wilayah; ?></option>
              <?php
              }
              ?>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Lokasi</label>
          <div class="col-sm-6">
            <input type="text" name="lokasi" placeholder="Lokasi" class="form-control" value="<?= $lokasi; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Lokasi Lengkap</label>
          <div class="col-sm-6">
            <input type="text" name="lokasi_lengkap" placeholder="Lokasi Lengkap" class="form-control" value="<?= $lokasi_lengkap; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Latitude</label>
          <div class="col-sm-6">
            <input type="text" name="lok_lat" placeholder="Latitude" class="form-control" value="<?= $lok_lat; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Longitude</label>
          <div class="col-sm-6">
            <input type="text" name="lok_long" placeholder="Longitude" class="form-control" value="<?= $lok_long; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Tanggal</label>
          <div class="col-sm-6">
            <!-- <input type="text" name="tgl_mulai" placeholder="Tanggal Mulai" class="form-control" value="<?= $tgl_mulai; ?>"> -->
            <div class="input-daterange input-group" id="date-range">
              <input type="text" class="form-control" id="datetime-start" name="tgl_mulai" value="<?= $tgl_mulai; ?>" placeholder="yyyy-mm-dd" />
              <span class="input-group-addon bg-custom b-0 text-white">Sampai</span>
              <input type="text" class="form-control" id="datetime-end" name="tgl_selesai" value="<?= $tgl_selesai; ?>" placeholder="yyyy-mm-dd" />
            </div>
          </div>
        </div>
        <!-- <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Tanggal Selesai</label>
          <div class="col-sm-6">
            <input type="text" name="tgl_selesai" placeholder="Tanggal Selesai" class="form-control" value="<?= $tgl_selesai; ?>">
          </div>
        </div> -->
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Status Kegiatan</label>
          <div class="col-sm-6">
            <?php $check = ($status)? "checked" : "";?>
            <input type="checkbox" name="status_event" <?= $check; ?> data-plugin="switchery" data-color="#81c868" value="1"/>
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Gambar</label>
          <div class="col-sm-6">
            <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Materi Kegiatan</label>
          <div class="col-sm-10">
            <textarea id="elm1" name="materi_event"><?= $materi_event; ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("kegiatan/manage"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>

      <!-- <div class="pac-card" id="pac-card">
        <div>
            <div id="label">Location search</div>
        </div>
        <div id="pac-container">
            <input id="pac-input" type="text" placeholder="Enter a location">
            <div id="location-error"></div>
        </div>
      </div>
      <div id="map"></div>
      <div id="infowindow-content">
        <img src="" width="16" height="16" id="place-icon"> <span
          id="place-name" class="title"></span><br> <span
          id="place-address"></span>
      </div> -->

    </div>
  </div>
</div>

<?php } ?>