<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Kirim Email</h4>
    <p class="text-muted page-title-alt">Pengiriman Email</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Kirim Email
      </li>
    </ol>
  </div>
</div>

<?php if ($submit==0) { ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Pengiriman Email</b></h4>
      <hr>

      <form class="form-horizontal" method="post" action="<?php echo site_url("email/kirim"); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Target Email</label>
          <div class="col-sm-10">
            <select name="target" class="form-control">
              <option value="1">Pembeli tiket yang sudah bayar</option>
              <option value="0">Pembeli tiket yang belum bayar</option>
              <option value="2">Test Email ke abrahamsyah@gmail.com, novrand@gmail.com</option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Tujuan</label>
          <div class="col-sm-10">
            <input type="text" class="form-control" name="tujuan" value="">
          </div>
        </div>			  
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Isi Email</label>
          <div class="col-sm-10">
            <textarea class="form-control" name="isi" rows="10" cols="60"></textarea>
            <br>Gunakan kode {NAMA} dalam isi email
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-paper-plane-o"></i> Kirim Email
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php } else { ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Pengiriman Email target</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(sizeof($arr_tiket)>0){
              for ($i=0;$i<sizeof($arr_tiket);$i++) {
                $nama = $arr_tiket[$i]["nama"];
                $email = $arr_tiket[$i]["email"];
                $status = "xx";
            ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $nama; ?></td>
              <td><?= $email; ?></td>
              <td><?= $status; ?></td>
            </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php } ?>
