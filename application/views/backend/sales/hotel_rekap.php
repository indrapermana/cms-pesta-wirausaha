<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekap Pemesanan Kamar Hotel</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekap Pemesanan Kamar Hotel
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Kamar Hotel</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Room Type</th>
              <th>Pesan</th>
              <th>Lunas</th>
              <th>Expo</th>
              <th>Pembicara</th>
              <th>Sponsor</th>
              <th>Total</th>
            </tr>
          </thead>
          <?php
            $arrRekap = array(); $arrRekap2 = array(); $arrRekap3 = array();
            if (sizeof($data_hotel2)>0) {
              for ($i=0;$i<sizeof($data_hotel2);$i++) { 
                $cekin = substr($data_hotel2[$i]["tgl_cekin"],8,2);
                $cekout = substr($data_hotel2[$i]["tgl_cekout"],8,2);
                //$numpax = $data_hotel2[$i]["numpax"];
                $roomtype = $data_hotel2[$i]["room_type"]; 
                $jml_kamar = $data_hotel2[$i]["jumlah"];
                $status = $data_hotel2[$i]["status_bayar"];

                $xroomtype = "";
                if ($roomtype=="1") { $xroomtype = "R1 - 1 Bedroom"; }
                if ($roomtype=="2") { $xroomtype = "R2 - 2 Bedroom"; }

                if (!isset($arrRekap[$cekin][$cekout][$roomtype][$status])) {
                  $arrRekap[$cekin][$cekout][$roomtype][$status] = 0;
                }

                if (!isset($arrRekap2[$cekin][$cekout][$roomtype])) {
                  $arrRekap2[$cekin][$cekout][$roomtype] = 0;
                  $arrRekap3[] = array("cekin"=>$cekin,"cekout"=>$cekout,"roomtype"=>$roomtype);
                }

                $arrRekap[$cekin][$cekout][$roomtype][$status] = $jml_kamar;
                $arrRekap2[$cekin][$cekout][$roomtype]++;
              }
            }

            function dataKamar($arrRekap,$cekin,$cekout,$roomtype,$status) {
              $jml = 0;
              if (isset($arrRekap[$cekin][$cekout][$roomtype][$status])) {
                $jml = $arrRekap[$cekin][$cekout][$roomtype][$status];
              }

              return $jml;
            }

            function totalKamar($arrRekap,$cekin,$cekout,$roomtype) {
              $jml = 0;
              if (isset($arrRekap[$cekin][$cekout][$roomtype]["0"])) {
                $jml += $arrRekap[$cekin][$cekout][$roomtype]["0"];
              }

              if (isset($arrRekap[$cekin][$cekout][$roomtype]["1"])) {
                $jml += $arrRekap[$cekin][$cekout][$roomtype]["1"];
              }

              if (isset($arrRekap[$cekin][$cekout][$roomtype]["3"])) {
                $jml += $arrRekap[$cekin][$cekout][$roomtype]["3"];
              }

              if (isset($arrRekap[$cekin][$cekout][$roomtype]["4"])) {
                $jml += $arrRekap[$cekin][$cekout][$roomtype]["4"];
              }

              if (isset($arrRekap[$cekin][$cekout][$roomtype]["6"])) {
                $jml += $arrRekap[$cekin][$cekout][$roomtype]["6"];
              }

              return $jml;
            }

            if (sizeof($arrRekap3)>0) {
              $t_pesan = 0; $t_lunas = 0; $t_expo = 0; $t_pembicara = 0; $t_sponsor = 0; $t_total = 0;
          ?>
          <tbody>
          <?php
              for ($i=0;$i<sizeof($arrRekap3);$i++) {
                $cekin = $arrRekap3[$i]["cekin"];
                $cekout = $arrRekap3[$i]["cekout"];
                $roomtype = $arrRekap3[$i]["roomtype"];

                $xroomtype = "";

                if ($roomtype=="1") { $xroomtype = "R1 1 Bedroom"; } 
                if ($roomtype=="2") { $xroomtype = "R2 2 Bedroom"; }

                $t_pesan += dataKamar($arrRekap,$cekin,$cekout,$roomtype,"0");
                $t_lunas += dataKamar($arrRekap,$cekin,$cekout,$roomtype,"1");
                $t_expo += dataKamar($arrRekap,$cekin,$cekout,$roomtype,"3");
                $t_pembicara += dataKamar($arrRekap,$cekin,$cekout,$roomtype,"4");
                $t_sponsor += dataKamar($arrRekap,$cekin,$cekout,$roomtype,"6");
              ?>
              <tr>
                <td><?= ($i+1); ?></td>
                <td><?= $cekin." - ".$cekout; ?></td>
                <td><?= $xroomtype; ?></td>
                <td><?= dataKamar($arrRekap,$cekin,$cekout,$roomtype,"0"); ?></td>
                <td><?= dataKamar($arrRekap,$cekin,$cekout,$roomtype,"1"); ?></td>
                <td><?= dataKamar($arrRekap,$cekin,$cekout,$roomtype,"3"); ?></td>
                <td><?= dataKamar($arrRekap,$cekin,$cekout,$roomtype,"4"); ?></td>
                <td><?= dataKamar($arrRekap,$cekin,$cekout,$roomtype,"6"); ?></td>
                <td><?= totalKamar($arrRekap,$cekin,$cekout,$roomtype); ?></td>
              </tr>	
              <?php } ?>
          </tbody>

          <tfoot>
              <tr>
              <td></td>
              <td></td>
              <td></td>
              <td><?= $t_pesan; ?></td>
              <td><?= $t_lunas; ?></td>
              <td><?= $t_expo; ?></td>
              <td><?= $t_pembicara; ?></td>
              <td><?= $t_sponsor; ?></td>
              <td><?= ($t_pesan+$t_lunas+$t_expo+$t_pembicara+$t_sponsor); ?></td>
            </tr>
          </tfoot>
          <?php	} ?>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Detail Pemesanan Kamar Hotel</b></h4>
      <div class="filtter-right">
        <form action="<?php echo base_url("sales/hotelrekap"); ?>" method="post">
          <select name="status" onchange="this.form.submit();" class="form-control ">
            <option value="0"<?php if ($status=="0") { echo " selected"; } ?>>Semua Status</option>
						<option value="1"<?php if ($status=="1") { echo " selected"; } ?>>Waiting List</option>
						<option value="2"<?php if ($status=="2") { echo " selected"; } ?>>Pemesanan Booth</option>
						<option value="3"<?php if ($status=="3") { echo " selected"; } ?>>Pembayaran Sebagian</option>
						<option value="4"<?php if ($status=="4") { echo " selected"; } ?>>Pembayaran Lunas</option>
						<option value="6"<?php if ($status=="6") { echo " selected"; } ?>>Sponsorship</option>
					</select>
        </form>
      </div>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Kontak</th>
              <th>Paket</th>
              <th>Cek In</th>
              <th>Cek Out</th>
              <th>Room Type</th>
              <th>Num Pax</th>
              <th>Breakfast</th>
              <th>Num Room</th>
              <th>Nilai</th>
              <th>Nilai Sarapan</th>
              <th>Total</th>
              <th>Catatan Hotel</th>
              <th>Catatan Panitia</th>
            </tr>
          </thead>

          <?php 
            if (sizeof($data_hotel)>0) {
              $t_total = 0; $t_nilai = 0; $t_nilaib = 0; $t_kamar = 0; $t_pax = 0;
          ?>
          <body>
          <?php
              for ($i=0;$i<sizeof($data_hotel);$i++) { 
                $id = $data_hotel[$i]["id"];
                $tanggal = $data_hotel[$i]["tanggal"];
                $kodebooking = $data_hotel[$i]["kode_booking"];
                $jenis = $data_hotel[$i]["jenis_kamar"];
                $cekin = substr($data_hotel[$i]["tgl_cekin"],8,2);
                $cekout = substr($data_hotel[$i]["tgl_cekout"],8,2);
                $numpax = $data_hotel[$i]["numpax"];
                $roomtype = $data_hotel[$i]["room_type"];
                $breakfast = $data_hotel[$i]["breakfast"];
                $nama = $data_hotel[$i]["nama_lengkap"];
                $email = $data_hotel[$i]["email"];
                $hp = $data_hotel[$i]["nohp"];
                $no_tiket = $data_hotel[$i]["no_tiket"];
                $asal_tda = $data_hotel[$i]["asal_tda"];
                $no_ktp = $data_hotel[$i]["no_ktp"];
                $keterangan = $data_hotel[$i]["keterangan"];
                $keterangan2 = $data_hotel[$i]["keterangan2"];
                $jml_kamar = $data_hotel[$i]["jml_kamar"];
                $nilai = $data_hotel[$i]["nilai"];
                $nilaib = $data_hotel[$i]["nilai_bf"];
                $status = $data_hotel[$i]["status_bayar"];

                $xjenis = $jenis;
                if ($jenis=="1") { $xjenis = "Paket 3 Malam "; }
                if ($jenis=="2") { $xjenis = "Paket 2 Malam "; }

                $xroomtype = "";
                if ($roomtype=="1") { $xroomtype = "R1 1 Bedroom"; } 
                if ($roomtype=="2") { $xroomtype = "R2 2 Bedroom"; }

                $xbreakfast = "";
                if ($breakfast=="0") { $xbreakfast = "Tidak"; }
                if ($breakfast=="1") { $xbreakfast = "Termasuk"; } 

                $total = $nilai + $nilaib;

                $t_nilai += $nilai;
                $t_nilaib += $nilaib;
                $t_total += $total;
                $t_kamar += $jml_kamar;
                $t_pax += $numpax;
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $kodebooking; ?></td>
              <td><?= $nama."<br>".$email."<br>".$hp."<br>".$no_ktp; ?></td>
              <td><?= $xjenis; ?></td>
              <td><?= $cekin; ?></td>
              <td><?= $cekout; ?></td>
              <td><?= $xroomtype; ?></td>
              <td><?= $numpax; ?></td>
              <td><?= $xbreakfast; ?></td>
              <td><?= $jml_kamar; ?></td>
              <td class="text-center"><?= number_format($nilai); ?></td>
              <td class="text-center"><?= number_format($nilaib); ?></td>
              <td class="text-center"><?= number_format($total); ?></td>
              <td><?= $keterangan; ?></td>
              <td><?= $keterangan2; ?></td>
            </tr>";
          <?php } ?>
          </body>
          
          <thead>
              <tr>
                <td colspan=6></td>
                <td><?= $t_pax; ?></td>
                <td></td>
                <td><?= $t_kamar; ?></td>
                <td class="text-center"><?= number_format($t_nilai); ?></td>
                <td class="text-center"><?= number_format($t_nilaib); ?></td>
                <td class="text-center"><?= number_format($t_total); ?></td>
                <td></td>
                <td></td>
              </tr>
          </thead>
          <?php } ?>				
        </table>
      </div>
    </div>
  </div>
</div>