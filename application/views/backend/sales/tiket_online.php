<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pendapatan Pesta Wirausaha 2019</h4>
    <p class="text-muted page-title-alt">Tiket</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Tiket Online
      </li>
    </ol>
  </div>
</div>

<?php
  if ($target!="") { 

    $nilai = 0;
    if (sizeof($data_tiket)>0)
      $nilai = $data_tiket[0]["nilai"];
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Target Penjualan Tiket</b></h4>
      <hr>
      <div class="row">
        <div class="col-sm-12">
          <form class="form-horizontal" method="post" action="<?= base_url('sales/tiketonline/target'); ?>" role="form">                                    
            <div class="form-group">
              <label class="col-sm-2 control-label">Target</label>
              <div class="col-sm-10">
                <input type="text" name="nilai" value="<?php echo $nilai; ?>" class="form-control" placholder="Nilai Target">
              </div>
            </div>
            <button type="submit"  class="btn btn-primary waves-effect waves-light pull-right">Ubah</button>
          </form>
        </div>
      </div>
    </div>                     
  </div>
</div>

<?php 
  }else{ 
    $t_target = 0;
    if (sizeof($data_target)>0) { $t_target = $data_target[0]["nilai"]; }
    
    $t_nilai = 0; $t_pw = 0; $t_hadiah = 0; $t_daerah = 0; $t_affiliate = 0; $t_kode = 0; $t_jumlah = 0;
    if (sizeof($data_tiket)>0) {
      $t_jumlah = $data_tiket[0]["jml_tiket"];
      $t_nilai = $data_tiket[0]["nilai"];
      $t_pw = $data_tiket[0]["pw"];
      $t_daerah = $data_tiket[0]["daerah"];
      $t_hadiah = $data_tiket[0]["hadiah"]; 
      $t_affiliate = $data_tiket[0]["affiliate"];
      $t_kode = $data_tiket[0]["kode"];
    }
?>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?= $t_target; ?>
          </b>
        </h3>
        <p class="text-muted">Target Penjualan Tiket</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-info text-info">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="ti-ticket text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_jumlah . " (" . number_format($t_jumlah/$t_target*100,2)." %)"; ?>
          </b>
        </h3>
        <p class="text-muted">Jumlah Penjualan Tiket</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-purple pull-left">
        <i class="fa fa-soccer-ball-o text-purple"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_nilai); ?>
          </b>
        </h3>
        <p class="text-muted">Total Nilai</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-purple text-purple">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
          <?php echo "Rp ".number_format($t_pw); ?>
          </b>
        </h3>
        <p class="text-muted">Alokasi PW</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
          <?php echo "Rp ".number_format($t_daerah); ?>
          </b>
        </h3>
        <p class="text-muted">Alokasi TDA Daerah</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-trophy text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
          <?php echo "Rp ".number_format($t_hadiah); ?>
          </b>
        </h3>
        <p class="text-muted">Alokasi Hadiah</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-users text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
          <?php echo "Rp ".number_format($t_affiliate); ?>
          </b>
        </h3>
        <p class="text-muted">Alokasi Affiliate</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-danger pull-left">
        <i class="fa fa-smile-o text-danger"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
          <?php echo "Rp ".number_format($t_kode); ?>
          </b>
        </h3>
        <p class="text-muted">Total Kode</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-danger text-danger">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>

<div class="row">
  <!-- Data TDA Daerah -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>TDA Daerah</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table m-0">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Daerah</th>
              <th class="text-center">Jml Tiket</th>
              <th class="text-center">Nilai</th>
            </tr>
          </thead>

          <?php if (sizeof($data_daerah)>0) { ?>
          <tbody>
          <?php
              $t_jumlah = 0; $t_nilai = 0;
              for ($i=0;$i<sizeof($data_daerah);$i++) {
                $daerah = $data_daerah[$i]["daerah"];
                $jumlah = $data_daerah[$i]["jml_tiket"];
                $nilai = $data_daerah[$i]["nilai"];
                $t_jumlah += $jumlah;
                $t_nilai += $nilai;
            ?>
            <tr>
              <td class="text-center"><?= $i+1; ?></td>
              <td class="text-left"><?= $daerah; ?></td>
              <td class="text-center"><?= $jumlah; ?></td>
              <td class="text-right"><?= number_format($nilai); ?></td>
            </tr>
            <?php } ?>
          </tbody>
          <tfoot>
            <tr>
              <th class="text-right" colspan="2">Total</th>
              <th class="text-center"><?= $t_jumlah; ?></th>
              <th class="text-right"><?= number_format($t_nilai); ?></th>
            </tr>
          </tfoot>
          <?php } ?>

        </table>
      </div>
    </div>    
  </div>

  <!-- Data Affiliate -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>Affiliate</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table m-0">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Affiliate</th>
              <th class="text-center">Jml Tiket</th>
              <th class="text-center">Nilai</th>
            </tr>
          </thead>

          <?php 
            $arr_aff = array();
            $arr_aff["0"] = "Tanpa Affiliate";
            if (sizeof($data_kodeaff)>0) {
              for ($i=0;$i<sizeof($data_kodeaff);$i++) {
                $aff = $data_kodeaff[$i]["affid"];
                $nama = $data_kodeaff[$i]["nama"];
                $usaha = $data_kodeaff[$i]["usaha"];
                $daerah = $data_kodeaff[$i]["daerah"];

                $arr_aff[$aff] = $nama . " " . $usaha . " " . $daerah;
              }
            }

            if (sizeof($data_affiliate)>0) {
              $t_jumlah = 0; $t_nilai = 0;
          ?>
          <tbody>
          <?php
              for ($i=0;$i<sizeof($data_affiliate);$i++) {
                $aff = $data_affiliate[$i]["affid"];
                $jumlah = $data_affiliate[$i]["jml_tiket"];
                $nilai = $data_affiliate[$i]["nilai"];
                
                $xnama = $aff; 
                if (isset($arr_aff[$aff])) { $xnama = $arr_aff[$aff] . " [$aff]"; }
                $t_jumlah += $jumlah; $t_nilai += $nilai;
          ?>
            <tr>
              <td class="text-center"><?= $i+1; ?></td>
              <td class="text-left"><?= $xnama; ?></td>
              <td class="text-center"><?= $jumlah; ?></td>
              <td class="text-right"><?= number_format($nilai); ?></td>
            </tr>
          <?php } ?>
          </tbody>

          <tfoot>
            <tr>
              <th class="text-right" colspan="2">Total</th>
              <th class="text-center"><?= $t_jumlah; ?></th>
              <th class="text-right"><?= number_format($t_nilai); ?></th>
            </tr>
          <?php } ?>
          </tfoot>
        </table>
      </div>
    </div>   
  </div>
</div>
<?php } ?>