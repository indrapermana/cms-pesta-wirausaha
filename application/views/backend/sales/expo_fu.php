<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Expo Pesta Wirausaha 2019</h4>
    <p class="text-muted page-title-alt">Follow Up Expo</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Follow Up Expo
      </li>
    </ol>
  </div>
</div>

<?php
	$tanggal = ""; $nama = ""; $usaha = ""; $email = ""; $hp = ""; $sminat = ""; $ocatatan = ""; $ostatus = "";
	$xstatus = ""; $obooth = ""; $opremium = ""; $oreguler = ""; $okuliner1 = ""; $okuliner2 = ""; $onilai = 0;
	
	if (sizeof($data_expo)>0) {
		$tanggal = $data_expo[0]["tanggal"];
		$nama = $data_expo[0]["nama"];
		$usaha = $data_expo[0]["usaha"];
		$email = $data_expo[0]["email"];
		$hp = $data_expo[0]["no_hp"];
		$minat = $data_expo[0]["minat"];
		$ocatatan = $data_expo[0]["catatan"];
		$ostatus = $data_expo[0]["status_pesan"];
		$obooth = $data_expo[0]["nobooth"];
		$opremium = $data_expo[0]["premium"];
		$oreguler = $data_expo[0]["reguler"];
		$okuliner1 = $data_expo[0]["kuliner"];
		$okuliner2 = $data_expo[0]["kuliner2"];
		$onilai = $data_expo[0]["nilai_bayar"];
		
		if ($minat!="") {
			$xminat = explode(",",$minat);
			for ($j=0;$j<sizeof($xminat);$j++) {
				if (trim($xminat[$j])!="") {
					if ($xminat[$j]=="1") { $sminat .= " Multi Premium,"; }
					if ($xminat[$j]=="2") { $sminat .= " Multi Reguler,"; }
					if ($xminat[$j]=="3") { $sminat .= " Kuliner Premium,"; }
					if ($xminat[$j]=="4") { $sminat .= " Kuliner Reguler,"; }
				}
			}
		}
		if ($sminat!="") { $sminat = substr($sminat,0,-1); }
		
		$xstatus = "";
		if ($ostatus=="1") { $xstatus = "Waiting List"; }
		if ($ostatus=="2") { $xstatus = "Pemesanan Booth"; }
		if ($ostatus=="3") { $xstatus = "Pembayaran Uang Muka"; }
		if ($ostatus=="4") { $xstatus = "Pembayaran Lunas"; }
		if ($ostatus=="5") { $xstatus = "Batal"; }
		if ($ostatus=="6") { $xstatus = "Sponsorship"; }
		
	}
	
?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Info Kontak</b></h4>
      <hr>
      <dl>
        <dt>Tanggal</dt>
        <dd><?php echo $tanggal; ?></dd>
        <dt>Kontak Person</dt>
        <dd><?php echo $nama."<br>".$usaha."<br>".$email."<br>".$hp; ?></dd>
        <?php 
          if ($sminat!="") { echo "<dt>Peminatan Booth</dt><dd>".$sminat."</dd>"; }
          if ($opremium!="") { echo "<dt>Multi Premium</dt><dd>".$opremium."</dd>"; }
          if ($oreguler!="") { echo "<dt>Multi Reguler</dt><dd>".$oreguler."</dd>"; }
          if ($okuliner1!="") { echo "<dt>Kuliner Premium</dt><dd>".$okuliner1."</dd>"; }
          if ($okuliner2!="") { echo "<dt>Kuliner Reguler</dt><dd>".$okuliner2."</dd>"; }
        ?>	
        <dt>Status Pemesanan</dt>
        <dd><?php echo $xstatus; ?></dd>
        <dt>Nilai Pembayaran</dt>
        <dd><?php echo "Rp. " . number_format($onilai); ?></dd>				
        <dt>Catatan</dt>
        <dd><?php echo $ocatatan; ?></dd>
      </dl>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Update Follow Up</b></h4>
      <hr>

      <?php
        if (sizeof($data_exponote)>0) {
      ?>
      <ul class="timeline">
      <?php 
          for ($i=0;$i<sizeof($data_exponote);$i++) {
            $nama = $data_exponote[$i]["nama"]; if ($nama=="") { $nama = "System"; }
            $tanggal = $data_exponote[$i]["tanggal"];
            $catatan = $data_exponote[$i]["catatan"];
      ?>
        <li>
          <i class="fa fa-user bg-aqua"></i>
          <div class="timeline-item">
            <span class="time"><i class="fa fa-clock-o"></i> <?= $tanggal; ?></span>
            <h3 class="timeline-header no-border"><a href="#"><?= $nama; ?></a></h3>
            <div class="timeline-body"><?= $catatan; ?></div>
          </div>
        </li>
      <?php
          }
      ?>
        <li><i class="fa fa-clock-o bg-gray"></i></li></ul>
      <?php
        }
      ?>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Pemesanan Booth Expo</b></h4>
      <hr>

      <form method="post" action="<?php echo site_url("sales/expofu/".$expoid); ?>">
			  <input type="hidden" name="step" value="<?php echo $step; ?>">
      
      <?php if ($step=="1") { ?>
        <input type="hidden" name="ostat" value="<?php echo $ostatus; ?>">
        <input type="hidden" name="obooth" value="<?php echo $obooth; ?>">
        <input type="hidden" name="onilai" value="<?php echo $onilai; ?>">
        
        <div class="form-group">
          <label for="input1">Status Pemesanan</label>
          <select name="status" class="form-control">
            <option value="1"<?php if ($ostatus=="1") { echo " selected"; } ?>>Waiting List</option>
            <option value="2"<?php if ($ostatus=="2") { echo " selected"; } ?>>Pemesanan Booth</option>
            <option value="3"<?php if ($ostatus=="3") { echo " selected"; } ?>>Pembayaran Uang Muka</option>
            <option value="4"<?php if ($ostatus=="4") { echo " selected"; } ?>>Pembayaran Lunas</option>
            <option value="6"<?php if ($ostatus=="6") { echo " selected"; } ?>>Sponsorship</option>
            <option value="5"<?php if ($ostatus=="5") { echo " selected"; } ?>>Batal</option>
				  </select> 
        </div>
        <div class="form-group">
          <label for="input1">No Booth</label>
          <input type="text" name="nobooth" class="form-control" placeholder="No Booth" value="<?php echo $obooth; ?>">
				  <p class="help-block">Gunakan koma untuk beberapa booth : 10,11,12</p>
        </div>
        <div class="form-group">
          <label for="input1">Catatan</label>
          <textarea name="catatan" class="form-control" cols="60" rows="4"></textarea>
        </div>
        <div class="form-group">
          <a href="<?= base_url("sales/expowl"); ?>" class="btn btn-default">Kembali</a>
          <input type="submit" class="btn btn-primary" name="tombol1" value="submit">
        </div>
      
      <?php } else if ($step=="2") { ?>
        <input type="hidden" name="ostat" value="<?php echo $status; ?>">
        <input type="hidden" name="obooth" value="<?php echo $nobooth; ?>">
        <input type="hidden" name="onilai" value="<?php echo $nilai; ?>">
        <input type="hidden" name="premium" value="<?php echo $premium; ?>">
        <input type="hidden" name="reguler" value="<?php echo $reguler; ?>">
        <input type="hidden" name="kuliner1" value="<?php echo $kuliner1; ?>">
        <input type="hidden" name="kuliner2" value="<?php echo $kuliner2; ?>">
        <input type="hidden" name="catatan" value="<?php echo $catatan; ?>">

        <div class="table-responsive">
          <table class="table table-hover">
            <thead>
              <tr>
                <th width="5%">No</th>
                <th width="25%">No Booth</th>
                <th width="30%">Tipe</th>
                <th width="20%">Status</th>
                <th width="20%">Harga</th>
              </tr>
            </thead>
            <?php
              if (sizeof($data_booth)>0) {
                $total = 0;
            ?>
            <tbody>
            <?php
                for ($i=0;$i<sizeof($data_booth);$i++) {
                  $no = $data_booth[$i]["no"];
                  $tipe = $data_booth[$i]["tipe"];
                  $status = $data_booth[$i]["status"];
                  $harga = $data_booth[$i]["harga"];
                  $total += $harga;
            ?>
              <tr>
                <td><?= ($i+1); ?></td>
                <td><?= $no; ?></td>
                <td><?= $tipe; ?></td>
                <td><?= $status; ?></td>
                <td><?= number_format($harga); ?></td>
              </tr> 
            <?php
                }
            ?>
            </tbody>
            <thead>
              <tr>
                <td colspan="4">&nbsp;</td>
                <td><?= number_format($total); ?></td>
              </tr>
            </thead>
            <?php
              }
            ?>
          </table>
        </div>
      <?php } else if ($step=="3") { ?>
        <div class="form-group">
          <p>Catatan telah disimpan </p>
        </div>
      <?php } ?>
      </form>
    </div>
  </div>
</div>