<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pendaftaran Stand Expo</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Follow Up Expo PW
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Expo</b></h4>
      <div class="filtter-right">
        <form action="<?php echo base_url("sales/expowl"); ?>" method="post">
          <select name="status" onchange="this.form.submit();" class="form-control ">
            <option value="0"<?php if ($status=="0") { echo " selected"; } ?>>Semua Status</option>
						<option value="1"<?php if ($status=="1") { echo " selected"; } ?>>Waiting List</option>
						<option value="2"<?php if ($status=="2") { echo " selected"; } ?>>Pemesanan Booth</option>
						<option value="3"<?php if ($status=="3") { echo " selected"; } ?>>Pembayaran Sebagian</option>
						<option value="4"<?php if ($status=="4") { echo " selected"; } ?>>Pembayaran Lunas</option>
						<option value="6"<?php if ($status=="6") { echo " selected"; } ?>>Sponsorship</option>
						<option value="5"<?php if ($status=="5") { echo " selected"; } ?>>Batal</option>
          </select>
        </form>
      </div>
      <hr> 

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Tanggal</th>
              <th width="20%">Kontak</th>
              <th width="20%">No Booth</th>
              <th width="20%">Nilai</th>
              <th width="10%">Status</th>
              <th width="10%">&nbsp;</th>
            </tr>
          </thead>
          <?php 
            $j_multi_premium = 0; $j_multi_reguler = 0; $j_kuli_premium = 0; $j_kuli_reguler = 0; $total = 0; $tpiutang = 0;
            if (sizeof($data_expo)>0) {
              for ($i=0;$i<sizeof($data_expo);$i++) {
                $id = $data_expo[$i]["id"];
                $tanggal = $data_expo[$i]["tanggal"];
                $nama = $data_expo[$i]["nama"];
                $usaha = $data_expo[$i]["usaha"];
                $email = $data_expo[$i]["email"];
                $hp = $data_expo[$i]["no_hp"];
                $minat = $data_expo[$i]["minat"];
                $premium = $data_expo[$i]["premium"];
                $reguler = $data_expo[$i]["reguler"];
                $kuliner = $data_expo[$i]["kuliner"];
                $kuliner2 = $data_expo[$i]["kuliner2"];
                $catatan = $data_expo[$i]["catatan"];
                $status = $data_expo[$i]["status_pesan"];
                $nilai = $data_expo[$i]["nilai_bayar"];
                $bayar1 = $data_expo[$i]["bayar1"];
                
                $sminat = "";
                if ($minat!="") {
                  $xminat = explode(",",$minat);
                  for ($j=0;$j<sizeof($xminat);$j++) {
                    if (trim($xminat[$j])!="") {
                      if ($xminat[$j]=="1") { $sminat .= " Multi Premium,"; }
                      if ($xminat[$j]=="2") { $sminat .= " Multi Reguler,"; }
                      if ($xminat[$j]=="3") { $sminat .= " Kuliner Premium,"; }
                      if ($xminat[$j]=="4") { $sminat .= " Kuliner Reguler,"; }
                    }
                  }
                }
                if ($sminat!="") { $sminat = substr($sminat,0,-1); }
                
                $nobooth = "";
                if ($premium!="") { $nobooth .= "Multi Premium : ".$premium; $xpremium = explode(",",$premium); $j_multi_premium += sizeof($xpremium)-1; }
                if ($reguler!="") { $nobooth .= "Multi Reguler : ".$reguler; $xreguler = explode(",",$reguler); $j_multi_reguler += sizeof($xreguler)-1;}
                if ($kuliner!="") { $nobooth .= "Kuliner Premium : ".$kuliner; $xkuliner = explode(",",$kuliner); $j_kuli_premium += sizeof($xkuliner)-1;}
                if ($kuliner2!="") { $nobooth .= "Kuliner Reguler : ".$kuliner2; $xkuliner2 = explode(",",$kuliner2); $j_kuli_reguler += sizeof($xkuliner2)-1;}
                
                $xnobooth = "";
                if (sizeof($data_expo2)>0) {
                  $multi1 = ""; $multi2 = ""; $kuli1 = ""; $kuli2 = ""; $sponsor = ""; $nilai = 0;
                  for ($j=0;$j<sizeof($data_expo2);$j++) {
                    $bid = $data_expo2[$j]["id"];
                    $bbookedby = $data_expo2[$j]["booked_by"];
                    $bnobooth = $data_expo2[$j]["no_booth"];
                    $btipe = $data_expo2[$j]["tipe_booth"];
                    $bnilai = $data_expo2[$j]["nilai_booth"];
                    
                    if ($id==$bbookedby) {
                      if ($btipe=="1") { $multi1 .= $bnobooth . ","; }
                      if ($btipe=="2") { $multi2 .= $bnobooth . ","; }
                      if ($btipe=="3") { $kuli2 .= $bnobooth . ","; }
                      if ($btipe=="8") { $kuli1 .= $bnobooth . ","; }
                      if ($btipe=="4") { $sponsor .= $bnobooth . ","; }
                      
                      $nilai += $bnilai;
                    }
                  }
                  if ($multi1!="") { $xnobooth .= "Premium : " . $multi1 . "<br>"; }
                  if ($multi2!="") { $xnobooth .= "Reguler : " . $multi2 . "<br>"; }
                  if ($kuli1!="") { $xnobooth .= "Premium : " . $kuli1 . "<br>"; }
                  if ($kuli2!="") { $xnobooth .= "Reguler : " . $kuli2 . "<br>"; }
                  if ($sponsor!="") { $xnobooth .= "Sponsor : " . $sponsor . "<br>"; }
                }			
                
                $xstatus = $status;
                if ($status=="1") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-default\">Waiting List</a>"; }
                if ($status=="2") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-info\">Pemesanan Booth</a>"; }
                if ($status=="3") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Pembayaran Uang Muka</a>"; }
                if ($status=="4") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Pembayaran Lunas</a>"; }
                if ($status=="5") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-warning\">Batal</a>"; }
                if ($status=="6") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Sponsorship</a>"; }
                
                $xnilai = 0; $xpiutang = 0;
                if ($status=="2") { $xpiutang = $nilai; }
                if ($status=="3") { $xnilai = $bayar1; $xpiutang = $nilai - $bayar1; }
                if ($status=="4") { $xnilai = $nilai; }
                $total += $xnilai; $tpiutang += $xpiutang;      
          ?>
          <tbody>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $tanggal; ?></td>
              <td><?= $nama; ?><br><?= $usaha; ?><br><?= $email; ?><br><?= $hp; ?></td>
              <td><?= $xnobooth?></td>
              <td calss="text-right"><?= number_format($xnilai)?></td>
              <td><?= $xstatus?></td>
              <td><a href="<?= base_url("sales/expofu/".$id); ?>" class="btn btn-block btn-primary">Follow Up</a></td>
            </tr>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="4"></td>
              <td class="text-right"><?= number_format($total); ?></td>
              <td colspan="2">Piutang : <?= number_format($tpiutang); ?></td>
            </tr>
          </tfoot>
          <?php
              } 
            } 
          ?>
        </table>
      </div>
    </div>
  </div>
</div>