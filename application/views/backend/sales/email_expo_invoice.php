<!doctype html>
<html>
<head>
 <meta charset="utf-8">
 <title>Invoice Expo Pesta Wirausaha 2019</title>
 <style>
 .invoice-box{
	max-width:800px;
	margin:auto;
	padding:30px;
	border:1px solid #eee;
	box-shadow:0 0 10px rgba(0, 0, 0, .15);
	font-size:16px;
	line-height:24px;
	font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
	color:#555;
 }
 .invoice-box table{
	width:100%;
	line-height:inherit;
	text-align:left;
 }
 .invoice-box table td{
	padding:5px;
	vertical-align:top;
 }
 .invoice-box table tr td:nth-child(2){
	text-align:right;
 }
 .invoice-box table tr.top table td{
	padding-bottom:20px;
 }
 .invoice-box table tr.top table td.title{
	font-size:45px;
	line-height:45px;
	color:#333;
 }
 .invoice-box table tr.information table td{
	padding-bottom:40px;
 }
 .invoice-box table tr.heading td{
	background:#eee;
	border-bottom:1px solid #ddd;
	font-weight:bold;
 }
 .invoice-box table tr.details td{
	padding-bottom:20px;
 }
 .invoice-box table tr.item td{
	border-bottom:1px solid #eee;
 }
 .invoice-box table tr.item.last td{
	border-bottom:none;
 }
 .invoice-box table tr.total td:nth-child(2){
	border-top:2px solid #eee;
	font-weight:bold;
 }
 @media only screen and (max-width: 600px) {
	.invoice-box table tr.top table td{
	width:100%;
	display:block;
	text-align:center;
 }
 .invoice-box table tr.information table td{
	width:100%;
	display:block;
	text-align:center;
 }
 
</style>
</head>

<body>
<?php
	$invoice = ""; $tanggal = ""; $expdate = ""; $nama = ""; $email = ""; $hp = ""; $bayar1 = 0; $bayar2 = 0;
	if (sizeof($data_expo)>0) {
		$invoice = $data_expo[0]["invoice"];
		$tanggal = $data_expo[0]["stanggal"];
		$expdate = $data_expo[0]["expdate"];
		$nama = $data_expo[0]["nama"];
		$email = $data_expo[0]["email"];
		$hp = $data_expo[0]["no_hp"];
		$bayar1 = $data_expo[0]["bayar1"];
		$bayar2 = $data_expo[0]["bayar2"];
	}
?>
 <div class="invoice-box">
 <table cellpadding="0" cellspacing="0">
 <tr class="top">
 <td colspan="2">
 <table>
 <tr>
 <td class="title"><img src="http://www.pestawirausaha.com/themes/backend/img/logo.png" style="width:100%; max-width:200px;"></td>
 <td> Invoice: # <?php echo $invoice; ?><br>Tanggal: <?php echo $tanggal; ?><br> </td>
 </tr>
 </table>
 </td>
 </tr>
 <tr class="information">
 <td colspan="2">
 <table>
 <tr>
 <td><?php echo $nama . "<br>" . $email . "<br>" . $hp; ?></td>
 <td>Panitia Pesta Wirausaha 2019<br>Divisi Expo<br>expo@pestawirausaha.com</td>
 </tr>
 </table>
 </td>
 </tr>
 <tr class="heading">
 <td> Item </td>
 <td> Harga </td> 
 </tr>
 <?php 
	$total = 0;
	if (sizeof($data_booth)>0) {
		for ($i=0;$i<sizeof($data_booth);$i++) {
			$nama = $data_booth[$i]["nama_booth"];
			$no = $data_booth[$i]["no_booth"];
			$harga = $data_booth[$i]["nilai_booth"];
			$total += $harga;
			
			echo "<tr class=\"item\"><td>".$nama." #".$no."</td><td>Rp. ".number_format($harga)."</td></tr>";
		}
		echo "<tr class=\"total\"><td></td><td>Rp. ".number_format($total)."</td></tr>";
	}
?>
<tr>
<td colspan="2">Silahkan lakukan pembayaran melalui transfer ke rekening berikut:<br><br>
 Jumlah Transfer : Rp. <?php echo number_format($total); ?><br><br>
 Untuk pembayaran termin : <br>
 - Termin 1 : Rp. <?php echo number_format($bayar1); ?> (maksimal 24 jam setelah pemesanan)<br>
 - Termin 2 : Rp. <?php echo number_format($bayar2); ?> (maksimal 30 hari setelah pemesanan)<br><br>
 
 Bank : Bank Syariah Mandiri <br>
 No. Rekening : 711-004-1009 <br>
 A.n : Komunitas Tangan Di Atas<br><br>
 *Cantumkan no.invoice <?php echo $invoice; ?>, sebagai deskipsi transfer Anda.<br>
 *Transaksi ini akan expired dalam 24 Jam pada tanggal <?php echo $expdate; ?> segera lakukan pembayaran tersebut dan lakukan konfirmasi pembayaran
</td>
</tr>
 </table>
 </div>
</body>
</html>