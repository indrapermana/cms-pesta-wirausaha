<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekap Tiket Daerah TDA</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Distribusi Tiket Daerah TDA
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Distribusi Tiket Daerah TDA</b></h4>
      <div class="filtter-right">
        <form action="<?php echo base_url("sales/distribusitiket"); ?>" method="post">
          <select name="urutan" onchange="this.form.submit();" class="form-control ">
            <option value="1"<?php if ($urutan=="1") { echo " selected"; } ?>>Jumlah Anggota</option>
            <option value="2"<?php if ($urutan=="2") { echo " selected"; } ?>>Tiket Online</option>
            <option value="3"<?php if ($urutan=="3") { echo " selected"; } ?>>Tiket Group</option>
            <option value="4"<?php if ($urutan=="4") { echo " selected"; } ?>>Total Tiket</option>
          </select>
        </form>
      </div>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="20%">Daerah</th>
              <th width="20%">Wilayah</th>
              <th width="10%">Jml Member</th>
              <th width="10%">Tiket Online</th>
              <th width="10%">Tiket Group</th>
              <th width="10%">Total Peserta</th>
            </tr>
          </thead>
          <tbody>
            <?php
            $arr_daerah = array();
            if (sizeof($data_daerah)>0) {
              for ($i=0;$i<sizeof($data_daerah);$i++) {
                $ddaerah = $data_daerah[$i]["daerah"];
                $djumlah = $data_daerah[$i]["jml_tiket"];
                $arr_daerah[$ddaerah] = $djumlah;
              }
            }

            $arr_group = array();
            if (sizeof($data_group)>0) {
              for ($i=0;$i<sizeof($data_group);$i++) {
                $gaffid = $data_group[$i]["affid"];
                $gjumlah = $data_group[$i]["jml_tiket"];
                $arr_group[$gaffid] = $gjumlah;
              }
            }
            
            $arr_wilayah = array(); $arr_urut = array();
              $tmember = 0; $ttekol = 0; $tgroup = 0;
              if (sizeof($data_wilayah)>0) {
                for ($i=0;$i<sizeof($data_wilayah);$i++) {
                  $wid = $data_wilayah[$i]["id"];
                  $wdaerah = $data_wilayah[$i]["daerah"];
                  $wwilayah = $data_wilayah[$i]["wilayah"];
                  $wjml = $data_wilayah[$i]["jml_anggota"];
                  $wtekol = $data_wilayah[$i]["tiket_online"];
                  $waffid = $data_wilayah[$i]["tiket_group_affid"];

                  $tmember += $wjml;

                  $jtekol = 0; 
                  if (isset($arr_daerah[$wtekol])) { $jtekol = $arr_daerah[$wtekol]; }
                  $ttekol += $jtekol;

                  $jaffid = 0; 
                  if (isset($arr_group[$waffid])) { $jaffid = $arr_group[$waffid]; }
                  $tgroup += $jaffid;
                  $total = $jtekol + $jaffid;

                  $arr_wilayah[$wid] = array("daerah"=>$wdaerah,"wilayah"=>$wwilayah,"jml"=>$wjml,"tekol"=>$jtekol,"affid"=>$jaffid,"total"=>$total);

                  if ($urutan=="1") { $arr_urut[$wid] = $wjml; }
                  if ($urutan=="2") { $arr_urut[$wid] = $jtekol; }
                  if ($urutan=="3") { $arr_urut[$wid] = $jaffid; }
                  if ($urutan=="4") { $arr_urut[$wid] = $total; }

                  //echo "<tr><td>".($i+1)."</td><td>".$wdaerah."</td><td>".$wwilayah."</td><td align=center>".number_format($wjml)."</td><td align=center>".$jtekol."</td><td align=center>".$jaffid."</td><td align=center>".$total."</td></tr>";
                }
                //echo "<tr><td colspan=3></td><td align=center>".number_format($tmember)."</td><td align=center>".number_format($ttekol)."</td><td align=center>".number_format($tgroup)."</td><td align=center>".number_format($ttekol+$tgroup)."</td></tr>";
              }

              arsort($arr_urut); $i = 0;
              foreach ($arr_urut as $key => $val) {
            ?>
            <tr>
              <td><?= $i+1; ?></td>
              <td><?= $arr_wilayah[$key]["daerah"]; ?></td>
              <td><?= $arr_wilayah[$key]["wilayah"]; ?></td>
              <td class="text-center"><?= number_format($arr_wilayah[$key]["jml"]); ?></td>
              <td class="text-center"><?= $arr_wilayah[$key]["tekol"]; ?></td>
              <td class="text-center"><?= $arr_wilayah[$key]["affid"]; ?></td>
              <td class="text-center"><?= $arr_wilayah[$key]["total"]; ?></td>
            </tr>
            <?php
                $i++;
              }
            ?>
          </tbody>
          <tfoot>
            <tr>
              <td colspan="3" class="text-center">Total</td>
              <td class="text-center"><?= number_format($tmember); ?></td>
              <td class="text-center"><?= number_format($ttekol); ?></td>
              <td class="text-center"><?= number_format($tgroup); ?></td>
              <td class="text-center"><?= number_format($ttekol+$tgroup); ?></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>