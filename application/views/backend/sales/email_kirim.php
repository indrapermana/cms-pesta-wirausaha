<div class="content-wrapper">
	<section class="content-header">
		<h1> Kirim Email <small>Pengiriman Email</small></h1>
		<ol class="breadcrumb">
			<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
			<li class="active">Kirim Email</li>
		</ol>
	</section>
	
		<section class="content">
		<div class="row">
        <div class="col-xs-12">

          <div class="box box-info">
<?php if ($submit==0) { ?>
            <div class="box-header with-border">
              <h3 class="box-title">Pengiriman Email</h3>
            </div>
            <form class="form-horizontal" method="post" action="<?php echo site_url("email/kirim"); ?>">
              <div class="box-body">
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Target Email</label>
                  <div class="col-sm-10">
                    <select name="target" class="form-control">
						<option value="1">Pembeli tiket yang sudah bayar</option>
						<option value="0">Pembeli tiket yang belum bayar</option>
						<option value="2">Test Email ke abrahamsyah@gmail.com, novrand@gmail.com</option>
					</select>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Tujuan</label>
                  <div class="col-sm-10">
                    <input type="text" class="form-control" name="tujuan" value="">
                  </div>
                </div>			  
                <div class="form-group">
                  <label for="input" class="col-sm-2 control-label">Isi Email</label>
                  <div class="col-sm-10">
                    <textarea class="form-control" name="isi" rows="10" cols="60"></textarea>
					<br>Gunakan kode {NAMA} dalam isi email
                  </div>
                </div>
              </div>
              <div class="box-footer">
                <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">Kirim Email</button>
              </div>
            </form>
<?php } else { ?>
            <div class="box-header with-border">
              <h3 class="box-title">Pengiriman Email target</h3>
            </div>
            <div class="box-body table-responsive no-padding">
              <table class="table table-hover">
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Status</th>
                </tr>
<?php 
	if (sizeof($arr_tiket)>0) {
		for ($i=0;$i<sizeof($arr_tiket);$i++) {
			$nama = $arr_tiket[$i]["nama"];
			$email = $arr_tiket[$i]["email"];
			
			echo "<tr><td>" . ($i+1) . "</td><td>" . $nama . "</td><td>" . $email . "</td><td>xx</td></tr>";
		}
	}
?>				
				
				</table>
			</div> 

<?php } ?>			
          </div>
		  
        </div>
		
		</div>
	</section>
	
</div>	 