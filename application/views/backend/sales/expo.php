<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pendapatan Pesta Wirausaha 2019 </h4>
    <p class="text-muted page-title-alt">Expo</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <?php if($param!=""){ ?>
      <li>
        <a href="<?= base_url('sales/expo'); ?>">Expo</a>
      </li>
      <li class="active">
        <?= ucfirst($param); ?>
      </li>
      <?php }else{ ?>
      <li class="active">
        Expo
      </li>
      <?php } ?>
    </ol>
  </div>
</div>

<?php
  if ($param=="") {
    $t_pesan   = 0; $t_total = 0; $t_nilai = 0; $t_booking = 0; $t_piutang = 0;
    $t_premium = 0; $t_reguler = 0; $t_kuliner1 = 0; $t_kuliner2 = 0;  
    $j_premium = 0; $j_reguler = 0; $j_kuliner1 = 0; $j_kuliner2 = 0; 
    
    $x_infobooth = "";
    if (sizeof($data_booth)>0) {
      for ($i=0;$i<sizeof($data_booth);$i++) {
        $tipeid = $data_booth[$i]["tipe_booth"];
        $tipe   = $data_booth[$i]["nama_booth"];
        $nomor  = $data_booth[$i]["no_booth"];
        $status = $data_booth[$i]["status_booth"];
        $pesan  = $data_booth[$i]["status_pesan"];
        $nilai  = $data_booth[$i]["nilai_booth"];
        $nama   = $data_booth[$i]["booked_nama"];
        $bayar1 = $data_booth[$i]["bayar1"];
        
        if ($tipeid=="1") {
          if ($status!="0") { $t_premium++; $t_pesan++; $t_nilai += $nilai; }
          $j_premium++;
        } else if ($tipeid=="2") {
          if ($status!="0") { $t_reguler++; $t_pesan++; $t_nilai += $nilai; }
          $j_reguler++;
        } else if ($tipeid=="3") { 
          if ($status!="0") { $t_kuliner2++; $t_pesan++; $t_nilai += $nilai; }
          $j_kuliner2++;
        } else if ($tipeid=="8") { 
          if ($status!="0") { $t_kuliner1++; $t_pesan++; $t_nilai += $nilai; }
          $j_kuliner1++;
        }
        $t_total++;
        
        $bstatus = "";
        if ($status=="0") { $bstatus = "<button type=\"button\" class=\"btn btn-block btn-default\">Kosong</button>"; } 
        if ($status=="1") { 
          if ($pesan=="2") {
            $bstatus = "<button type=\"button\" class=\"btn btn-block btn-warning\">".$nama."</button>"; 
            $t_piutang += $nilai; $t_booking++;
          } else if ($pesan=="3") {
            $bstatus = "<button type=\"button\" class=\"btn btn-block btn-info\">".$nama."</button>"; 
            $t_piutang += $nilai - $bayar1;
          } else {
            $bstatus = "<button type=\"button\" class=\"btn btn-block btn-success\">".$nama."</button>"; 
          }
        } 
        
        $x_infobooth .= "<tr><td>".($i+1)."</td><td>".$tipe."</td><td>".$nomor."</td><td>".$bstatus."</td></tr>";
      }
    }	
?>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_pesan . "/". $t_total . " (". number_format($t_pesan/$t_total*100,2)." %)"; ?>
          </b>
        </h3>
        <p class="text-muted">Rekap Penjualan Expo</p>
      </div>
      <!-- <div class="clearfix"></div> -->
      <a href="<?php echo base_url("sales/expo/harga"); ?>" class="small-box-footer bg-icon-info text-info">Ubah Harga <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_booking; ?>
          </b>
        </h3>
        <p class="text-muted">Booking Stand Expo</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-info text-info">Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="fa fa-soccer-ball-o text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark" style="font-size: 22px;">
          <b class="counter">
            <?php echo "Rp ".number_format($t_nilai); ?>
          </b>
        </h3>
        <p class="text-muted">Total Nilai Penjualan</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="fa fa-soccer-ball-o text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_piutang); ?>
          </b>
        </h3>
        <p class="text-muted">Total Piutang Penjualan</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_premium . "/".$j_premium; ?>
          </b>
        </h3>
        <p class="text-muted">Multi Premium</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?php echo base_url("sales/expo/premium"); ?>" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_reguler . "/".$j_reguler; ?>
          </b>
        </h3>
        <p class="text-muted">Multi Reguler</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?php echo base_url("sales/expo/reguler"); ?>" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa  fa-trophy text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_kuliner1 . "/" . $j_kuliner1; ?>
          </b>
        </h3>
        <p class="text-muted">Kuliner Premium</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?php echo base_url("sales/expo/kuliner1"); ?>" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa  fa-trophy text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_kuliner2 . "/" . $j_kuliner2; ?>
          </b>
        </h3>
        <p class="text-muted">Kuliner Reguler</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?php echo base_url("sales/expo/kuliner2"); ?>" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Expo</b></h4>
      <hr>
      <?php 
        $arr_expo1 = array();
        if (sizeof($data_expo1)>0) {
          for ($i=0;$i<sizeof($data_expo1);$i++) {
            $xid = $data_expo1[$i]["id"];
            $xstatus = $data_expo1[$i]["status_pesan"];
            $arr_expo1[$xid] = $xstatus;
          }
        }
        
        $lunas = array(); $dp = array(); $belum = array(); $kosong = array();
        $arr_expo2 = array(); $arr_nilai = array();
        if (sizeof($data_expo2)>0) {
          for ($i=0;$i<sizeof($data_expo2);$i++) {
            $xno = $data_expo2[$i]["no_booth"];
            $xtipe = $data_expo2[$i]["tipe_booth"];
            $xbooked = $data_expo2[$i]["booked_by"];
            $xstatus = $data_expo2[$i]["status_booth"];
            $xnilai = $data_expo2[$i]["nilai_booth"];
            
            if ($xno>9) {
              if ($xstatus=="0") {
                if (!isset($arr_expo2[$xtipe][$xstatus])) { $arr_expo2[$xtipe][$xstatus] = 0; }
                $arr_expo2[$xtipe][$xstatus]++;
                
                if (!isset($arr_nilai[$xtipe][$xstatus])) { $arr_nilai[$xtipe][$xstatus] = 0; }
                $arr_nilai[$xtipe][$xstatus] += $xnilai;
              } else {
                $bstatus = "2";
                if (isset($arr_expo1[$xbooked])) { $bstatus = $arr_expo1[$xbooked]; }
                if (!isset($arr_expo2[$xtipe][$bstatus])) { $arr_expo2[$xtipe][$bstatus] = 0; }
                $arr_expo2[$xtipe][$bstatus]++;
                
                if (!isset($arr_nilai[$xtipe][$bstatus])) { $arr_nilai[$xtipe][$bstatus] = 0; }
                $arr_nilai[$xtipe][$bstatus] += $xnilai;
                
              }
            }
          }
        }
        
        function dox($arrdata,$tipe,$status) {
          if (isset($arrdata[$tipe][$status])) { echo $arrdata[$tipe][$status]; } else { echo "0"; }
        }
        
        function dot($arrdata,$status) {
          $xdot = 0;
          if (isset($arrdata["1"][$status])) { $xdot += $arrdata["1"][$status]; } 
          if (isset($arrdata["2"][$status])) { $xdot += $arrdata["2"][$status]; } 
          if (isset($arrdata["3"][$status])) { $xdot += $arrdata["3"][$status]; } 
          if (isset($arrdata["4"][$status])) { $xdot += $arrdata["4"][$status]; } 
          if (isset($arrdata["8"][$status])) { $xdot += $arrdata["8"][$status]; } 
          echo $xdot;
        }
      ?>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tipe Booth</th>
              <th>Slot</th>
              <th>Lunas</th>
              <th>DP</th>
              <th>Belum Bayar</th>
              <th>Sponsor</th>
              <th>Kosong</th>
            </tr>
            <tr>
              <td>1</td>
              <td>Multi Premium</td>
              <td>24</td>
              <td><?php dox($arr_expo2,"1","4"); ?></td>
              <td><?php dox($arr_expo2,"1","3"); ?></td>
              <td><?php dox($arr_expo2,"1","2"); ?></td>
              <td><?php dox($arr_expo2,"1","6"); ?></td>
              <td><?php dox($arr_expo2,"1","0"); ?></td>
            </tr>				
            <tr>
              <td>2</td>
              <td>Multi Reguler</td>
              <td>139</td>
              <td><?php dox($arr_expo2,"2","4"); ?></td>
              <td><?php dox($arr_expo2,"2","3"); ?></td>
              <td><?php dox($arr_expo2,"2","2"); ?></td>
              <td><?php dox($arr_expo2,"2","6"); ?></td>
              <td><?php dox($arr_expo2,"2","0"); ?></td>
            </tr>				
            <tr>
              <td>3</td>
              <td>Kuliner Premium</td>
              <td>4</td>
              <td><?php dox($arr_expo2,"8","4"); ?></td>
              <td><?php dox($arr_expo2,"8","3"); ?></td>
              <td><?php dox($arr_expo2,"8","2"); ?></td>
              <td><?php dox($arr_expo2,"8","6"); ?></td>
              <td><?php dox($arr_expo2,"8","0"); ?></td>
            </tr>				
            <tr>
              <td>4</td>
              <td>Kuliner Reguler</td>
              <td>32</td>
              <td><?php dox($arr_expo2,"3","4"); ?></td>
              <td><?php dox($arr_expo2,"3","3"); ?></td>
              <td><?php dox($arr_expo2,"3","2"); ?></td>
              <td><?php dox($arr_expo2,"3","6"); ?></td>
              <td><?php dox($arr_expo2,"3","0"); ?></td>
            </tr>
            <tr>
              <td>5</td>
              <td>Sponsor</td>
              <td>40</td>
              <td><?php dox($arr_expo2,"4","4"); ?></td>
              <td><?php dox($arr_expo2,"4","3"); ?></td>
              <td><?php dox($arr_expo2,"4","2"); ?></td>
              <td><?php dox($arr_expo2,"4","6"); ?></td>
              <td><?php dox($arr_expo2,"4","0"); ?></td>
            </tr>
            <tr>
              <td colspan=2 align=right>Total</td>
              <td>239</td>
              <td><?php dot($arr_expo2,"4"); ?></td>
              <td><?php dot($arr_expo2,"3"); ?></td>
              <td><?php dot($arr_expo2,"2"); ?></td>
              <td><?php dot($arr_expo2,"6"); ?></td>
              <td><?php dot($arr_expo2,"0"); ?></td>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>Pendaftaran Expo</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Email</th>
              <th>HP</th>
              <th>Status</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            if (sizeof($data_expo)>0) {
              for ($i=0;$i<sizeof($data_expo);$i++) {
                $tanggal = $data_expo[$i]["tanggal"];
                $nama = $data_expo[$i]["nama"];
                $usaha = $data_expo[$i]["usaha"];
                $email = $data_expo[$i]["email"];
                $hp = $data_expo[$i]["no_hp"];
                $status = $data_expo[$i]["status_pesan"];
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $tanggal; ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $email; ?></td>
              <td><?= $hp; ?></td>
              <td><?= $status; ?></td>
            </tr>
          <?php
              }
            }
          ?>				
          </tbody>
        </table>
      </div>
    </div>
  </div>

  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>Status Booth</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tipe Booth</th>
              <th>Nomor</th>
              <th>Status</th>
            </tr>	
          </thead>
          <tbody>
          <?= $x_infobooth; ?>	
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php } else if ($param=="harga") { ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Status Booth</b></h4>
      <hr>
      <div class="table-responsive">
        <form method="post" action="<?php echo site_url("sales/expo/harga"); ?>">
          <table class="table table-hover">
            <thead>
              <tr>
                <th>No</th>
                <th>Tipe Booth</th>
                <th>Harga</th>
              </tr>
            </thead>
            <tbody>
            <?php 
              if (sizeof($data_booth)>0) {
                for ($i=0;$i<sizeof($data_booth);$i++) {
                  $b_id = $data_booth[$i]["id"];
                  $b_nama = $data_booth[$i]["nama"];
                  $b_harga = $data_booth[$i]["harga"];
            ?>
              <tr>
                <td><?= ($i+1); ?></td>
                <td><?= $b_nama; ?></td>
                <td>
                  <input type="text" class="form-control" name="harga[]" value="<?= $b_harga; ?>">
                  <input type="hidden" class="form-control" name="bid[]" value="<?= $b_id; ?>">
                </td>
              </tr>
            <?php
                }
              }
            ?>
            </tbody>
            <tfoot>
              <tr>
                <td colspan=2></td>
                <td><input type="submit" class="btn btn-info pull-right" name="tombol1" value="Ubah"></td>
              </tr>
            </tfoot>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>
<?php }else{ ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Peta Expo <?= $param; ?></b></h4>
      <hr>
      <div class="text-center">
        <img src="<?php echo base_url("assets/backend/images/map_expo.jpg"); ?>">
      </div>
    </div>
  </div>
</div>
<?php } ?>