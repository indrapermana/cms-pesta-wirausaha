<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pendaftaran Tiket Daerah </h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Tiket Group
      </li>
    </ol>
  </div>
</div>

<?php 
if ($site_form=="list") {

  $t_tiket = 0; $t_terjual = 0; $t_nilai = 0; $t_bayar = 0; $t_piutang = 0; $t_html = "";
  if (sizeof($data_tiket)>0) {
    for ($i=0;$i<sizeof($data_tiket);$i++) {
      $id = $data_tiket[$i]["id"];
      $affid = $data_tiket[$i]["affid"];
      $tanggal = $data_tiket[$i]["tanggal"];
      $nama = $data_tiket[$i]["nama"];
      $kontak = $data_tiket[$i]["kontak"];
      $email = $data_tiket[$i]["email"];
      $hp = $data_tiket[$i]["no_hp"];
      $nilai = $data_tiket[$i]["nilai"];
      $nilai_lunas = $data_tiket[$i]["nilai_lunas"];
      $status = $data_tiket[$i]["status_bayar"];
      $jmltiket = $data_tiket[$i]["jml_tiket"];
      $jmlterjual = $data_tiket[$i]["jml_terjual"];
      $piutang = $nilai - $nilai_lunas;
      
      $t_tiket += $jmltiket;
      $t_terjual += $jmlterjual;
      $t_nilai += $nilai;
      $t_bayar += $nilai_lunas;
      $t_piutang += $piutang;
      //$xnama = ""; if ($nama!="") { $xnama = "<br>" . $daerah; }
      
      $xstatus = "";
      if ($status=="1") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-default\">Pemesanan</a>"; }
      if ($status=="3") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-info\">Pembayaran Sebagian</a>"; }
      if ($status=="4") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Pembayaran Lunas</a>"; }
      
      $t_html .= "<tr><td>".($i+1)."</td><td>".$nama." [".$affid."]</td><td>".$kontak." / ".$hp." / ".$email."</td><td>".$jmltiket."</td><td>".$jmlterjual."</td><td align=right>".number_format($nilai)."</td><td align=right>".number_format($nilai_lunas)."</td><td align=right>".number_format($piutang)."</td><td>".$xstatus."</td>";
      $t_html .= "<td><a href=\"".site_url("sales/tiketgroup/".$id)."\" class=\"btn btn-block btn-primary\">Rincian</a> </td>";  
      $t_html .= "</tr>";
    }
    $t_html .= "<tr><td colspan=3></td><td>".$t_tiket."</td><td>".$t_terjual."</td><td align=right>".number_format($t_nilai)."</td><td align=right>".number_format($t_bayar)."</td><td align=right>".number_format($t_piutang)."</td><td colspan=2></td></tr>";
    
  }
?>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?= $t_tiket; ?>
          </b>
        </h3>
        <p class="text-muted">Target Penjualan Tiket</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-info text-info">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="ti-ticket text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp. " . number_format($t_nilai); ?>
          </b>
        </h3>
        <p class="text-muted">Nilai Penjualan Tiket</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-purple pull-left">
        <i class="fa fa-soccer-ball-o text-purple"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_bayar); ?>
          </b>
        </h3>
        <p class="text-muted" style="font-size:13px;">Total Pembayaran Lunas</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-purple text-purple">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_piutang); ?>
          </b>
        </h3>
        <p class="text-muted">Total Piutang</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>TDA Daerah</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="20%">Wilayah</th>
              <th width="20%">Kontak</th>
              <th width="10%">Jml Tiket</th>
              <th width="10%">Jml Terjual</th>
              <th width="10%">Nilai</th>
              <th width="10%">Pembayaran</th>
              <th width="10%">Piutang</th>
              <th width="10%">Status</th>
              <th width="10%">&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php echo $t_html; ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php }else{ ?>
<div class="row">
  <!-- Data Wilayah -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>Data Wilayah</b></h4>
      <hr>
      <?php
        $nama = ""; $affid = ""; $kontak = ""; $hp = ""; $email = "";
        $jmltiket = 0; $jmlterjual = 0; $nilai = 0; $bayar = 0; $status = 0; $xstatus = "";
        if (sizeof($data_tiket)>0) {
          $nama = $data_tiket[0]["nama"];
          $affid = $data_tiket[0]["affid"];
          $kontak = $data_tiket[0]["kontak"];
          $hp = $data_tiket[0]["no_hp"];
          $email = $data_tiket[0]["email"];
          $status = $data_tiket[0]["status_bayar"];
          $nilai = $data_tiket[0]["nilai"];
          $bayar = $data_tiket[0]["nilai_lunas"];
          $jmltiket = $data_tiket[0]["jml_tiket"];
          $jmlterjual = $data_tiket[0]["jml_terjual"]; 
          
          if ($status=="1") { $xstatus = "Pemesanan"; }
          if ($status=="3") { $xstatus = "Pembayaran Sebagian"; }
          if ($status=="4") { $xstatus = "Pembayaran Lunas"; }
          
        }
      ?>
      <dl>
        <dt>Nama Wilayah</dt>
        <dd><?php echo $nama; ?></dd>
        <dt>Kontak Person</dt>
        <dd><?php echo $kontak . " / " . $hp . " / " . $email; ?></dd>
        <dt>Status Pemesanan</dt>
        <dd><?php echo $xstatus; ?></dd>
        <dt>Jumlah Tiket</dt>
        <dd><?php echo number_format($jmltiket); ?></dd>				
        <dt>Jumlah Tiket Terjual</dt>
        <dd><?php echo number_format($jmlterjual); ?></dd>				
        <dt>Nilai Total Tiket</dt>
        <dd><?php echo "Rp. " . number_format($nilai); ?></dd>				
        <dt>Nilai Pembayaran</dt>
        <dd><?php echo "Rp. " . number_format($bayar); ?></dd>				
        <dt>Nilai Piutang</dt>
        <dd><?php echo "Rp. " . number_format($nilai-$bayar); ?></dd>				
      </dl>
    </div>
  </div>

  <!-- Detail Transaksi -->
  <div class="col-sm-12 col-md-6 col-lg-6">
    <div class="card-box">
      <h4 class="header-title"><b>Detail Transaksi</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Tanggal</th>
              <th width="20%">Tiket</th>
              <th width="20%">Nilai Pembayaran</th>
              <th width="20%">Catatan</th>
              <th width="10%">&nbsp;</th>
            </tr>
          </thead>
          
          <?php
            $ttiket = 0; $tnilai = 0;
            if (sizeof($data_tiket_trans)>0) {
          ?>
          <tbody>
          <?php
              for ($i=0;$i<sizeof($data_tiket_trans);$i++) {
                $dt_id = $data_tiket_trans[$i]["id"];
                $dt_tanggal = $data_tiket_trans[$i]["tanggal"];
                $dt_tiket = $data_tiket_trans[$i]["jml_tiket"];
                $dt_bayar = $data_tiket_trans[$i]["nilai_bayar"];
                $dt_catatan = $data_tiket_trans[$i]["catatan"];
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $dt_tanggal; ?></td>
              <td><?= $dt_tiket; ?></td>
              <td><?= number_format($dt_bayar); ?></td>
              <td><?= $dt_catatan; ?></td>
              <td></td>
            </tr>
          <?php
                $ttiket += $dt_tiket;
                $tnilai += $dt_bayar;
              }
              echo "<tr><td></td><td></td><td>".$ttiket."</td><td>".$tnilai."</td><td></td></tr>";
          ?>
          </tbody>
          <?php } ?>

        </table>
      </div>
    </div>

    <div class="card-box">
      <h4 class="header-title"><b>Form Transaksi</b></h4>
      <hr>
      <div class="row">
        <div class="col-sm-12">
          <form class="form-horizontal" method="post" action="<?= base_url("sales/tiketgroup/".$tgid); ?>" role="form">                                    
            <div class="form-group">
              <label for="input1">Jumlah Tiket </label>
              <input type="text" name="tiket" class="form-control" placeholder="Jumlah Tiket" value="">
            </div>
            <div class="form-group">
              <label for="input1">Nilai Pembayaran </label> 
              <input type="text" name="bayar" class="form-control" placeholder="Nilai Bayar" value="">
            </div>
            <div class="form-group">
              <label for="input1">Catatan</label>
              <textarea name="catatan" class="form-control" cols="60" rows="4"></textarea>
            </div>
            <div class="form-group">
              <a href="<?php echo base_url("sales/tiketgroup"); ?>" class="btn btn-default">Kembali</a>
              <button type="submit" class="btn btn-primary">Ubah</button>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>
<?php } ?>
