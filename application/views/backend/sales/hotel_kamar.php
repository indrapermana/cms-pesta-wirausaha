<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pemesanan Kamar Hotel</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Pemesanan Kamar Hotel
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Data Pemesanan Hotel</b></h4>
      <hr>

      <?php 
        $tanggal = ""; $jenis_kamar = ""; $kode_booking = ""; $cekin = ""; $cekout = ""; $nama = ""; $email = ""; $nohp = ""; $jml_kamar = "";
        if (sizeof($data_hotel)>0) {
          $tanggal = $data_hotel[0]["tanggal"];
          $jenis_kamar = $data_hotel[0]["room_type"];
          $kode_booking = $data_hotel[0]["kode_booking"];
          $cekin = $data_hotel[0]["cekin"];
          $cekout = $data_hotel[0]["cekout"];
          $nama = $data_hotel[0]["nama_lengkap"];
          $email = $data_hotel[0]["email"];
          $nohp = $data_hotel[0]["nohp"];
          $jml_kamar = $data_hotel[0]["jml_kamar"];
        }
      ?>
      <form class="form-horizontal">
        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Tanggal</label>
          <div class="col-sm-10"><?= $tanggal; ?></div> 
        </div>

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Kode Booking</label>
          <div class="col-sm-10"><?= $kode_booking; ?> </div>
        </div> 

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Nama</label>
          <div class="col-sm-10"><?= $nama; ?> </div>
        </div>

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">No HP</label>
          <div class="col-sm-10"><?= $nohp; ?> </div>
        </div> 	

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Periode</label>
          <div class="col-sm-10"><?= "Cek In : " . $cekin . ", Cek Out : ".$cekout; ?> </div>
        </div> 

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Kamar</label>
          <div class="col-sm-10"><?= "Jenis : " . $jenis_kamar . ", Jumlah : ".$jml_kamar; ?> </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php if ($site_form=="list") { ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Data Kamar Hotel</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("sales/kamar/".$hotelid."/add/"); ?>" class="btn btn-primary">
          <i class="fa fa-plus"></i> Tambah Kamar
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <form class="form-horizontal" method="post" action="#">
          <table class="table table-hover">
            <thead>
              <tr>
                <th width="10%">No</th>
                <th width="15%">No Kamar</th>
                <th width="15%">Tipe</th>
                <th width="15%">Nama</th>
                <th width="15%">Telepon</th>
                <th width="10%">Periode</th>
                <th width="10%">Hal</th>
                <th width="10%">&nbsp;</th>
              </tr>
            </thead>
            <tbody>
            <?php 
              if (sizeof($data_kamar)>0) {
                for ($i=0;$i<sizeof($data_kamar);$i++) {
                  $k_id = $data_kamar[$i]["id"];
                  $k_no = $data_kamar[$i]["no_kamar"];
                  $tipe = $data_kamar[$i]["tipe_kamar"];
                  $k_nama = $data_kamar[$i]["nama"];
                  $k_telepon = $data_kamar[$i]["telepon"];
                  $k_cekin = $data_kamar[$i]["cekin"];
                  $k_cekout = $data_kamar[$i]["cekout"];
                  $k_hal = $data_kamar[$i]["hal"];

                  $k_tipe = "";
                  if ($tipe=="1") { $k_tipe = "R1 - 1 Bedroom"; }
                  if ($tipe=="2") { $k_tipe = "R2 - Reguler"; }
                  if ($tipe=="3") { $k_tipe = "R3 - Deluxe"; }

            ?>
              <tr>
                <td><?= ($i+1); ?></td>
                <td><?= $k_no; ?></td>
                <td><?= $k_tipe; ?></td>
                <td><?= $k_nama; ?></td>
                <td><?= $k_telepon; ?></td>
                <td><?= $k_cekin."-".$k_cekout; ?></td>
                <td><?= $k_hal; ?></td>
                <td>
                  <a href="<?= site_url("sales/kamar/".$hotelid."/edit/".$k_id); ?>">Edit</a>
                  <a href="<?= site_url("sales/kamar/".$hotelid."/rem/".$k_id); ?>">Delete</a>
                </td>
              </tr>
            <?php
                }
              }
            ?>
            </tbody>
          </table>
        </form>
      </div>
    </div>
  </div>
</div>

<?php 
  } else {
    $nokamar = ""; $nama = ""; $nohp = ""; $cekin = ""; $cekout = ""; $roomtype = ""; $hal = "";
    if (sizeof($data_kamar)>0) {
      $nokamar = $data_kamar[0]["no_kamar"];
      $nama = $data_kamar[0]["nama"];
      $nohp = $data_kamar[0]["telepon"];
      $cekin = $data_kamar[0]["cekin"];
      $cekout = $data_kamar[0]["cekout"];
      $roomtype = $data_kamar[0]["tipe_kamar"];
      $hal = $data_kamar[0]["hal"];
    }
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Data Kamar Hotel</b></h4>
      <hr>
      <form class="form-horizontal" method="post" action="<?= base_url("sales/kamar/".$hotelid."/".$aksi."/".$kamarid); ?>">
        <div class="form-group"> 
          <label for="input1" class="col-sm-2 control-label">No Kamar</label> 
          <div class="col-sm-10"> <input type="text" name="nokamar" value="<?php echo $nokamar; ?>" class="form-control"></div>
        </div>

        <div class="form-group"> 
          <label for="input1" class="col-sm-2 control-label">Nama</label> 
          <div class="col-sm-10"> <input type="text" name="nama" value="<?php echo $nama; ?>" class="form-control"></div>
        </div>

        <div class="form-group"> 
          <label for="input1" class="col-sm-2 control-label">HP</label> 
          <div class="col-sm-10"> <input type="text" name="hp" value="<?php echo $nohp; ?>" class="form-control"></div>
        </div>				

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Check In</label>
          <div class="col-sm-10">
            <select name="cekin" class="form-control">
              <option value="23"<?php if ($cekin=="23") { echo " selected"; } ?>>23 Jan 2019</option> 
              <option value="24"<?php if ($cekin=="24") { echo " selected"; } ?>>24 Jan 2019</option> 
              <option value="25"<?php if ($cekin=="25") { echo " selected"; } ?>>25 Jan 2019</option> 
              <option value="26"<?php if ($cekin=="26") { echo " selected"; } ?>>26 Jan 2019</option> 
              <option value="27"<?php if ($cekin=="27") { echo " selected"; } ?>>27 Jan 2019</option> 
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Check Out</label>
          <div class="col-sm-10">
            <select name="cekout" class="form-control">
              <option value="24"<?php if ($cekout=="24") { echo " selected"; } ?>>24 Jan 2019</option> 
              <option value="25"<?php if ($cekout=="25") { echo " selected"; } ?>>25 Jan 2019</option> 
              <option value="26"<?php if ($cekout=="26") { echo " selected"; } ?>>26 Jan 2019</option> 
              <option value="27"<?php if ($cekout=="27") { echo " selected"; } ?>>27 Jan 2019</option> 
              <option value="28"<?php if ($cekout=="28") { echo " selected"; } ?>>28 Jan 2019</option> 
            </select>
          </div> 
        </div>

        <div class="form-group">
          <label for="input1" class="col-sm-2 control-label">Room Type</label>
          <div class="col-sm-10">
            <select name="roomtype" class="form-control">
              <option value="1"<?php if ($roomtype=="1") { echo " selected"; } ?>>R1 - 1 Bedroom</option>
              <option value="2"<?php if ($roomtype=="2") { echo " selected"; } ?>>R2 - Reguler</option> 
              <option value="3"<?php if ($roomtype=="3") { echo " selected"; } ?>>R2 - Deluxe</option> 
            </select>
          </div>
        </div>	

        <div class="form-group"> 
          <label for="input1" class="col-sm-2 control-label">Hal</label> 
          <div class="col-sm-10"> <input type="text" name="hal" value="<?php echo $hal; ?>" class="form-control"></div>
        </div>

        <div class="form-group">
          <div class="col-xs-10 col-xs-offset-2 text-right">
            <a href="<?= base_url("sales/kamar/".$hotelid); ?>" class="btn btn-default">
              <i class="fa fa-times"></i> Kembali
            </a>
            <button type="submit" class="btn btn-primary" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button>
          </div>
        </div>

      </form>
    </div>
  </div>
</div>
<?php } ?>