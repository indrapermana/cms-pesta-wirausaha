<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekap Pendapatan Pesta Wirausaha 2019</h4>
    <p class="text-muted page-title-alt">Total Pendapatan Pesta Wirausaha</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekap
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <!-- Top -->
  <div class="col-md-6 col-lg-6">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="fa fa-shopping-basket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">3.000.000.000</b></h3>
        <p class="text-muted">Target Pendapatan</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-info text-info">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-6">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="ion ion-stats-bars text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">250.000.000</b></h3>
        <p class="text-muted">Realisasi Pendapatan</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>

  <!-- Bottom -->
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-purple pull-left">
        <i class="fa fa-users text-purple"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">44</b></h3>
        <p class="text-muted">Kas TDA & Sponsor ACT</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-purple text-purple">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-danger pull-left">
        <i class="fa fa-pie-chart text-danger"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">65</b></h3>
        <p class="text-muted">tiket</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-danger text-danger">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-danger pull-left">
        <i class="fa fa-pie-chart text-danger"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">65</b></h3>
        <p class="text-muted">Expo</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-danger text-danger">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-danger pull-left">
        <i class="fa fa-pie-chart text-danger"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">65</b></h3>
        <p class="text-muted">Sponsor</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-danger text-danger">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>