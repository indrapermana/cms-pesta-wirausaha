<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Informasi Tambahan Tiket PWN</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Informasi Tambahan Tiket PWN
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Expo</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="10%">Nama</th>
              <th width="10%">Usaha</th>
              <th width="10%">No Booth Premium</th>
              <th width="10%">No Booth Reguler</th>
              <th width="10%">Nama Tiket</th>
              <th width="10%">Email Tiket</th>
              <th width="10%">Biaya</th>
              <th width="10%">Status</th>
            </tr>
          </thead>

          <tbody>
          <?php
          if (sizeof($data_expo)>0) {
            for ($i=0;$i<sizeof($data_expo);$i++) {
              $nama = $data_expo[$i]["nama"];
              $usaha = $data_expo[$i]["usaha"];
              $multi1 = $data_expo[$i]["premium"];
              $multi2 = $data_expo[$i]["reguler"];
              $kuli1 = $data_expo[$i]["kuliner"];
              $kuli2 = $data_expo[$i]["kuliner2"];
              $nama_tiket = $data_expo[$i]["nama_tiket"];
              $email_tiket = $data_expo[$i]["email_tiket"];
              $status = $data_expo[$i]["status_tiket"];
              $nilai = $data_expo[$i]["nilai"];

              $premium = $multi1 . $kuli1;
              $reguler = $multi2 . $kuli2;
            ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $premium; ?></td>
              <td><?= $reguler; ?></td>
              <td><?= $nama_tiket; ?></td>
              <td><?= $email_tiket; ?></td>
              <td><?= $nilai; ?></td>
              <td><?= $status; ?></td>
            </tr>
          <?php 
              }
            }             
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>