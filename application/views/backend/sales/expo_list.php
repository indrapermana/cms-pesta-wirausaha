<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Expo List</h4>
    <p class="text-muted page-title-alt">Expo</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Expo List
      </li>
    </ol>
  </div>
</div>

<?php if(sizeof($data_expo1)>0){ ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Multi Produk</b></h4>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <td>No</td>
              <td>Nama</td>
              <td>Usaha</td>
              <td>Email</td>
              <td>HP</td>
              <td>No Booth</td>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1; $xstatus = "";
              for ($i=0;$i<sizeof($data_expo1);$i++) {
                $id = $data_expo1[$i]["id"];
                $nama = $data_expo1[$i]["nama"];
                $usaha = $data_expo1[$i]["usaha"];
                $email = $data_expo1[$i]["email"];
                $hp = $data_expo1[$i]["no_hp"];
                $status = $data_expo1[$i]["status_pesan"];

                $xnobooth = "";
                if (sizeof($data_expo2)>0) {
                  $multi1 = ""; $multi2 = ""; $kuli1 = ""; $kuli2 = ""; $sponsor = "";
                  for ($j=0;$j<sizeof($data_expo2);$j++) {
                    $bid = $data_expo2[$j]["id"];
                    $bbookedby = $data_expo2[$j]["booked_by"];
                    $bnobooth = $data_expo2[$j]["no_booth"];
                    $btipe = $data_expo2[$j]["tipe_booth"];
                    $bmap = $data_expo2[$j]["map_type"];

                    if (($id==$bbookedby) && ($bmap=="1")) {
                      if ($btipe=="1") { $multi1 .= $bnobooth . ","; }
                      if ($btipe=="2") { $multi2 .= $bnobooth . ","; }
                      if ($btipe=="3") { $kuli2 .= $bnobooth . ","; }
                      if ($btipe=="8") { $kuli1 .= $bnobooth . ","; }
                      if ($btipe=="4") { $sponsor .= $bnobooth . ","; }
                    }
                  }

                  if ($multi1!="") { $xnobooth .= "Premium : " . $multi1 . "<br>"; }
                  if ($multi2!="") { $xnobooth .= "Reguler : " . $multi2 . "<br>"; }
                  if ($kuli1!="") { $xnobooth .= "Premium : " . $kuli1 . "<br>"; }
                  if ($kuli2!="") { $xnobooth .= "Reguler : " . $kuli2 . "<br>"; }
                  if ($sponsor!="") { $xnobooth .= "Sponsor : " . $sponsor . "<br>"; }
                }

                if ($xstatus!=$status) {
                  if ($status=="2") { echo "<tr><td colspan=6>Belum dibayar</td></tr>"; }
                  if ($status=="3") { echo "<tr><td colspan=6>Pembayaran Sebagian</td></tr>"; }
                  if ($status=="4") { echo "<tr><td colspan=6>Lunas</td></tr>"; }
                  if ($status=="6") { echo "<tr><td colspan=6>Sponsor</td></tr>"; }
                  $no = 1;
                }			

                if ($xnobooth!="") {
            ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $email; ?></td>
              <td><?= $hp; ?></td>
              <td><?= $xnobooth; ?></td>
            </tr>
            <?php
                  $no++;
                }

                $xstatus = $status;
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php } ?>

<?php if(sizeof($data_expo1)>0){ ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Kuliner : </b></h4>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <td>No</td>
              <td>Nama</td>
              <td>Usaha</td>
              <td>Email</td>
              <td>HP</td>
              <td>No Booth</td>
            </tr>
          </thead>
          <tbody>
            <?php
              $no = 1; $xstatus = "";
              for ($i=0;$i<sizeof($data_expo1);$i++) {
                $id = $data_expo1[$i]["id"];
                $nama = $data_expo1[$i]["nama"];
                $usaha = $data_expo1[$i]["usaha"];
                $email = $data_expo1[$i]["email"];
                $hp = $data_expo1[$i]["no_hp"];
                $status = $data_expo1[$i]["status_pesan"];

                $xnobooth = "";
                if (sizeof($data_expo2)>0) {
                  $multi1 = ""; $multi2 = ""; $kuli1 = ""; $kuli2 = ""; $sponsor = "";
                  for ($j=0;$j<sizeof($data_expo2);$j++) {
                    $bid = $data_expo2[$j]["id"];
                    $bbookedby = $data_expo2[$j]["booked_by"];
                    $bnobooth = $data_expo2[$j]["no_booth"];
                    $btipe = $data_expo2[$j]["tipe_booth"];
                    $bmap = $data_expo2[$j]["map_type"];

                    if (($id==$bbookedby) && ($bmap=="1")) {
                      if ($btipe=="1") { $multi1 .= $bnobooth . ","; }
                      if ($btipe=="2") { $multi2 .= $bnobooth . ","; }
                      if ($btipe=="3") { $kuli2 .= $bnobooth . ","; }
                      if ($btipe=="8") { $kuli1 .= $bnobooth . ","; }
                      if ($btipe=="4") { $sponsor .= $bnobooth . ","; }
                    }
                  }

                  if ($multi1!="") { $xnobooth .= "Premium : " . $multi1 . "<br>"; }
                  if ($multi2!="") { $xnobooth .= "Reguler : " . $multi2 . "<br>"; }
                  if ($kuli1!="") { $xnobooth .= "Premium : " . $kuli1 . "<br>"; }
                  if ($kuli2!="") { $xnobooth .= "Reguler : " . $kuli2 . "<br>"; }
                  if ($sponsor!="") { $xnobooth .= "Sponsor : " . $sponsor . "<br>"; }
                }

                if ($xstatus!=$status) {
                  if ($status=="2") { echo "<tr><td colspan=6>Belum dibayar</td></tr>"; }
                  if ($status=="3") { echo "<tr><td colspan=6>Pembayaran Sebagian</td></tr>"; }
                  if ($status=="4") { echo "<tr><td colspan=6>Lunas</td></tr>"; }
                  if ($status=="6") { echo "<tr><td colspan=6>Sponsor</td></tr>"; }
                  $no = 1;
                }			

                if ($xnobooth!="") {
            ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $email; ?></td>
              <td><?= $hp; ?></td>
              <td><?= $xnobooth; ?></td>
            </tr>
            <?php
                  $no++;
                }

                $xstatus = $status;
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php } ?>

