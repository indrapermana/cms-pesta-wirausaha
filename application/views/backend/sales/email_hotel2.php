<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Email E-Tiket</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
    body {
      font-family: Arial, Helvetica, sans-serif;
      margin: 0;
      padding: 0;
      color: #434343;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      background: #e6eaed;
    }
    img {
      border: 0 none;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    a img {
      border: 0 none;
    }
    .imageFix {
      display: block;
    }
    table,
    td {
      border-collapse: collapse;
    }
    table {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-weight: 400;
      margin: 0;
    }
    p {
      font-size: 16px;
      line-height: 1.5;
    }
    #bodyTable {
      height: 100% !important;
      margin: 0;
      padding: 0;
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      font-family: Arial, Helvetica, sans-serif;
    }
    .box {
      margin-top: 20px;
      margin-bottom: 20px;
      background: #fff;
    }
    .rounded {
      border-radius: 5px;
    }
    .header {
      font-size: 32px;
      color: #434343;
    }
    .textOutside {
      font-size: 13px;
      line-height: 18px;
      color: #727272;
      padding-left: 20px;
      padding-right: 20px;
    }
    .textOutside a {
      color: #727272;
    }
    .innerPadding {
      padding: 20px 20px 20px 20px;
    }
    .bold {
      font-weight: 700;
    }
    .footer {
      background: #1ba0e2;
      /*padding: 50px 20px;*/
      text-align: center;
    }

    .ExternalClass {
      width: 100%;
    }
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span, 
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    .wrapper {
      width: 100% !important;
      max-width: 450px;
    }
    .templateSidebar {
      display: block !important;
      /*width:33.33% !important;*/
      width: 100% !important;
    }
    .templateBody {
      display: block !important;
      /*width:66.66% !important;*/
      width: 100% !important;
    }
    .templateSidebarTitle {
      display: block !important;
      /*width:33.33% !important;*/
      width: 100% !important;
    }
    .templateBodyTitle {
      display: block !important;
      /*width:66.66% !important;*/
      width: 100% !important;
    }
    @media only screen and (max-width: 480px) {
      .wrapper {
        width: 90% !important;
      }
      .templateSidebar,
      .templateBody,
      .templateSidebarTitle,
      .templateBodyTitle {
        display: block !important;
        width: 90% !important;
      }
      .sidebarContent {
        font-size: 16px !important;
        width: 100%;
      }
      .bodyImage {
        height: auto !important;
        max-width: 480px !important;
        width: 100% !important;
      }
      .bodyContent {
        font-size: 18px !important;
        width: 100%;
      }
    }
   
     @media only screen {
        html {
          min-height: 100%;
          background: #e6eaed;
        }
      }
       
      @media only screen and (max-width: 616px) {
        table.body img {
          width: auto;
          height: auto;
        }
     
        table.body center {
          min-width: 0 !important;
        }
     
        table.body .container {
          width: 100% !important;
        }
     
        table.body .columns {
          height: auto !important;
          -moz-box-sizing: border-box;
          -webkit-box-sizing: border-box;
          box-sizing: border-box;
          padding-left: 16px !important;
          padding-right: 16px !important;
        }
     
        table.body .columns .columns {
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
     
        table.body .collapse .columns {
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
     
        th.small-1 {
          display: inline-block !important;
          width: 8.33333% !important;
        }
     
        th.small-2 {
          display: inline-block !important;
          width: 16.66667% !important;
        }
     
        th.small-6 {
          display: inline-block !important;
          width: 50% !important;
        }
     
        th.small-10 {
          display: inline-block !important;
          width: 83.33333% !important;
        }
     
        th.small-11 {
          display: inline-block !important;
          width: 91.66667% !important;
        }
     
        th.small-12 {
          display: inline-block !important;
          width: 100% !important;
        }
     
        .columns th.small-12 {
          display: block !important;
          width: 100% !important;
        }
      }
  </style>
 </head> 
 <body>
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="background:#e6eaed;">
   <tbody>
    <tr>
     <td align="center">
      <table border="0" cellpadding="0" cellspacing="0" style="background:#e6eaed;height:62px;width:90%;max-width:450px;">
       <tbody>
        <tr>
         <td align="center" valign="middle" style="padding-bottom:x;padding-top:50px;"> 
		 <img src="http://hotel.pestawirausaha.com/gambar/logo_kolaboraksi2.png" style="padding-top: 48px;"> </td>
        </tr>
       </tbody>
      </table> </td>
    </tr>
    <tr>
     <td align="center">
      <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:90%;max-width:450px;">
       <tbody>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td style="background-image: url(http://hotel.pestawirausaha.com/gambar/strip.png);background-repeat:repeat-x no-repeat;width:100%;height:4px;background-color:#1ba0e2;display:block;"> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td align="center" valign="middle" style="padding-top:27px;padding-left:26px;padding-right:26px; padding-bottom:20px">
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="padding-bottom:0px;">
               <tbody>
                <tr>
                 <td align="left" valign="middle">
                  <table align="left" border="0" cellpadding="0" cellspacing="0">
                   <tbody>
                    <tr>
                     <td align="left" valign="middle" style="padding-right:10px;"> </td>
                     <td>
                      <table align="left" border="0" cellpadding="0" cellspacing="0">
                       <tbody>
                        <tr>
                         <td style="font-size:14px;color:#727272;padding-bottom:3px;">Kontak </td>
                        </tr>
                        <tr>
                         <td style="font-size:18px;color:#1A1A1A;padding-bottom:0px;">6281xxxxx</td>
                        </tr>
                        <tr>
                         <td style="font-size:18px;color:#1ba0e2;">info@pestawirausaha.com</td>
                        </tr>
                       </tbody>
                      </table> </td>
                    </tr>
                   </tbody>
                  </table> </td>
                </tr>
               </tbody>
              </table>
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="margin-left:45px;margin-top:10px;">
               <tbody>
                <tr>
                 <td align="left" valign="middle">
                  <table align="left" border="0" cellpadding="0" cellspacing="0">
                   <tbody>
                    <tr>
                     <td>
                      <table align="left" border="0" cellpadding="0" cellspacing="0">
                       <tbody>
                        <tr>
                         <td style="font-size:14px;color:#727272;padding-bottom:3px;">Kode Booking</td>
                        </tr>
                        <tr>
                         <td style="font-size:18px;color:#1A1A1A;">12345</td>
                        </tr>
                       </tbody>
                      </table> </td>
                    </tr>
                   </tbody>
                  </table> </td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td>
              <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
               <tbody>
                <tr>
                 <td style="background-image: url(http://hotel.pestawirausaha.com/gambar/dashed-dot.jpg);background-repeat:repeat-x no-repeat;width:100%;height:1px;background-color:#dedede;display:block;"> </td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
        <tr>
         <td align="center" valign="top" width="100%">
          <div style="background: #fff;border-radius: 4px;border-bottom: 2px solid #dadada; overflow: hidden;">
          </div>
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td align="center" valign="top">
              <div>
               <!-- GREETINGS -->
               <div style="padding-top: 38px;background:#ffffff;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                 <tbody>
                  <tr>
                   <td align="center" valign="top"> <h1 style="font-weight:normal;font-size:30px;color:#434343;margin:0px;padding:0px 16px;">Voucher Hotel!</h1> </td>
                  </tr>
                  <tr>
                   <td align="left" valign="top" style="padding:0px 16px;">
                    <div style="width:100%;height:38px;display:block;"></div> <h3 style="font-size:16px;margin:0px;padding:0px;color:#434343;font-weight:bold;"><strong>Hai kak nama,</strong></h3>
                    <div style="width:100%;height:28px;display:block;"></div> <p style="line-height:25px;font-size:16px;padding:0px;margin:0px;">Pesanan kamar hotel anda telah dikonfirmasi.</p>
                    <div style="width:100%;height:18px;display:block;"></div> </td>
                  </tr>
                 </tbody>
                </table>
               </div>
               <!-- ISSUANCE DETAIL -->
               <div style="background:#ffffff; margin-bottom: 16px; border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;border-bottom: 2px solid #dadada;">
                <div style="padding: 0 16px; margin-bottom: 10px">
                 <table border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding: 0 16px;width:100%;">
                  <tbody>
                   <tr>
                    <td align="left" valign="middle" width="100%;"> <span style="font-size:15px;text-transform:uppercase;color:#1ba0e2;font-weight:bold;margin:0px;padding:0px;">Informasi Pesanan</span> </td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <div style="padding: 0 16px;">
                 <table border="0" cellpadding="0" cellspacing="0" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#656262;border-top-width:1px;border-top-style:solid;border-top-color:#656262;width:100%;padding: 0 16px;">
                  <tbody>
                   <tr>
                    <td align="left" valign="top" width="100%" style="padding-top:10px;padding-bottom:10px;"> <h2 style="font-size:16px;line-height:16px;font-weight:bold;width:100%;margin:0px;padding:0px;margin-top:10px">Aston Marina Ancol</h2>
                     <div style="margin:0px;padding:0px;width:100%;">
                      <img src="http://hotel.pestawirausaha.com/gambar/4.png" alt="rating">
                     </div> <p style="font-size:15px;color:#737374;margin:0px;padding:0px;margin-bottom:5px;">Marina Mediterania, Tower A. Jl. Lodan Raya No. 2A Tower A, Ancol, Pademangan, Jakarta, Indonesia, 14430</p> </td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                 <tbody>
                  <tr>
                   <td align="left" style="width:100%;">
                    <div style="padding: 0 16px;">
                     <p style="font-size:14px;margin-top:15px;">Nama:<br><span style="font-weight:bold;font-size:16px;color:#1ba0e2;">Deddy Novrandianto</span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">No Invoice: <span style="font-weight:bold;">1234</span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">Dipesan melalui PestaWirausaha</p>
                     <br>
                    </div>
                    <div style="padding: 0 16px;">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;margin-bottom:20px;">
                      <tbody>
                       <tr>
                        <td align="left" style="width:50%;"> <p style="font-size:13px;margin:0px;padding:0px;width:100%;">Check-in</p> <p style="font-size:14px;font-weight:bold;margin:0px;padding:0px;width:100%;">24 Jan 2019</p> <p style="font-size:12px;margin:0px;padding:0px;width:100%;color:#1ba0e2;">from 14:00</p> </td>
                        <td align="left" style="width:50%;"> <p style="font-size:13px;margin:0px;padding:0px;width:100%;">Check-out</p> <p style="font-size:14px;font-weight:bold;margin:0px;padding:0px;width:100%;">27 Jan 2019</p> <p style="font-size:12px;margin:0px;padding:0px;width:100%;color:#1ba0e2;">to 12:00</p> </td>
                       </tr>
                      </tbody>
                     </table>
                    </div>
                    <div style="padding: 0 16px;">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;margin-bottom:20px;">
                      <tbody>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Room type</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;">Bedroom Superior Room Only</p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Number of rooms</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;">1 Room(s)</p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Room Capacity</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;">4 Guest per room</p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Breakfast</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> Not included </p> </td>
                       </tr>
                       <tr style="">
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Facilities</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> <img src="http://hotel.pestawirausaha.com/gambar/wifi-icon.png" style="width:11px;height:auto;">&nbsp;Free Wifi </p> </td>
                       </tr>
                       <tr style="">
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Note</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> xxx </p> </td>
                       </tr>					   
                      </tbody>
                     </table>
                    </div> </td>
                  </tr>
                 </tbody>
                </table>
               </div>
               <!-- END of ISSUANCE DETAIL -->
               <!-- CROSSSELL A&A -->
              </div></td>
            </tr>
			
            <tr style="padding:0;text-align:left;vertical-align:top">
             <td class="wrapper-inner" style="border-collapse:collapse!important;color:#434343;font-size:16px;font-weight:normal;hyphens:auto;line-height:1.5;margin:0;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">
              <div class="box-shadow rounded-shadow overflow-hidden" style="background: #fff; hyphens: none; overflow: hidden; width: 100%;border-radius: 4px; overflow: hidden;border-bottom: 2px solid #dadada;">
               <table align="center" class="container" style="Margin: 0 auto; background: transparent; border-collapse: collapse; border-spacing: 0; margin: 0 auto; padding: 0; text-align: inherit; vertical-align: top; ">
                <tbody>
                 <tr style="padding: 0; text-align: left; vertical-align: top;">
                  <td style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 1.5; margin: 0; padding: 0 8px; text-align: left; vertical-align: top; word-wrap: break-word;">
                   <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>
                     <tr style="padding: 0; text-align: left; vertical-align: top;">
                      <th class="small-12 large-12 columns first last" style="Margin: 0 auto; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0 auto; padding: 0 8px; padding-bottom: 0; text-align: left; width: 584px;">
                       <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                         <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <th style="Margin: 0; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: left;">
                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                             <tr style="padding: 0; text-align: left; vertical-align: top;">
                              <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                             </tr>
                            </tbody>
                           </table> <h3 class="h3" style="Margin: 0; Margin-bottom: 8px; color: inherit; font-family: Helvetica, Arial, sans-serif; font-size: 1.125rem; font-weight: bold; letter-spacing: .3px; line-height: 26px; margin: 0; margin-bottom: 8px; padding: 0; text-align: left; word-wrap: normal;"> Informasi Pesta Wirausaha </h3> <p class="h5" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: normal; line-height: 22px; margin: 0; margin-bottom: 8px; padding: 0; text-align: left;"> Nikmati kegiatan menarik di helaran Pesta Wirausaha 2019 ! </p>
                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                             <tr style="padding: 0; text-align: left; vertical-align: top;">
                              <td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                             </tr>
                            </tbody>
                           </table> </th>
                          <th class="expander" style="Margin: 0; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0 !important; text-align: left; visibility: hidden; width: 0;"></th>
                         </tr>
                        </tbody>
                       </table></th>
                     </tr>
                    </tbody>
                   </table>
                   <table class="row" style="border-collapse: collapse; border-spacing: 0; display: table; padding: 0; position: relative; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>
                     <tr style="padding: 0; text-align: left; vertical-align: top;">
                      <th class="small-6 large-3 columns first" style="Margin:0 auto;color:#434343;font-size:16px;font-weight:normal;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;text-align:left;">
                       <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                         <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <th style="Margin: 0; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: left;"> <a href="" title="Entrepreneurun" style="Margin: 0; color: #0770cd; display: block; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.5; margin: 0; padding: 0 8px; text-align: left; text-decoration: none;">
                            <div style="border-radius: 2px; margin-bottom: 8px; width: auto; height: 120px; background-image: url(http://hotel.pestawirausaha.com/gambar/lari.png); background-position: 50% 100%; background-size: cover;">
                            </div> <p class="h5 bold" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: bold; line-height: 22px; margin: 0; margin-bottom: 0; max-height: 44px; overflow: hidden; padding: 0; text-align: left; word-wrap: break-word; text-overflow: ellipsis;"> Entrepreneurun </p> <p class="h5" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: normal; line-height: 22px; margin: 0; margin-bottom: 8px; margin-top: 4px; padding: 0; text-align: left;"> Lari Tumak Tumuk 5K </p>  <p class="h6" style="Margin: 0; Margin-bottom: 8px; color: #8f8f8f; font-family: Helvetica, Arial, sans-serif; font-size: 0.75rem; font-weight: normal; line-height: 20px; margin: 0; margin-bottom: 0; padding: 0; text-align: left;"> Tiket IDR 75,000 </p> <p class="bold" style="Margin: 0; Margin-bottom: 8px; color: #0770cd; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 4px; padding: 0; text-align: left;"><b>Info Lengkap</b></p> </a>
                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                             <tr style="padding: 0; text-align: left; vertical-align: top;">
                              <td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                             </tr>
                            </tbody>
                           </table> </th>
                         </tr>
                        </tbody>
                       </table></th>
                      <th class="small-6 large-3 columns" style="Margin:0 auto;color:#434343;font-size:16px;font-weight:normal;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;text-align:left;">
                       <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                         <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <th style="Margin: 0; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: left;"> <a href="" title="Malam Donasi Amal" style="Margin: 0; color: #0770cd; display: block; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.5; margin: 0; padding: 0 8px; text-align: left; text-decoration: none;">
                            <div style="border-radius: 2px; margin-bottom: 8px; width: auto; height: 120px; background-image: url(http://hotel.pestawirausaha.com/gambar/maherz.png); background-position: 50% 100%; background-size: cover;">
                            </div> <p class="h5 bold" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: bold; line-height: 22px; margin: 0; =
margin-bottom: 0; max-height: 44px; overflow: hidden; padding: 0; text-align: left; word-wrap: break-word; text-overflow: ellipsis;"> Malam Donasi Amal </p> <p class="h5" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: normal; line-height: 22px; margin: 0; margin-bottom: 8px; margin-top: 4px; padding: 0; text-align: left;"> Bersama Maher Zain </p> <p class="h6" style="Margin: 0; Margin-bottom: 8px; color: #8f8f8f; font-family: Helvetica, Arial, sans-serif; font-size: 0.75rem; font-weight: normal; line-height: 20px; margin: 0; margin-bottom: 0; padding: 0; text-align: left;"> Donasi IDR 600,000 </p> <p class="bold" style="Margin: 0; Margin-bottom: 8px; color: #0770cd; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 4px; padding: 0; text-align: left;"><b>Info Lengkap</b></p> </a>
                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                             <tr style="padding: 0; text-align: left; vertical-align: top;">
                              <td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                             </tr>
                            </tbody>
                           </table> </th>
                         </tr>
                        </tbody>
                       </table></th>
                      <th class="small-6 large-3 columns last" style="Margin:0 auto;color:#434343;font-size:16px;font-weight:normal;line-height:1.5;margin:0 auto;padding:0;padding-bottom:0;text-align:left;">
                       <table style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                        <tbody>
                         <tr style="padding: 0; text-align: left; vertical-align: top;">
                          <th style="Margin: 0; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; line-height: 1.5; margin: 0; padding: 0; text-align: left;"> <a href="" title="TDA Gathering" style="Margin: 0; color: #0770cd; display: block; font-family: Helvetica, Arial, sans-serif; font-weight: normal; line-height: 1.5; margin: 0; padding: 0 8px; text-align: left; text-decoration: none;">
                            <div style="border-radius: 2px; margin-bottom: 8px; width: auto; height: 120px; background-image: url(http://hotel.pestawirausaha.com/gambar/gather.png); background-position: 50% 100%; background-size: cover;">
                            </div> <p class="h5 bold" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: bold; line-height: 22px; margin: 0; margin-bottom: 0; max-height: 44px; overflow: hidden; padding: 0; text-align: left; word-wrap: break-word; text-overflow: ellipsis;"> TDA Gathering </p> <p class="h5" style="Margin: 0; Margin-bottom: 8px; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 0.875rem; font-weight: normal; line-height: 22px; margin: 0; margin-bottom: 8px; margin-top: 4px; padding: 0; text-align: left;"> Perlombaan antar wilayah TDA </p>  <p class="h6" style="Margin: 0; Margin-bottom: 8px; color: #8f8f8f; font-family: Helvetica, Arial, sans-serif; font-size: 0.75rem; font-weight: normal; line-height: 20px; margin: 0; margin-bottom: 0; padding: 0; text-align: left;"></p> <p class="bold" style="Margin: 0; Margin-bottom: 8px; color: #0770cd; font-family: Helvetica, Arial, sans-serif; font-size: 14px; font-weight: bold; line-height: 1.5; margin: 0; margin-bottom: 4px; padding: 0; text-align: left;"><b>Info Lengkap</b></p> </a>
                           <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                            <tbody>
                             <tr style="padding: 0; text-align: left; vertical-align: top;">
                              <td height="8px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 8px; font-weight: normal; hyphens: auto; line-height: 8px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                             </tr>
                            </tbody>
                           </table> </th>
                         </tr>
                        </tbody>
                       </table></th>
                     </tr>
                    </tbody>
                   </table>
                   <div style="padding: 0 8px;">
                    <a href="" style="Margin: 0; background: #0770cd; border-radius: 4px; color: #fff; display: block; font-family: Helvetica, Arial, sans-serif; font-size: 12px; font-weight: bold; line-height: 1.5; margin: 0; padding: 8px; text-align: center; text-decoration: none;"> Info Lainnya </a>
                   </div>
                   <table class="spacer" style="border-collapse: collapse; border-spacing: 0; padding: 0; text-align: left; vertical-align: top; width: 100%;">
                    <tbody>
                     <tr style="padding: 0; text-align: left; vertical-align: top;">
                      <td height="16px" style="-moz-hyphens: auto; -webkit-hyphens: auto; Margin: 0; border-collapse: collapse !important; color: #434343; font-family: Helvetica, Arial, sans-serif; font-size: 16px; font-weight: normal; hyphens: auto; line-height: 16px; margin: 0; mso-line-height-rule: exactly; padding: 0; text-align: left; vertical-align: top; word-wrap: break-word;">&nbsp;</td>
                     </tr>
                    </tbody>
                   </table> </td>
                 </tr>
                </tbody>
               </table>
              </div> </td>
            </tr>
			
			
            <tr>
             <td style="background-color:#e6eaed">
              <table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%">
               <tbody>
                <tr style="padding:0;text-align:left;vertical-align:top">
                 <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#434343;font-size:16px;font-weight:normal;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&nbsp;</td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
            <!-- CROSSSELL F&B --> 
           </tbody>
          </table>

          <table border="0" cellpadding="0" cellspacing="0" style="background:#e6eaed;width:100%;max-width:450px;">
           <tbody>
            <tr>
             <td align="center" valign="middle" style="background:#e6eaed;padding-top:25px;"> <p class="textOutside"><br>Pesta Wirausaha Nasional TDA 2019. All Rights Reserved.<br><br> </p> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
       </tbody>
      </table>  </td>
    </tr>
   </tbody>
  </table>
  <img style="display:block;font-size:0px;line-height:0em;" src="" alt="" width="1" height="1" border="0">
 </body>
</html>