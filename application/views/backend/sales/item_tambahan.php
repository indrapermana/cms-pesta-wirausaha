<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Informasi Tambahan Stand Expo</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Informasi Tambahan Expo PW
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Expo</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="10%">Nama</th>
              <th width="10%">Usaha</th>
              <th width="10%">No Booth</th>
              <th width="10%">Penjaga</th>
              <th width="10%">Daya</th>
              <th width="10%">Meja</th>
              <th width="10%">Kursi</th>
              <th width="10%">Tiket</th>
              <th width="10%">TV</th>
              <th width="10%">Lainnya</th>
              <th width="10%">Penanggung Jawab</th>
              <th width="10%">Biaya</th>
            </tr>
          </thead>

          <tbody>
          <?php
            if (sizeof($data_expo)>0) {
              for ($i=0;$i<sizeof($data_expo);$i++) {
                $nama = $data_expo[$i]["nama"];
                $usaha = $data_expo[$i]["usaha"];
                $multi1 = $data_expo[$i]["premium"];
                $multi2 = $data_expo[$i]["reguler"];
                $kuli1 = $data_expo[$i]["kuliner"];
                $kuli2 = $data_expo[$i]["kuliner2"];
                $penjaga = $data_expo[$i]["tbh_penjaga"];
                $daya = $data_expo[$i]["tbh_daya"];
                $meja = $data_expo[$i]["tbh_meja"];
                $kursi = $data_expo[$i]["tbh_kursi"];
                $tiket = $data_expo[$i]["tbh_tiket"];
                $tv = $data_expo[$i]["tbh_tv"];
                $lainnya = $data_expo[$i]["tbh_lainnya"];
                $penanggungjawab = $data_expo[$i]["tbh_penanggungjawab"];

                $nobooth = $multi1 . $multi2 . $kuli1 . $kuli2;
                $biaya = 0;          
                $xpenjaga = "";

                if ($penjaga=="1") { $xpenjaga = "1 orang"; }
                if ($penjaga=="2") { $xpenjaga = "2 orang"; }
                if ($penjaga=="3") { $xpenjaga = "3 orang"; }
                if ($penjaga=="4") { $xpenjaga = "4 orang"; }
                if ($penjaga=="5") { $xpenjaga = "&gt; 4 orang"; }

                $xdaya = "";

                if ($daya=="1") { $xdaya = "450 watt"; $biaya += 250000; }
                if ($daya=="2") { $xdaya = "900 watt"; $biaya += 500000; }
                if ($daya=="3") { $xdaya = "tidak"; }
                if ($daya=="4") { $xdaya = "&gt; 900 watt"; $biaya += 500000; }

                $xmeja = "";

                if ($meja=="1") { $xmeja = "1 meja"; $biaya += 150000; }
                if ($meja=="2") { $xmeja = "2 meja"; $biaya += 300000; }
                if ($meja=="3") { $xmeja = "3 meja"; $biaya += 450000; }
                if ($meja=="4") { $xmeja = "&gt; 3 meja"; $biaya += 450000; }
                if ($meja=="5") { $xmeja = "tidak"; }

                $xkursi = "";

                if ($kursi=="1") { $xkursi = "1 kursi"; $biaya += 75000; }
                if ($kursi=="2") { $xkursi = "2 kursi"; $biaya += 150000; }
                if ($kursi=="3") { $xkursi = "3 kursi"; $biaya += 225000; }
                if ($kursi=="4") { $xkursi = "&gt; 3 kursi"; $biaya += 225000; }
                if ($kursi=="5") { $xkursi = "tidak"; }	

                $xtiket = "";

                if ($tiket=="1") { $xtiket = "1 tiket"; $biaya += 60000; }
                if ($tiket=="2") { $xtiket = "2 tiket"; $biaya += 120000; }
                if ($tiket=="3") { $xtiket = "3 tiket"; $biaya += 180000; }
                if ($tiket=="4") { $xtiket = "4 tiket"; $biaya += 240000; }
                if ($tiket=="5") { $xtiket = "&gt; 4 tiket"; $biaya += 240000; }
                if ($tiket=="6") { $xtiket = "tidak"; }	

                $xtv = "";

                if ($tv=="1") { $xtv = "1 tv"; }
                if ($tv=="2") { $xtv = "2 tv"; }
                if ($tv=="3") { $xtv = "&gt; 2 tv"; }
                if ($tv=="4") { $xtv = "tidak"; }			
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $nobooth; ?></td>
              <td><?= $xpenjaga; ?></td>
              <td><?= $xdaya; ?></td>
              <td><?= $xmeja; ?></td>
              <td><?= $xkursi; ?></td>
              <td><?= $xtiket; ?></td>
              <td><?= $xtv; ?></td>
              <td><?= $lainnya; ?></td>
              <td><?= $penanggungjawab; ?></td>
              <td><?= number_format($biaya); ?></td>
            </tr>
          </tbody>
          <?php 
              }
            } 
          ?>
        </table>
      </div>
    </div>
  </div>
</div>