<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Pemesanan Kamar Hotel</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Pemesanan Kamar Hotel
      </li>
    </ol>
  </div>
</div>

<?php
if ($site_form=="list") {
	$t_pesan3 = 0; $t_total3 = 50; 
	$t_pesan2 = 0; $t_total2 = 50; 
	$t_lunas = 0; $t_piutang = 0; 
	$j_lunas = 0; $j_piutang = 0;

	if (sizeof($data_rekap)>0) {
		for ($i=0;$i<sizeof($data_rekap);$i++) {
			$rjenis = $data_rekap[$i]["jenis_kamar"];
			$rstatus = $data_rekap[$i]["status_bayar"];
			$rjumlah = $data_rekap[$i]["jumlah"];
			$rnilai = $data_rekap[$i]["nilai"];

			if ($rjenis=="1") { $t_pesan3 += $rjumlah; }
			if ($rjenis=="2") { $t_pesan2 += $rjumlah; }
			if ($rstatus=="0") { $t_piutang += $rnilai; $j_piutang += $rjumlah; }
			if ($rstatus=="1") { $t_lunas += $rnilai; $j_lunas += $rjumlah; }
		}
	}

?>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_pesan3 . "/". $t_total3 . " (". number_format($t_pesan3/$t_total3*100,2)." %)"; ?>
          </b>
        </h3>
        <p class="text-muted">Kamar 3 Malam</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-info text-info">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="ti-ticket text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo $t_pesan2 . "/". $t_total2 . " (". number_format($t_pesan2/$t_total2*100,2)." %)"; ?>
          </b>
        </h3>
        <p class="text-muted">Kamar 2 Malam</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-purple pull-left">
        <i class="fa fa-soccer-ball-o text-purple"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_lunas); ?>
          </b>
        </h3>
        <p class="text-muted">Total Nilai Lunas (<?php echo $j_lunas . " kamar"; ?>)</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-purple text-purple">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-warning pull-left">
        <i class="fa fa-soccer-ball-o text-warning"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">
          <b class="counter">
            <?php echo "Rp ".number_format($t_piutang); ?>
          </b>
        </h3>
        <p class="text-muted">Total Nilai Pemesanan (<?php echo $j_piutang . " kamar"; ?>)</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-warning text-warning">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Expo</b></h4>
      <div class="filtter-right">
        <form action="<?php echo base_url("sales/hotel"); ?>" method="post">
          <select name="status" onchange="this.form.submit();" class="form-control ">
            <option value="0"<?php if ($status=="0") { echo " selected"; } ?>>Semua Status</option>
						<option value="1"<?php if ($status=="1") { echo " selected"; } ?>>Waiting List</option>
						<option value="2"<?php if ($status=="2") { echo " selected"; } ?>>Pemesanan Booth</option>
						<option value="3"<?php if ($status=="3") { echo " selected"; } ?>>Pembayaran Sebagian</option>
						<option value="4"<?php if ($status=="4") { echo " selected"; } ?>>Pembayaran Lunas</option>
						<option value="6"<?php if ($status=="6") { echo " selected"; } ?>>Sponsorship</option>
						<option value="5"<?php if ($status=="5") { echo " selected"; } ?>>Batal</option>
          </select>
        </form>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th width="5%">No</th>
              <th width="15%">Tanggal</th>
              <th width="20%">Kontak</th>
              <th width="10%">Jenis Kamar</th>
              <th width="10%">Jml Kamar</th>
              <th width="20%">Catatan</th>
              <th width="10%">&nbsp;</th>
            </tr>
          </thead>

          <?php if (sizeof($data_hotel)>0) { ?>
          <tbody>
          <?php 
            $total = 0;
            for ($i=0;$i<sizeof($data_hotel);$i++) {
              $id = $data_hotel[$i]["id"];
              $tanggal = $data_hotel[$i]["tanggal"];
              $kodebooking = $data_hotel[$i]["kode_booking"];
              $jenis = $data_hotel[$i]["jenis_kamar"];
              $cekin = $data_hotel[$i]["tgl_cekin"];
              $cekout = $data_hotel[$i]["tgl_cekout"];
              $roomtype = $data_hotel[$i]["room_type"];
              $breakfast = $data_hotel[$i]["breakfast"];
              $nama = $data_hotel[$i]["nama_lengkap"];
              $email = $data_hotel[$i]["email"];
              $hp = $data_hotel[$i]["nohp"];
              $no_tiket = $data_hotel[$i]["no_tiket"];
              $asal_tda = $data_hotel[$i]["asal_tda"];
              $no_ktp = $data_hotel[$i]["no_ktp"];
              $keterangan = $data_hotel[$i]["keterangan"];
              $jml_kamar = $data_hotel[$i]["jml_kamar"];
              $nilai = $data_hotel[$i]["nilai"];
              $status = $data_hotel[$i]["status_bayar"];

              $xstatus = $status;
              if ($status=="0") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-info\">Pemesanan</a>"; }
              if ($status=="2") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Pembayaran Uang Muka</a>"; }
              if ($status=="1") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Pembayaran Lunas</a>"; }
              if ($status=="3") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Bundling Expo</a>"; }
              if ($status=="4") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Akomodasi Pemateri</a>"; }
              if ($status=="6") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Sponsorship</a>"; }
              if ($status=="5") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-warning\">Batal</a>"; }

              $xjenis = $jenis;
              if ($jenis=="1") { $xjenis = "Paket 3 Malam"; }
              if ($jenis=="2") { $xjenis = "Paket 2 Malam"; }
              if ($jenis=="3") { $xjenis = "Paket 1 Malam"; }

              $xroomtype = "";
              if ($roomtype=="1") { $xroomtype = "R1 - 1 Bedroom"; } 
              if ($roomtype=="2") { $xroomtype = "R2 - 2 Bedroom"; }

              $xbreakfast = "";
              if ($breakfast=="0") { $xbreakfast = "Sarapan : Tidak Termasuk"; }
              if ($breakfast=="1") { $xbreakfast = "Sarapan : Termasuk"; }

              $linkHP = "";
              if ($hp!="") {
                $linkHP = $hp . " (<a href=\"https://api.whatsapp.com/send?phone=".$hp."\" target=\"_blank\">Kontak WA</a>)<br>";
              }

              // $linkInvoice = "<a href=\"http://hotel.pestawirausaha.com/invoice/".$kodebooking."\">".$kodebooking."</a>";
              $linkInvoice = '<a href="'.base_url("hotel/invoice/".$kodebooking).'" target="blank">'.$kodebooking.'</a>';
              
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $tanggal."<br>Kode : ".$linkInvoice; ?></td>
              <td><?= $nama."<br>".$email."<br>".$linkHP.$no_ktp; ?></td>
              <td><?= $xjenis."<br>".$xroomtype."<br>".$xbreakfast."<br>Cek In : ".$cekin."<br>Cek Out : ".$cekout;?></td>
              <td><?= $jml_kamar; ?></td>
              <td><?= $keterangan; ?></td>
              <td><?= $xstatus; ?></td>
              <td>
                <a href="<?= site_url("sales/hotel/".$id); ?>" class="btn btn-block btn-primary">Update Status</a> 
                <a href="<?= site_url("sales/kamar/".$id); ?>" class="btn btn-block btn-primary">Kamar</a>
              </td>
            </tr>
          <?php
              $total += $jml_kamar;
            }
          ?>
          </tbody>

          <tfoot>
            <tr>
              <th colspan="4" class="text-center">Total Kamar</th>
              <td><?= $total; ?></td>
              <td></td>
              <td></td>
              <td></td>
            </tr>
          </tfoot>
          <?php } ?>
        </table>
      </div>
    </div>
  </div>
</div>

<?php } else{ ?>

<?php } ?>