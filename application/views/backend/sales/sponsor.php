<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Sponsor</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Sponsor
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Detail Jenis Tiket</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("sales/jnstiket/tambah"); ?>" class="btn btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Nama</th>
              <th class="text-center">Usaha</th>
              <th class="text-center">Email</th>
              <th class="text-center">No HP</th>
              <th class="text-center">Asal TDA</th>
              <th class="test-center">Tanggal</th>
              <th class="test-center">Jenis Paket</th>
              <th class="test-center">Nilai</th>
            </tr>
          </thead>
          <tbody>
            <?php
              if(count($sponsor) > 0){
                $no = 1;
                $total = 0;
                foreach($sponsor as $data){
                  $nilai = 0; $namapaket = "";
                  if ($data->jenis_paket=="1") { $nilai = 5000000; $namapaket = "Paket Sponsor Internal Premium"; }
                  if ($data->jenis_paket=="2") { $nilai = 2500000; $namapaket = "Paket Sponsor Internal VIP"; }
                  if ($data->jenis_paket=="3") { $nilai = 1500000; $namapaket = "Paket Sponsor Internal Super"; }
                  if ($data->jenis_paket=="4") { $nilai = 500000; $namapaket = "Paket Sponsor Internal Insert"; }
                  if ($data->jenis_paket=="5") { $nilai = 300000; $namapaket = "Paket Sponsor Internal Reguler"; }
                  if ($data->jenis_paket=="6") { $nilai = 20000; $namapaket = "Paket Sponsor Internal Test"; }
                  if ($data->jenis_paket=="7") { $nilai = 800000; $namapaket = "Paket Sponsor Internal Reguler Insert"; }
            ?>
            <tr>
              <td class="text-center"><?= $no; ?></td>
              <td><?= $data->nama; ?></td>
              <td><?= $data->usaha; ?></td>
              <td><?= $data->email; ?></td>
              <td><?= $data->no_hp; ?></td>
              <td><?= $data->asal_tda; ?></td>
              <td><?= $data->tanggal; ?></td>
              <td><?= $namapaket; ?></td>
              <td class="text-right"><?= number_format($nilai,0,',','.'); ?></td>
            </tr>
            <?php
                  $total = $nilai + $total;
                  $no++;
                }
            ?>
            <tr>
              <th colspan="8" class="text-center">Total</th>
              <th class="text-right"><?= number_format($total,0,',','.'); ?></th>
            </tr>
            <?php
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>