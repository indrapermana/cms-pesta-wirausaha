<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekening Bank</h4>
    <p class="text-muted page-title-alt">Informasi Mutasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekening Bank
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekon Transaksi Rekening BSM</b></h4>
      <hr>
      <?php if ($submit==0) { ?>
      <form class="form-horizontal" method="post" action="<?= base_url('sales/rekon'); ?>" role="form">                                    
        <div class="form-group">
          <label class="col-sm-2 control-label">Textfile</label>
          <div class="col-sm-10">
          <textarea class="form-control" name="txtbank" rows="10" cols="60"></textarea>
          </div>
        </div>
        <button type="submit" name="tombol" value="true" class="btn btn-primary waves-effect waves-light pull-right">Submit</button>
      </form>
      <?php } else { ?>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Reff</th>
              <th>Keterangan</th>
              <th>Nilai</th>
              <th>Status Tiket</th>
              <th>Pembeli</th>
            </tr>
          </thead>
          <tbody>
          <?php 
            $arr_tiket = array();
            if (sizeof($data_tiket)>0) {
              for ($i=0;$i<sizeof($data_tiket);$i++) {
                $t_tanggal = $data_tiket[$i]["tanggal"];
                $t_nama = $data_tiket[$i]["nama"];
                $t_nilai = $data_tiket[$i]["n_nilai"];
                $t_bayar = $data_tiket[$i]["kode_bayar"];
                $arr_tiket[$t_nilai][] = array("nama"=>$t_nama,"tanggal"=>$t_tanggal,"bayar"=>$t_bayar);
              }
            }
            
            if (sizeof($data_rekon)>0) {
              for ($i=0;$i<sizeof($data_rekon);$i++) {
                $tanggal = $data_rekon[$i][0];
                $reff = $data_rekon[$i][1];
                $keterangan = $data_rekon[$i][2];
                $nilai = $data_rekon[$i][4];
                
                $xbayar = 1; $xpembeli = "";
                if (isset($arr_tiket[$nilai])) {
                  if (sizeof($arr_tiket[$nilai])>0) {
                    for ($j=0;$j<sizeof($arr_tiket[$nilai]);$j++) {
                      if ($j>0) { $xpembeli .= "<br>"; }
                      $xpembeli .= "- ".$arr_tiket[$nilai][$j]["nama"] . ", " . $arr_tiket[$nilai][$j]["tanggal"];
                      if ($arr_tiket[$nilai][$j]["bayar"]=="0") { $xbayar = 0; $xpembeli .= ", NOK"; } else { $xpembeli .= ", OK"; }
                    }
                  }
                } else {
                  $xbayar = 2;
                }
                
                if ($xbayar==0) {
                  $status_bayar = '<button type="button" class="btn btn-block btn-warning">Belum</button>';
                } else if ($xbayar==2) {
                  $status_bayar = '<button type="button" class="btn btn-block btn-default">Tidak Ada</button>';
                } else {
                  $status_bayar = '<button type="button" class="btn btn-block btn-success">Sudah</button>';
                }
          ?>
            <tr>
              <td><?= ($i+1); ?></td>
              <td><?= $tanggal; ?></td>
              <td><?= $reff; ?></td>
              <td><?= $keterangan; ?></td>
              <td><?= number_format($nilai); ?></td>
              <td><?= $status_bayar; ?></td>
              <td><?= $xpembeli; ?></td>
            </tr>
          <?php
              }
            }
          ?>
          </tbody>
        </table>
      </div>
      <?php } ?>
    </div>
  </div>
</div>