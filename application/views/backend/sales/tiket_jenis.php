<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Data Jenis Tiket</h4>
    <p class="text-muted page-title-alt">Pesta Wirausaha 2019</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Jenis Tiket
      </li>
    </ol>
  </div>
</div>

<?php if ($site_form=="list") { ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Detail Jenis Tiket</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("sales/jnstiket/tambah"); ?>" class="btn btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Jenis tiket</th>
              <th class="text-center">Harga tiket</th>
              <th class="text-center">Deskripsi</th>
              <th class="text-center">Status</th>
              <th class="text-center"></th>
            </tr>
          </thead>
          <tbody>
            <?php
              if(count($jenis_tiket) > 0){
                for($i=0; $i<count($jenis_tiket); $i++){
            ?>
            <tr>
              <td class="text-center"><?= $jenis_tiket[$i]['id']; ?></td>
              <td><?= $jenis_tiket[$i]['nama']; ?></td>
              <td class="text-center"><?= number_format($jenis_tiket[$i]['harga'],0,',','.'); ?></td>
              <td><?= $jenis_tiket[$i]['deskripsi']; ?></td>
              <td><?= ($jenis_tiket[$i]['status']=="1")? "Aktif" : "Tidak Aktif"; ?></td>
              <td>
                <a href="<?= base_url("sales/jnstiket/ubah/".$jenis_tiket[$i]['id']); ?>" class="btn btn-warning">
                  <i class="fa fa-pencil"></i> Ubah
                </a>
                <a href="<?= base_url("sales/jnstiket/delete/".$jenis_tiket[$i]['id']); ?>" class="btn btn-danger">
                  <i class="fa fa-trash"></i> Hapus
                </a>
              </td>
            </tr>
            <?php
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php
} else { 
  $nama = ""; $harga = "0"; $status = "0";  $waktu = ""; $komisi = "0"; $daerah = "0"; 
  $hadiah = "0"; $deskripsi = ""; $url = ""; $reseller = ""; $jumlah = "0";
  if (sizeof($data_jenis)>0) {
    $nama = $data_jenis[0]["nama"];
    $harga = $data_jenis[0]["harga"];
    $status = $data_jenis[0]["status"];
    $waktu = $data_jenis[0]["waktu"];
    $komisi = $data_jenis[0]["komisi"];
    $daerah = $data_jenis[0]["daerah"];
    $hadiah = $data_jenis[0]["hadiah"];
    $deskripsi = $data_jenis[0]["deskripsi"];
    $url = $data_jenis[0]["url"];
    $reseller = $data_jenis[0]["reseller"];
    $jumlah = $data_jenis[0]["jumlah"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Jenis Tiket"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Jenis Tiket"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Jenis Tiket"; }
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?= $xjudul; ?></b></h4>
      <hr>

      <form class="form-horizontal" method="post" action="<?= base_url("sales/jnstiket/".$aksi."/".$jenisid); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Nama Jenis Tiket</label>
          <div class="col-sm-10">
            <input type="text" name="nama" class="form-control" placeholder="Nama Jenis Tiket" value="<?= $nama; ?>">
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Harga Tiket</label>
          <div class="col-sm-10"><input type="text" name="harga" class="form-control" placeholder="Harga" value="<?= $harga; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Status</label>
          <div class="col-sm-10">
            <select name="status" class="form-control">
              <option value="">Pilih</option>
              <option value="1">Aktif</option>
              <option value="0">Tidak AKtif</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Waktu</label>
          <div class="col-sm-10"><input type="text" name="waktu" class="form-control" placeholder="Waktu (Jumlah Hari)" value="<?= $waktu; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Komisi</label>
          <div class="col-sm-10"><input type="text" name="komisi" class="form-control" placeholder="Komisi" value="<?= $waktu; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Daerah</label>
          <div class="col-sm-10"><input type="text" name="daerah" class="form-control" placeholder="Daerah" value="<?= $daerah; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Hadiah</label>
          <div class="col-sm-10"><input type="text" name="hadiah" class="form-control" placeholder="hadiah" value="<?= $hadiah; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Jumlah</label>
          <div class="col-sm-10"><input type="text" name="jumlah" class="form-control" placeholder="jumlah" value="<?= $jumlah; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Deskripsi</label>
          <div class="col-sm-10">
            <textarea name="deskripsi" class="form-control" placeholder="Deskripsi"><?= $deskripsi; ?></textarea>
          </div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("sales/jnstiket"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>