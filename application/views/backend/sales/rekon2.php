<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekening Bank</h4>
    <p class="text-muted page-title-alt">Informasi Mutasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekening Bank
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekon Transaksi Rekening BSM</b></h4>
      <hr>
      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Reff</th>
              <th>Keterangan</th>
              <th>Nilai</th>
              <th>Status Tiket</th>
              <th>Pembeli</th>
            </tr>
          </thead>
          <tbody>
          <?php 
	$arr_bank = array();
	if (sizeof($data_bank)>0) {
		for ($i=0;$i<sizeof($data_bank);$i++) {
			$b_id = $data_bank[$i]["id"];
			$b_tanggal = $data_bank[$i]["tanggal"];
			$b_deskripsi = $data_bank[$i]["deskripsi"];
			$b_kredit = trim($data_bank[$i]["kredit"]);
			
			if ($b_kredit>0) {
				$arr_bank[$b_kredit][] = array("id"=>$b_id,"tanggal"=>$b_tanggal,"deskripsi"=>$b_deskripsi,"nilai"=>$b_kredit);
			}
		}
		
	}
	$nt1 = 0; $nt2 = 0; $nt3 = 0; $nt4 = 0; $nt5 = 0; 
	$xt1 = ""; $xt2 = ""; $xt3 = ""; $xt4 = ""; $xt5 = "";
	if (sizeof($data_tiket)>0) {
		for ($i=0;$i<sizeof($data_tiket);$i++) {
			$t_id = $data_tiket[$i]["id"];
			$t_invoice = $data_tiket[$i]["invoice"];
			$t_tanggal = $data_tiket[$i]["tanggal"];
			$t_nama = $data_tiket[$i]["nama"];
			$t_usaha = $data_tiket[$i]["usaha"];
			$t_hp = $data_tiket[$i]["no_hp"];
			$t_bayar = $data_tiket[$i]["kode_bayar"];
			$t_nilai = trim($data_tiket[$i]["n_nilai"]);
			
			if ($t_nilai<1000000) {
				
			$xt = "<tr><td>".($i+1)."</td><td>".$t_tanggal."</td><td>".$t_invoice."</td><td>".$t_nama." ".$t_usaha." $t_hp ($t_id)</td><td>".$t_bayar."</td><td>".$t_nilai."</td>";
			$xt .= "<td>";
			$jt = 0;
			if (isset($arr_bank[$t_nilai])) {
				$jt = sizeof($arr_bank[$t_nilai]);
				if (sizeof($arr_bank[$t_nilai])>0) {
					for ($j=0;$j<sizeof($arr_bank[$t_nilai]);$j++) {
						$b_id = $arr_bank[$t_nilai][$j]["id"];
						$b_tanggal = $arr_bank[$t_nilai][$j]["tanggal"];
						$b_deskripsi = $arr_bank[$t_nilai][$j]["deskripsi"];
						$b_nilai = $arr_bank[$t_nilai][$j]["nilai"];
						
						$xt .= "- " . $b_tanggal . " " . $b_deskripsi . " " . $b_nilai . " ($b_id) ";
						if ($t_bayar=="1") {
							$xt .= " (<a href=\"".site_url("sales/pasang/".$t_id."/".$b_id)."\">Pasang</a>)";
						}
						$xt .= "<br>";
						
						/*if (($jt==1) && ($t_bayar==1)) { 
							$this->PW_model->upd_mutasi($b_id,"1",$t_invoice); 
							$this->PW_model->upd_tiket_bayar($t_id,$b_id,$b_tanggal);
						}*/
					}
				}
			}
			$xt .= "</td></tr>";
				
			if ($jt==0) {
				if ($t_bayar==0) {
					$xt1 .= $xt; $nt1++;
				} else {
					$xt2 .= $xt; $nt2++;
				}
			} else if ($jt==1) {
				$xt3 .= $xt; $nt3++;
			} else if ($jt==2) {
				$xt4 .= $xt; $nt4++;
			} else {
				$xt5 .= $xt; $nt5++;
			}
			
			}
		 

		}
	}
	$bt = "<tr><td colspan=7>&nbsp;</td></tr>";
	echo $xt1 . $bt . $xt2 . $bt . $xt3 . $bt . $xt4 . $bt . $xt5;
?>				
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>