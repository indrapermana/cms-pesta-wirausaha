<!doctype html>
<html>
<head>
 <meta charset="utf-8">
 <title>Waiting List Expo Pesta Wirausaha 2019</title>
 <style>
 .invoice-box{
	max-width:800px;
	margin:auto;
	padding:30px;
	border:1px solid #eee;
	box-shadow:0 0 10px rgba(0, 0, 0, .15);
	font-size:16px;
	line-height:24px;
	font-family:"Helvetica Neue", "Helvetica", Helvetica, Arial, sans-serif;
	color:#555;
 }
 .invoice-box table{
	width:100%;
	line-height:inherit;
	text-align:left;
 }
 .invoice-box table td{
	padding:5px;
	vertical-align:top;
 }
 .invoice-box table tr td:nth-child(2){
	text-align:right;
 }
 .invoice-box table tr.top table td{
	padding-bottom:20px;
 }
 .invoice-box table tr.top table td.title{
	font-size:45px;
	line-height:45px;
	color:#333;
 }
 .invoice-box table tr.information table td{
	padding-bottom:40px;
 }
 .invoice-box table tr.heading td{
	background:#eee;
	border-bottom:1px solid #ddd;
	font-weight:bold;
 }
 .invoice-box table tr.details td{
	padding-bottom:20px;
 }
 .invoice-box table tr.item td{
	border-bottom:1px solid #eee;
 }
 .invoice-box table tr.item.last td{
	border-bottom:none;
 }
 .invoice-box table tr.total td:nth-child(2){
	border-top:2px solid #eee;
	font-weight:bold;
 }
 @media only screen and (max-width: 600px) {
	.invoice-box table tr.top table td{
	width:100%;
	display:block;
	text-align:center;
 }
 .invoice-box table tr.information table td{
	width:100%;
	display:block;
	text-align:center;
 }
 
</style>
</head>

<body>
<?php
	$invoice = ""; $kodepesan = ""; $tanggal = ""; $nama = ""; $email = ""; $hp = "";
	if (sizeof($data_expo)>0) {
		$invoice = $data_expo[0]["invoice"];
		$kodepesan = $data_expo[0]["kode_pesan"];
		$tanggal = $data_expo[0]["stanggal"];
		$nama = $data_expo[0]["nama"];
		$email = $data_expo[0]["email"];
		$hp = $data_expo[0]["no_hp"];
	}
?>
 <div class="invoice-box">
 <table cellpadding="0" cellspacing="0">
 <tr class="top">
 <td colspan="2">
 <table>
 <tr>
 <td class="title"><img src="http://www.pestawirausaha.com/themes/backend/img/logo.png" style="width:100%; max-width:200px;"></td>
 <td> Invoice: #<?php echo $invoice; ?><br>Tanggal: <?php echo $tanggal; ?><br> </td>
 </tr>
 </table>
 </td>
 </tr>
 <tr class="information">
 <td colspan="2">
 <table>
 <tr>
 <td><?php echo $nama . "<br>" . $email . "<br>" . $hp; ?></td>
 <td>Panitia Pesta Wirausaha 2019<br>Divisi Expo<br>expo@pestawirausaha.com</td>
 </tr>
 </table>
 </td>
 </tr>
<tr>
<td colspan="2">

Terima kasih sudah mendaftar pada waiting list stand expo di acara Pesta Wirausaha 2019. <br><br>

<b>Komunitas Tangan Di Atas (TDA)</b> memberikan kesempatan untuk mengenalkan brand dan produk anda dalam "Business Expo" <b>Pesta Wirausaha Nasional 2019</b><br><br>

Catat tanggal dan tempatnya:<br>
Tgl : 25, 26, & 27 januari 2019<br>
@Ecovantion ancol<br><br> 

<b>Pesta Wirausaha</b> adalah acara besar dari tahun ke tahun yang selalu mampu menarik 3000-5000 pengunjung perharinya. Pastikan produk dan usaha anda ikut berpartisipasi di <b>Pesta Wirausaha 2019</b> kali ini, serta raih potensi market dari seluruh pengunjung yang hadir.<br><br>

Ketentuan Stand:<br>
- Ukuran 2x2 m<br>
- Fasilitas 1 meja, 2 kursi & listrik 450 w<br>
- Peserta stand bisa ikut acara Business Clinic selama 3 hari<br>
- Tiket Seminar Pesta Wirausaha<br><br>

Harga untuk 3 hari:<br>
- Rp 3.500.000 (stand regular)<br>
- Rp 5.000.000 (stand premium)<br><br>

Harga Early Bird sampai tanggal 31 Oktober 2018:<br>
- Rp 3.000.000 (stand regular)<br>
- Rp 4.000.000 (stand premium)<br><br>

<b>Penawaran Khusus :</b><br><br>

<b>PREMIUM BOOTH</b><br>
- Early Bird 4 juta<br>
- Normal 5 juta<br>
*termasuk: 3 tiket PW, 3 tiket masuk Ancol selama 3 hari, materi promosi didalam Goody Bags, 1 meja, 2 kursi dan listrik 450 watt<br>
- Stand pojok/Dua muka<br><br>

<b>REGULER BOOTH</b><br>
- Early Bird 3 juta<br>
- Normal 3,5 juta<br>
*termasuk: 1 tiket PW, 1 tiket masuk Ancol selama 3 hari, 1 meja, 2 kursi dan listrik 450 watt<br><br>

Informasi layout :<br><br>

<a href="http://www.pestawirausaha.com/expo/map">www.pestawirausaha.com/expo/map</a><br><br>

Pemesanan Lokasi Stand :<br><br>

<a href="http://www.pestawirausaha.com/expo/pesan/<?php echo $invoice . "/" . $kodepesan; ?>">www.pestawirausaha.com/expo/pesan</a><br><br>


WA<br><br>

1. Zona Kuliner<br>
Sobirin <a href="https://api.whatsapp.com/send?phone=081315350699">081315350699</a><br><br>

2. Zona Multi Produk<br>
 Fitra sony <a href="https://api.whatsapp.com/send?phone=08995708330">08995708330</a><br><br>

<a href="http://www.pestawirausaha.com/expo">www.pestawirausaha.com/expo</a><br><br>

#PWTDA2019<br>
#Pestawirausaha<br>
#KolaborAksi<br><br>

</td>
</tr>
 </table>
 </div>
</body>
</html>