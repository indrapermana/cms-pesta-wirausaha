<html xmlns="http://www.w3.org/1999/xhtml">
 <head>
  <title>Voucher Hotel E-Tiket - Pesta Wirausaha 2019</title>
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <style type="text/css">
    body {
      font-family: Arial, Helvetica, sans-serif;
      margin: 0;
      padding: 0;
      color: #434343;
      -webkit-text-size-adjust: 100%;
      -ms-text-size-adjust: 100%;
      background: #e6eaed;
    }
    img {
      border: 0 none;
      height: auto;
      line-height: 100%;
      outline: none;
      text-decoration: none;
      -ms-interpolation-mode: bicubic;
    }
    a img {
      border: 0 none;
    }
    .imageFix {
      display: block;
    }
    table,
    td {
      border-collapse: collapse;
    }
    table {
      mso-table-lspace: 0pt;
      mso-table-rspace: 0pt;
    }
    h1,
    h2,
    h3,
    h4,
    h5,
    h6 {
      font-weight: 400;
      margin: 0;
    }
    p {
      font-size: 16px;
      line-height: 1.5;
    }
    #bodyTable {
      height: 100% !important;
      margin: 0;
      padding: 0;
      width: 100% !important;
      -webkit-text-size-adjust: 100%;
      font-family: Arial, Helvetica, sans-serif;
    }
    .box {
      margin-top: 20px;
      margin-bottom: 20px;
      background: #fff;
    }
    .rounded {
      border-radius: 5px;
    }
    .header {
      font-size: 32px;
      color: #434343;
    }
    .textOutside {
      font-size: 13px;
      line-height: 18px;
      color: #727272;
      padding-left: 20px;
      padding-right: 20px;
    }
    .textOutside a {
      color: #727272;
    }
    .innerPadding {
      padding: 20px 20px 20px 20px;
    }
    .bold {
      font-weight: 700;
    }
    .footer {
      background: #1ba0e2;
      /*padding: 50px 20px;*/
      text-align: center;
    }

    .ExternalClass {
      width: 100%;
    }
    .ExternalClass,
    .ExternalClass p,
    .ExternalClass span, 
    .ExternalClass font,
    .ExternalClass td,
    .ExternalClass div {
      line-height: 100%;
    }
    .wrapper {
      width: 100% !important;
      max-width: 450px;
    }
    .templateSidebar {
      display: block !important;
      /*width:33.33% !important;*/
      width: 100% !important;
    }
    .templateBody {
      display: block !important;
      /*width:66.66% !important;*/
      width: 100% !important;
    }
    .templateSidebarTitle {
      display: block !important;
      /*width:33.33% !important;*/
      width: 100% !important;
    }
    .templateBodyTitle {
      display: block !important;
      /*width:66.66% !important;*/
      width: 100% !important;
    }
    @media only screen and (max-width: 480px) {
      .wrapper {
        width: 90% !important;
      }
      .templateSidebar,
      .templateBody,
      .templateSidebarTitle,
      .templateBodyTitle {
        display: block !important;
        width: 90% !important;
      }
      .sidebarContent {
        font-size: 16px !important;
        width: 100%;
      }
      .bodyImage {
        height: auto !important;
        max-width: 480px !important;
        width: 100% !important;
      }
      .bodyContent {
        font-size: 18px !important;
        width: 100%;
      }
    }
   
     @media only screen {
        html {
          min-height: 100%;
          background: #e6eaed;
        }
      }
       
      @media only screen and (max-width: 616px) {
        table.body img {
          width: auto;
          height: auto;
        }
     
        table.body center {
          min-width: 0 !important;
        }
     
        table.body .container {
          width: 100% !important;
        }
     
        table.body .columns {
          height: auto !important;
          -moz-box-sizing: border-box;
          -webkit-box-sizing: border-box;
          box-sizing: border-box;
          padding-left: 16px !important;
          padding-right: 16px !important;
        }
     
        table.body .columns .columns {
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
     
        table.body .collapse .columns {
          padding-left: 0 !important;
          padding-right: 0 !important;
        }
     
        th.small-1 {
          display: inline-block !important;
          width: 8.33333% !important;
        }
     
        th.small-2 {
          display: inline-block !important;
          width: 16.66667% !important;
        }
     
        th.small-6 {
          display: inline-block !important;
          width: 50% !important;
        }
     
        th.small-10 {
          display: inline-block !important;
          width: 83.33333% !important;
        }
     
        th.small-11 {
          display: inline-block !important;
          width: 91.66667% !important;
        }
     
        th.small-12 {
          display: inline-block !important;
          width: 100% !important;
        }
     
        .columns th.small-12 {
          display: block !important;
          width: 100% !important;
        }
      }
  </style>
 </head> 
 <?php
	$nama = ""; $tanggal = ""; $jenis = ""; $ktp = ""; $jumlah = ""; $keterangan = ""; $xstatus = "";
	$checkin = ""; $checkout = ""; $paket = ""; $xroomtype = ""; $xbreakfast; $kodebooking = "";
	if (sizeof($data_hotel)>0) {
		$nama = $data_hotel[0]["nama_lengkap"];
		$kodebooking = $data_hotel[0]["kode_booking"];
		$tanggal = $data_hotel[0]["tanggal"];
		$jenis = $data_hotel[0]["jenis_kamar"];
		$roomtype = $data_hotel[0]["room_type"];
		$breakfast = $data_hotel[0]["breakfast"];
		$ktp = $data_hotel[0]["no_ktp"];
		$keterangan = $data_hotel[0]["keterangan"];
		$jumlah = $data_hotel[0]["jml_kamar"];
		$status = $data_hotel[0]["status_bayar"];
		$checkin = $data_hotel[0]["cekin"];
		$checkout = $data_hotel[0]["cekout"];
		
		if ($status=="0") { $xstatus = "Belum Lunas"; }
		else { $xstatus = "Lunas"; }
		
		if ($jenis=="1") { $paket = "Paket 3 Malam"; }
		if ($jenis=="2") { $paket = "Paket 2 Malam"; }
		if ($jenis=="3") { $paket = "Paket 1 Malam"; }
		
		if ($roomtype=="1") { $xroomtype = "R1 - 1 Bedroom"; }
		if ($roomtype=="2") { $xroomtype = "R2 - 2 Bedroom"; }
		
		if ($breakfast=="0") { $xbreakfast = "Not included"; }
		if ($breakfast=="1") { $xbreakfast = "Included"; }
	}

 ?>
 
 <body>
  <table width="100%" height="100%" border="0" cellspacing="0" cellpadding="0" style="background:#e6eaed;">
   <tbody>
    <tr>
     <td align="center">
      <table border="0" cellpadding="0" cellspacing="0" style="background:#e6eaed;height:62px;width:90%;max-width:450px;">
       <tbody>
        <tr>
         <td align="center" valign="middle" style="padding-bottom:x;padding-top:50px;"> 
		 <img src="http://hotel.pestawirausaha.com/gambar/logo_kolaboraksi2.png" style="padding-top: 48px;"> </td>
        </tr>
       </tbody>
      </table> </td>
    </tr>
    <tr>
     <td align="center">
      <table border="0" align="center" cellpadding="0" cellspacing="0" style="width:90%;max-width:450px;">
       <tbody>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td style="background-image: url(http://hotel.pestawirausaha.com/gambar/strip.png);background-repeat:repeat-x no-repeat;width:100%;height:4px;background-color:#1ba0e2;display:block;"> </td>
            </tr>
           </tbody>
          </table> </td> 
        </tr>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td align="center" valign="middle" style="padding-top:27px;padding-left:26px;padding-right:26px; padding-bottom:20px">
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="padding-bottom:0px;">
               <tbody>
                <tr>
                 <td align="left" valign="middle">
                  <table align="left" border="0" cellpadding="0" cellspacing="0">
                   <tbody>
                    <tr>
                     <td align="left" valign="middle" style="padding-right:10px;"> </td>
                     <td>
                      <table align="left" border="0" cellpadding="0" cellspacing="0">
                       <tbody>
                        <tr>
                         <td style="font-size:14px;color:#727272;padding-bottom:3px;">Kontak </td>
                        </tr>
                        <tr>
                         <td style="font-size:18px;color:#1ba0e2;">Riawan (0818 915 000)</td>
                        </tr>
                       </tbody>
                      </table> </td>
                    </tr>
                   </tbody>
                  </table> </td>
                </tr>
               </tbody>
              </table>
              <table align="left" border="0" cellpadding="0" cellspacing="0" style="margin-left:45px;margin-top:10px;">
               <tbody>
                <tr>
                 <td align="left" valign="middle">
                  <table align="left" border="0" cellpadding="0" cellspacing="0">
                   <tbody>
                    <tr>
                     <td>
                      <table align="left" border="0" cellpadding="0" cellspacing="0">
                       <tbody>
                        <tr>
                         <td style="font-size:14px;color:#727272;padding-bottom:3px;">Kode Booking</td>
                        </tr>
                        <tr>
                         <td style="font-size:18px;color:#1A1A1A;"><?php echo $kodebooking; ?></td>
                        </tr>
                       </tbody>
                      </table> </td>
                    </tr>
                   </tbody>
                  </table> </td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
        <tr style="background:#ffffff;">
         <td>
          <table align="center" border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td>
              <table align="center" border="0" cellpadding="0" cellspacing="0" style="width:100%;">
               <tbody>
                <tr>
                 <td style="background-image: url(http://hotel.pestawirausaha.com/gambar/dashed-dot.jpg);background-repeat:repeat-x no-repeat;width:100%;height:1px;background-color:#dedede;display:block;"> </td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
        <tr>
         <td align="center" valign="top" width="100%">
          <div style="background: #fff;border-radius: 4px;border-bottom: 2px solid #dadada; overflow: hidden;">
          </div>
          <table border="0" cellpadding="0" cellspacing="0" width="100%">
           <tbody>
            <tr>
             <td align="center" valign="top">
              <div>
               <!-- GREETINGS -->
               <div style="padding-top: 38px;background:#ffffff;">
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                 <tbody>
                  <tr>
                   <td align="center" valign="top"> <h1 style="font-weight:normal;font-size:30px;color:#434343;margin:0px;padding:0px 16px;">Voucher Hotel!</h1> </td>
                  </tr>
                  <tr>
                   <td align="left" valign="top" style="padding:0px 16px;">
                    <div style="width:100%;height:38px;display:block;"></div> <h3 style="font-size:16px;margin:0px;padding:0px;color:#434343;font-weight:bold;"><strong>Hai kak <?php echo $nama; ?>,</strong></h3>
                    <div style="width:100%;height:28px;display:block;"></div> <p style="line-height:25px;font-size:16px;padding:0px;margin:0px;">Pesanan kamar hotel anda telah dikonfirmasi.</p>
                    <div style="width:100%;height:18px;display:block;"></div> </td>
                  </tr>
                 </tbody>
                </table>
               </div>
               <!-- ISSUANCE DETAIL -->
               <div style="background:#ffffff; margin-bottom: 16px; border-bottom-left-radius: 4px;border-bottom-right-radius: 4px;border-bottom: 2px solid #dadada;">
                <div style="padding: 0 16px; margin-bottom: 10px">
                 <table border="0" cellpadding="0" cellspacing="0" style="margin:0px;padding: 0 16px;width:100%;">
                  <tbody>
                   <tr>
                    <td align="left" valign="middle" width="100%;"> <span style="font-size:15px;text-transform:uppercase;color:#1ba0e2;font-weight:bold;margin:0px;padding:0px;">Informasi Pesanan</span> </td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <div style="padding: 0 16px;">
                 <table border="0" cellpadding="0" cellspacing="0" style="border-bottom-width:1px;border-bottom-style:solid;border-bottom-color:#656262;border-top-width:1px;border-top-style:solid;border-top-color:#656262;width:100%;padding: 0 16px;">
                  <tbody>
                   <tr>
                    <td align="left" valign="top" width="100%" style="padding-top:10px;padding-bottom:10px;"> <h2 style="font-size:16px;line-height:16px;font-weight:bold;width:100%;margin:0px;padding:0px;margin-top:10px">Aston Marina Ancol</h2>
                     <div style="margin:0px;padding:0px;width:100%;">
                      <img src="http://hotel.pestawirausaha.com/gambar/4.png" alt="rating">
                     </div> <p style="font-size:15px;color:#737374;margin:0px;padding:0px;margin-bottom:5px;">Marina Mediterania, Tower A. Jl. Lodan Raya No. 2A Tower A, Ancol, Pademangan, Jakarta, Indonesia, 14430</p> </td>
                   </tr>
                  </tbody>
                 </table>
                </div>
                <table border="0" cellpadding="0" cellspacing="0" width="100%">
                 <tbody>
                  <tr>
                   <td align="left" style="width:100%;"> 
                    <div style="padding: 0 16px;">
                     <p style="font-size:14px;margin-top:15px;">Nama:<br><span style="font-weight:bold;font-size:16px;color:#1ba0e2;"><?php echo $nama; ?></span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">No KTP: <span style="font-weight:bold;"><?php echo $ktp; ?></span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">Kode Booking: <span style="font-weight:bold;"><?php echo $kodebooking; ?></span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">Paket: <span style="font-weight:bold;"><?php echo $paket; ?></span></p>
                     <p style="font-size:13px;margin:0px;padding:0px;">Dipesan melalui PestaWirausaha</p>
                     <br>
                    </div>
                    <div style="padding: 0 16px;">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;margin-bottom:20px;">
                      <tbody>
                       <tr>
                        <td align="left" style="width:50%;"> <p style="font-size:13px;margin:0px;padding:0px;width:100%;">Check-in</p> <p style="font-size:14px;font-weight:bold;margin:0px;padding:0px;width:100%;"><?php echo $checkin; ?></p> <p style="font-size:12px;margin:0px;padding:0px;width:100%;color:#1ba0e2;">from 14:00</p> </td>
                        <td align="left" style="width:50%;"> <p style="font-size:13px;margin:0px;padding:0px;width:100%;">Check-out</p> <p style="font-size:14px;font-weight:bold;margin:0px;padding:0px;width:100%;"><?php echo $checkout; ?></p> <p style="font-size:12px;margin:0px;padding:0px;width:100%;color:#1ba0e2;">to 12:00</p> </td>
                       </tr>
                      </tbody> 
                     </table>
                    </div>
                    <div style="padding: 0 16px;">
                     <table border="0" cellpadding="0" cellspacing="0" width="100%" style="margin-top:20px;margin-bottom:20px;">
                      <tbody>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Room type</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"><?php echo $xroomtype; ?></p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Number of rooms</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"><?php echo $jumlah; ?> Room(s)</p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Room Capacity</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;">4 Guest per room</p> </td>
                       </tr>
                       <tr>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Breakfast</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"><?php echo $xbreakfast; ?></p> </td>
                       </tr>
                       <tr style="">
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Facilities</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> <img src="http://hotel.pestawirausaha.com/gambar/wifi-icon.png" style="width:11px;height:auto;">&nbsp;Free Wifi </p> </td>
                       </tr>
                       <tr style="">
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Note</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> <?php echo $keterangan; ?> </p> </td>
                       </tr>	
                       <tr style="">
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="color:#737374;font-size:13px;margin:0px;padding:0px;">Status Pembayaran</p> </td>
                        <td align="left" style="width:50%;margin:0px;padding:0px;padding-bottom:3px;" valign="top"> <p style="font-size:13px;margin:0px;padding:0px;"> <?php echo $xstatus; ?> </p> </td>
                       </tr>						   
                      </tbody>
                     </table>
                    </div> </td>
                  </tr>
                 </tbody>
                </table>
               </div>
               <!-- END of ISSUANCE DETAIL -->
               <!-- CROSSSELL A&A -->
              </div></td>
            </tr>
			
            <tr>
             <td style="background-color:#e6eaed">
              <table class="spacer float-center" style="Margin:0 auto;border-collapse:collapse;border-spacing:0;float:none;margin:0 auto;padding:0;text-align:center;vertical-align:top;width:100%">
               <tbody>
                <tr style="padding:0;text-align:left;vertical-align:top">
                 <td height="16px" style="-moz-hyphens:auto;-webkit-hyphens:auto;Margin:0;border-collapse:collapse!important;color:#434343;font-size:16px;font-weight:normal;hyphens:auto;line-height:16px;margin:0;mso-line-height-rule:exactly;padding:0;text-align:left;vertical-align:top;word-wrap:break-word">&nbsp;</td>
                </tr>
               </tbody>
              </table> </td>
            </tr>
            <!-- CROSSSELL F&B --> 
           </tbody>
          </table>

          <table border="0" cellpadding="0" cellspacing="0" style="background:#e6eaed;width:100%;max-width:450px;">
           <tbody>
            <tr>
             <td align="center" valign="middle" style="background:#e6eaed;padding-top:25px;"> <p class="textOutside"><br>Pesta Wirausaha Nasional TDA 2019. All Rights Reserved.<br><br> </p> </td>
            </tr>
           </tbody>
          </table> </td>
        </tr>
       </tbody>
      </table>  </td>
    </tr>
   </tbody>
  </table>
  <img style="display:block;font-size:0px;line-height:0em;" src="http://hotel.pestawirausaha.com/track/<?php echo $kodebooking; ?>" alt="" width="1" height="1" border="0">
 </body>
</html>