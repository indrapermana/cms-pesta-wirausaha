<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekening Bank</h4>
    <p class="text-muted page-title-alt">Informasi Mutasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekap Rekening Bank
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap Rekening</b></h4>
      <div class="filtter-right">
        <form method="post" action="<?php echo site_url("bank/rekap"); ?>">
          <select name="tipe" onchange="this.form.submit();" class="form-control">
            <option value="0"<?php if ($tipetrans=="0") { echo " selected"; } ?>>Non PW</option>
            <option value="1"<?php if ($tipetrans=="1") { echo " selected"; } ?>>Tiket Online</option>
            <option value="2"<?php if ($tipetrans=="2") { echo " selected"; } ?>>Tiket Group</option>
            <option value="3"<?php if ($tipetrans=="3") { echo " selected"; } ?>>Expo</option>
            <option value="4"<?php if ($tipetrans=="4") { echo " selected"; } ?>>Sponsor</option>
            <option value="7"<?php if ($tipetrans=="7") { echo " selected"; } ?>>Sponsor Internal</option>
            <option value="5"<?php if ($tipetrans=="5") { echo " selected"; } ?>>Deposit</option>
            <option value="6"<?php if ($tipetrans=="6") { echo " selected"; } ?>>Hotel</option>
            <option value="8"<?php if ($tipetrans=="8") { echo " selected"; } ?>>Entreprenerun</option>
          </select>
        </form>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>No Referensi</th>
              <th>Deskripsi</th>
              <th>Kredit</th>
              <th>Saldo</th>
              <th>Tipe</th>
              <th>Keterangan</th>
              <th>Settle Code</th>
            </tr>
          </thead>
          <tbody>
          <?php

            $t_nonpw = 0; $t_tiketpw = 0; $t_resellerpw = 0; $t_expopw = 0; $t_sponsorpw = 0; $t_sponinpw = 0; $t_depositpw = 0; $t_hotelpw = 0;
            $j_nonpw = 0; $j_tiketpw = 0; $j_resellerpw = 0; $j_expopw = 0; $j_sponsorpw = 0; $j_sponinpw = 0; $j_depositpw = 0; $j_hotelpw = 0;

            if (sizeof($data_rekening)>0) {
              $t_debet = 0; $t_kredit = 0; $t_saldoawal = 0; $t_saldoakhir = 0; $saldo = 0;
              for ($i=0;$i<sizeof($data_rekening);$i++) {
                $id = $data_rekening[$i]["id"];
                $tanggal = $data_rekening[$i]["tanggal"];
                $no_referensi = $data_rekening[$i]["no_referensi"];
                $deskripsi = $data_rekening[$i]["deskripsi"];
                $debet = $data_rekening[$i]["debet"];
                $kredit = $data_rekening[$i]["kredit"];
                //$saldo = $data_rekening[$i]["saldo"];
                $tipe_trans = $data_rekening[$i]["tipe_trans"];
                $keterangan = $data_rekening[$i]["keterangan"];
                $settlecode = $data_rekening[$i]["settle_code"];

                if ($kredit>0) {
                  $saldo += $kredit;

                  echo "<tr><td>".($i+1)."</td><td>".$tanggal."</td>";
                  echo "<td><a href=\"".site_url("manage/rekapbank/".$id."/0")."\">".$no_referensi."</a></td>";
                  echo "<td>".$deskripsi."</td><td align=right>".number_format($kredit)."</td><td align=right>".number_format($saldo)."</td><td>";

                if ($tipe_trans=="0") { 
                  echo "<button type=\"button\" class=\"btn bg-default btn-xs\">Non PW</button>"; 
                  $t_nonpw += $kredit; $j_nonpw++;
                } else if ($tipe_trans=="1") { 
                  echo "<button type=\"button\" class=\"btn bg-olive btn-xs\">Tiket PW</button>"; 
                  $t_tiketpw += $kredit; $j_tiketpw++;
                } else if ($tipe_trans=="2") { 
                  echo "<button type=\"button\" class=\"btn bg-orange btn-xs\">Tiket Group PW</button>"; 
                  $t_resellerpw += $kredit; $j_resellerpw++;
                } else if ($tipe_trans=="3") { 
                  echo "<button type=\"button\" class=\"btn bg-purple btn-xs\">Expo PW</button>"; 
                  $t_expopw += $kredit; $j_expopw++;
                } else if ($tipe_trans=="4") { 
                  echo "<button type=\"button\" class=\"btn btn-block btn-info btn-xs\">Sponsor PW</button>"; 
                  $t_sponsorpw += $kredit; $j_sponsorpw++;
                } else if ($tipe_trans=="7") { 
                  echo "<button type=\"button\" class=\"btn btn-block btn-info btn-xs\">Sponsor Internal PW</button>"; 
                  $t_sponinpw += $kredit; $j_sponinpw++;
                } else if ($tipe_trans=="5") { 
                  echo "<button type=\"button\" class=\"btn bg-maroon btn-xs\">Deposit PW</button>"; 
                  $t_depositpw += $kredit; $j_depositpw++;
                } else if ($tipe_trans=="6") { 
                  echo "<button type=\"button\" class=\"btn bg-blue btn-xs\">Hotel PW</button>"; 
                  $t_hotelpw += $kredit; $j_hotelpw++;
                }

                echo "</td><td>".$keterangan."</td><td>".$settlecode."</td>"; 	
                echo "</tr>";

                $t_debet += $debet;
                $t_kredit += $kredit;
                }
              }

              echo "<tr><td></td><td></td><td></td><td></td><td align=right>".number_format($t_kredit)."</td><td align=right>".number_format($saldo)."</td><td></td><td></td></tr>";		

            } else {

              echo "<tr><td colspan=6>Belum ada transaksi</td></tr>";

            }

          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>