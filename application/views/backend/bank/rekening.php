<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekening Bank</h4>
    <p class="text-muted page-title-alt">Informasi Mutasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rekening Bank
      </li>
    </ol>
  </div>
</div>

<?php 
$nama_rek = ""; $nomor_rek = ""; $nama_bank = "";
if (sizeof($data_bank)>0) {
	$nama_rek = $data_bank[0]["nama"];
	$nomor_rek = $data_bank[0]["nomor"];
	$nama_bank = $data_bank[0]["bank"];	
}
?>

<?php if ($site_form=="list") { ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekening <?= $nama_bank . " " . $nomor_rek . " an : ". $nama_rek; ?></b></h4>
      <div class="filtter-right">
        <form method="post" action="<?= base_url("bank/".$kodebank); ?>">
					<select name="periode" class="form-control" onchange="this.form.submit();">
						<option value="0418"<?php if ($periode=="0418") { echo " selected"; } ?>>April 2018</option>
						<option value="0518"<?php if ($periode=="0518") { echo " selected"; } ?>>Mei 2018</option>
						<option value="0618"<?php if ($periode=="0618") { echo " selected"; } ?>>Juni 2018</option>
						<option value="0718"<?php if ($periode=="0718") { echo " selected"; } ?>>Juli 2018</option>
						<option value="0818"<?php if ($periode=="0818") { echo " selected"; } ?>>Agustus 2018</option>
						<option value="0918"<?php if ($periode=="0918") { echo " selected"; } ?>>September 2018</option>
						<option value="1018"<?php if ($periode=="1018") { echo " selected"; } ?>>Oktober 2018</option>
						<option value="1118"<?php if ($periode=="1118") { echo " selected"; } ?>>November 2018</option>
						<option value="1218"<?php if ($periode=="1218") { echo " selected"; } ?>>Desember 2018</option>
						<option value="0119"<?php if ($periode=="0119") { echo " selected"; } ?>>Januari 2019</option>
						<option value="0219"<?php if ($periode=="0219") { echo " selected"; } ?>>Februari 2019</option>
					</select>
				</form>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>No Referensi</th>
              <th>Deskripsi</th>
              <th>Debet</th>
              <th>Kredit</th>
              <th>Saldo</th>
              <th>Tipe</th>
              <th>Keterangan</th>
            </tr>
          </thead>
          <tbody>
            <?php
              $t_nonpw = 0; $t_tiketpw = 0; $t_resellerpw = 0; $t_expopw = 0; $t_sponsorpw = 0; $t_sponinpw = 0; $t_depositpw = 0; $t_hotelpw = 0; $t_entrunpw = 0;
              $j_nonpw = 0; $j_tiketpw = 0; $j_resellerpw = 0; $j_expopw = 0; $j_sponsorpw = 0; $j_sponinpw = 0; $j_depositpw = 0; $j_hotelpw = 0; $j_entrunpw = 0;
              if (sizeof($data_rekening)>0) {
                $t_debet = 0; $t_kredit = 0; $t_saldoawal = 0; $t_saldoakhir = 0;
                for ($i=0;$i<sizeof($data_rekening);$i++) {
                  $id = $data_rekening[$i]["id"];
                  $tanggal = $data_rekening[$i]["tanggal"];
                  $no_referensi = $data_rekening[$i]["no_referensi"];
                  $deskripsi = $data_rekening[$i]["deskripsi"];
                  $debet = $data_rekening[$i]["debet"];
                  $kredit = $data_rekening[$i]["kredit"];
                  $saldo = $data_rekening[$i]["saldo"];
                  $tipe_trans = $data_rekening[$i]["tipe_trans"];
                  $keterangan = $data_rekening[$i]["keterangan"];
                  
                  if ($i==0) {
                    $t_saldoawal = $saldo-$kredit+$debet;
                    echo "<tr><td></td><td></td><td></td><td></td><td align=right></td><td align=right></td><td align=right>".number_format($t_saldoawal)."</td><td></td><td></td></tr>";
                  }
                  
                  echo "<tr><td>".($i+1)."</td><td>".$tanggal."</td>";
                  if ($user_id==3) {
                    echo "<td><a href=\"".site_url("bank/".$kodebank."/".$id)."\">".$no_referensi."</a></td>";
                  } else {
                    echo "<td>".$no_referensi."</td>";
                  }
                  echo "<td>".$deskripsi."</td><td align=right>".number_format($debet)."</td><td align=right>".number_format($kredit)."</td><td align=right>".number_format($saldo)."</td><td>";
                  if ($tipe_trans=="0") { 
                    echo "<button type=\"button\" class=\"btn bg-default btn-xs\">Non PW</button>"; 
                    $t_nonpw += $kredit; $j_nonpw++;
                  } else if ($tipe_trans=="1") { 
                    echo "<button type=\"button\" class=\"btn bg-olive btn-xs\">Tiket PW</button>"; 
                    $t_tiketpw += $kredit; $j_tiketpw++;
                  } else if ($tipe_trans=="2") { 
                    echo "<button type=\"button\" class=\"btn bg-orange btn-xs\">Tiket Group PW</button>"; 
                    $t_resellerpw += $kredit; $j_resellerpw++;
                  } else if ($tipe_trans=="3") { 
                    echo "<button type=\"button\" class=\"btn bg-purple btn-xs\">Expo PW</button>"; 
                    $t_expopw += $kredit; $j_expopw++;
                  } else if ($tipe_trans=="4") { 
                    echo "<button type=\"button\" class=\"btn btn-block btn-info btn-xs\">Sponsor PW</button>"; 
                    $t_sponsorpw += $kredit; $j_sponsorpw++;
                  } else if ($tipe_trans=="7") { 
                    echo "<button type=\"button\" class=\"btn btn-block btn-info btn-xs\">Sponsor Internal PW</button>"; 
                    $t_sponinpw += $kredit; $j_sponinpw++;
                  } else if ($tipe_trans=="5") { 
                    echo "<button type=\"button\" class=\"btn bg-maroon btn-xs\">Deposit PW</button>"; 
                    $t_depositpw += $kredit; $j_depositpw++;
                  } else if ($tipe_trans=="6") { 
                    echo "<button type=\"button\" class=\"btn bg-blue btn-xs\">Hotel PW</button>"; 
                    $t_hotelpw += $kredit; $j_hotelpw++;
                  } else if ($tipe_trans=="8") { 
                    echo "<button type=\"button\" class=\"btn bg-blue btn-xs\">Entrun PW</button>"; 
                    $t_entrunpw += $kredit; $j_entrunpw++;
                  }
                  echo "</td><td>".$keterangan."</td>"; 	
                  echo "</tr>";
                  
                  $t_debet += $debet;
                  $t_kredit += $kredit;
                }
                echo "<tr><td></td><td></td><td></td><td></td><td align=right>".number_format($t_debet)."</td><td align=right>".number_format($t_kredit)."</td><td align=right>".number_format($t_saldoawal-$t_debet+$t_kredit)."</td><td></td><td></td></tr>";		
              } else {
                echo "<tr><td colspan=9 align=center>Belum ada transaksi</td></tr>";
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap</b></h4>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tipe Transaksi</th>
              <th>Jml Transaksi</th>
              <th>Nilai Transaksi</th>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td>1</td>
              <td>Non PW</td>
              <td class="text-center"><?= $j_nonpw; ?></td>
              <td class="text-right"><?= number_format($t_nonpw,0); ?></td>
            </tr>
            <tr>
              <td>2</td>
              <td>Tiket PW</td>
              <td class="text-center"><?= $j_tiketpw; ?></td>
              <td class="text-right"><?= number_format($t_tiketpw,0); ?></td>
            </tr>
            <tr>
              <td>3</td>
              <td>Tiket Reseller PW</td>
              <td class="text-center"><?= $j_resellerpw; ?></td>
              <td class="text-right"><?= number_format($t_resellerpw,0); ?></td>
            </tr>
            <tr>
              <td>4</td>
              <td>Expo PW</td>
              <td class="text-center"><?= $j_expopw; ?></td>
              <td class="text-right"><?= number_format($t_expopw,0); ?></td>
            </tr>
            <tr>
              <td>5</td>
              <td>Sponsor PW</td>
              <td class="text-center"><?= $j_sponsorpw; ?></td>
              <td class="text-right"><?= number_format($t_sponsorpw,0); ?></td>
            </tr>
            <tr>
              <td>6</td>
              <td>Sponsor Internal PW</td>
              <td class="text-center"><?= $j_sponinpw; ?></td>
              <td class="text-right"><?= number_format($t_sponinpw,0); ?></td>
            </tr>
            <tr>
              <td>7</td>
              <td>Hotel PW</td>
              <td class="text-center"><?= $j_hotelpw; ?></td>
              <td class="text-right"><?= number_format($t_hotelpw,0); ?></td>
            </tr>
            <tr>
              <td>8</td>
              <td>Deposit PW</td>
              <td class="text-center"><?= $j_depositpw; ?></td>
              <td class="text-right"><?= number_format($t_depositpw,0); ?></td>
            </tr>
            <tr>
              <td>9</td>
              <td>Entrun PW</td>
              <td class="text-center"><?= $j_entrunpw; ?></td>
              <td class="text-right"><?= number_format($t_entrunpw,0); ?></td>
            </tr>				
            <tr>
              <td></td> 
              <td>Total</td>
              <td class="text-center"><?= $j_nonpw+$j_tiketpw+$j_resellerpw+$j_expopw+$j_sponsorpw+$j_sponinpw+$j_hotelpw+$j_depositpw+$j_entrunpw; ?></td>
              <td class="text-right"><?= number_format($t_nonpw+$t_tiketpw+$t_resellerpw+$t_expopw+$t_sponsorpw+$t_sponinpw+$t_hotelpw+$t_depositpw+$t_entrunpw,0); ?></td>
            </tr>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php
	$arr_rekap = array(); 
	if (sizeof($data_rekap_rekening)>0) {
		for ($i=0;$i<sizeof($data_rekap_rekening);$i++) {
			$arr_rekap[$data_rekap_rekening[$i]["periode"]][$data_rekap_rekening[$i]["tipe_trans"]] = $data_rekap_rekening[$i]["nilai"];
		}
	}

	function getdata($xarr,$periode,$tipetrans) {
		$hdata = 0;
		if (isset($xarr[$periode][$tipetrans])) { $hdata = $xarr[$periode][$tipetrans]; }
		return $hdata;
	}

	function total1($xarr,$periode) {
		$ttotal1 = 0;
		for ($i=0;$i<8;$i++) {
			if (isset($xarr[$periode][$i])) { $ttotal1 += $xarr[$periode][$i]; }
		}
		return $ttotal1;
	}

	function total2($xarr,$periode) {
		$ttotal2 = 0;
		for ($i=1;$i<8;$i++) {
			if (isset($xarr[$periode][$i])) { $ttotal2 += $xarr[$periode][$i]; }
		}
		return $ttotal2;
	}	

?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekap</b></h4>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tipe Transaksi</th>
              <th>Jul 18</th>
              <th>Ags 18</th>
              <th>Sep 18</th>
              <th>Okt 18</th>
              <th>Nov 18</th>
              <th>Des 18</th>
              <th>Jan 19</th>
              <th>Feb 19</th>
              <th>Total</th>
            </tr>
          </thead>
          <tbody>
          <tr>
              <td>1</td>
              <td>Non PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","0"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","0"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","0")+getdata($arr_rekap,"0818","0")+getdata($arr_rekap,"0918","0")+getdata($arr_rekap,"1018","0")+
                          getdata($arr_rekap,"1118","0")+getdata($arr_rekap,"1218","0")+getdata($arr_rekap,"0119","0")+getdata($arr_rekap,"0219","0"),0); ?></td>
            </tr>
            <tr>
              <td>2</td>
              <td>Tiket PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","1"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","1"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","1")+getdata($arr_rekap,"0818","1")+getdata($arr_rekap,"0918","1")+getdata($arr_rekap,"1018","1")+
                          getdata($arr_rekap,"1118","1")+getdata($arr_rekap,"1218","1")+getdata($arr_rekap,"0119","1")+getdata($arr_rekap,"0219","1"),0); ?></td>
            </tr>
            <tr>
              <td>3</td>
              <td>Tiket Reseller PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","2"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","2"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","2")+getdata($arr_rekap,"0818","2")+getdata($arr_rekap,"0918","2")+getdata($arr_rekap,"1018","2")+
                          getdata($arr_rekap,"1118","2")+getdata($arr_rekap,"1218","2")+getdata($arr_rekap,"0119","2")+getdata($arr_rekap,"0219","2"),0); ?></td>
            </tr>
            <tr>
              <td>4</td>
              <td>Expo PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","3"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","3"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","3")+getdata($arr_rekap,"0818","3")+getdata($arr_rekap,"0918","3")+getdata($arr_rekap,"1018","3")+
                          getdata($arr_rekap,"1118","3")+getdata($arr_rekap,"1218","3")+getdata($arr_rekap,"0119","3")+getdata($arr_rekap,"0219","3"),0); ?></td>
            </tr>
            <tr>
              <td>5</td>
              <td>Sponsor PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","4"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","4"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","4")+getdata($arr_rekap,"0818","4")+getdata($arr_rekap,"0918","4")+getdata($arr_rekap,"1018","4")+
                          getdata($arr_rekap,"1118","4")+getdata($arr_rekap,"1218","4")+getdata($arr_rekap,"0119","4")+getdata($arr_rekap,"0219","4"),0); ?></td>
            </tr>	
            <tr>
              <td>6</td>
              <td>Sponsor Internal PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","7"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","7"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","7")+getdata($arr_rekap,"0818","7")+getdata($arr_rekap,"0918","7")+getdata($arr_rekap,"1018","7")+
                          getdata($arr_rekap,"1118","7")+getdata($arr_rekap,"1218","7")+getdata($arr_rekap,"0119","7")+getdata($arr_rekap,"0219","7"),0); ?></td>
            </tr>					
            <tr>
              <td>7</td>
              <td>Hotel PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","6"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","6"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","6")+getdata($arr_rekap,"0818","6")+getdata($arr_rekap,"0918","6")+getdata($arr_rekap,"1018","6")+
                          getdata($arr_rekap,"1118","6")+getdata($arr_rekap,"1218","6")+getdata($arr_rekap,"0119","6")+getdata($arr_rekap,"0219","6"),0); ?></td>
            </tr>				
            <tr>
              <td>8</td>
              <td>Deposit PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","5"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","5"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","5")+getdata($arr_rekap,"0818","5")+getdata($arr_rekap,"0918","5")+getdata($arr_rekap,"1018","5")+
                          getdata($arr_rekap,"1118","5")+getdata($arr_rekap,"1218","5")+getdata($arr_rekap,"0119","5")+getdata($arr_rekap,"0219","5"),0); ?></td>
            </tr>
            <tr>
              <td>9</td>
              <td>Entrun PW</td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0818","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0918","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1018","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1118","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"1218","8"),0); ?></td> 
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0119","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0219","8"),0); ?></td>
              <td class="text-right"><?= number_format(getdata($arr_rekap,"0718","8")+getdata($arr_rekap,"0818","8")+getdata($arr_rekap,"0918","8")+getdata($arr_rekap,"1018","8")+
                          getdata($arr_rekap,"1118","8")+getdata($arr_rekap,"1218","8")+getdata($arr_rekap,"0119","8")+getdata($arr_rekap,"0219","8"),0); ?></td>
            </tr>
          </tbody>
          <tfoot>
          <tr>
              <td></td> 
              <td>Total</td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"0718"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"0818"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"0918"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"1018"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"1118"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"1218"),0); ?></td> 
              <td class="text-right"><?= number_format(total1($arr_rekap,"0119"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"0219"),0); ?></td>
              <td class="text-right"><?= number_format(total1($arr_rekap,"0718")+total1($arr_rekap,"0818")+total1($arr_rekap,"0918")+total1($arr_rekap,"1018")+
                          total1($arr_rekap,"1118")+total1($arr_rekap,"1218")+total1($arr_rekap,"0119")+total1($arr_rekap,"0219"),0); ?></td>
            </tr>		
            <tr>
              <td></td> 
              <td>Total PW</td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"0718"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"0818"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"0918"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"1018"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"1118"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"1218"),0); ?></td> 
              <td class="text-right"><?= number_format(total2($arr_rekap,"0119"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"0219"),0); ?></td>
              <td class="text-right"><?= number_format(total2($arr_rekap,"0718")+total2($arr_rekap,"0818")+total2($arr_rekap,"0918")+total2($arr_rekap,"1018")+
                          total2($arr_rekap,"1118")+total2($arr_rekap,"1218")+total2($arr_rekap,"0119")+total2($arr_rekap,"0219"),0); ?></td>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<?php 
} else { 
  $tanggal = ""; $noreff = ""; $deskripsi = ""; $nilai = 0;
  $trans_mutasi = ""; $trans_status = 0; $keterangan = "";
  if (sizeof($data_rekening)>0) {
    $tanggal = $data_rekening[0]["tanggal"];
    $noreff = $data_rekening[0]["no_referensi"];
    $deskripsi = $data_rekening[0]["deskripsi"];
    $nilai = $data_rekening[0]["kredit"];
    $trans_status = $data_rekening[0]["tipe_trans"];
    $keterangan = $data_rekening[0]["keterangan"];
  }
?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Update Keterangan Mutasi</b></h4>
      <hr>
      <form class="form-horizontal" method="post" action="<?php echo site_url("bank/".$kodebank."/".$mutasiid); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Mutasi Transaksi</label>
          <div class="col-sm-10"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Tanggal</label>
          <div class="col-sm-10"><?php echo $tanggal; ?></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">No Referensi</label>
          <div class="col-sm-10"><?php echo $noreff; ?></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Deskripsi</label>
          <div class="col-sm-10"><?php echo $deskripsi; ?></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Nilai Transaksi</label>
          <div class="col-sm-10"><?php echo number_format($nilai,0); ?></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Tipe Transaksi</label>
          <div class="col-sm-5">
            <select name="tipetrans" class="form-control">
              <option value="0"<?php if ($trans_status=="0") { echo " selected"; } ?>>Non PW</option>
              <option value="1"<?php if ($trans_status=="1") { echo " selected"; } ?>>Tiket PW</option>
              <option value="2"<?php if ($trans_status=="2") { echo " selected"; } ?>>Tiket Group PW</option>
              <option value="3"<?php if ($trans_status=="3") { echo " selected"; } ?>>Expo PW</option>
              <option value="4"<?php if ($trans_status=="4") { echo " selected"; } ?>>Sponsor PW</option>
              <option value="7"<?php if ($trans_status=="7") { echo " selected"; } ?>>Sponsor Internal PW</option>
              <option value="6"<?php if ($trans_status=="6") { echo " selected"; } ?>>Hotel PW</option>
              <option value="5"<?php if ($trans_status=="5") { echo " selected"; } ?>>Deposit PW</option>
              <option value="8"<?php if ($trans_status=="8") { echo " selected"; } ?>>Entrun PW</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-5"><input type="text" name="keterangan" class="form-control" placeholder="Keterangan" value="<?php echo $keterangan; ?>"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-10 col-xs-offset-2">
            <a href="<?= base_url("bank"); ?>" class="btn btn-default">
              <i class="fa fa-times"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>