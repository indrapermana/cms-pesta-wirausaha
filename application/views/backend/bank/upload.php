<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rekening Bank</h4>
    <p class="text-muted page-title-alt">Informasi Mutasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Upload Textfile BSM
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Upload Textfile BSM</b></h4>
      <hr>

      <?php if ($submit==0) { ?>
      <form class="form-horizontal" method="post" action="<?php echo site_url("bank/upload"); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Rekening</label>
          <div class="col-sm-10">
            <select name="rekening_id" class="form-control select2">
              <?php
                for($i=0; $i<count($rekening); $i++){
                  $id = $rekening[$i]['id'];
                  $kode = $rekening[$i]['kode'];
                  $nomor = $rekening[$i]['nomor'];
                  $nama = $rekening[$i]['nama'];
                  $bank = $rekening[$i]['bank'];
              ?>
              <option value="<?= $id; ?>"><?=  $bank." (".$nama." - ".$nomor.")"; ?></option>
              <?php
                }
              ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Textfile</label>
          <div class="col-sm-10">
            <textarea class="form-control" name="txtbank" rows="10" cols="60"></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button>
          </div>
        </div>
      </form>
      <?php } else { ?>
        <div class="form-group"><?php echo $jmldata; ?> data telah dimasukkan</div>
      <?php } ?>
    </div>
  </div>
</div>