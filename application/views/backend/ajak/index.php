<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Ayo Ajak Teman Ke <?= $event[0]['nama']; ?></h4>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-md-7">
    <div class="card-box m-t-20">
      <div class="p-20">
        <p>
          Hi <?= $this->user_nama; ?>, <br><br>
          Yuk Ajak teman dan jaringan Anda untuk hadir di coba, Dengan mengajak teman dan jaringan Anda, maka makin banyak kesempatan untuk mendapatkan Komisi Penjualan sebanyak-banyaknya dari Penjualan Tiket coba. <br><br>
          Share link Affiliate Anda dan dapatkan komisi penjualan Rp 100.000 untuk penjualan Tiket: 
          <br><br>
          <a href="<?= base_url("tiket/".$event[0]['id']."/".$event[0]['slug']."/?aff=".$this->user_id); ?>" target="_new"><?= base_url("tiket/".$event[0]['id']."/".$event[0]['slug']."/?aff=".$this->user_id); ?></a>
          <br><br>
          Buktikan Anda adalah WIRAUSAHA SEJATI!<br>
          <!-- Info lengkap bisa dilihat di link ini <a href="http://localhost/tiket/kontes">http://localhost/tiket/kontes</a><br>
          *Anda hanya bisa mengirimkan maksimal 5 ajakan Perhari<br><br> -->
        </p>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>TABLE TRANSAKSI PEMBELIAN TIKET DARI AFFILIATE ANDA</b></h4>
      <hr>

      <div class="table-responsive">
        <table id="datatable-buttons" class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Tiket</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Harga</th>
              <th>Affid</th>
              <th>Status</th>
              <th>HP</th>
              <?php if($this->user_level=="1"){ ?>
              <th>Action</th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
            <?php
              if(count($ajak)>0){
                $no = 1;
                foreach($ajak as $data){
            ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $data->invoice; ?></td>
              <td><?= $data->nama_layanan; ?></td>
              <td><?= $data->nama; ?></td>
              <td><?= $data->usaha; ?></td>
              <td><?= number_format($data->nilai,0,'.',','); ?></td>
              <td><?= $data->affid; ?></td>
              <td><?= ($data->status_bayar == "0")? '<span class="label label-danger" style="padding: 5px;"><i class="fa fa-times"></i>&nbsp; Belum Bayar</span>' : '<span class="label label-success" style="padding: 5px;"><i class="fa fa-check"></i>&nbsp;Sudah Bayar</span>'; ?></td>
              <td><?= $data->no_hp; ?></td>
              <?php if($this->user_level=="1") { ?>
              <td>
                <?php if($data->status_bayar=="1") { ?>
                <a href="<?= base_url("tiket/aktivasi/".$data->id) ?>" class="btn btn-purple waves-effect waves-light">
                  <span>Validasi Pembayaran</span>
                  <i class="fa fa-money m-l-5"></i>
                </a>
                <?php } else { ?>
                  <a href="#" class="btn btn-success waves-effect waves-light">
                    <span>Sudah dibayar</span>
                    <i class="fa fa-money m-l-5"></i>
                  </a>
                <?php } ?>
              </td>
              <?php } ?>
            </tr>
            <?php
                $no++;
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>