<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Transaksi Pembelian</h4>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>TABLE Database Penjualan Tiket</b></h4>
      <hr>

      <div class="table-responsive">
        <table id="datatable-buttons" class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Tgl Invoice</th>
              <th>Tiket</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Harga</th>
              <th>Affid</th>
              <th>No Hp</th>
              <th>Daerah</th>
              <th>Status</th>
              <?php if ($this->user_level == '1'){ ?>                                    
              <th>Email</th>
              <th>Action</th>
              <th>Approver</th>
              <?php }?>
              </tr>
            </tr>
          </thead>
          <tbody>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>