<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Table Database Penjualan Tiket</h4>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>TABLE Database Penjualan Tiket</b></h4>
      <hr>

      <div class="table-responsive">
        <table id="datatable-buttons" class="table table-hover table-striped table-bordered">
          <thead>
            <tr>
              <th>Id Tiket</th>
              <th>Tiket</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Asal TDA</th>
              <th>No Hp</th>
              <th>Email</th>                                    
              <th>Status</th>
              <?php if ($this->user_level == '1') {?>
              <th style="width: 425px !important;">Kirim Email ke peserta</th>
              <?php } ?>
            </tr>
          </thead>
          <tbody>
          <?php 
            if(count($tiket)>0){ 
              foreach ($tiket as $data) {
          ?>
          <tr>
            <td>#PW<?= str_pad($trx->id, 3, '0', STR_PAD_LEFT); ?></td>
            <td><?= $data->nama_layanan; ?></td>
            <td><?= $data->nama; ?></td>
            <td><?= $data->usaha; ?></td>
            <td>TDA <?= $data->daerah; ?></td>
            <td><?= $data->no_hp; ?></td>
            <td><?= $data->email; ?></td>
            <td><?= ($data->status_bayar=="0")? '<a href="#" class="btn btn-danger">INVALID</a>' : '<a href="#" class="btn btn-success">VALID</a>'; ?></td>
            <td></td>
          </tr>
          <?php
              }
            }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>