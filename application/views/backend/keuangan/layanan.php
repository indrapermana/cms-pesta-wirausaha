<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Layanan</h4>
    <p class="text-muted page-title-alt">Informasi Layanan PW Nas</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <?php if($aksi!=""){ ?>
      <li>
        <a href="<?= base_url('keuangan/layanan'); ?>">Layanan</a>
      </li>
      <li class="active">
        <?= ucfirst($aksi); ?> Layanan
      </li>
      <?php } else { ?>
      <li class="active">
        Layanan
      </li>
      <?php } ?>
    </ol>
  </div>
</div>

<?php if ($site_form=="list") { ?>
  <div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Data Layanan</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("keuangan/layanan/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga</th>
              <th>Status</th>
              <th>Waktu</th>
              <th>Komisi</th>
              <th>Daerah</th>
              <th>Hadiah</th>
              <th>Harga Fix</th>
              <th>Jumlah</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php
              if(sizeof($data_layanan)){
                for($i=0; $i<sizeof($data_layanan); $i++){
                  $id = $data_layanan[$i]['id'];
                  $nama = $data_layanan[$i]['nama'];
                  $harga = $data_layanan[$i]['harga'];
                  $status = ($data_layanan[$i]['status']=="1")? "Aktif" : "Tidak Aktif";
                  $waktu = ($data_layanan[$i]['waktu']=="1")? "Aktif" : "Tidak Aktif";
                  $komisi = $data_layanan[$i]['komisi'];
                  $daerah = $data_layanan[$i]['daerah'];
                  $hadiah = $data_layanan[$i]['hadiah'];
                  $harga_fix = $data_layanan[$i]['harga_fix'];
                  $jumlah = $data_layanan[$i]['jumlah'];
  
              ?>
              <tr>
                <td style="vertical-align:middle; text-align: center;"><?= ($i+1); ?></td>
                <td style="vertical-align:middle;"><?= $nama; ?></td>
                <td style="vertical-align:middle;"><?= "Rp. ".number_format($harga,2,',','.'); ?></td>
                <td style="vertical-align:middle;"><?= $status; ?></td>
                <td style="vertical-align:middle;"><?= $waktu; ?></td>
                <td style="vertical-align:middle;"><?= "Rp. ".number_format($komisi,2,',','.'); ?></td>
                <td style="vertical-align:middle;"><?= "Rp. ".number_format($daerah,2,',','.'); ?></td>
                <td style="vertical-align:middle;"><?= "Rp. ".number_format($hadiah,2,',','.'); ?></td>
                <td style="vertical-align:middle;"><?= "Rp. ".number_format($harga_fix,2,',','.'); ?></td>
                <td style="vertical-align:middle;"><?= $jumlah; ?></td>
                <td>
                  <a href="<?= base_url("keuangan/layanan/ubah/".$id); ?>" class="btn btn-warning">
                    <i class="fa fa-pencil"></i>
                  </a>
                  &nbsp;&nbsp;
                  <a href="<?= base_url("keuangan/layanan/hapus/".$id); ?>" class="btn btn-default">
                    <i class="fa fa-trash"></i>
                  </a>
                </td>
                
              </tr>
              <?php
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php 
} else {
  $nama = ""; $harga = "0"; $status = "0"; $waktu = "0";  $komisi = "0"; $daerah = "0"; $hadiah = "0"; 
  $deskripsi = ""; $url = ""; $reseller = "0"; $harga_fix = "0"; $jumlah = "0";
  if (sizeof($data_layanan)>0) {
    $nama = $data_layanan[0]["nama"];
    $harga = $data_layanan[0]["harga"];
    $status = $data_layanan[0]["status"];
    $waktu = $data_layanan[0]["waktu"];
    $komisi = $data_layanan[0]["komisi"];
    $daerah = $data_layanan[0]["daerah"];
    $hadiah = $data_layanan[0]["hadiah"];
    $deskripsi = $data_layanan[0]["deskripsi"];
    $url = $data_layanan[0]["url"];
    $reseller = $data_layanan[0]["reseller"];
    $harga_fix = $data_layanan[0]["harga_fix"];
    $jumlah = $data_layanan[0]["jumlah"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Layanan"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Layanan"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Layanan"; }
?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?php echo $xjudul; ?></b></h4>
      <hr>

      <form action="<?= base_url("keuangan/layanan/".$aksi."/".$layananid); ?>" method="post" class="form-horizontal">
        <div class="form-group">
          <label class="col-md-2 control-label">Nama</label>
          <div class="col-md-10">
            <input type="text" name="nama" class="form-control" value="<?= $nama; ?>" placeholder="Nama">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Harga</label>
          <div class="col-md-10">
            <input type="text" name="harga" class="form-control" value="<?= $harga; ?>" placeholder="Harga">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Status</label>
          <div class="col-md-10">
          <?php $check = ($status=="1")? "checked" : "";?>
            <input type="checkbox" name="status" <?= $check; ?> data-plugin="switchery" data-color="#81c868" value="1"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Bulan</label>
          <div class="col-md-10">
            <input type="text" name="waktu" class="form-control" value="<?= $waktu; ?>" placeholder="Jumlah Bulan"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Komisi</label>
          <div class="col-md-10">
            <input type="text" name="komisi" class="form-control" value="<?= $komisi; ?>" placeholder="Komisi">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Daerah</label>
          <div class="col-md-10">
            <input type="text" name="daerah" class="form-control" value="<?= $daerah; ?>" placeholder="Daerah">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Hadiah</label>
          <div class="col-md-10">
            <input type="text" name="hadiah" class="form-control" value="<?= $hadiah; ?>" placeholder="Hadiah">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Url</label>
          <div class="col-md-10">
            <input type="text" name="url" class="form-control" value="<?= $url; ?>" placeholder="Url">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Reseller</label>
          <div class="col-md-10">
          <?php $check = ($reseller=="1")? "checked" : "";?>
            <input type="checkbox" name="reseller" <?= $check; ?> data-plugin="switchery" data-color="#81c868" value="1"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Harga Fix</label>
          <div class="col-md-10">
            <input type="text" name="harga_fix" class="form-control" value="<?= $harga_fix; ?>" placeholder="Harga Fix">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Jumlah</label>
          <div class="col-md-10">
            <input type="text" name="jumlah" class="form-control" value="<?= $jumlah; ?>" placeholder="Jumlah">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Deskripsi</label>
          <div class="col-md-10">
            <textarea name="deskripsi" class="form-control" placeholder="Deskripsi"><?= $deskripsi; ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("keuangan/layanan"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php } ?>