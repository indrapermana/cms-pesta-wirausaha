<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rencana Pengeluaran</h4>
    <p class="text-muted page-title-alt">Informasi Budget PW Nas</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rencana Pengeluaran
      </li>
    </ol>
  </div>
</div>

<?php 
if ($site_form=="list") { 
  $arr_item = array();
  if (sizeof($data_item)>0) {
    for ($i=0;$i<sizeof($data_item);$i++) {
      $budget = $data_item[$i]["budget_id"];
      $jenis = $data_item[$i]["jenis_budget"];
      $nilai = $data_item[$i]["total"];

      $arr_item[$budget][$jenis] = $nilai;
    }
  }
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rencana Pengeluaran</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("keuangan/anggaran/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Item Anggaran</th>
              <th class="text-right">Ideal</th>
              <th class="text-right">Standard</th>
              <th class="text-right">Minimal</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
            function crawl_anggar($data_anggar,$arritem,$parent,$level) {
              if (sizeof($data_anggar)>0) {
                for ($i=0;$i<sizeof($data_anggar);$i++) {
                  $id = $data_anggar[$i]["id"];
                  $induk = $data_anggar[$i]["parent_id"];
                  $kode = $data_anggar[$i]["kode_budget"];
                  $item = $data_anggar[$i]["item_budget"];
                  $keterangan = $data_anggar[$i]["keterangan"];

                  if ($induk==$parent) {
                    echo "<tr><td>";
                    if ($level>1) {
                      for ($j=2;$j<$level+1;$j++) {
                        echo "&nbsp;&nbsp;";
                      }
                    } 

                    $ideal = 0; $standard = 0; $minimal = 0;
                    if (isset($arritem[$id]["1"])) { $minimal = $arritem[$id]["1"]; }
                    if (isset($arritem[$id]["2"])) { $standard = $arritem[$id]["2"]; }
                    if (isset($arritem[$id]["3"])) { $ideal = $arritem[$id]["3"]; }

                    echo $kode . ". " . $item . "</td><td align=\"right\">" . number_format($ideal) . "</td><td align=\"right\">" . number_format($standard) . "</td><td align=\"right\">" . number_format($minimal) . "</td><td align=\"center\">";
                    echo "<a href=\"".site_url("keuangan/itemanggaran/".$id)."\" class=\"btn btn-primary\"><i class=\"fa fa-eye\"></i> Item</a> ";
                    echo "<a href=\"".site_url("keuangan/anggaran/ubah/".$id)."\" class=\"btn btn-warning\"><i class=\"fa fa-pencil\"></i> Ubah</a> ";
                    echo "<a href=\"".site_url("keuangan/anggaran/hapus/".$id)."\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i> Hapus</a>";
                    echo "</td></tr>";
                    crawl_anggar($data_anggar,$arritem,$id,$level+1);
                  }
                } 
              }
            }
            crawl_anggar($data_anggaran,$arr_item,"0",1);
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>

<?php 
} else { 
  $induk = ""; $kode = ""; $anggaran = "";  $keterangan = "";
  if (sizeof($data_anggaran)>0) {
    $induk = $data_anggaran[0]["parent_id"];
    $kode = $data_anggaran[0]["kode_budget"];
    $anggaran = $data_anggaran[0]["item_budget"];
    $keterangan = $data_anggaran[0]["keterangan"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Anggaran"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Anggaran"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Anggaran"; }
?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?php echo $xjudul; ?></b></h4>
      <hr>

      <form class="form-horizontal" method="post" action="<?= base_url("keuangan/anggaran/".$aksi."/".$anggaranid); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Induk Anggaran</label>
          <div class="col-sm-10">
            <select name="induk" class="form-control">
              <option value="0">-- Tanpa Induk --</option>
              <?php
              function crawl_option($data_anggar,$parent,$level,$induk) {
                if (sizeof($data_anggar)>0) {
                  for ($i=0;$i<sizeof($data_anggar);$i++) {
                    $id = $data_anggar[$i]["id"];
                    $pinduk = $data_anggar[$i]["parent_id"];
                    $kode = $data_anggar[$i]["kode_budget"];
                    $item = $data_anggar[$i]["item_budget"];

                    if ($pinduk==$parent) {
                      echo "<option value=\"".$id."\"";
                      if ($induk==$id) { echo " selected"; }
                      echo ">";
                      if ($level>1) {
                        for ($j=2;$j<$level+1;$j++) {
                          echo "--";
                        }
                      }					

                      echo $kode . " " . $item . "</option>";
                      crawl_option($data_anggar,$id,$level+1,$induk);
                    }
                  }
                }
              }

              crawl_option($data_induk,"0",1,$induk);
              ?>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Kode</label>
          <div class="col-sm-10"><input type="text" name="kode" class="form-control" placeholder="Kode" value="<?php echo $kode; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Anggaran</label>
          <div class="col-sm-10"><input type="text" name="anggaran" class="form-control" placeholder="Item Anggaran" value="<?php echo $anggaran; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Keterangan</label>
          <div class="col-sm-10"><input type="text" name="keterangan" class="form-control" placeholder="Keterangan" value="<?php echo $keterangan; ?>"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("keuangan/anggaran/"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php } ?>