<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">NicePay Tiket</h4>
    <p class="text-muted page-title-alt">Informasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Transaksi Nicepay
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Daftar Transaksi Tiket via Nicepay</b></h4>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Invoice</th>
              <th>Jenis Transaksi</th>
              <th>Nama</th>
              <th>Transaksi</th>
              <th>Nilai</th>
              <th>Payment Fee</th>
              <th>Total</th>
              <th>Kode Bank</th>
              <th>No VA</th>
              <th>Status Bayar</th>
            </tr>
          </thead>

          <tbody>
          <?php
            if (sizeof($data_trans)>0) {
              for ($i=0;$i<sizeof($data_trans);$i++) {
                $id = $data_trans[$i]["id"];
                $tanggal = $data_trans[$i]["tanggal"];
                $invoice = $data_trans[$i]["no_invoice"];
                $jenis = $data_trans[$i]["jenis_trans"];
                $nama = $data_trans[$i]["nama_lengkap"];
                $note = $data_trans[$i]["note_trans"];
                $nilai = $data_trans[$i]["nilai"];
                $payfee = $data_trans[$i]["payfee"];
                $total = $data_trans[$i]["total"];
                $kodebank = $data_trans[$i]["np_bankcode"];
                $vanum = $data_trans[$i]["np_vanum"];
                $status = $data_trans[$i]["status_bayar"];

                $xjenis = "";
                if ($jenis=="1") { $xjenis = "Tiket Online"; }

                $xstatus = "";
                if ($status=="0") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-info\">Belum</a>"; }
                if ($status=="1") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Lunas</a>"; }

                echo "<tr><td>".($i+1)."</td><td>".$tanggal."</td><td>".$invoice."</td><td>".$xjenis."</td><td>".$nama."</td><td>".$note."</td><td>".number_format($nilai)."</td><td>".number_format($payfee)."</td><td>".number_format($total)."</td><td>".$kodebank."</td><td>".$vanum."</td><td>".$xstatus."</td></tr>";
              }
            }
          ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>