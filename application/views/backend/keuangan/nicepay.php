<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">NicePay</h4>
    <p class="text-muted page-title-alt">Informasi Transaksi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Transaksi Nicepay
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rekening</b></h4>
      <div class="filtter-right">
        <form method="post" action="<?php echo site_url("keuangan/nicepay/"); ?>">
          <select name="tipe" onchange="this.form.submit();" class="form-control">
						<option value="0"<?php if ($tipetrans=="0") { echo " selected"; } ?>>Rekap</option>
						<option value="1"<?php if ($tipetrans=="1") { echo " selected"; } ?>>Tiket Online</option>
						<option value="2"<?php if ($tipetrans=="2") { echo " selected"; } ?>>Tiket Group</option>
						<option value="3"<?php if ($tipetrans=="3") { echo " selected"; } ?>>Expo</option>
						<option value="4"<?php if ($tipetrans=="4") { echo " selected"; } ?>>Sponsor</option>
						<option value="7"<?php if ($tipetrans=="7") { echo " selected"; } ?>>Sponsor Internal</option>
						<option value="6"<?php if ($tipetrans=="6") { echo " selected"; } ?>>Hotel</option>
						<option value="8"<?php if ($tipetrans=="8") { echo " selected"; } ?>>Entrun</option>
					</select>
				</form>
      </div>
      <hr>

      <div class="table-responsive">
      <?php if($tipetrans=="0") { ?>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Tanggal</th>
              <th>Invoice</th>
              <th>Jenis Transaksi</th>
              <th>Nama</th>
              <th>Transaksi</th>
              <th>Nilai</th>
              <th>Payment Fee</th>
              <th>Total</th>
              <th>Kode Bank</th>
              <th>No VA</th>
              <th>VA Expdate</th>
              <th>Status Bayar</th>
            </tr>
          </thead>
          <tbody>
          <?php
            if (sizeof($data_trans)>0) {
              for ($i=0;$i<sizeof($data_trans);$i++) {
                $id = $data_trans[$i]["id"];
                $tanggal = $data_trans[$i]["tanggal"];
                $invoice = $data_trans[$i]["no_invoice"];
                $jenis = $data_trans[$i]["jenis_trans"];
                $nama = $data_trans[$i]["nama_lengkap"];
                $note = $data_trans[$i]["note_trans"];
                $nilai = $data_trans[$i]["nilai"];
                $payfee = $data_trans[$i]["payfee"];
                $total = $data_trans[$i]["total"];
                $kodebank = $data_trans[$i]["np_bankcode"];
                $vanum = $data_trans[$i]["np_vanum"];
                $status = $data_trans[$i]["status_bayar"];

                $vadate = $data_trans[$i]["np_vavaliddate"];
                $vatime = $data_trans[$i]["np_vavalidtime"];

                $tahun = substr($vadate, 0, 4);
                $bulan = substr($vadate, 4, 2);
                $tanggal = substr($vadate, -2);
                $tanggallimit = $tanggal . '-' . $bulan . '-' . $tahun;

                $jam = substr($vatime, 0, 2);
                $menit = substr($vatime, 2, 2);
                $detik = substr($vatime, -2);
                $jamlimit = $jam . ':' . $menit . ':' . $detik;

                $vaexp = $tanggallimit . " " . $jamlimit;			

                $xjenis = "";
                if ($jenis=="1") { $xjenis = "Tiket Online"; }
                if ($jenis=="3") { $xjenis = "Stand Expo"; }
                if ($jenis=="6") { $xjenis = "Hotel"; }

                $dellink = "<a href=\"".site_url("keuangan/nicepay/hapus/".$id)."\">Hapus</a>";

                $xstatus = "";
                if ($status=="0") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-info\">Belum</a> " . $dellink; }
                if ($status=="1") { $xstatus = "<a href=\"#\" class=\"btn btn-block btn-success\">Lunas</a>"; }

                echo "<tr><td>".($i+1)."</td><td>".$tanggal."</td><td>".$invoice."</td><td>".$xjenis."</td><td>".$nama."</td><td>".$note."</td><td>".number_format($nilai)."</td><td>".number_format($payfee)."</td><td>".number_format($total)."</td><td>".$kodebank."</td><td>".$vanum."</td><td>".$vaexp."</td><td>".$xstatus."</td></tr>";
              }
            }
          ?>
          </tbody>
        </table>
      <?php } else{ ?>
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Tanggal</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Transaksi</th>
              <th>Nilai</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
            $arr_nicepay = array();
            if (sizeof($data_nicepay)>0) {
              for ($i=0;$i<sizeof($data_nicepay);$i++) {
                $n_invoice = $data_nicepay[$i]["no_invoice"];
                $n_jenis = $data_nicepay[$i]["jenis_trans"];
                $n_vanum = $data_nicepay[$i]["np_vanum"];

                if (($n_jenis==$tipetrans) && ($n_vanum!="")) {
                  $arr_nicepay[substr($n_invoice,1)] = $n_vanum;
                }
              }
            } 

            if (sizeof($data_trans)>0) {
              for ($i=0;$i<sizeof($data_trans);$i++) {
                $id = $data_trans[$i]["id"];
                $invoice = $data_trans[$i]["invoice"];
                $tanggal = $data_trans[$i]["tanggal"];
                $nama = $data_trans[$i]["nama"];
                $usaha = $data_trans[$i]["usaha"];

                if ($tipetrans=="1") {
                  $nilai = $data_trans[$i]["n_nilai"];
                  $transaksi = "Tiket PWN 2019";
                } else if ($tipetrans=="3") {
                  $nilai = $data_trans[$i]["nilai_bayar"];
                  $nobooth = $data_trans[$i]["nobooth"];
                  $transaksi = "Stand Expo PWN 2019 #".$nobooth;
                } else if ($tipetrans=="6") {
                  $nilai = $data_trans[$i]["nilai"];
                  $transaksi = "Hotel PWN 2019";
                }

                if (!isset($arr_nicepay[$invoice])) {
                  echo "<tr><td>".($i+1)."</td><td>".$invoice."</td><td>".$tanggal."</td><td>".$nama."</td><td>".$usaha."</td><td>".$transaksi."</td><td>".number_format($nilai)."</td>";
                  echo "<td><a href=\"".site_url("keuangan/nicepay/".$tipetrans."/".$id)."\">Create VA</a></td>";
                  echo "</tr>";
                }
              }
            }
          ?>
          </tbody>
        </table>
      <?php } ?>
      </div>
    </div>
  </div>
</div>