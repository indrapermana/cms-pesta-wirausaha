<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Rencana Item Pengeluaran</h4>
    <p class="text-muted page-title-alt">Informasi Budget PW Nas</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Rencana Item Pengeluaran
      </li>
    </ol>
  </div>
</div>

<?php
  $nmitem = "";
  if (sizeof($data_anggaran)>0) {
    $kode = $data_anggaran[0]["kode_budget"];
    $item = $data_anggaran[0]["item_budget"];
    $nmitem = $kode . " " . $item;
  }

  if ($site_form=="list") {
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Rencana Item Anggaran (<?php echo $nmitem; ?>)</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("keuangan/itemanggaran/".$anggaranid."/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th class="text-center">No</th>
              <th class="text-center">Jenis</th>
              <th class="text-center">Jumlah</th>
              <th class="text-center">Harga</th>
              <th class="text-center">PPN</th>
              <th class="text-center">Total</th>
              <th class="text-center">Jumlah Hari</th>
              <th class="text-center">Total</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
          <?php
            if (sizeof($data_item)>0) {
              for ($i=0;$i<sizeof($data_item);$i++) {
                $i_id = $data_item[$i]["id"];
                $i_jenis = $data_item[$i]["jenis_budget"];
                $i_jml = $data_item[$i]["jml"];
                $i_harga = $data_item[$i]["harga"];
                $i_ppn = $data_item[$i]["ppn"];
                $i_jmlhari = $data_item[$i]["jml_hari"];

                $xjenis = "";
                if ($i_jenis=="1") { $xjenis = "Minimal"; }
                if ($i_jenis=="2") { $xjenis = "Standard"; }
                if ($i_jenis=="3") { $xjenis = "Ideal"; }

                $xppn = 0;
                if ($i_ppn=="1") { $xppn = $i_harga * 0.1; }

                $total1 = ($i_harga + $xppn) * $i_jml;
                $total2 = $total1 * $i_jmlhari;

                echo "<tr><td align=\"center\">".($i+1)."</td><td>".$xjenis."</td><td align=\"center\">".$i_jml."</td><td align=\"right\">".number_format($i_harga)."</td><td align=\"right\">".number_format($xppn)."</td><td align=\"right\">".number_format($total1)."</td><td align=\"center\">".$i_jmlhari."</td><td align=\"right\">".number_format($total2)."</td><td align=\"center\">";
                echo "<a href=\"".site_url("keuangan/itemanggaran/".$anggaranid."/ubah/".$i_id)."\" class=\"btn btn-warning\"><i class=\"fa fa-pencil\"></i> Ubah</a> ";
                echo "<a href=\"".site_url("keuangan/itemanggaran/".$anggaranid."/hapus/".$i_id)."\" class=\"btn btn-danger\"><i class=\"fa fa-times\"></i> Hapus</a>";			
                echo "</td></tr>";
              }
            }
          ?>
          </tbody>
        </table>
      </div>
      <a href="<?= base_url("keuangan/anggaran"); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a>
    </div>
  </div>
</div>

<?php 
} else { 
  $jenis = ""; $jml = ""; $harga = "";  $ppn = ""; $jml_hari = "";
  if (sizeof($data_item)>0) {
    $jenis = $data_item[0]["jenis_budget"];
    $jml = $data_item[0]["jml"];
    $harga = $data_item[0]["harga"];
    $ppn = $data_item[0]["ppn"];
    $jml_hari = $data_item[0]["jml_hari"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Item Anggaran"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Item Anggaran"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Item Anggaran"; }  
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?php echo $xjudul; ?> (<?php echo $nmitem; ?>)</b></h4>
      <hr>

      <form class="form-horizontal" method="post" action="<?php echo site_url("keuangan/itemanggaran/".$anggaranid."/".$aksi."/".$itemid); ?>">
        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Jenis Anggaran</label>
          <div class="col-sm-10">
            <select name="jenis" class="form-control">
              <option value="1"<?php if ($jenis=="1") { echo " selected"; } ?>>Minimal</option>
              <option value="2"<?php if ($jenis=="2") { echo " selected"; } ?>>Standard</option>
              <option value="3"<?php if ($jenis=="3") { echo " selected"; } ?>>Ideal</option>
            </select>
          </div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Jumlah</label>
          <div class="col-sm-10"><input type="text" name="jumlah" class="form-control" placeholder="Jumlah" value="<?php echo $jml; ?>"></div>
        </div>				

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Harga</label>
          <div class="col-sm-10"><input type="text" name="harga" class="form-control" placeholder="Harga" value="<?php echo $harga; ?>"></div>
        </div>

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">PPN</label>
          <div class="col-sm-10">
            <select name="ppn" class="form-control">
              <option value="1"<?php if ($ppn=="1") { echo " selected"; } ?>>Ya</option>
              <option value="0"<?php if ($ppn=="0") { echo " selected"; } ?>>Tidak</option>
            </select>				  
          </div>
        </div>					

        <div class="form-group">
          <label for="input" class="col-sm-2 control-label">Jumlah Hari</label>
          <div class="col-sm-10"><input type="text" name="jml_hari" class="form-control" placeholder="Jumlah Hari" value="<?php echo $jml_hari; ?>"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?php echo site_url("keuangan/itemanggaran/".$anggaranid); ?>" class="btn btn-default"><i class="fa fa-reply"></i> Kembali</a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit"><i class="fa fa-save"></i> Submit</button>
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php } ?>