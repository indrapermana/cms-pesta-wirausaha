<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Kupon</h4>
    <p class="text-muted page-title-alt">Informasi Kupon PW Nas</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <?php if($aksi!=""){ ?>
      <li>
        <a href="<?= base_url('keuangan/kupon'); ?>">Kupon</a>
      </li>
      <li class="active">
        <?= ucfirst($aksi); ?> Kupon
      </li>
      <?php } else { ?>
      <li class="active">
        Kupon
      </li>
      <?php } ?>
    </ol>
  </div>
</div>

<?php if ($site_form=="list") { ?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Data Kupon</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("keuangan/kupon/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Kode</th>
              <th>Type</th>
              <th>Diskon</th>
              <th>Komisi</th>
              <th>Status</th>
              <th>Pembuat</th>
              <th>Tanggal Buat</th>
              <th>Tanggal Kadaluarsa</th>
              <th>&nbsp;</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(sizeof($data_kupon)){
              for($i=0; $i<sizeof($data_kupon); $i++){
                $id = $data_kupon[$i]['id'];
                $kode = $data_kupon[$i]['kode'];
                $type = $data_kupon[$i]['type'];
                $diskon = $data_kupon[$i]['diskon'];
                $komisi = $data_kupon[$i]['komisi'];
                $status = ($data_kupon[$i]['status']=="1")? "Aktif" : "Tidak Aktif";
                $nama_user = $data_kupon[$i]['nama'];
                $tgl_buat = $data_kupon[$i]['tgl_buat'];
                $tgl_exp = $data_kupon[$i]['tgl_exp'];

            ?>
            <tr>
              <td style="vertical-align:middle; text-align: center;"><?= ($i+1); ?></td>
              <td style="vertical-align:middle;"><?= $kode; ?></td>
              <td style="vertical-align:middle;"><?= $type; ?></td>
              <td style="vertical-align:middle;"><?= "Rp. ".number_format($diskon,2,',','.'); ?></td>
              <td style="vertical-align:middle;"><?= $komisi; ?></td>
              <td style="vertical-align:middle;"><?= $status; ?></td>
              <td style="vertical-align:middle;"><?= $nama_user; ?></td>
              <td style="vertical-align:middle;"><?= set_datetime($tgl_buat); ?></td>
              <td style="vertical-align:middle;"><?= set_datetime($tgl_exp); ?></td>
              <td>
                <a href="<?= base_url("keuangan/kupon/ubah/".$id); ?>" class="btn btn-warning">
                  <i class="fa fa-pencil"></i>
                </a>
                &nbsp;&nbsp;
                <a href="<?= base_url("keuangan/kupon/hapus/".$id); ?>" class="btn btn-default">
                  <i class="fa fa-trash"></i>
                </a>
              </td>
              
            </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php 
} else{
  $kode = ""; $type = "";  $diskon = "";  $komisi = ""; $status = "0"; $tgl_exp = "";
  if (sizeof($data_kupon)>0) {
    $kode = $data_kupon[0]["kode"];
    $type = $data_kupon[0]["type"];
    $diskon = $data_kupon[0]["diskon"];
    $komisi = $data_kupon[0]["komisi"];
    $status = $data_kupon[0]["status"];
    $tgl_exp = $data_kupon[0]["tgl_exp"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Kupon"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Kupon"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Kupon"; }
?>
<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?php echo $xjudul; ?></b></h4>
      <hr>

      <form action="<?= base_url("keuangan/kupon/".$aksi."/".$kuponid); ?>" method="post" class="form-horizontal">
        <div class="form-group">
          <label class="col-md-2 control-label">Kode</label>
          <div class="col-md-10">
            <input type="text" name="kode" class="form-control" value="<?= $kode; ?>" placeholder="Kode">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Type</label>
          <div class="col-md-10">
            <select name="type" class="form-control select2">
              <option value="">Pilih<option>
              <option value="1" <?php if($type=="1") echo "selected"; ?>>Regurer<option>
              <option value="2" <?php if($type=="2") echo "selected"; ?>>Mahasiswa<option>
            </select>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Diskon</label>
          <div class="col-md-10">
            <input type="text" name="diskon" class="form-control" value="<?= $diskon; ?>" placeholder="Diskon">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Komisi</label>
          <div class="col-md-10">
            <input type="text" name="komisi" class="form-control" value="<?= $komisi; ?>" placeholder="Komisi">
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Status</label>
          <div class="col-md-10">
          <?php $check = ($status=="1")? "checked" : "";?>
            <input type="checkbox" name="status" <?= $check; ?> data-plugin="switchery" data-color="#81c868" value="1"/>
          </div>
        </div>
        <div class="form-group">
          <label class="col-md-2 control-label">Tanggal Kadaluarsa</label>
          <div class="col-md-10">
          <input type="text" class="form-control" id="datetime-start" name="tgl_exp" value="<?= $tgl_exp; ?>" placeholder="yyyy-mm-dd" />
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("keuangan/kupon"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>
<?php }?>