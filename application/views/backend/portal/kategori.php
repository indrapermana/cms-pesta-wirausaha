<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Kategori</h4>
      <p class="text-muted page-title-alt">Informasi PW</p>
      <ol class="breadcrumb">
        <li>
          <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
        </li>
        <li class="active">
          Kategori
        </li>
      </ol>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>List Kategori</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("portal/kategori/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th style="width: 5%; text-align: center;">No</th>
              <th style="width: 10%;">Nama</th>
              <th style="width: 10%;">Slug</th>
              <th style="width: 10%; text-align:center">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
              if(sizeof($data_kategori)>0){
                for($i=0; $i<sizeof($data_kategori); $i++){
                  $id = $data_kategori[$i]['id'];
                  $nama = $data_kategori[$i]['nama'];
                  $slug = $data_kategori[$i]['slug'];
            ?>
            <tr>
              <td style="vertical-align:middle; text-align: center;"><?= ($i+1); ?></td>
              <td><?= $nama; ?></td>
              <td><?= $slug; ?></td>
              <td style="vertical-align:middle; text-align: center;">
                <a href="<?= base_url('portal/kategori/ubah/'.$id)?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                <a href="<?= base_url('portal/kategori/hapus/'.$id)?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <?php
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>