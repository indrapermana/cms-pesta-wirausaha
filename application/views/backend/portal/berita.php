<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Berita</h4>
    <p class="text-muted page-title-alt">Informasi PW</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Berita
      </li>
    </ol>
  </div>
</div>

<?php if($site_form=="list") { ?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>List Berita</b></h4>
      <div class="filtter-right">
        <a href="<?= base_url("portal/berita/tambah/"); ?>" class="col-sm-2 btn btn-block btn-success">
          <i class="fa fa-plus"></i> Tambah
        </a>
      </div>
      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th style="width: 10%;">Gambar</th>
              <th style="width: 10%;">Judul</th>
              <th style="width: 10%;">Tanggal</th>
              <th style="width: 60%;">Berita</th>
              <th style="width: 10%;">Action</th>
            </tr>
          </thead>
          <tbody>
            <?php
              if(sizeof($data_berita)>0){
                for($i=0; $i<sizeof($data_berita); $i++){
                  $id = $data_berita[$i]['id'];
                  $gambar = $data_berita[$i]['gambar'];
                  $judul = $data_berita[$i]['judul'];
                  $tanggal = $data_berita[$i]['tanggal'];
                  $berita = $data_berita[$i]['berita'];
            ?>
            <tr>
              <td style="vertical-align:middle; text-align: center;"><img src="<?= $gambar?>" width="150"></td>
              <td><?= $judul; ?></td>
              <td style="vertical-align:middle; text-align: center;"><?= $tanggal; ?></td>
              <td><?= substr($berita, 0, 400)."..."; ?></td>
              <td style="vertical-align:middle; text-align: center;">
                <a href="<?= base_url('portal/berita/ubah/'.$id)?>" class="btn btn-warning"><i class="fa fa-pencil"></i></a>
                <a href="<?= base_url('portal/berita/hapus/'.$id)?>" class="btn btn-danger"><i class="fa fa-trash"></i></a>
              </td>
            </tr>
            <?php
                }
              }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>
<?php 
}else{ 

  $judul = ""; $tanggal = ""; $gambar = "";  $berita = "";
  if (sizeof($data_berita)>0) {
    $judul = $data_berita[0]["judul"];
    $tanggal = $data_berita[0]["tanggal"];
    $gambar = $data_berita[0]["gambar"];
    $berita = $data_berita[0]["berita"];
  }

  $xjudul = "";
  if ($aksi=="tambah") { $xjudul = "Tambah Data Berita"; }
  if ($aksi=="ubah") { $xjudul = "Ubah Data Berita"; }
  if ($aksi=="hapus") { $xjudul = "Hapus Data Berita"; }
?>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b><?php echo $xjudul; ?></b></h4>
      <hr>

      <form class="form-horizontal" action="<?= base_url('portal/berita/'.$aksi."/".$beritaid); ?>" method="post" enctype="multipart/form-data">
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Judul</label>
          <div class="col-sm-10">
            <input type="text" name="judul" placeholder="Judul" class="form-control" value="<?= $judul; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Tanggal</label>
          <div class="col-sm-10">
            <input type="text" name="Tanggal" placeholder="Tanggal" class="form-control" value="<?= $tanggal; ?>">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Gambar</label>
          <div class="col-sm-10">
            <input type="file" name="foto" class="filestyle" data-iconname="fa fa-cloud-upload">
          </div>
        </div>
        <div class="form-group">
          <label for="judul" class="col-sm-2 control-label">Berita</label>
          <div class="col-sm-10">
            <textarea class="form-control" name="berita" placeholder="Berita"><?= $berita; ?></textarea>
          </div>
        </div>
        <div class="form-group">
          <div class="col-sm-12">
            <a href="<?= base_url("portal/berita/"); ?>" class="btn btn-default">
              <i class="fa fa-reply"></i> Kembali
            </a> 
            <button type="submit" class="btn btn-info pull-right" name="tombol" value="submit">
              <i class="fa fa-save"></i> Submit
            </button> 
          </div>
        </div>
      </form>
    </div>
  </div>
</div>

<?php } ?>