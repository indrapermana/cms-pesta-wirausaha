<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Dashboard</h4>
    <p class="text-muted page-title-alt">Welcome to Dashboard</p>
  </div>
</div>

<div class="row">
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="ti-ticket text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?= count($total_sales); ?></b></h3>
        <p class="text-muted">Tiket Sales</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?= base_url("dashboard/detailtiket"); ?>" class="small-box-footer bg-icon-info text-info">
        More Info <i class="fa fa-arrow-circle-right"></i>
      </a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-success pull-left">
        <i class="ti-bookmark text-success"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?= count($total_sponsor);?></b></h3>
        <p class="text-muted">Spnsor</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?= base_url("dashboard/detailsponsor"); ?>" class="small-box-footer bg-icon-success text-success">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-purple pull-left">
        <i class="fa fa-users text-purple"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter"><?= count($total_user_registrasi); ?></b></h3>
        <p class="text-muted">Users Resgistraion</p>
      </div>
      <div class="clearfix"></div>
      <a href="<?= base_url("dashboard/detailuser"); ?>" class="small-box-footer bg-icon-purple text-purple">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box fadeInDown animated">
      <div class="bg-icon bg-icon-danger pull-left">
        <i class="md md-equalizer text-danger"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">65</b></h3>
        <p class="text-muted">Unique Visitors</p>
      </div>
      <div class="clearfix"></div>
      <a href="#" class="small-box-footer bg-icon-danger text-danger">More Info <i class="fa fa-arrow-circle-right"></i></a>
    </div>
  </div>
</div>