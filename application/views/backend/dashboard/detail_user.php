<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">User Registrasi</h4>
    <p class="text-muted page-title-alt">Informasi User Registrasi</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        User Registrasi
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Detail User Registrasi</b></h4>

      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Email</th>
              <th>No HP</th>
              <th>Tipe</th>
              <th>Nilai</th>
              <th>Tanggal</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(sizeof($total_user_registrasi)> 0){
              for($i=0; $i<sizeof($total_user_registrasi); $i++){
                $no = $i+1;
                $invoice = $total_user_registrasi[$i]['invoice'];
                $nama = $total_user_registrasi[$i]['nama'];
                $usaha = $total_user_registrasi[$i]['usaha'];
                $email = $total_user_registrasi[$i]['email'];
                $no_hp = $total_user_registrasi[$i]['no_hp'];
                $tipe = $total_user_registrasi[$i]['type_tiket'];
                $nilai = $total_user_registrasi[$i]['nilai'];
                $tanggal = $total_user_registrasi[$i]['tgl_bayar'];
            ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $invoice; ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $email; ?></td>
              <td><?= $no_hp; ?></td>
              <td><?= $tipe; ?></td>
              <td><?= number_format($nilai); ?></td>
              <td><?= $tanggal; ?></td>
            </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>