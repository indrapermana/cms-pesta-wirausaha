<div class="row">
  <div class="col-sm-12">
    <h4 class="page-title">Penjualan Tiket</h4>
    <p class="text-muted page-title-alt">Informasi Penjualan Tiket</p>
    <ol class="breadcrumb">
      <li>
        <a href="<?= base_url('dashboard'); ?>">Dashboard</a>
      </li>
      <li class="active">
        Penjualan Tiket
      </li>
    </ol>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <div class="card-box">
      <h4 class="header-title"><b>Detail Penjualain Tiket</b></h4>

      <hr>

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>No</th>
              <th>Invoice</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>Email</th>
              <th>No HP</th>
              <th>Tipe</th>
              <th>Nilai</th>
              <th>Tanggal</th>
            </tr>
          </thead>
          <tbody>
            <?php
            if(sizeof($total_sales)> 0){
              for($i=0; $i<sizeof($total_sales); $i++){
                $no = $i+1;
                $invoice = $total_sales[$i]['invoice'];
                $nama = $total_sales[$i]['nama'];
                $usaha = $total_sales[$i]['usaha'];
                $email = $total_sales[$i]['email'];
                $no_hp = $total_sales[$i]['no_hp'];
                $tipe = $total_sales[$i]['type_tiket'];
                $nilai = $total_sales[$i]['nilai'];
                $tanggal = $total_sales[$i]['tgl_bayar'];
            ?>
            <tr>
              <td><?= $no; ?></td>
              <td><?= $invoice; ?></td>
              <td><?= $nama; ?></td>
              <td><?= $usaha; ?></td>
              <td><?= $email; ?></td>
              <td><?= $no_hp; ?></td>
              <td><?= $tipe; ?></td>
              <td><?= number_format($nilai); ?></td>
              <td><?= $tanggal; ?></td>
            </tr>
            <?php
              }
            }
            ?>
          </tbody>
        </table>
      </div>
    </div>
  </div>
</div>