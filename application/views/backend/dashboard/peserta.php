<!-- Page-Title -->
<div class="row">
  <div class="col-sm-12">
    <div class="page-header-2">
      <h4 class="page-title">Hai, <?= $this->user_nama; ?></h4>
      <p class="text-muted page-title-alt">Welcome to Dashboard <?= $event[0]['nama']?></p>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12">
    <!-- <?php foreach ($tiket as $tiket) { ?>
    <div class="alert alert-success">
      Tiket Anda adalah <strong> #PW
      <?php echo str_pad($tiket->id, 3, '0', STR_PAD_LEFT); ?>
      </strong>, Tunjukan No Tiket tersebut saat menghadiri <?php echo $event[0]['nama'] ?> sebagai Tanda Masuk. <a href="<?php echo base_url() ?>peserta/validasi/?tiket=<?php echo $tiket->id ?>" target="_new">Klik disini </a>
    </div>
    <?php  } ?> -->
    
    <?php if ($reseller == '0') { ?>
    <div class="alert alert-success">
      Ajak teman Anda, dapatkan komisi Rp 100.000 untuk penjualan tiket share link berikut ini : --> <strong><a href="<?= base_url("tiket/".$event[0]['id']."/".$event[0]['slug']."/?aff=".$this->user_id) ?>" target="_new"><?= base_url("tiket/".$event[0]['id']."/".$event[0]['slug']."/?aff=".$this->user_id) ?></a></strong>
    </div>
    <?php } ?>
  </div>

  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="md md-attach-money text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark">Rp. <b class="counter"> 1.500.000 </b></h3>
        <p class="text-muted">Total Penjualan</p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box">
      <div class="bg-icon bg-icon-custom pull-left">
        <i class="fa fa-ticket text-custom"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">120.000</b></h3>
        <p class="text-muted">Tiket Terjual</p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box">
      <div class="bg-icon bg-icon-info pull-left">
        <i class="md md-equalizer text-info"></i>
      </div>
      <div class="text-right">
        <h3 class="text-dark"><b class="counter">23</b> %</h3>
        <p class="text-muted">Konversi Penjualan</p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

  <div class="col-md-6 col-lg-3">
    <div class="widget-bg-color-icon card-box">
      <div class="bg-icon bg-icon-custom pull-left">
        <i class="icon icon-speedometer text-custom"></i>
      </div>                                    
      <div class="text-right">
        <h3 class="text-dark"><b class="">Rp. 250.000</b></h3>
        <p class="text-muted"><a href='<?= base_url("pencairan") ?>'>Total Komisi Penjualan</a></p>
      </div>
      <div class="clearfix"></div>
    </div>
  </div>

</div>

<div class="row">
  <div class="col-lg-4">
    <div class="card-box">
      <h4 class="text-dark header-title m-t-0 m-b-30">Penjualan Hari ini</h4>

      <div class="widget-chart text-center">
        <input class="knob" data-width="150" data-height="150" data-linecap=round data-fgColor="#34d3eb" value="130" data-skin="tron" data-angleOffset="180" data-readOnly=true data-thickness=".15"/>
          <h5 class="text-muted m-t-20">Total Pembayaran Hari ini.</h5>
          <h2 class="font-600">Rp. 150.000</h2>
          <ul class="list-inline m-t-15">
            <li>
              <h5 class="text-muted m-t-20">Kemaren</h5>
              <h4 class="m-b-0">Rp. 100.000</h4>
            </li>
            <li>
              <h5 class="text-muted m-t-20">Bulan ini</h5>
              <h4 class="m-b-0">Rp. 250.000</h4>
            </li>
            <li>
              <h5 class="text-muted m-t-20">Bulan Lalu</h5>
              <h4 class="m-b-0">Rp. 0</h4>
            </li>
          </ul>
      </div>
    </div>
  </div>

  <div class="col-lg-8">
    <div class="card-box">
      <h4 class="text-dark header-title m-t-0">Graphic Penjualan</h4>
      <!-- <div class="text-center">
        <ul class="list-inline chart-detail-list">
          <li>
            <h5><i class="fa fa-circle m-r-5" style="color: #7e57c2;"></i>VIP</h5>
          </li>
          <li>
            <h5><i class="fa fa-circle m-r-5" style="color: #34d3eb;"></i>Regular</h5>
          </li>
        </ul>
      </div> -->
      <div class="chart-container">
        <div class="chart has-fixed-height" id="basic_lines" style="width: 100%; height: 346px"></div>
      </div>
    </div>
  </div>
</div>

<div class="row">
  <div class="col-xs-12 col-md-6">
    <div class="card-box">
      <h4 class="header-title"><b>5 TIKET TERJUAL</b></h4>
      <p>Penjualan 5 Tiket Terbaru.</p>
      <div class="filtter-right">
        <a href="#" class="btn btn-success">View All</a>
      </div>
      <!-- <hr> -->

      <div class="table-responsive">
        <table class="table table-hover">
          <thead>
            <tr>
              <th>Id Tiket</th>
              <th>Tiket</th>
              <th>Nama</th>
              <th>Usaha</th>
              <th>No HP</th>
              <th>Tipe Tiket</th>
            </tr>
          </thead>
          <?php if(count($tiket_lima)>0) { ?>
          <tbody>
            <?php foreach($tiket_lima as $tiket){ ?>
            <tr>
              <td><?= $tiket->id; ?></td>
              <td><?= $tiket->nama_event; ?></td>
              <td><?= $tiket->nama; ?></td>
              <td><?= $tiket->usaha; ?></td>
              <td><?= $tiket->no_hp; ?></td>
              <td><?= $tiket->nama_layanan; ?></td>
            </tr>
            <?php } ?>
          </tbody>
          <?php } else{ ?>
          <tfoot>
            <tr>
              <td colspan="6">
                <a href="<?= base_url("ajak"); ?>">
                  <i class="fa fa-plus"></i> Belum ada tiket terjual? Yuk Ajak Teman!
                </a>
              </td>
            </tr>
          </tfoot>
          <?php } ?>
        </table>
      </div>
    </div>
  </div>

  <div class="col-xs-12 col-md-6">
    <div class="card-box">
      <h4 class="header-title"><b>LEADER BOARD TOP 10 PENJUALAN TIKET</b></h4>
      <p>Reseller & Affiliate.</p>
      <!-- <hr> -->

      <div class="table-responsive">
        <table class="table table-striped table-bordered">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Email</th>
              <th>Penjualan</th>
              <th>Asal TDA</th>
              <th>Aff ID</th>
            </tr>
          </thead>
          <tbody>

          </tbody>
          <tfoot>
            
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>