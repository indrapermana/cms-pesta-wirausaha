<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="A fully featured admin theme which can be used to build CRM, CMS, etc.">
    <meta name="author" content="Coderthemes">

    <link rel="shortcut icon" href="<?= base_url('assets/backend/images/logo-pw.png'); ?>">

    <title>Ubold - Responsive Admin Dashboard Template</title>

    <link href="<?= base_url("assets/backend/css/bootstrap.min.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/core.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/components.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/icons.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/pages.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/responsive.css"); ?>" rel="stylesheet" type="text/css" />
    <link href="<?= base_url("assets/backend/css/style.css"); ?>" rel="stylesheet" type="text/css" />

    <!-- HTML5 Shiv and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->

    <script src="<?= base_url("assets/backend/js/modernizr.min.js"); ?>"></script>

  </head>
  <body class="login-page">

    <!-- <div class="account-pages"></div> -->
    <div class="clearfix"></div>

    <div class="wrapper-page">
      <div class=" card-box">

        <div class="panel-heading text-center"> 
          <h3 class="text-center"> <b>CMS</b> App </h3>
		      <img src="<?= base_url('assets/backend/images/logo.png'); ?>" width="150">
        </div> 

        <div class="panel-body">
          <?= $content; ?>
        </div>

      </div>        
    </div>

    <script>
    var resizefunc = [];
    </script>

    <!-- jQuery  -->
    <script src="<?= base_url("assets/backend/js/jquery.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/bootstrap.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/detect.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/fastclick.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.slimscroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.blockUI.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/waves.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/wow.min.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.nicescroll.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.scrollTo.min.js");?>"></script>


    <script src="<?= base_url("assets/backend/js/jquery.core.js");?>"></script>
    <script src="<?= base_url("assets/backend/js/jquery.app.js");?>"></script>
  </body>
</html>