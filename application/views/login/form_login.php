          <?php if(isset($error)){ ?>
          <div class="alert alert-danger alert-styled-left alert-bordered" style="width:320px;margin:0px auto 10px;">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <span class="text-semibold">Maaf! </span><?php echo $error ?>
          </div>
          <?php } ?>

          <?php if(isset($success)){ ?>
          <div class="alert alert-success alert-styled-left alert-bordered" style="width:320px;margin:0px auto 10px;">
            <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
            <?php echo $success ?>
          </div>
          <?php } ?>

          <form class="form-horizontal m-t-20" method="post" action="<?= base_url('login'); ?>">
            <div class="form-group ">
              <div class="col-xs-12">
                <input class="form-control" name="username" type="text" required="" placeholder="Username">
              </div>
            </div>
            <div class="form-group">
              <div class="col-xs-12">
                <input class="form-control" name="password" type="password" required="" placeholder="Password">
              </div>
            </div>
            <div class="form-group text-center m-t-40">
              <div class="col-xs-12">
                <button class="btn btn-primary btn-block text-uppercase waves-effect waves-light" type="submit" name="submit" value="true">Log In</button>
              </div>
            </div>
            <div class="form-group text-center m-t-40">
              <div class="col-xs-12">
                <p>Komunitas Tangan Di Atas</p>
              </div>
            </div>
          </form>