<!--************************************
Inner Baner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-bgparallax">
  <div class="container">
    <div class="tg-innerbannercontent">
      <div class="tg-pagetitle">
        <h1>Select Your City</h1>
      </div>
      <ol class="tg-breadcrumb">
        <li><a href="<?= base_url("home"); ?>">Beranda</a></li>
        <li class="tg-active">City</li>
      </ol>
    </div>
  </div>
</div>
<!--************************************
Inner Baner End
*************************************-->

<!--************************************
Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
  <section class="tg-sectionspace tg-haslayout">
    <div class="container">
      <div class="row">
        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="wrapper">
            <?php
              if(count($wilayah)>0){
                for($i=0; $i<count($wilayah); $i++){
            ?>
            <div class="titles-region">
              <?= $wilayah[$i]['wilayah_induk']; ?>
            </div>
            <div class="body-city row">
            <?php
                  $daerah = $this->PW_Model->get_wilayah_by_induk_wilayah($wilayah[$i]['id']);
                  if(count($daerah)>0){
                    for($x=0; $x<count($daerah); $x++){
                      $active_css = ($this->session->userdata('id_city')==$daerah[$x]['id'])? 'city-active' : "city";
                      $active_icon = ($this->session->userdata('id_city')==$daerah[$x]['id'])? '<i class="fa fa-check"></i>' : "";
            ?>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/".$daerah[$x]['slug']); ?>" class="<?= $active_css; ?>"><?= $daerah[$x]['wilayah']." ".$active_icon; ?></a>
              </div>
            <?php
                    }
                  } else {
                    echo '<h3 class="text-center">Not Found City</h3>';
                  }
            ?>
            </div>
            <?php
                }
              } else {
            ?>
            <div class="titles-region">
              Jawa Barat
            </div>
            <div class="body-city row">
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/bandung"); ?>" class="city-active">Bandung <i class="fa fa-check"></i></a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/garut"); ?>" class="city">Garut</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/cimahi"); ?>" class="city">Cimahi</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/tasik"); ?>" class="city">Tasik</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/tasik"); ?>" class="city">Tasik</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2 citys">
                <a href="<?= site_url("city/redirect/tasik"); ?>" class="city">Tasik</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2  citys">
                <a href="<?= site_url("city/redirect/tasik"); ?>" class="city">Tasik</a>
              </div>
              <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2  citys">
                <a href="<?= site_url("city/redirect/tasik"); ?>" class="city">Tasik</a>
              </div>
            </div>
            <?php } ?>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>