    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Pesan Expo</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("expo");?>">Expo</a></li>
						<li class="tg-active">Pesan Expo</li>
					</ol>
				</div>
			</div>
		</div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              	<img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Informasi Stand <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                <div class="tg-eventspeaker">
                </div>
              </figcaption>

              <div class="tg-detailinfo" style="margin-top:50px">
								<form class="tg-formtheme form-horizontal" action="<?= base_url("expo/pesan"); ?>" method="post" novalidate="novalidate">
									<?php if ($pesan!="") { ?>
									<div class="alert alert-danger alert-styled-left alert-bordered" style="width:320px;margin:0px auto 10px;">
										<button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
										<span class="text-semibold"><?php echo $pesan; ?></span>
									</div>
									<?php } ?>

									<?php if($page=="0") {?>
									<div class="form-group">
										<label><strong>Pilih Event Expo : </strong></label>
									</div>
									
									<div class="form-group">
										<div class="col-xs-12">
											<div class="row">
                        <?php
                          if(count($event) > 0){
                            for($i=0; $i<count($event); $i++){
															$id_event = $event[$i]['id'];
															$nama_event = $event[$i]['nama_event'];
															$gambar = $event[$i]['gambar'];
															$tanggal_event = $event[$i]['tgl_mulai'];
															$tahun = $event[$i]['tahun'];
															$lokasi = $event[$i]['lokasi'];
															$lokasi_lengkap = $event[$i]['lokasi_lengkap'];
															
                              $checked = "";
                              if(isset($event_id)){
                                $checked = ($id_event == $event_id)? "checked" : "";
															}
															
															if($gambar!=""){
                                $img = "/upload/event/".$tahun."/".$gambar;
                              }else{
                                $img = "/upload/big/img3.jpg";
                              }
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px #d6cfcf solid; border-radius:4px; margin-bottom:10px; padding-top: 10px;">
                          <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
														<img src="<?= base_url($img); ?>"  style="margin-top: 20px;" alt="image description">
													</div>
													<div class="col-xs-7 col-sm-7 col-md-9 col-lg-9">
                            <h4 style="margin: 0;"><?= $nama_event; ?></h5>
                            <h5 style="margin: 0;">
															<time datetime="2017-12-12" style="margin: 9px 0 0;">
																<i class="fa fa-clock-o"></i> &nbsp;
																<?= set_datetime($tanggal_event); ?>
															</time>
                            </h5>
                            <hr style="margin: 10px 0;">
                            <p><?= $lokasi."<br>".$lokasi_lengkap; ?></p>
                          </div>
                          <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding-top: 37px;">
                            <input name="event_id" type="radio" <?= $checked; ?> class="form-control" value="<?= $id_event; ?>" style="width:25px;" required >
                          </div>
                        </div>
                        <?php
                            }
                          }
                        ?>
                        </div>
										</div>
									</div>
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="0" type="submit">Lanjutkan</button>
										</div>
									</div>
									<?php } elseif ($page=="1") { ?>
									<div class="form-group">
										<label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Lengkap</label>
										<div class="col-xs-12 col-sm-8 col-md-9">
											<input class="form-control" type="text" name="nama" value="" required="" placeholder="Nama">
										</div>
									</div>
									<div class="form-group ">
										<label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Usaha / Brand</label>
										<div class="col-xs-12 col-sm-8 col-md-9">
											<input class="form-control" type="text" value="" name="usaha" required="" placeholder="Nama Usaha / Brand ">
										</div>
									</div>
									<div class="form-group ">
										<label class="control-label col-xs-12 col-sm-4 col-md-3">Email</label>
										<div class="col-xs-12 col-sm-8 col-md-9">
											<input class="form-control" type="text" value="" name="email" required="" placeholder="Email">
										</div>
									</div>
									<div class="form-group ">
										<label class="control-label col-xs-12 col-sm-4 col-md-3">No Hp</label>
										<div class="col-xs-12 col-sm-8 col-md-9">
											<input class="form-control" type="text" value="" name="hp" required="" placeholder="No Hp">
										</div>
									</div>
									
									<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
									
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="1" type="submit">Lanjutkan</button>
										</div>
									</div>
								<?php } else if ($page=="2") { ?>
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Layout Expo Multi Produk</label>
										</div>
									</div>
									<div class="form-group ">
										<div class="col-xs-12" align="center">
											<img src="<?php echo site_url("expo/maplayout/1"); ?>" width="900">
										</div>
									</div>
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Multi Premium - Harga Rp. <?php echo number_format($harga_premium); ?></label>
										</div>
									</div>
									<div class="form-group ">
									<?php 
										if (sizeof($data_booth)>0) {
											for ($i=0;$i<sizeof($data_booth);$i++) {
												$nobooth = $data_booth[$i]["no_booth"];
												$tipebooth = $data_booth[$i]["tipe_booth"];
												$statusbooth = $data_booth[$i]["status_booth"];
												
												if ($tipebooth=="1") {
													if ($statusbooth=="0") {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"premium[]\" value=\"$nobooth\"> $nobooth</div>";
													} else {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"premium[]\" value=\"$nobooth\" disabled> <strike>$nobooth</strike></div>";
													}
												}
											}
										}
									?>
                	</div>
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Multi Reguler - Harga Rp. <?php echo number_format($harga_reguler); ?></label>
										</div>
									</div>
									<div class="form-group ">
									<?php 
										if (sizeof($data_booth)>0) {
											for ($i=0;$i<sizeof($data_booth);$i++) {
												$nobooth = $data_booth[$i]["no_booth"];
												$tipebooth = $data_booth[$i]["tipe_booth"];
												$statusbooth = $data_booth[$i]["status_booth"];
												
												if ($tipebooth=="2") {
													if ($statusbooth=="0") {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"reguler[]\" value=\"$nobooth\"> $nobooth</div>";
													} else {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"reguler[]\" value=\"$nobooth\" disabled> <strike>$nobooth</strike></div>";
													}
												}
											}
										} 
									?>
									</div>
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Layout Expo Kuliner</label>
										</div>
									</div>
									<div class="form-group ">
										<div class="col-xs-12" align="center">
											<img src="<?php echo site_url("expo/maplayout/2"); ?>" width="900">
										</div>
									</div>
					
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Kuliner Premium - Harga Rp. <?php echo number_format($harga_kuliner1); ?></label>
										</div>
									</div>
									<div class="form-group ">
									<?php 
										if (sizeof($data_booth)>0) {
											for ($i=0;$i<sizeof($data_booth);$i++) {
												$nobooth = $data_booth[$i]["no_booth"];
												$tipebooth = $data_booth[$i]["tipe_booth"];
												$statusbooth = $data_booth[$i]["status_booth"];
												
												if ($tipebooth=="8") {
													if ($statusbooth=="0") {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"kuliner1[]\" value=\"$nobooth\"> $nobooth</div>";
													} else {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"kuliner1[]\" value=\"$nobooth\" disabled> <strike>$nobooth</strike></div>";
													}
												}
											}
										}
									?>
									</div>				
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Kuliner Reguler - Harga Rp. <?php echo number_format($harga_kuliner2); ?></label>
										</div>
									</div>				
									<div class="form-group ">
									<?php 
										if (sizeof($data_booth)>0) {
											for ($i=0;$i<sizeof($data_booth);$i++) {
												$nobooth = $data_booth[$i]["no_booth"];
												$tipebooth = $data_booth[$i]["tipe_booth"];
												$statusbooth = $data_booth[$i]["status_booth"];
												
												if ($tipebooth=="3") {
													if ($statusbooth=="0") {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"kuliner2[]\" value=\"$nobooth\"> $nobooth</div>";
													} else {
														echo "<div class=\"col-xs-1\"><input type=\"checkbox\" name=\"kuliner2[]\" value=\"$nobooth\" disabled> <strike>$nobooth</strike></div>";
													}
												}
											}
										}
									?>
									</div>				
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="2" type="submit">Lanjutkan</button>
										</div>
									</div>
									
									<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
									<input type="hidden" name="nama" value="<?php echo $nama; ?>">
									<input type="hidden" name="usaha" value="<?php echo $usaha; ?>">
									<input type="hidden" name="email" value="<?php echo $email; ?>">
									<input type="hidden" name="hp" value="<?php echo $hp; ?>">

								<?php } else if ($page=="3") { ?>

									<div class="form-group ">
										<div class="col-xs-6 col-sm-4 col-md-3"><label>Nama Lengkap</label></div>
										<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $nama; ?></div>
									</div>
									<div class="form-group ">
										<div class="col-xs-6 col-sm-4 col-md-3"><label>Nama Usaha / Brand</label></div>
										<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $usaha; ?></div>
									</div>
									<div class="form-group ">
										<div class="col-xs-6 col-sm-4 col-md-3"><label>Email</label></div>
										<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $email; ?></div>
									</div>
									<div class="form-group ">
										<div class="col-xs-6 col-sm-4 col-md-3"><label>No Hp</label></div>
										<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $hp; ?></div>
									</div>

									<div class="form-group ">
										<div class="col-xs-12">
											<label>Pesanan</label>
										</div>
									</div>
									<?php
										$tpremium = "-"; if (($premium!="") && (trim($premium)!=",")) { $tpremium = substr($premium,0,-1); }
										$treguler = "-"; if (($reguler!="") && (trim($reguler)!=",")) { $treguler = substr($reguler,0,-1); }
										$tkuliner1 = "-"; if (($kuliner1!="") && (trim($kuliner1)!=",")) { $tkuliner1 = substr($kuliner1,0,-1); }
										$tkuliner2 = "-"; if (($kuliner2!="") && (trim($kuliner2)!=",")) { $tkuliner2 = substr($kuliner2,0,-1); }
									?>
									<div class="form-group ">
											<div class="col-xs-6 col-sm-4 col-md-3"><label>- Multi Premium</label></div>
											<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $tpremium; ?></div>
									</div>
									<div class="form-group ">
											<div class="col-xs-6 col-sm-4 col-md-3"><label>- Multi Reguler</label></div>
											<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $treguler; ?></div>
									</div>
									<div class="form-group ">
											<div class="col-xs-6 col-sm-4 col-md-3"><label>- Kuliner Premium</label></div>
											<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $tkuliner1; ?></div>
									</div>
									<div class="form-group ">
											<div class="col-xs-6 col-sm-4 col-md-3"><label>- Kuliner Reguler</label></div>
											<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo $tkuliner2; ?></div>
									</div>
									<div class="form-group ">
											<div class="col-xs-6 col-sm-4 col-md-3"><label>Total Pembayaran</label></div>
											<div class="col-xs-6 col-sm-8 col-md-9 text-right"><?php echo "Rp. ".number_format($nilai_bayar); ?></div>
									</div>
									<div class="form-group ">
										<div class="col-xs-12">
											<label>Captcha</label><br>
											<?php echo $img_captcha; ?><br>
											<input class="form-control" type="text" value="" name="captcha" placeholder="">
										</div>
									</div>					
									<div class="form-group">
										<div class="col-xs-12">
											<input class="form-check-input" type="checkbox" value="1" id="tatatertib" name="tatatertib" required>
											<label class="form-check-label">Setuju dengan <a href="<?php echo site_url("expo/tatatertib"); ?>" target="_blank">tata tertib</a></label>
										</div>
									</div>
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="3" type="submit">Pesan Booth</button>
										</div>
									</div>
									
									<input type="hidden" name="event_id" value="<?php echo $event_id; ?>">
									<input type="hidden" name="nama" value="<?php echo $nama; ?>">
									<input type="hidden" name="usaha" value="<?php echo $usaha; ?>">
									<input type="hidden" name="email" value="<?php echo $email; ?>">
									<input type="hidden" name="hp" value="<?php echo $hp; ?>">
									<input type="hidden" name="spremium" value="<?php echo $premium; ?>">
									<input type="hidden" name="sreguler" value="<?php echo $reguler; ?>">
									<input type="hidden" name="skuliner1" value="<?php echo $kuliner1; ?>">
									<input type="hidden" name="skuliner2" value="<?php echo $kuliner2; ?>">

									<?php } else if ($page=="4") { ?>

									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											Terima kasih atas pesanannya. Tim expo PW 2019 akan segera menghubungi anda.
										</div>
									</div>

									<?php } ?>

								</form>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>