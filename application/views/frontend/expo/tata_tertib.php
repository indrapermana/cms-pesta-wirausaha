    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Tata Tertib Expo</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("expo");?>">Expo</a></li>
						<li class="tg-active">Tata Tertib Expo</li>
					</ol>
				</div>
			</div>
		</div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Informasi Stand <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                  Econvention Hall, Ancol, 25-27 Januari 2018
                </h3>
                <div class="tg-eventspeaker">
                </div>
              </figcaption>

              <div class="tg-detailinfo" style="margin-top:50px">

                <b>I.	DEFINISI</b>
                <ol type="1" style="padding-left: 40px;">
                  <li>Function Room : area dan atau ruang-ruang seminar  yang berada di lingkungan Econvention Hall.</li>
                  <li>Pengguna : pihak, baik langsung maupun tidak langsung, yang telah mendapatkan ijin dari pihak pengelola untuk dapat melakukan persiapan dan atau menggunakan function room sesuai dengan ketentuan dan kesepakatan yang telah dibuat.</li>
                  <li>Pengelola : pihak panitia expo yang mewakili pengelolaan fasilitas Econvention Hall.</li>
                </ol>

                <b>II.	KETENTUAN UMUM</b> 
                <ol type="1" style="padding-left: 40px;">
                  <li>Pengguna wajib mematuhi peraturan dan tata tertib yang berlaku di lingkungan Econvention Hall.</li>
                  <li>Lingkungan Econvention Hall adalah kawasan bebas narkoba dan asap rokok. Merokok diijinkan di area yang ditentukan oleh Pengelola. Pelanggaran atas larangan merokok akan dikenakan denda Rp 1.000.000,-</li>
                  <li>Pengguna wajib menjaga kebersihan dan kenyamanan bersama selama menggunakan fasilitas Econvention Hall.</li>
                  <li>Pengelola berhak mengambil tindakan yang diperlukan demi keamanan, kenyamanan dan keselamatan bersama.</li>
                  <li>Segala kerusakan pada peralatan yang disebabkan oleh pengguna menjadi tanggungjawab pengguna, Pengguna wajib mengganti atas kerusakan peralatan tersebut.</li>
                  <li>Pengguna wajib memberitahukan dan mendapatkan ijin pengelola untuk menambah peralatan yang akan dipergunakan untuk acara.</li>
                  <li>Pengguna wajib menjaga atau menyiapkan alat keamanan stand dan atau produk.</li>
                </ol>
                    
                <b>III.	DEKORASI, BAZAAR & STAND</b>
                <ol type="1" style="padding-left: 40px;">
                  <li>Segala biaya pemasangan, pembongkaran dekorasi, bazaar & stand menjadi tanggung jawab pengguna.</li>
                  <li>Dekorasi, bazaar & stand tidak boleh merubah, merusak, mengganti, meninggalkan bekas pada partisi.</li>
                  <li>Dilarang keras memaku, mengecat, mengebor, membobok, memotong, menggergaji, mengelupas,menggali dll pada partisi,lantai dan atau stand.</li>
                  <li>Pengguna wajib minta persetujuan tertulis pengelola atas jumlah dan rencana design dekorasi, bazaar & stand, serta materialnya.</li>
                  <li>Pengelola berhak minta pengguna untuk merubah , menghilangkan, mengganti design dekorasi, bazaar & stand, dan atau materialnya.</li>
                  <li>Pengguna wajib menggunakan lakban kertas sebagai media perekat langsung ke partisi.</li>
                  <li>Khusus stand zona kuliner disediakan dapur umum, sehingga dilarang keras menggunakan kompor gas tabung di dalam stand kuliner, hanya diperbolehkan  menggunakan kompor listrik atau kompor gas portable.</li>
                  <li>Dilarang keras menggunakan bahan berbahaya, diantaranya namun tidak terbatas pada petasan/mercon, kembang api, bahan bakar cair dan gas, lilin,dll.</li>
                  <li>Penggunaan bahan mudah terbakar  seperti namun tidak terbatas pada kayu, styreofoam, busa, plastik, kertas, kain,dll. Harus mendapatkan persetujuan tertulis dari Pengelola.</li>
                  <li>Dilarang menggunakan dekorasi hingga menyentuh plafon atau lampu plafon.</li>
                  <li>Pengguna wajib menggunakan alas kerja untuk setiap pekerjaan dekorasi di Econvention Hall.</li>
                  <li>Pengguna wajib menjaga kebersihan pada saat persiapan, pemasangan maupun pembongkaran dekorasi, bazaar & stand.</li>
                  <li>Sampah, puing dan barang bekas lainnya sisa dekorasi, bazaar & stand harus sudah dibersihkan Pengguna paling lambat 2 jam setelah acara selesai.</li>
                  <li>Pengelola berhak mengambil langkah yang diperlukan , termasuk menghentikan persiapan, dan atau acara yang berlangsung, membongkar, menolak dekorasi, bazaar & stand apabila ditemukannya pelanggaran atas tatib ini. Pengelola tidak bertanggung jawab atas segala biaya yang ditimbulkannya.</li>
                </ol>

                <b>IV.	PEMAKAIAN LISTRIK</b>
                <ol type="1" style="padding-left: 40px;">
                  <li>Setiap stand mendapatkan daya listrik 450 watt, untuk penambahan listrik wajib dibayarkan langsung kepada pengelola .</li>
                  <li>Pengguna dilarang keras menggunakan, menarik, melepas sambungan tenaga listrik sendiri tanpa sepengetahuan pengelola.</li>
                  <li>Stand yang menggunakan daya listrik >450 watt  akan dikenakan biaya tambahan sebesar Rp250.000,-/ 2 Ampere berlaku kelipatan.</li>
                  <li>Dilarang menggunakan sumber tenaga listrik selain yang diijinkan pengelola.</li>
                </ol>

                <b>V.	LAIN-LAIN</b>
                <ol type="1" style="padding-left: 40px;">
                  <li>Tata tertib ini mulai berlaku efektif mulai  1 Juni 2018. Pengelola sewaktu-waktu dapat menyempurnakan tata tertib ini tanpa adanya pemberitahuan terlebih dahulu.</li>
                  <li>Pengguna dapat menyampaikan  saran, kritik, atau keluhan kepada pengelola.</li>
                  <li>Pengelola tidak bertanggung jawab terhadap kehilangan, kerusakan produk pengguna.</li>
                </ol>

                <b>VI.	PERNYATAAN</b>
                <ol type="1" style="padding-left: 40px;">
                  <li>Dengan menandatangani lembar ini maka pengguna telah membaca, mengerti dan memahami serta akan mentaati tata tertib penggunaan stand pesta wirausaha komunitas Tangan Di Atas.</li>
                  <li>Pengguna dengan ini bersedia dikenakan sanksi berupa denda Rp 100.000 dan atau bentuk lainnya yang ditentukan oleh pengelola bila ditemukan  pelanggaran atas tata tertib ini.</li>
                </ol>

                <div class="tg-tagsshare">
									<!-- <div class="tg-tags">
										<span>Tags:</span>
										<div class="tg-tagsholder">
											<a class="tg-tag" href="#">Gadgets</a>
											<a class="tg-tag" href="#">Online</a>
											<a class="tg-tag" href="#">Banking</a>
											<a class="tg-tag" href="#">Finance</a>
										</div>
									</div> -->
									<div class="tg-socialshare">
										<span>Share:</span>
										<ul class="tg-socialicons">
											<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
											<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
											<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>