    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Peta Expo</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("expo");?>">Expo</a></li>
						<li class="tg-active">Peta Expo</li>
					</ol>
				</div>
			</div>
		</div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Informasi Stand <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                  Econvention Hall, Ancol, 25-27 Januari 2018
                </h3>
                <div class="tg-eventspeaker">
                </div>
              </figcaption>

              <div class="tg-detailinfo" style="margin-top:50px">

              <div class="form-group ">
                <div class="col-xs-12">
                  <label><strong>Layout Expo Multi Produk</strong></label>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12" align="center">
                  <img src="<?= site_url("assets/frontend/images/expo/expo1.jpg"); ?>" width="900">
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <label><strong>Daftar Pemesan Stand :</strong></label>
                </div>
              </div>				
              <div class="form-group ">
                <div class="col-xs-12" align="center">
					        <div class="row">
                    <?php 
                      if (sizeof($data_booth)>0) {
                        echo "<div class=\"col-xs-3\" align=\"left\">";
                        $j=0;
                        for ($i=0;$i<sizeof($data_booth);$i++) {
                          $nobooth = $data_booth[$i]["no_booth"];
                          $maptype = $data_booth[$i]["map_type"];
                          $usaha = $data_booth[$i]["booked_usaha"];
                          $usaha2 = $data_booth[$i]["booked_usaha2"];
                          $status = $data_booth[$i]["status_pesan"];
                          
                          if ($maptype=="1") {
                            $xpesan = ""; if ($status=="2") { $xpesan = " (B)"; }
                            if ($usaha2!="") { $usaha = $usaha2; }
                            echo "$nobooth. $usaha $xpesan<br>";
                            $j++;
                            if ($j%49==0) { echo "</div><div class=\"col-xs-3\" align=\"left\">"; }
                          }
                        }
                        echo "</div>";
                      }
                    ?>
					        </div>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">&nbsp;</div>
              </div>				
              <div class="form-group ">
                <div class="col-xs-12">
                  <label><strong>Layout Expo Kuliner</strong></label>
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12" align="center">
					        <img src="<?= site_url("assets/frontend/images/expo/kuliner1.jpg"); ?>" width="900">
                </div>
              </div>
              <div class="form-group ">
                <div class="col-xs-12">
                  <label><strong>Daftar Pemesan Stand :</strong></label>
                </div>
              </div>				
              <div class="form-group ">
                <div class="col-xs-12" align="center">
					        <div class="row">
                    <?php 
                      if (sizeof($data_booth)>0) {
                        echo "<div class=\"col-xs-3\" align=\"left\">";
                        $j=0;
                        for ($i=0;$i<sizeof($data_booth);$i++) {
                          $nobooth = $data_booth[$i]["no_booth"];
                          $maptype = $data_booth[$i]["map_type"];
                          $usaha = $data_booth[$i]["booked_usaha"];
                          $usaha2 = $data_booth[$i]["booked_usaha2"];
                          $status = $data_booth[$i]["status_pesan"];
                          
                          if ($maptype=="2") {
                            $xpesan = ""; if ($status=="2") { $xpesan = " (B)"; }
                            if ($usaha2!="") { $usaha = $usaha2; }
                            echo "$nobooth. $usaha $xpesan<br>";
                            $j++;
                            if ($j%9==0) { echo "</div><div class=\"col-xs-3\" align=\"left\">"; }
                          }
                        }
                        echo "</div>";
                      }
                    ?>
                  </div>
                </div>
                
                <div class="tg-tagsshare">
									<!-- <div class="tg-tags">
										<span>Tags:</span>
										<div class="tg-tagsholder">
											<a class="tg-tag" href="#">Gadgets</a>
											<a class="tg-tag" href="#">Online</a>
											<a class="tg-tag" href="#">Banking</a>
											<a class="tg-tag" href="#">Finance</a>
										</div>
									</div> -->
									<div class="tg-socialshare">
										<span>Share:</span>
										<ul class="tg-socialicons">
											<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
											<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
											<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>