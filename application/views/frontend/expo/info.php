    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Info Expo</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("expo");?>">Expo</a></li>
						<li class="tg-active">Info Expo</li>
					</ol>
				</div>
			</div>
		</div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Informasi Stand <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                  Econvention Hall, Ancol, 25-27 Januari 2018
                </h3>
                <div class="tg-eventspeaker">
                </div>
              </figcaption>

              <div class="tg-detailinfo" style="margin-top:50px">
                
                <b>Hai sahabat TDA</b><br><br>

                Kabar gembira untuk anda para pengusaha dan pemilik brand yang ingin memasarkan produknya, mencari reseller, ataupun ingin ekspansi usaha.<br><br>

                <b>Komunitas Tangan Di Atas (TDA)</b> memberikan kesempatan untuk mengenalkan brand dan produk anda dalam "Business Expo" <b>Pesta Wirausaha Nasional 2019</b><br><br>

                Catat tanggal dan tempatnya:<br>
                Tgl : 25, 26, & 27 januari 2019<br>
                @Ecovantion ancol<br><br>

                <b>Pesta Wirausaha</b> adalah acara besar dari tahun ke tahun yang selalu mampu menarik 3000-5000 pengunjung perharinya. Pastikan produk dan usaha anda ikut berpartisipasi di <b>Pesta Wirausaha 2019</b> kali ini, serta raih potensi market dari seluruh pengunjung yang hadir.<br><br>

                Ketentuan Stand:<br>
                - Ukuran 2x2 m<br>
                - Fasilitas 1 meja, 2 kursi & listrik 450 w<br>
                - Peserta stand bisa ikut acara Business Clinic selama 3 hari<br>
                - Tiket Seminar Pesta Wirausaha<br><br>

                Harga untuk 3 hari:<br>
                - Rp 3.500.000 (stand regular)<br>
                - Rp 5.000.000 (stand premium)<br><br>

                Harga Early Bird sampai tanggal 31 Oktober 2018:<br>
                - Rp 3.000.000 (stand regular)<br>
                - Rp 4.000.000 (stand premium)<br><br>

                <b>Penawaran Khusus :</b><br><br>

                <b>PREMIUM BOOTH</b><br>
                - Early Bird 4 juta<br>
                - Normal 5 juta<br>
                *termasuk: 3 tiket PW, 3 tiket masuk Ancol selama 3 hari, materi promosi didalam Goody Bags, 1 meja, 2 kursi dan listrik 450 watt<br>
                - Stand pojok/Dua muka<br><br>

                <b>REGULER BOOTH</b><br>
                - Early Bird 3 juta<br>
                - Normal 3,5 juta<br>
                *termasuk: 1 tiket PW, 1 tiket masuk Ancol selama 3 hari, 1 meja, 2 kursi dan listrik 450 watt<br><br>

                Info layout & pemesanan stand:<br><br>

                <a href="http://expo.pestawirausaha.com/map">expo.pestawirausaha.com/map</a><br><br>

                WA<br><br>

                1. Zona Kuliner<br>
                Sobirin <a href="https://api.whatsapp.com/send?phone=081315350699">081315350699</a><br><br>

                2. Zona Multi Produk<br>
                Fitra sony <a href="https://api.whatsapp.com/send?phone=08995708330">08995708330</a><br><br>

                <a href="http://expo.pestawirausaha.com/">expo.pestawirausaha.com</a><br><br>

                #PWTDA2019<br>
                #Pestawirausaha<br>
                #KolaborAksi<br><br>
 
								<div class="tg-tagsshare">
									<!-- <div class="tg-tags">
										<span>Tags:</span>
										<div class="tg-tagsholder">
											<a class="tg-tag" href="#">Gadgets</a>
											<a class="tg-tag" href="#">Online</a>
											<a class="tg-tag" href="#">Banking</a>
											<a class="tg-tag" href="#">Finance</a>
										</div>
									</div> -->
									<div class="tg-socialshare">
										<span>Share:</span>
										<ul class="tg-socialicons">
											<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
											<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
											<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>