    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Pesan Hotel</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("hotel");?>">Hotel</a></li>
						<li class="tg-active">Pesan Hotel</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
					  <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Pesan Paket Kamar Hotel  <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                <div class="tg-eventspeaker" style="margin-bottom: 50px;">
                  <div class="tg-contentbox">
                    <div class="tg-speakername">
                      <span class="tg-eventcatagory">
                        Paket 3 Malam Aston Marina Jakarta<br>
                        Room Only, 1 Bedroom Superior <br>
                        Checkin 24 Jan 2019 - Checkout 27 Jan 2019<br>
                        Dapat diisi sampai 4 orang<br>
                      </span>
                      <h4 class="text-center">
                        <strong class="text-custom">
													Pemesanan Hotel telah ditutup
                        </strong>
                      </h4>

                    </div>
                  </div>
                </div>
              </figcaption>

              <div class="tg-detailinfo">
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>
    <!--************************************
				Main End
		*************************************-->