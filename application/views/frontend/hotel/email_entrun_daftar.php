<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
<html>
<head><title>Pendaftaran Peserta Entrepreneurun PWN 2019</title></head>
<body>
<font color="#000000" size="2" face="Trebuchet MS">Anda telah melakukan pendaftaran peserta Entrepreneurun PWN 2019.</font><br>
<font color="#000000" size="2" face="Trebuchet MS">Berikut ini adalah data pendaftaran anda :</font><br>
&nbsp;<br>
<table height="0" border="0" cellspacing="0" cellpadding="0">
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Tanggal</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo $tanggal; ?></font>
    </td>
  </tr>
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Jenis Transaksi</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Tiket Entrepreneurun PWN 2019 </font>
    </td>
  </tr>
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Kode Registrasi</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo $kode; ?></font>
    </td>
  </tr>
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Nama</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo $nama; ?></font>
    </td>
  </tr>
    <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Jenis Kelamin</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo $jeniskel; ?></font>
    </td>
  </tr>
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Metode Pembayaran</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo $metodabayar; ?></font>
    </td>
  </tr>  
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Nilai</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo number_format($nilai); ?></font>
    </td>
  </tr> 
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Payment Fee</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo number_format($paymentfee); ?></font>
    </td>
  </tr> 
  <tr>
    <td width="25" height="0" align="left" valign="top">&nbsp;</td>
    <td width="180" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">Total</font>
    </td>
    <td width="0" height="0" align="center" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS">&nbsp;:&nbsp;</font>
    </td>
    <td width="300" height="0" align="left" valign="top">
      <font color="#000000" size="2" face="Trebuchet MS"><?php echo number_format($total); ?></font>
    </td>
  </tr> 
</table>
&nbsp;<br>

   <font color="#000000" size="2" face="Trebuchet MS">Proses pendaftaran Anda belum lengkap. Untuk menyelesaikan transaksi Anda, silakan untuk melakukan pembayaran dalam waktu 48 jam. Jika Anda tidak melakukan pembayaran pada periode waktu yang telah disebutkan di atas maka transaksi Anda akan dibatalkan secara otomatis.</font><br>
   <font color="#000000" size="2" face="Trebuchet MS"></font><br>&nbsp;<br>
   <font color="#000000" size="2" face="Trebuchet MS">Terima kasih.</font><br>&nbsp;<br>
<font color="#000000" size="2" face="Trebuchet MS">Hormat Kami,</font><br>
&nbsp;<br>

<font color="#000000" size="2" face="Trebuchet MS">Panitia Entrepreneurun PWN 2019</font><br>

</body>

</html>