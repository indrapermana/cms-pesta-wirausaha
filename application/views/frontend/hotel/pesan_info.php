    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Pesan Info Hotel</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("hotel");?>">Hotel</a></li>
						<li class="tg-active">Pesan Info Hotel</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Pesan Paket Kamar Hotel  <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                
                <div class="tg-eventspeaker" style="margin-bottom: 50px;">
                  <div class="tg-contentbox">
                    <div class="tg-speakername">
                      <div class="col-xs-6">
                        <span class="tg-eventcatagory">
                          Paket 3 Malam Aston Marina Jakarta<br>
                          Room Only, 1 Bedroom Superior <br>
                          Checkin 24 Jan 2019 - Checkout 27 Jan 2019<br>
                          Dapat diisi sampai 4 orang<br>
                        </span>
                        <h4 class="text-center">
                          <strong class="text-custom">
                            Harga : Rp. 1.800.000 per kamar
                          </strong>
                        </h4>
                      </div>

                      <div class="col-xs-6">
                        <span class="tg-eventcatagory">
                          Paket 2 Malam Aston Marina Jakarta<br>
                          Room Only, 1 Bedroom Superior <br>
                          Checkin 24 Jan 2019 - Checkout 27 Jan 2019<br>
                          Dapat diisi sampai 4 orang<br>
                        </span>
                        <h4 class="text-center">
                          <strong class="text-custom">
                            Harga : Rp. 1.200.000 per kamar
                          </strong>
                        </h4>
                      </div>

                    </div>
                  </div>
                </div>
              </figcaption>
<?php
  $nama_lengkap = ""; $jenis = ""; $jml_kamar = ""; $nilai = ""; $metoda = "";
  $txid = ""; $resultcode = ""; $resultmsg = ""; $resulttext = ""; $authno = ""; 
  $creditcard = ""; $expdate = ""; $bankcode = ""; $vanum = ""; $vavaliddate = ""; 
  $vavalidtime = ""; 

  if (sizeof($hotel)>0) {
    $nama_lengkap = $hotel[0]["nama_lengkap"];
    $jenis = $hotel[0]["jenis_kamar"];
    $jml_kamar = $hotel[0]["jml_kamar"];
    $nilai = $hotel[0]["nilai"];
    $metoda = $hotel[0]["metoda_bayar"];
    $txid = $hotel[0]["np_txid"];
    $resultcode = $hotel[0]["np_resultCode"];
    $resultmsg = $hotel[0]["np_resultMsg"];
    $resulttext = $hotel[0]["np_resultText"];
    $authno = $hotel[0]["np_authNo"];
    $creditcard = $hotel[0]["np_creditcard"];
    $expdate = $hotel[0]["np_expdate"];
    $bankcode = $hotel[0]["np_bankcode"];
    $vanum = $hotel[0]["np_vanum"];
    $vavaliddate = $hotel[0]["np_vavaliddate"];
    $vavalidtime = $hotel[0]["np_vavalidtime"];
  }

  $paymethod = "00";
  $metode = "";
  if ($metoda=="1") { $paymethod = "01"; $metode = 'Kartu Kredit'; }
  if (($metoda=="2") || ($metoda=="3") || ($metoda=="4") || ($metoda=="5") || ($metoda=="6") || ($metoda=="7")) { $paymethod = "02"; $metode = 'Virtual Account Bank'; }
  if (($metoda=="8") || ($metoda=="9")) { $paymethod = "03"; $metode = 'Convencience Store'; }
?>

              <div class="tg-detailinfo">
                <div class="col-xs-12">
                  <p><?php echo $nama_lengkap; ?>, 
                    Pembayaran Pemesanan Hotel PWN 2019 di Hotel Aston Marina Ancol telah dicatat.
                    Berikut Informasi Pembayaran Anda: </p>

                  <p>
                    Invoice : #<?php echo $transid; ?><br>
                    Jumlah Kamar : <?php echo $jml_kamar; ?><br>
                    Pembayaran : Paket Kamar 3 malam<br><br>
                    Metode Pembayaran : <?php echo $metode;?><br>
                    ID Transaksi Pembayaran : <?php echo $txid;?><br><br>
                    <?php
                      if($paymethod == '01') {
                        $tahun = substr($expdate, 0, 2);
                        $bulan = substr($expdate, -2);
                        $cardexp = $bulan . '-' . $tahun;
                    ?>
                    No Kartu : <?php echo $creditcard;?><br>
                    Exp Kartu : <?php echo $cardexp;?><br><br>
                    <?php
                      }else if($paymethod == '02'){
                        $tahun = substr($vavaliddate, 0, 4);
                        $bulan = substr($vavaliddate, 4, 2);
                        $tanggal = substr($vavaliddate, -2);
                        $tanggallimit = $tanggal . '-' . $bulan . '-' . $tahun;

                        $jam = substr($vavalidtime, 0, 2);
                        $menit = substr($vavalidtime, 2, 2);
                        $detik = substr($vavalidtime, -2);
                        $jamlimit = $jam . ':' . $menit . ':' . $detik;
                    ?>

                    Batas Waktu Pembayaran : <strong><?php echo $tanggallimit . ' ' . $jamlimit;?></strong><br>
                    Kode Bank : <?php echo $bankcode;?><br>
                    No Pembayaran : <strong><?php echo $vanum;?></strong><br><br>

                    <?php
                    }else if($paymethod == '03') {
                    ?>

                    Kode Mitra : <?php echo $bankcode;?><br>
                    No Pembayaran : <strong><?php echo $vanum;?></strong><br><br>

                    <?php
                    } else if($paymethod == '04') {
                    ?>

                    No Pembayaran : <strong><?php echo $result->receiptCode;?></strong><br><br>

                    <?php } ?>

                    Status : <?php echo $resultmsg;?><br>
                    <strong>Total Bayar : Rp <?php echo number_format($nilai); ?></strong><br><br>
                    - Apaila pembayaran telah selesai, Lakukan konfirmasi dengan mengirimkan bukti transfer ke  <a href="https://api.whatsapp.com/send?phone=628112238737&amp;text=%20Halo%20Kakak,%20Saya%20<?php echo $nama_lengkap; ?>%20Akan%20Segera%20Bayar%20Rp.%20<?php echo number_format($nilai); ?>%20untuk%20Penginapan%20dengan%20Invoice%20<?php echo $transid; ?>%20Segera%20akan%20kirimkan%20bukti%20transfer">WA +62 811-2238-737 Tim Akotrans PW 2019 Klik disini </a> <br>
                    - Pembayaran akan diproses maksimal 3 x 24 Jam<br>
                    - Informasi pembayaran dari Nicepay dan informasi kamar akan dikirimkan via email saat pembayaran diterima <br><br>

                  </p>
                </div>

                <div class="col-xs-12">
                  <a href="<?= base_url("hotel/pesan"); ?>" class="btn btn-info btn-block text-uppercase waves-effect waves-light">
                    Selesai
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>