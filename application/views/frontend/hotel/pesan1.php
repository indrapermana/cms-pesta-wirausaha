    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Pesan Hotel</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("hotel");?>">Hotel</a></li>
						<li class="tg-active">Pesan Hotel</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

<?php 
$cssclass = "wrapper-page1";
$jml_pesan = 0; $stock_total = 200;
if (sizeof($data_jumlah)>0) {
  $jml_pesan = $data_jumlah->jumlah;
}
$sisa_stock = $stock_total - $jml_pesan;
?>

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Pesan Paket Kamar Hotel  <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                <div class="tg-eventspeaker" style="margin-bottom: 50px;">
                  <div class="tg-contentbox">
                    <div class="tg-speakername">
                      <span class="tg-eventcatagory">
                        Paket 3 Malam Aston Marina Jakarta<br>
                        Room Only, 1 Bedroom Superior <br>
                        Checkin 24 Jan 2019 - Checkout 27 Jan 2019<br>
                        Dapat diisi sampai 4 orang<br>
                      </span>
                      <h4 class="text-center">
                        <strong class="text-custom">
                          Harga : Rp. 1.800.000 per kamar
                        </strong>
                      </h4>
				            
                      <h4 class="text-center">
                        <strong class="text-custom">
                          Jumlah Stock : <?php echo $sisa_stock; ?> kamar
                        </strong>
                      </h4>
                    </div>
                  </div>
                </div>
              </figcaption>

              <div class="tg-detailinfo">
                <hr>

                <?php if ($pesan!="") { ?>
                <div class="alert alert-danger alert-styled-left alert-bordered" style="width:320px;margin:0px auto 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                  <span class="text-semibold"><?php echo $pesan; ?></span>
                </div>
                <?php } ?>


                <?php if($page=="0") { ?>
                <form action="<?= base_url("hotel/pesan1"); ?>" method="post" novalidate="novalidate">
                  <div class="form-group">
										<label><strong>Pilih Event : </strong></label>
									</div>
									
									<div class="form-group">
										<div class="col-xs-12">
											<div class="row">
                        <?php
                          if(count($event) > 0){
                            for($i=0; $i<count($event); $i++){
															$id_event = $event[$i]['id'];
															$nama_event = $event[$i]['nama_event'];
															$gambar = $event[$i]['gambar'];
															$tanggal_event = $event[$i]['tgl_mulai'];
															$tahun = $event[$i]['tahun'];
															$lokasi = $event[$i]['lokasi'];
															$lokasi_lengkap = $event[$i]['lokasi_lengkap'];
															
                              $checked = "";
                              if(isset($event_id)){
                                $checked = ($id_event == $event_id)? "checked" : "";
															}
															
															if($gambar!=""){
                                $img = "/upload/event/".$tahun."/".$gambar;
                              }else{
                                $img = "/upload/big/img3.jpg";
                              }
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px #d6cfcf solid; border-radius:4px; margin-bottom:10px; padding-top: 10px;">
                          <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
														<img src="<?= base_url($img); ?>"  style="margin-top: 20px;" alt="image description">
													</div>
													<div class="col-xs-7 col-sm-7 col-md-9 col-lg-9">
                            <h4 style="margin: 0;"><?= $nama_event; ?></h5>
                            <h5 style="margin: 0;">
															<time datetime="2017-12-12" style="margin: 9px 0 0;">
																<i class="fa fa-clock-o"></i> &nbsp;
																<?= set_datetime($tanggal_event); ?>
															</time>
                            </h5>
                            <hr style="margin: 10px 0;">
                            <p><?= $lokasi."<br>".$lokasi_lengkap; ?></p>
                          </div>
                          <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding-top: 37px;">
                            <input name="event_id" type="radio" <?= $checked; ?> class="form-control" value="<?= $id_event; ?>" style="width:25px;" required >
                          </div>
                        </div>
                        <?php
                            }
                          }
                        ?>
                        </div>
										</div>
									</div>
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="0" type="submit">Lanjutkan</button>
										</div>
									</div>
                </form>

                <?php } else if ($page=="1") { ?>

                <form action="<?= base_url("hotel/pesan1"); ?>" method="post" novalidate="novalidate" enctype="multipart/form-data">
                  <input name="event_id" type="hidden" class="form-control" value="<?= $id_event; ?>">
                  <div class="row">
                    <!-- Kiri -->
                    <div class="col-xs-6">
                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Nama Lengkap</label>
                          <input class="form-control" type="text" name="nama" value="" required="" placeholder="Nama">
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Email</label>
                          <input class="form-control" type="text" value="" name="email" required="" placeholder="Email">
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>No Hp</label>
                          <input class="form-control" type="text" value="" name="hp" required="" placeholder="No Hp">
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>No KTP</label>
                          <input class="form-control" type="text" value="" name="ktp" required="" placeholder="No KTP (Untuk konfirmasi saat checkin)">
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Foto KTP</label>
                          <input class="form-control" type="file" value="" name="foto" required="">
                        </div>
                      </div>
                    </div>

                    <!-- Kanan -->
                    <div class="col-xs-6">
                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>No Tiket PWN</label>
                          <input class="form-control" type="text" value="" name="tiket" required="" placeholder="No Tiket PWN">
                        </div>
                      </div>		

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Asal TDA</label>
                          <input class="form-control" type="text" value="" name="asaltda" required="" placeholder="Asal TDA ">
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Jumlah Kamar</label>
                          <select name="jumlah" class="form-control">
                            <option value="1">1 Kamar</option>
                            <option value="2">2 Kamar</option>
                            <option value="3">3 Kamar</option>
                            <option value="4">4 Kamar</option>
                            <option value="5">5 Kamar</option>
                            <option value="6">6 Kamar</option>
                            <option value="7">7 Kamar</option>
                            <option value="8">8 Kamar</option>
                            <option value="9">9 Kamar</option>
                            <option value="10">10 Kamar</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group ">
                        <div class="col-xs-12">
                          <label>Catatan</label>
                          <textarea class="form-control" name="keterangan" required=""></textarea>
                        </div>
                      </div>
                    </div>
                  </div>

                  <div class="form-group text-center" style="margin-top: 40px;">
                    <div class="col-xs-12">
                      <button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="1" type="submit">Pesan Kamar</button>
                    </div>
                  </div>
                </form>

                <?php } else { ?>
                  <div class="form-group text-center">
                    <div class="col-xs-12">
                      <p>
                        Terima kasih <?php echo $nama; ?>, Pemesanan Akomodasi PWN 2019 di Hotel Aston Marina Ancol telah dicatat. Untuk itu Mohon segera lakukan pembayaran berikut untuk mengamankan pesanan Anda:
                      </p>

                      <p>
                        Invoice : #<?php echo $invoice; ?><br>
                        Jumlah Tiket : <?php echo $jumlah; ?><br>
                        Harga Paket : Rp.<?php echo number_format($harga); ?><br>
                        Pembayaran : Paket Kamar 3 malam<br>
                        <strong>Total Bayar : Rp <?php echo number_format($nilai); ?></strong><br><br>
                        Silahkan lakukan Transfer sejumlah Rp <?php echo number_format($nilai); ?> Ke Rek. 
                        <br><strong> Bank Syariah Mandiri (BSM) 7110041009 a.n Komunitas Tangan Di Atas</strong> <br>sebelum <?php echo $waktu; ?>. <br><br>
                        - Lakukan konfirmasi dengan mengirimkan bukti transfer ke  <a href="https://api.whatsapp.com/send?phone=628112238737&amp;text=%20Halo%20Kakak,%20Saya%20<?php echo $nama; ?>%20Akan%20Segera%20Bayar%20Rp.%20<?php echo number_format($nilai); ?>%20untuk%20Penginapan%20dengan%20Invoice%20<?php echo $invoice; ?>%20Segera%20akan%20kirimkan%20bukti%20transfer">WA +62 811-2238-737 Tim Akotrans PW 2019 Klik disini </a> <br>
                        - Pembayaran akan diproses maksimal 3 x 24 Jam<br>
                        - Informasi kamar akan dikirimkan via email saat pembayaran diterima <br><br>

                        Salam TIM Akotrans Pesta Wirausaha Nasional 2019,
                      </p>
                    </div>
                  </div>
                <?php } ?>

              </div>
            </div>
          </div>
        </div>
      </section>
    </main>
    <!--************************************
				Main End
		*************************************-->