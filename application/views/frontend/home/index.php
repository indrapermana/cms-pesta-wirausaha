      <!--************************************
          Home Slider Start
      *************************************-->
      <div class="tg-bannerholder">
        <a class="tg-btnscroll tg-floating" href="<?= site_url("event"); ?>">
          <img src="<?= base_url("assets/frontend/images/btn-scroll.png"); ?>" alt="image description"> 
          <span>Pesan Booth SEKARANG!</span>
          <i class="fa fa-angle-double-down"></i>
        </a>
        <div id="tg-homeslider" class="tg-homeslider tg-haslayout owl-carousel">				
          <figure class="item tg-bannerimg"  data-vide-options="position: 0% 50%">
            <figcaption>
              <div class="container">
                <div class="tg-slidercontent">
                  <span>Pesta Wirausaha Nasional 2019</span>
                  <h1>#KOLABORAKSI</span></h1>
                  <!-- <div class="tg-upcomingeventcounter"></div>
                  <ul class="tg-matadata tg-eventmatadata">
                    <li>
                      <i class="fa fa-calendar-check-o"></i>
                      <time datetime="2018-05-05">25 - 27 Januari 2019</time>
                    </li>
                    <li>
                      <i class="fa fa-location-arrow"></i>
                      <span>ECO Convention Ancol, Jakarta - Indonesia</span>
                    </li>
                  </ul> -->
                  <div class="search">
                    <div class="warpper">
                      <form action="<?= site_url("home"); ?>" method="post">
                        <input type="text" name="search" id="searchs" class="input-search" placeholder="Search for Event, Movie, Concert, etc." style="border-radius: 2px 0 0 2px; height: 50px;">
                        <button type="submit" name="submit" value="true" id="btn_search" class="btn-search">
                          <i class="fa fa-search"></i> SEARCH
                        </button>
                      </form>
                    </div>
                  </div>
                </div>
              </div>
            </figcaption>
            <!-- <video autoplay="" loop="" muted=""  style="margin: 60px 0px 0px; position: absolute; z-index: -1000; top: 50%; right: 50%; transform: translate(-50%, -50%); visibility: visible; opacity: 0.1; width: 1922px; height: auto;">
              <source src="<?= base_url("assets/frontend/video/pw2016_small.mp4"); ?>" type="video/mp4">
            </video> -->
          </figure>
        </div>
      </div>
      <!--************************************
          Home Slider End
      *************************************-->

      <!--************************************
        Event Start
      *************************************-->
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
						<div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
							<div class="tg-sectionhead">
								<div class="tg-sectionheading">
									<h2>Don’t Miss This Event</h2>
									<h3>Great Event Schedule</h3>
								</div>
								<div class="tg-description">
									<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt utnaecte laboret dolore magna aliqua enim adiatai minim veniam quista nostrud exercitation ullamco laboris nisi ut aliquip exeation.</p>
								</div>
							</div>
						</div>
						<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
							<div class="tg-eventscheduletabs">
								<div class="tg-eventschedulecontent tab-content">
									<div role="tabpanel" class="tab-pane active" id="day-one">
										<div class="tg-eventschaduletime">
											<h2>Day 01 full schedule</h2>
											<h3>June 27, 2017 @ 09 - 11 am</h3>
										</div>
										<div class="tg-eventvenuetabs">
											<!-- <ul class="tg-eventvenuenav" role="tablist">
												<li role="presentation" class="active"><a href="#hall-one" aria-controls="hall-one" role="tab" data-toggle="tab">Hall Title 01</a></li>
												<li role="presentation"><a href="#hall-two" aria-controls="hall-two" role="tab" data-toggle="tab">Hall Title 02</a></li>
												<li role="presentation"><a href="#hall-three" aria-controls="hall-three" role="tab" data-toggle="tab">Hall Title 03</a></li>
												<li role="presentation"><a href="#hall-four" aria-controls="hall-four" role="tab" data-toggle="tab">Hall Title 04</a></li>
												<li role="presentation"><a href="#hall-five" aria-controls="hall-four" role="tab" data-toggle="tab">Hall Title 05</a></li>
                      </ul> -->
                      <select name="wilayah" id="wilayah" class="form-control">
                        <option value="">Pilih</option>
                        <?php
                          if(count($wilayah) > 0){
                            for($i=0; $i<count($wilayah); $i++){
                              $wilayah_id = $wilayah[$i]['id'];
                              $nama_wilayah = $wilayah[$i]['wilayah'];
                              $selected = ($id_wilayah==$wilayah_id)? "selected" : "";
                        ?>
                        <option value="<?= $wilayah_id; ?>" <?= $selected; ?>><?= $nama_wilayah; ?></option>
                        <?php
                            }
                          }
                        ?>
                      </select>
                      <br><br>
											<div class="tg-eventvenuecontent tab-content">
												<div role="tabpanel" class="tab-pane active" id="views">
                          <?php 
                            if(sizeof($event)>0){
                              for($i=0; $i<sizeof($event); $i++){
                                $id = $event[$i]['id'];
                                $gambar = $event[$i]['gambar'];
                                $judul = $event[$i]['nama_event'];
                                $slug = $event[$i]['slug'];
                                $tanggal = $event[$i]['tgl_mulai'];
                                $tahun = $event[$i]['tahun'];
                                $lokasi = $event[0]['lokasi'];
                                $lokasi_lengakp = $event[0]['lokasi_lengkap'];
              
                                if($gambar!=""){
                                  $img = "/upload/event/".$tahun."/".$gambar;
                                }else{
                                  $img = "/upload/big/img3.jpg";
                                }
                          ?>
													<div class="tg-event">
														<div class="tg-eventspeaker">
															<figure class="tg-eventspeakerimg tg-themeimgborder">
																<img src="<?= base_url($img); ?>" style="max-width: 200px;" width="200" alt="image description">
															</figure>
															<div class="tg-contentbox">
																<div class="tg-eventhead">
																  <div class="tg-leftarea">
                                    <div class="tg-title">
                                      <h2><?= $judul; ?></h2>
                                    </div>
                                    <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($tanggal); ?></time>
																  </div>
                                  <div class="tg-rightarea">
                                    <a class="tg-btnfarword" href="#"><i class="fa fa-mail-forward"></i></a>
                                  </div>
                                </div>
                                <div class="tg-description">
                                  <p><?= $lokasi."<br>".$lokasi_lengakp; ?></p>
                                </div>
															</div>
														</div>
                          </div>
                          <?php 
                              }
                            }
                          ?>
                        </div>
                      </div>
										</div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--************************************
          Event Stop
        *************************************-->

        <!--************************************
            About Us Start					
        *************************************-->
        <section id="tg-aboutus" class="tg-sectionspace tg-haslayout">
          <div class="container">
            <div class="row">
              <div class="tg-shortcode tg-aboutusshortcode">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-left">
                  <div class="tg-shortcodetext">
                    <div class="tg-sectionhead tg-textalignleft">
                      <div class="tg-sectionheading">
                        <h2>Cari Tahu lebih Banyak Bagaimana</h2>
                        <h3>Cara berKOLABORAKSI untuk kemajuan bisnis Anda!</h3>
                      </div>
                      <div class="tg-description">
                        <p>Dapatkan Ilmu dan Inspirasi di Pesta Wirausaha Komunitas Tangan Diatas 2019. Anda bisa konsultasi dengan para narasumber bagaimana mereka menaikan kelas bisnisnya sehingga bisa Anda terapkan dalam bisnis Anda.</p>
                      </div>
                    </div>
                    <div class="tg-btnarea">
                      <a class="tg-btn" href="<?= site_url('event'); ?>">Pesan Booth</a>
                    </div>
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6 pull-right">
                  <figure class="tg-shortcodeimg tg-shadow" style="padding: 0">
                    <video width="100%" controls>
                      <source src="<?= base_url("assets/frontend/video/mainvideo.mp4"); ?>" type="video/mp4">
                    </video>
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--************************************
            About Us End					
        *************************************-->

        <!--************************************
            Statistics Start				
        *************************************-->
        <section class="tg-haslayout tg-bgparallax tg-bgcounter">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <div id="tg-statisticscounters" class="tg-statisticscounters">
                  <div class="tg-counterholder">
                    <div class="tg-counter">
                      <h2><span data-from="0" data-to="1000" data-speed="2000" data-refresh-interval="50">1000</span>+</h2>
                      <h3>Coaches Indonesia</h3>
                      <span class="tg-statisticicon icon-user"></span>
                    </div>
                  </div>
                  <div class="tg-counterholder">
                    <div class="tg-counter">
                      <h2><span data-from="0" data-to="03" data-speed="2000" data-refresh-interval="50">03</span></h2>
                      <h3>Hari Event</h3>
                      <span class="tg-statisticicon icon-calendar-full"></span>
                    </div>
                  </div>
                  <div class="tg-counterholder">
                    <div class="tg-counter">
                      <h2><span data-from="0" data-to="10" data-speed="2000" data-refresh-interval="50">10</span>+</h2>
                      <h3>Workshops</h3>
                      <span class="tg-statisticicon icon-briefcase"></span>
                    </div>
                  </div>
                  <div class="tg-counterholder">
                    <div class="tg-counter">
                      <h2><span data-from="0" data-to="1105" data-speed="2000" data-refresh-interval="50">1105</span></h2>
                      <h3>Peserta Terdaftar</h3>
                      <span class="tg-statisticicon icon-enter"></span>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--************************************
            Statistics End				
        *************************************-->

        <!--************************************
					Speakers Start				
        *************************************-->
        <section class="tg-sectionspace tg-haslayout">
          <div class="container">
            <div class="row">
              <div class="tg-shortcode tg-speakershortcode">
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6" style="text-align: center;margin: 0px 0px 50px;">
                  <div class="tg-shortcodetext">
                    <div class="tg-sectionhead tg-textaligncenter">
                      <div class="tg-sectionheading">
                        <h2>Dapatkan Ilmu dan Inspirasi dari Mereka di</h2>
                        <h3>Panggung Inspirasi</h3>
                      </div>
                      <div class="tg-description">
                        <p>Mereka akan berbagi pengalaman dan pengetahuan selama menjalani Bisnis dan akan membocorkan rahasianya bagaimana bisnis mereka bisa Naik Kelas.</p>
                      </div>
                    </div>
                    <!-- <div class="tg-btnarea">
                      <a class="tg-btn" href="#">View All</a>
                    </div>  -->
                  </div>
                </div>
                <div class="col-xs-12 col-sm-12 col-md-6 col-lg-6">
                  <div class="tg-themepost tg-speaker">
                    <figure>
                      <img src="<?= base_url("assets/frontend/images/pembicara/all.jpg") ?>" alt="image description">
                      <figcaption>
                        <ul class="tg-socialicons">
                          <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                          <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                          <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                          <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                          <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                        </ul>
                      </figcaption>
                    </figure>
                    <div class="tg-posttitle">
                      <h3></h3>										
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                  <div class="tg-themepost tg-speaker">
                    <figure>
                      <img src="<?= base_url("assets/frontend/images/pembicara/cristiansugiono.jpg") ?>" alt="image description">
                      <figcaption>
                        <ul class="tg-socialicons">
                          <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                          <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                          <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                          <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                          <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                        </ul>
                      </figcaption>
                    </figure>
                    <div class="tg-posttitle">
                      <h3><a href="">Christian Sugiono</a></h3>
                      <span>Pengusaha dan Selebritis</span>
                      
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                  <div class="tg-themepost tg-speaker">
                    <figure>
                      <img src="<?= base_url("assets/frontend/images/pembicara/donnykris.jpg") ?>" alt="image description">
                      <figcaption>
                        <ul class="tg-socialicons">
                          <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                          <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                          <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                          <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                          <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                        </ul>
                      </figcaption>
                    </figure>
                    <div class="tg-posttitle">
                      <h3><a href="">Donny Kris Puriyono</a></h3>
                      <span>CEO Malang Strudel</span>
                      
                    </div>
                  </div>
                </div>
                <div class="col-xs-6 col-sm-6 col-md-4 col-lg-4">
                  <div class="tg-themepost tg-speaker">
                    <figure>
                      <img src="<?= base_url("assets/frontend/images/pembicara/wanmuhammad.jpg") ?>" alt="image description">
                      <figcaption>
                        <ul class="tg-socialicons">
                          <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                          <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                          <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                          <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                          <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                        </ul>
                      </figcaption>
                    </figure>
                    <div class="tg-posttitle">
                      <h3><a href="">Wan Muhammad Hasyim</a></h3>
                      <span>Owner Idolmart</span>
                    </div>
                  </div>
                </div>

                <div class="tg-btnarea" style="text-align: center;">
                  <a class="tg-btn" href="#">Dan masih banyak pembicara Lainnya...</a>
                </div> 
              </div>
            </div>
          </div>
        </section>

        
        <section class="tg-haslayout tg-bgparallax tg-bgcounter">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 wajib">
                <h2 style="width:100%;margin: 0px auto;padding: 100px 0px;color: #fff; text-align: center;">MENGAPA WAJIB HADIR DI PESTA WIRAUSAHA 2019?</h2>
                <ol type="1" style="color: #fff; font-size: 21px;">
                  <li style="line-height: 200%;">DIHADIRI 3.000++ PENGUSAHA</li>								
                  <li style="line-height: 200%;">MENEMUKAN INSPIRASI BARU </li>
                  <li style="line-height: 200%;">BERTEMU MITRA BISNIS BARU</li>
                  <li style="line-height: 200%;">BERTEMU SUPPLIER BARU</li>
                  <li style="line-height: 200%;">BERTEMU INVESTOR BARU</li>
                  <li style="line-height: 200%;">MENDAPAT ILMU DIGITAL MARKETING</li>
                  <li style="line-height: 200%;">MENINGKATKAN OMSET MELALUI PENJUALAN ONLINE</li>
                </ol>
                <div style="position: relative;margin: 130px 0px;">
                  <a class="tg-btnscroll tg-floating" href="<?= base_url("event"); ?>beli" style="color: #fff;padding: 14px 10px;border-radius: 10px;border: 1px solid;width: 300px;font-size: 26px;">Beli Tiket Sekarang!</a>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--************************************
            Speakers End				
        *************************************-->

        <!--************************************
            Gallery Start					
        *************************************-->
        <section class="tg-sectionspace tg-haslayout">
          <div class="container">
            <div class="row">
              <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
                <div class="tg-sectionhead">
                  <div class="tg-sectionheading">
                    <h2>Temukan Inspirasi di</h2>
                    <h3>Keseruan Pesta Wirausaha 2019</h3>
                  </div>
                  <div class="tg-description">
                    <p>Tidak hanya Inspirasi bisnis yang akan Anda dapatkan di Pesta Wirauusaha 2019 Tetapi keseruan lainnya untuk mengembangkan bisnis Anda.</p>
                  </div>
                </div>
              </div>
              <div class="col-xs-12 col-sm-12 col-mg-12 col-lg-12">
                <div class="tg-gallerymasnory">
                  <!-- <ul id="tg-navfilterbale" class="tg-navfilterbale tg-optionset">
                    <li><a class="tg-active" data-filter="*" href="javascript:void(0);">Event In 2013</a></li>
                    <li><a data-filter=".womenlaw" href="javascript:void(0);">Event In 2014</a></li>
                    <li><a data-filter=".criminallaw" href="javascript:void(0);">Event In 2015</a></li>
                    <li><a data-filter=".childlaw" href="javascript:void(0);">Event In 2016</a></li>
                    <li><a data-filter=".vatinarylaw" href="javascript:void(0);">Event In 2017</a></li>
                  </ul> -->
                  <div id="tg-galleryfilterable" class="tg-galleryfilterable">
                    <div class="tg-masonrygrid womenlaw">
                      <div class="tg-gallery">
                        <figure class="tg-themeimgborder">
                          <a href="#"><img src="<?= base_url("assets/frontend/images/kontent/1000coach.jpg"); ?>" alt="image description"></a>
                        </figure>
                        <div class="tg-galleryhover">
                          <div class="tg-leftarea">
                            <time datetime="2017-12-12">June 27, 2013</time>
                            <div class="tg-title">
                              <h2>How To Stay Alive In Market</h2>
                            </div>
                          </div>
                          <a class="tg-btnexpand" href="<?= base_url("assets/frontend/images/kontent/1000coach.jpg"); ?>" data-rel="prettyPhoto[gallery]"><i class="icon-frame-expand"></i></a>
                        </div>
                      </div>
                    </div>
                    <div class="tg-masonrygrid womenlaw">
                      <div class="tg-gallery">
                        <figure class="tg-themeimgborder">
                          <a href="#"><img src="<?= base_url("assets/frontend/images/kontent/1000coach.jpg"); ?>" alt="image description"></a>
                        </figure>
                        <div class="tg-galleryhover">
                          <div class="tg-leftarea">
                            <time datetime="2017-12-12">June 27, 2013</time>
                            <div class="tg-title">
                              <h2>How To Stay Alive In Market</h2>
                            </div>
                          </div>
                          <a class="tg-btnexpand" href="<?= base_url("assets/frontend/images/kontent/1000coach.jpg"); ?>" data-rel="prettyPhoto[gallery]"><i class="icon-frame-expand"></i></a>
                        </div>
                      </div>
                    </div>
                    <div class="tg-masonrygrid lethallaw">
                      <div class="tg-gallery">
                        <figure class="tg-themeimgborder">
                          <a href="#"><img src="<?= base_url("assets/frontend/images/kontent/askmentor.jpg"); ?>" alt="image description"></a>
                        </figure>
                        <div class="tg-galleryhover">
                          <div class="tg-leftarea">
                            <time datetime="2017-12-12">June 27, 2013</time>
                            <div class="tg-title">
                              <h2>How To Stay Alive In Market</h2>
                            </div>
                          </div>
                          <a class="tg-btnexpand" href="<?= base_url("assets/frontend/images/kontent/askmentor.jpg"); ?>" data-rel="prettyPhoto[gallery]"><i class="icon-frame-expand"></i></a>
                        </div>
                      </div>
                    </div>
                    <div class="tg-masonrygrid criminallaw">
                      <div class="tg-gallery">
                        <figure class="tg-themeimgborder">
                          <a href="#"><img src="<?= base_url("assets/frontend/images/kontent/askmentor.jpg"); ?>" alt="image description"></a>
                        </figure>
                        <div class="tg-galleryhover">
                          <div class="tg-leftarea">
                            <time datetime="2017-12-12">June 27, 2013</time>
                            <div class="tg-title">
                              <h2>How To Stay Alive In Market</h2>
                            </div>
                          </div>
                          <a class="tg-btnexpand" href="<?= base_url("assets/frontend/images/kontent/askmentor.jpg"); ?>" data-rel="prettyPhoto[gallery]"><i class="icon-frame-expand"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!--************************************
            Gallery End						
        *************************************-->

        <section class="tg-haslayout tg-bgparallax tg-bgcounter">
          <div class="container">
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <h2 style="margin: 0px auto;padding: 100px 0px;color: #fff;">
                  Amankan Seat Anda SEKARANG, Hanya Rp.900.000/Peserta 
                  <a class="tg-btnscroll tg-floating" href="<?= base_url("event"); ?>" style="color: #fff;background: red;padding: 5px 10px;border-radius: 10px;border: 1px solid; left: 30%;">Pesan Tiket Event</a>
                </h2>
              </div>
            </div>
          </div>
        </section>
			
      </main>
      <!--************************************
          Main End
      *************************************-->