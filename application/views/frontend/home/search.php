<!--************************************
Inner Baner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-bgparallax">
  <div class="container">
    <div class="tg-innerbannercontent">
      <div class="tg-pagetitle">
        <h1>Search by <?= $search; ?></h1>
      </div>
      <ol class="tg-breadcrumb">
        <li><a href="<?= base_url("home"); ?>">Beranda</a></li>
        <li class="tg-active">Search</li>
      </ol>
    </div>
  </div>
</div>
<!--************************************
Inner Baner End
*************************************-->

<!--************************************
Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
  <section class="tg-sectionspace tg-haslayout">
    <div class="container">
      <div class="row">
        <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
          <div class="tg-sectionhead">
            <div class="tg-sectionheading">
              <h3>Pencarian <br>" <?= $search; ?> "</h3>
            </div>
            <!-- <div class="tg-description">
              <p>Tidak hanya Inspirasi bisnis yang akan Anda dapatkan di Pesta Wirauusaha 2019 Tetapi keseruan lainnya untuk mengembangkan bisnis Anda.</p>
            </div> -->
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="event-item">
            <div class="item-body row">
            <?php
              // $event = $this->PW_Model->get_event_by_wilayah($wilayah[$i]['id'], 4);
              if(count($event)>0){
                for($x=0; $x<count($event); $x++){
                  $id = $event[$x]['id'];
                  $gambar = $event[$x]['gambar'];
                  $judul = $event[$x]['nama'];
                  $slug = $event[$x]['slug'];
                  $tanggal = $event[$x]['tgl_mulai'];
                  $tahun = $event[$x]['tahun'];
                  $wilayah = $event[$x]['wilayah_nama'];
                  $lokasi = $event[0]['lokasi'];
                  // $lokasi_lengakp = $event[0]['lokasi_lengkap'];
                  
                  if($gambar!=""){
                    $img = "/upload/event/".$tahun."/".$gambar;
                  }else{
                    $img = "/upload/big/img3.jpg";
                  }
            ?>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="<?= site_url("event/".$tahun."/".$slug); ?>" style="color:#444349;">
              <div class="item-event-image">
                <img src="<?= base_url($img); ?>">
              </div>
              <div class="item-event-body">
                <h4><?= $judul; ?></h4>
                <?= $wilayah; ?><br>
                <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($tanggal); ?></time>
                <p>Harga Rp. 20.000</p>
              </div>
              </a>
            </div>
            <?php 
                }
              }else{
            ?>
            <div class="col-xs-12 text-center">
              <img src="<?= base_url("assets/frontend/images/not_found.svg"); ?>">
              <h4>Tidak ada kegiatan</h4>
              <p>Coba refresh halaman atau ganti pencarian <a href="<?= site_url(""); ?>">Beranda</a></p>
            </div>
            <?php } ?>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>