<!--************************************
Inner Baner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-bgparallax">
  <div class="container">
    <div class="tg-innerbannercontent">
      <div class="tg-pagetitle">
        <h1>Kegiatan</h1>
      </div>
      <ol class="tg-breadcrumb">
        <li><a href="<?= base_url("home"); ?>">Beranda</a></li>
        <li class="tg-active">Kegiatan</li>
      </ol>
    </div>
  </div>
</div>
<!--************************************
Inner Baner End
*************************************-->

<!--************************************
Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
  <section class="tg-sectionspace tg-haslayout">
    <div class="container">
      <div class="row">
        <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
          <div class="tg-sectionhead">
            <div class="tg-sectionheading">
              <h2>Jangan Lewatkan Acara Ini</h2>
              <h3>Jadwan Acara</h3>
            </div>
            <div class="tg-description">
              <p>Tidak hanya Inspirasi bisnis yang akan Anda dapatkan di Pesta Wirauusaha 2019 Tetapi keseruan lainnya untuk mengembangkan bisnis Anda.</p>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
        <?php 
        if(count($kategori) > 0){
          for($i=0; $i<count($kategori); $i++){
        ?>
          <div class="event-item">
            <div class="item-header">
              <h3><?= $kategori[$i]['nama']; ?></h3>
              <a href="<?= site_url('event/kategori/'.$kategori[$i]['slug']); ?>" class="btn btn-success" style="width: 120px;height: 34px;">Lainnya</a>
            </div>
            <div class="item-body row">
              <?php
              $event = $this->PW_Model->get_event_by_kategori_and_wilayah($kategori[$i]['id'], $this->session->userdata('id_city'), 4);
              if(count($event)>0){
                for($x=0; $x<count($event); $x++){
                  $id = $event[$x]['id'];
                  $gambar = $event[$x]['gambar'];
                  $judul = $event[$x]['nama_event'];
                  $slug = $event[$x]['slug'];
                  $tanggal = $event[$x]['tgl_mulai'];
                  $tahun = $event[$x]['tahun'];
                  $lokasi = $event[0]['lokasi'];
                  $lokasi_lengakp = $event[0]['lokasi_lengkap'];
                  
                  if($gambar!=""){
                    $img = "/upload/event/".$tahun."/".$gambar;
                  }else{
                    $img = "/upload/big/img3.jpg";
                  }
              ?>
              <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
                <a href="<?= site_url("event/".$tahun."/".$slug); ?>" style="color:#444349;">
                <div class="item-event-image">
                  <img src="<?= base_url($img); ?>">
                </div>
                <div class="item-event-body">
                  <h4><?= $judul; ?></h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($tanggal); ?></time>
                  <p>Harga Rp. 20.000</p>
                </div>
                </a>
              </div>
              <?php
                }
              }
              ?>
            </div>
          </div>
        <?php
          }
        } else {
        ?>
          <div class="event-item">
            <div class="item-header">
              <h3>Pontianak</h3>
              <a href="#" class="btn btn-success" style="width: 120px;height: 34px;">Lainnya</a>
            </div>
            <div class="item-body row">
              <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </div>
              <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </div>
              <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </div>
              <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </div>
            </div>
          </div>
        <?php
        }
        ?>
        </div>
      </div>
    </div>
  </section>
</main>
<!--************************************
Main End
*************************************-->