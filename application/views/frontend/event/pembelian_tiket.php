    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Kegiatan</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("event");?>">Kegiatan</a></li>
						<li class="tg-active">Pesan Tiket</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Formulir Pesan Tiket <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                <div class="tg-eventspeaker" style="margin-bottom: 50px;">
                  
                </div>
              </figcaption>

              <div class="tg-detailinfo tg-contactus">
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                  <div class="tg-title">
                    <h2>Pesan Tiket</h2>
                  </div>
                  <form class="tg-formtheme form-horizontal" action="<?= base_url("bayar"); ?>" method="post">
                    <?php if(isset($error)){ ?>
                    <div class="alert alert-danger alert-styled-left alert-bordered" style="width:350px;margin:0px auto 10px;height: 80px;">
                      <button type="button" class="close" data-dismiss="alert">
                        <span>×</span>
                        <span class="sr-only">Close</span>
                      </button>
                      <span class="text-semibold">Maaf! </span><?= $error; ?>
                    </div>
                    <?php } ?>
                    <input type="hidden" name="event_id" value="<?= $id_event; ?>" >
                    <input type="hidden" name="affid" value="<?= $affid; ?>" >

                    <fieldset>
                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Pilih Jenis Tiket</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <div class="row">
                          <?php
                            if(count($jenis_tiket) > 0){
                              foreach($jenis_tiket as $jenis){
                                $checked = "";
                                if(isset($layanan)){
                                  $checked = ($jenis->id == $layanan)? "checked" : "";
                                }
                          ?>
                          <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px #d6cfcf solid; border-radius:4px; margin-bottom:10px; padding-top: 10px;">
                            <div class="col-xs-10 col-sm-10 col-md-11 col-lg-11">
                              <h4 style="margin: 0;"><?= $jenis->nama; ?></h5>
                              <h5 style="margin: 0;">
                                <i class="fa fa-ticket"></i> Rp. <?= number_format($jenis->harga,0,',','.'); ?>
                              </h5>
                              <hr style="margin: 10px 0;">
                              <p><?= $jenis->deskripsi?></p>
                            </div>
                            <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding-top: 37px;">
                              <input name="layanan" type="radio" <?= $checked; ?> class="form-control" value="<?= $jenis->id?>" style="width:25px;" required >
                            </div>
                          </div>
                          <?php
                              }
                            }
                          ?>
                          </div>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Jumlah Tiket</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="jumlah_tiket" class="form-control" placeholder="Jumlah Tiket" value="<?= (isset($telepon))? $telepon: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Panggilan</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="nama" class="form-control" placeholder="Nama Panggilan" value="<?= (isset($nama))? $nama: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Usaha / Brand</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="usaha" class="form-control" placeholder="Nama Usaha / Brand" value="<?= (isset($usaha))? $usaha: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Anggota TDA</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="radio" name="anggota" class="" value="1" required ><span> Sudah Anggota</span><br>
                          <input type="radio" name="anggota" class="" value="0" required ><span> Belum / Umum</span>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Asal Kota</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <select name="daerah" class="form-control" required>
                            <option value="">Pilih Kota</option>
                            <?php foreach($wilayah as $wl) { ?>
                            <option value="<?= $wl->id; ?>">TDA <?= $wl->wilayah?></option>
                            <?php } ?>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Email</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="email" name="email" class="form-control" placeholder="email" value="<?= (isset($email))? $email: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Username</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="username" class="form-control" placeholder="Username" value="<?= (isset($usernmae))? $username: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Password</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="password" name="password" class="form-control" placeholder="Password" value="<?= (isset($password))? $password: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">No. Telepon / HP</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="telepon" class="form-control" placeholder="No. Telepon / HP" value="<?= (isset($telepon))? $telepon: ''; ?>" required >
                        </div>
                      </div>

                      <?php if($status_promo){ ?>
                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Kode Promo</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="kode_promo" class="form-control" placeholder="Kode Promo" value="<?= (isset($kode_promo))? $kode_promo: ''; ?>">
                        </div>
                      </div>
                      <?php } ?>

                      <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                          <button class="btn btn-success text-uppercase " type="submit" name="submit" value="1">
                            <b>Beli Tiket Sekarang!</b>
                          </button>
                          <!-- <input type="submit" name="submit" value="Beli Tiket Sekarang!" class="btn btn-success" > -->
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                  <div class="tg-title">
                    <h2><?= $event[0]['nama']; ?></h2>
                    <div class="tg-decription">
                      <p></p>
                    </div>
                    <ul class="tg-address">
                      <li>
                        <img src="<?= base_url("/upload/event/".$event[0]['tahun']."/".$event[0]['gambar']); ?>" alt="image description">
                      </li>
                      <li>
                        <span>Tanggal : </span>
                        <span><?= set_datetime($event[0]['tgl_mulai'])." - ".set_datetime($event[0]['tgl_selesai']); ?></span>
                      </li>
                      <li>
                        <span>Kategori : </span>
                        <span><?= $event[0]['kategori_nama']; ?></span>
                      </li>
                      <li>
                        <span>Alamat : </span>
                        <span>
                          <em><?= $event[0]['lokasi']; ?></em>
                          <em><?= $event[0]['lokasi_lengkap']?></em>
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>