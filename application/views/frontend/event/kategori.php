<!--************************************
Inner Baner Start
*************************************-->
<div class="tg-haslayout">
  <div class="tg-innerbannercontent">
    <div class="page-header-filter">
      <div class="container">
        <div class="container-header">
          <div class="title-header">
            <h2><?= $kategori[0]['nama']?></h2>
            <p>Mencari hal-hal top yang akan dilakukan di kota ini? Berikut ini adalah acara yang direkomendasikan yang perlu Anda datangi!</p>
          </div>
          <div class="share-header">
            <h6>SHARE</h6>
            <div class="share">
              <a href="#" style="margin: 12px;">
                <img src="<?= base_url("assets/frontend/images/share/facebook.svg"); ?>" alt="FB">
              </a>
              <a href="#" style="margin: 12px;">
                <img src="<?= base_url("assets/frontend/images/share/twitter.svg"); ?>" alt="TW">
              </a>
              <a href="#" style="margin: 12px;">
                <img src="<?= base_url("assets/frontend/images/share/email.svg"); ?>" alt="email">
              </a>
              <a href="#" style="margin: 12px;">
                <img src="<?= base_url("assets/frontend/images/share/link.svg"); ?>" alt="link">
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
    <div class="page-header-filter">
      <div class="container ">
        <div class="content-filter">
          <div class="filter-header">
            <div class="title-filter">Filter Tanggal</div>
            <div class="dropdown dropdown-filter">
              <button class="btn btn-default dropdown-toggle btn-filter" type="button" data-toggle="dropdown">All Date <span class="caret"></span>
              </button>
              <!-- <div class="dropdown-menu">
                a
              </div> -->
              <ul class="dropdown-menu">
                <li><a href="<?= site_url("event/kategori/".$kategori[0]['slug']."/today"); ?>">Hari ini</a></li>
                <li><a href="<?= site_url("event/kategori/".$kategori[0]['slug']."/tomorrow"); ?>">Besok</a></li>
                <li><a href="<?= site_url("event/kategori/".$kategori[0]['slug']."/weekend"); ?>">Hari Libur</a></li>
              </ul>
            </div>
          </div>
          <div class="filter-separator"></div>
          <div class="filter-header">
            <div class="title-filter">Filter Kategori</div>
            <div class="dropdown dropdown-filter">
              <button class="btn btn-default dropdown-toggle btn-filter" type="button" data-toggle="dropdown">All Date <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <?php
                if(count($list_kategori)>0){
                  for($i=0; $i<count($list_kategori); $i++){
                ?>
                <li><a href="<?= site_url("event/kategori/".$list_kategori[$i]['slug']); ?>"><?= $list_kategori[$i]['nama']; ?></a></li>
                <?php
                  }
                }else{
                ?>
                <li><a href="#">Kategori Kosong</a></li>
                <?php
                }
                ?>
              </ul>
            </div>
          </div>
          <!-- <div class="filter-separator"></div>
          <div class="filter-header">
            <div class="title-filter">Filter Date</div>
            <div class="dropdown dropdown-filter">
              <button class="btn btn-default dropdown-toggle btn-filter" type="button" data-toggle="dropdown">All Date <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">HTML</a></li>
                <li><a href="#">CSS</a></li>
                <li><a href="#">JavaScript</a></li>
              </ul>
            </div>
          </div>
          <div class="filter-separator"></div>
          <div class="filter-header">
            <div class="title-filter">Filter Date</div>
            <div class="dropdown dropdown-filter">
              <button class="btn btn-default dropdown-toggle btn-filter" type="button" data-toggle="dropdown">All Date <span class="caret"></span>
              </button>
              <ul class="dropdown-menu">
                <li><a href="#">HTML</a></li>
                <li><a href="#">CSS</a></li>
                <li><a href="#">JavaScript</a></li>
              </ul>
            </div>
          </div> -->
        </div>
      </div>
    </div>
  </div>
</div>
<!--************************************
Inner Baner End
*************************************-->

<!--************************************
Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
  <section class="tg-haslayout">
    <div class="container">
      <div class="row">

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="titles-region">
            <?= count($event); ?> Kegiatan
          </div>
          <div class="body-city row">
            <?php
              if(count($event) > 0){
                for($i=0; $i<count($event); $i++){
                  $id = $event[$i]['id'];
                  $gambar = $event[$i]['gambar'];
                  $judul = $event[$i]['nama_event'];
                  $slug = $event[$i]['slug'];
                  $tanggal = $event[$i]['tgl_mulai'];
                  $tahun = $event[$i]['tahun'];
                  $lokasi = $event[0]['lokasi'];
                  $lokasi_lengakp = $event[0]['lokasi_lengkap'];

                  if($gambar!=""){
                    $img = base_url("upload/event/".$tahun."/".$gambar);
                  }else{
                    $img = base_url("/upload/img3.jpg");
                  }
            ?>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="<?= $img; ?>">
                </div>
                <div class="item-event-body">
                  <h4><?= $judul; ?></h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($tanggal); ?></time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <?php
                }
              } else {
            ?>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <div class="col-sx-12 col-sm-6 col-md-3 col-lg-3">
              <a href="#" style="color:#444349;">
                <div class="item-event-image">
                  <img src="https://d3hzxw6fwm263k.cloudfront.net/uploads/events/photos/1552641515-uFPZYMDlWAVE371pd5WEtNNpWSugzjFK.png?width=640&quality=78">
                </div>
                <div class="item-event-body">
                  <h4>Coba - Coba 1</h4>
                  <time datetime="2017-12-12" style="margin: 9px 0 0;">Sabtu, 31 Agustus 2019 13:00</time>
                  <p>Harga Rp. 20.000</p>
                </div>
              </a>
            </div>
            <?php } ?>
          </div>
        </div>

      </div>
    </div>
  </section>
</main>
<!--************************************
Main End
*************************************-->



