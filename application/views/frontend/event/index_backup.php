<!--************************************
Inner Baner Start
*************************************-->
<div class="tg-innerbanner tg-haslayout tg-bgparallax">
  <div class="container">
    <div class="tg-innerbannercontent">
      <div class="tg-pagetitle">
        <h1>Kegiatan</h1>
      </div>
      <ol class="tg-breadcrumb">
        <li><a href="<?= base_url("home"); ?>">Beranda</a></li>
        <li class="tg-active">Kegiatan</li>
      </ol>
    </div>
  </div>
</div>
<!--************************************
Inner Baner End
*************************************-->

<!--************************************
Main Start
*************************************-->
<main id="tg-main" class="tg-main tg-haslayout">
  <section class="tg-sectionspace tg-haslayout">
    <div class="container">
      <div class="row">
        <div class="col-xs-offset-0 col-xs-12 col-sm-offset-0 col-sm-12 col-md-offset-2 col-md-8 col-lg-offset-2 col-lg-8">
          <div class="tg-sectionhead">
            <div class="tg-sectionheading">
              <h2>Jangan Lewatkan Acara Ini</h2>
              <h3>Jadwan Acara</h3>
            </div>
            <div class="tg-description">
              <p>Tidak hanya Inspirasi bisnis yang akan Anda dapatkan di Pesta Wirauusaha 2019 Tetapi keseruan lainnya untuk mengembangkan bisnis Anda.</p>
            </div>
          </div>
        </div>

        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
          <div class="tg-eventscheduletabs">
            <div class="tg-eventschedulecontent tab-content">
              <div role="tabpanel" class="tab-pane active" id="day-one">
                <div class="tg-eventvenuetabs">
                  <div class="tg-eventvenuecontent tab-content">
                    <div role="tabpanel" class="tab-pane active" id="hall-one">

                      <?php 
                        if(sizeof($event)>0){
                          for($i=0; $i<sizeof($event); $i++){
                            $id = $event[$i]['id'];
                            $gambar = $event[$i]['gambar'];
                            $judul = $event[$i]['nama_event'];
                            $slug = $event[$i]['slug'];
                            $tanggal = $event[$i]['tgl_mulai'];
                            $tahun = $event[$i]['tahun'];
                            $lokasi = $event[0]['lokasi'];
                            $lokasi_lengakp = $event[0]['lokasi_lengkap'];

                            if($gambar!=""){
                            $img = "/upload/event/".$tahun."/".$gambar;
                            }else{
                            $img = "/upload/big/img3.jpg";
                            }
                      ?>
                      <div class="tg-event">
                        <div class="tg-eventspeaker">
                          <figure class="tg-eventspeakerimg tg-themeimgborder">
                            <img src="<?= base_url($img); ?>" style="max-width: 200px;" width="200" alt="image description">
                          </figure>
                          <div class="tg-contentbox">
                            <div class="tg-eventhead">
                              <div class="tg-leftarea">
                                <div class="tg-title">
                                <h2><?= $judul; ?></h2>
                                </div>
                                <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($tanggal); ?></time>
                              </div>
                              <div class="tg-rightarea">
                                <a class="btn btn-success" href="<?= base_url("tiket/".$id."/".$slug); ?>"><i class="fa fa-ticket"></i> Beli</a>
                                <a class="btn btn-default" href="<?= base_url("event/".$tahun."/".$slug); ?>" style="display: inline-block;"><i class="fa fa-eye"></i> Detail</a>
                              </div>
                            </div>
                            <div class="tg-description">
                              <p><?= $lokasi."<br>".$lokasi_lengakp; ?></p>
                            </div>
                          </div>
                        </div>
                      </div>
                      <?php 
                          }
                        }
                      ?>

                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</main>
<!--************************************
Main End
*************************************-->