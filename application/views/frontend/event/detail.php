    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Kegiatan</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("event");?>">Kegiatan</a></li>
						<li class="tg-active">Detail Kegiatan</li>
					</ol>
				</div>
			</div>
		</div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
              <figcaption class="text-center">
                <h3><?= $event->nama; ?></h3>
                <time datetime="2017-12-12" style="margin: 9px 0 0;"><?= set_datetime($event->tgl_mulai)." - ".set_datetime($event->tgl_selesai); ?></time>
                <div class="tg-eventspeaker">
                  <!-- <figure class="tg-eventspeakerimg tg-themeimgborder">
                    <img src="images/speakers/img-18.jpg" alt="image description">
                  </figure> -->
                  <div class="tg-contentbox">
                    <div class="tg-speakername">
                      <span class="tg-eventcatagory"><?= $event->lokasi."<br>".$event->lokasi_lengkap?></span>
                    </div>
                  </div>
                </div>
              </figcaption>
							<figure class="tg-eventspeakerimg">
								<img src="<?= base_url("/upload/event/".$event->tahun."/".$event->gambar); ?>" style="max-width:1170px; max-height:400px; margin: 25px 0" alt="image description">
              </figure>
              
              
              
              <div class="tg-detailinfo">
                <?= $event->materi_event; ?>
                
                <div class="tg-box text-center" style="margin-bottom: 20px;">
                  <a href="<?= base_url("tiket/".$event->id."/".$event->slug); ?>" class="tg-btn tg-btnbookseat">Pesan Tiket</a>
                </div>

								<div class="tg-tagsshare">
									<!-- <div class="tg-tags">
										<span>Tags:</span>
										<div class="tg-tagsholder">
											<a class="tg-tag" href="#">Gadgets</a>
											<a class="tg-tag" href="#">Online</a>
											<a class="tg-tag" href="#">Banking</a>
											<a class="tg-tag" href="#">Finance</a>
										</div>
									</div> -->
									<div class="tg-socialshare">
										<span>Share:</span>
										<ul class="tg-socialicons">
											<li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
											<li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
											<li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
											<li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
											<li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
										</ul>
									</div>
								</div>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>
    <!--************************************
				Main End
		*************************************-->