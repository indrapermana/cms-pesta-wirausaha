    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Kegiatan</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("event");?>">Kegiatan</a></li>
						<li class="tg-active">Pembayaran Tiket</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-contactus">
              <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                <div class="tg-title">
                  <h2>Pembayaran Tiket Anda</h2>
                </div>
                <div class="tg-decription">
                  <p>Terima kasih <?php echo $display_name ?>, Pembelian tiket <?php echo $layanan ?> di <strong><?php echo $event[0]['nama'] ?></strong> dicatat. Untuk itu Mohon segera lakukan pembayaran berikut untuk mengamankan kursi Anda: </p>
                  <p>
                      Tiket akan dikirim ke email <strong><?php echo $user_email; ?></strong>, Pastikan alamat email sudah benar<br>
                      Invoice : #<?php echo $invoice ?><br>
                      Jumlah Tiket : <?php echo $jmltiket ?><br>
                      Harga Tiket : Rp.<?php echo number_format($harga) ?><br>
                      Tambahan kode unik : <?php echo $tambahan ?><br>
                      Pembelian : Tiket <?php echo $layanan ?><br>
                      <?php echo $kalimat ?>
                      Diskon : Rp.<?php echo number_format($diskon) ?><br>
                      <strong>Total Bayar : Rp <?php echo number_format($totalharga) ?></strong><br><br>

                      Silahkan lakukan Transfer sejumlah Rp <?php echo number_format($totalharga) ?> Ke Rek. 
                      <br>
                      <table class="table table-bordered">
                        <tr>
                          <th>Kode Bank</th>
                          <th>Bank</th>
                          <th>Nomor Rekening</th>
                          <th>Atas Nama</th>
                        </tr>
                        
                        <?php for($i=0; $i<count($rekening); $i++){ ?>
                        <tr>
                          <td><strong><?= $rekening[$i]['kode']; ?></strong></td>
                          <td><strong><?= $rekening[$i]['bank']; ?></strong></td>
                          <td><strong><?= $rekening[$i]['nomor']; ?></strong></td>
                          <td><strong><?= $rekening[$i]['nama']; ?></strong></td>
                        </tr>
                        <?php } ?>
                      </table>
                      <br>
                      sebelum <?php  echo date('d M H:i:s', strtotime('+6 hours')) ?> WIB. <br><br>
                      - Lakukan konfirmasi dengan mengirimkan bukti transfer ke  <a href="https://api.whatsapp.com/send?phone=628112238737&text=%20Halo%20Admin,%20Saya%20 <?php echo $display_name ?>%20Akan%20Segera%20Bayar%20Rp.%20<?php echo number_format($totalharga) ?>%20untuk%20Invoice%20<?php echo $invoice ?>%20Segera%20akan%20kirimkan%20bukti%20transfer">WA +62 811-2238-737 Tim Ticketing PW 2019 Klik disini </a> email tiket@pestawirausaha.com setelah transfer <br>
                      - Pembayaran akan diproses maksimal 3 x 24 Jam<br>
                      - Tiket akan dikirimkan via email atau dashboard login saat pembayaran diterima <br><br>

                      Salam TIM Ticketing <?php echo $event[0]['nama'] ?>,
                  </p>
                </div>
              </div>

              <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                <div class="tg-title">
                  <h2><?= $event[0]['nama']; ?></h2>
                  <div class="tg-decription">
                    <p></p>
                  </div>
                  <ul class="tg-address">
                    <li>
                      <span>Tanggal : </span>
                      <span><?= set_datetime($event[0]['tgl_mulai'])." - ".set_datetime($event[0]['tgl_selesai']); ?></span>
                    </li>
                    <li>
                      <span>Kategori : </span>
                      <span><?= $event[0]['kategori_nama']; ?></span>
                    </li>
                    <li>
                      <span>Alamat : </span>
                      <span>
                        <em><?= $event[0]['lokasi']; ?></em>
                        <em><?= $event[0]['lokasi_lengkap']?></em>
                      </span>
                    </li>
                  </ul>
                </div>
              </div>
          </div>
        </div>
      </section>
    </main>