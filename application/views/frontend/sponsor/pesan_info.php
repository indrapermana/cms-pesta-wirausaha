    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Pesan Info Sponsor</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("sponsor");?>">Sponsor</a></li>
						<li class="tg-active">Pesan Info Sponsor</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Info Sponsor <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                
                <div class="tg-eventspeaker">
                </div>
              </figcaption>
<?php
  $nama_lengkap = ""; $jenis = ""; $jml_kamar = ""; $nilai = ""; $metoda = "";
  $txid = ""; $resultcode = ""; $resultmsg = ""; $resulttext = ""; $authno = ""; 
  $creditcard = ""; $expdate = ""; $bankcode = ""; $vanum = ""; $vavaliddate = ""; 
  $vavalidtime = ""; 

  if (sizeof($sponsor)>0) {
    $nama_lengkap = $sponsor[0]["nama"];
    $jenis_paket = $sponsor[0]["jenis_paket"];
    $asal = $sponsor[0]["asal_tda"];
    $usaha = $sponsor[0]["usaha"];
    $no_hp = $sponsor[0]["no_hp"];
    $metoda = $sponsor[0]["metode_bayar"];
    $txid = $sponsor[0]["np_txid"];
    $resultcode = $sponsor[0]["np_resultCode"];
    $resultmsg = $sponsor[0]["np_resultMsg"];
    $resulttext = $sponsor[0]["np_resultText"];
    $authno = $sponsor[0]["np_authNo"];
    $creditcard = $sponsor[0]["np_creditcard"];
    $expdate = $sponsor[0]["np_expdate"];
    $bankcode = $sponsor[0]["np_bankcode"];
    $vanum = $sponsor[0]["np_vanum"];
    $vavaliddate = $sponsor[0]["np_vavaliddate"];
    $vavalidtime = $sponsor[0]["np_vavalidtime"];
  }

  $paymethod = "00";
  $metode = "";
  if ($metoda=="1") { $paymethod = "01"; $metode = 'Kartu Kredit'; }
  if (($metoda=="2") || ($metoda=="3") || ($metoda=="4") || ($metoda=="5") || ($metoda=="6") || ($metoda=="7")) { $paymethod = "02"; $metode = 'Virtual Account Bank'; }
  if (($metoda=="8") || ($metoda=="9")) { $paymethod = "03"; $metode = 'Convencience Store'; }

  $nilai = "0"; $namapaket = "";
  if ($jenis_paket=="1") { $nilai = 5000000; $namapaket = "Paket Sponsor Internal Premium"; }
  if ($jenis_paket=="2") { $nilai = 2500000; $namapaket = "Paket Sponsor Internal VIP"; }
  if ($jenis_paket=="3") { $nilai = 1500000; $namapaket = "Paket Sponsor Internal Super"; }
  if ($jenis_paket=="4") { $nilai = 500000; $namapaket = "Paket Sponsor Internal Insert"; }
  if ($jenis_paket=="5") { $nilai = 300000; $namapaket = "Paket Sponsor Internal Reguler"; }
  if ($jenis_paket=="6") { $nilai = 20000; $namapaket = "Paket Sponsor Internal Test"; }
  if ($jenis_paket=="7") { $nilai = 800000; $namapaket = "Paket Sponsor Internal Reguler Insert"; }
?>

              <div class="tg-detailinfo">
                <div class="col-xs-12">
                  <p>Terima kasih <?= $nama_lengkap; ?>, 
                    atas pembayaran Paket Sponsor Internal PWN 2019. Berikut Informasi Pembayaran Anda :
                  </p>

                  <p>
                    Nama : <?= $nama_lengkap; ?><br>
                    Usaha : <?= $usaha; ?><br>
                    No HP : <?= $no_hp; ?><br>
                    Asal TDA : <?= $asal; ?><br><br>
                    Jenis : <?= $namapaket; ?>
                    Metode Pembayaran : <?php echo $metode;?><br>
                    ID Transaksi Pembayaran : <?php echo $txid;?><br><br>
                    <?php
                      if($paymethod == '01') {
                        $tahun = substr($expdate, 0, 2);
                        $bulan = substr($expdate, -2);
                        $cardexp = $bulan . '-' . $tahun;
                    ?>
                    No Kartu : <?php echo $creditcard;?><br>
                    Exp Kartu : <?php echo $cardexp;?><br><br>
                    <?php
                      }else if($paymethod == '02'){
                        $tahun = substr($vavaliddate, 0, 4);
                        $bulan = substr($vavaliddate, 4, 2);
                        $tanggal = substr($vavaliddate, -2);
                        $tanggallimit = $tanggal . '-' . $bulan . '-' . $tahun;

                        $jam = substr($vavalidtime, 0, 2);
                        $menit = substr($vavalidtime, 2, 2);
                        $detik = substr($vavalidtime, -2);
                        $jamlimit = $jam . ':' . $menit . ':' . $detik;
                    ?>

                    Batas Waktu Pembayaran : <strong><?php echo $tanggallimit . ' ' . $jamlimit;?></strong><br>
                    Kode Bank : <?php echo $bankcode;?><br>
                    No Pembayaran : <strong><?php echo $vanum;?></strong><br><br>

                    <?php
                    }else if($paymethod == '03') {
                    ?>

                    Kode Mitra : <?php echo $bankcode;?><br>
                    No Pembayaran : <strong><?php echo $vanum;?></strong><br><br>

                    <?php
                    } else if($paymethod == '04') {
                    ?>

                    No Pembayaran : <strong><?php echo $result->receiptCode;?></strong><br><br>

                    <?php } ?>

                    Status : <?php echo $resultmsg;?><br>
                    <strong>Total Bayar : Rp <?php echo number_format($nilai); ?></strong><br><br>
                    <ul>
                      <li>Pembayaran akan diproses maksimal 3 x 24 Jam</li>
                      <li>Informasi pembayaran dari Nicepay dan informasi kamar akan dikirimkan via email saat pembayaran diterima</li>
                    </ul>
                  </p>
                </div>

                <div class="col-xs-12">
                  <a href="<?= base_url("sponsor/pesan"); ?>" class="btn btn-info btn-block text-uppercase waves-effect waves-light">
                    Selesai
                  </a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </main>