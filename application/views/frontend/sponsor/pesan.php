    <!--************************************
				Inner Baner Start
		*************************************-->
		<div class="tg-innerbanner tg-haslayout tg-bgparallax">
			<div class="container">
				<div class="tg-innerbannercontent">
					<div class="tg-pagetitle">
						<h1>Formulir Sponsor Internal</h1>
					</div>
					<ol class="tg-breadcrumb">
						<li><a href="<?=base_url("home");?>">Beranda</a></li>
            <li><a href="<?=base_url("hotel");?>">Sponsor</a></li>
						<li class="tg-active">Formulir Sponsor</li>
					</ol>
				</div>
			</div>
    </div>
    <!--************************************
				Inner Baner End
    *************************************-->

    <!--************************************
				Main Start
		*************************************-->
    <main id="tg-main" class="tg-main tg-haslayout">
      <section class="tg-sectionspace tg-haslayout">
				<div class="container">
					<div class="row">
            <div class="tg-detailpage tg-scheduledetail">
							<figcaption class="text-center">
              <img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="width: 250px; margin-bottom: 20px;" class="text-center" alt="image description">
                <h3>
                  Formulir Sponsor Internal <br>
                  <strong class="text-custom">Pesta Wirausaha Nasional 2019</strong> <br>
                </h3>
                <div class="tg-eventspeaker" style="margin-bottom: 50px;">
                  
                </div>
              </figcaption>

              <div class="tg-detailinfo <?= ($id_event!="")? "tg-contactus" : ""; ?>">

                <?php if ($pesan!="") { ?>
                <div class="alert alert-danger alert-styled-left alert-bordered" style="width:320px;margin:0px auto 10px;">
                  <button type="button" class="close" data-dismiss="alert"><span>×</span><span class="sr-only">Close</span></button>
                  <span class="text-semibold"><?php echo $pesan; ?></span>
                </div>
                <?php } ?>

                <?php if($page=="0") { ?>
                <form action="<?= base_url("sponsor/pesan"); ?>" method="post" novalidate="novalidate">
                  <div class="form-group">
										<label><strong>Pilih Event : </strong></label>
									</div>
									
									<div class="form-group">
										<div class="col-xs-12">
											<div class="row">
                        <?php
                          if(count($event) > 0){
                            for($i=0; $i<count($event); $i++){
															$id_event = $event[$i]['id'];
															$nama_event = $event[$i]['nama_event'];
															$gambar = $event[$i]['gambar'];
															$tanggal_event = $event[$i]['tgl_mulai'];
															$tahun = $event[$i]['tahun'];
															$lokasi = $event[$i]['lokasi'];
															$lokasi_lengkap = $event[$i]['lokasi_lengkap'];
															
                              $checked = "";
                              if(isset($event_id)){
                                $checked = ($id_event == $event_id)? "checked" : "";
															}
															
															if($gambar!=""){
                                $img = "/upload/event/".$tahun."/".$gambar;
                              }else{
                                $img = "/upload/big/img3.jpg";
                              }
                        ?>
                        <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12" style="border: 1px #d6cfcf solid; border-radius:4px; margin-bottom:10px; padding-top: 10px;">
                          <div class="col-xs-3 col-sm-3 col-md-2 col-lg-2">
														<img src="<?= base_url($img); ?>"  style="margin-top: 20px;" alt="image description">
													</div>
													<div class="col-xs-7 col-sm-7 col-md-9 col-lg-9">
                            <h4 style="margin: 0;"><?= $nama_event; ?></h5>
                            <h5 style="margin: 0;">
															<time datetime="2017-12-12" style="margin: 9px 0 0;">
																<i class="fa fa-clock-o"></i> &nbsp;
																<?= set_datetime($tanggal_event); ?>
															</time>
                            </h5>
                            <hr style="margin: 10px 0;">
                            <p><?= $lokasi."<br>".$lokasi_lengkap; ?></p>
                          </div>
                          <div class="col-xs-2 col-sm-2 col-md-1 col-lg-1" style="padding-top: 37px;">
                            <input name="event_id" type="radio" <?= $checked; ?> class="form-control" value="<?= $id_event; ?>" style="width:25px;" required >
                          </div>
                        </div>
                        <?php
                            }
                          }
                        ?>
                        </div>
										</div>
									</div>
									<div class="form-group text-center m-t-40">
										<div class="col-xs-12">
											<button class="btn btn-info btn-block text-uppercase waves-effect waves-light" name="tombol" value="0" type="submit">Lanjutkan</button>
										</div>
									</div>
                </form>

                <?php } else if ($page=="1") { ?>
                
                <div class="col-xs-12 col-sm-7 col-md-8 col-lg-8">
                  <div class="tg-title">
                    <h2>Formulir Sponsor</h2>
                  </div>
                  <form class="tg-formtheme form-horizontal" action="<?= base_url("sponsor/pesan"); ?>" method="post">
                    <input type="hidden" name="event_id" value="<?= $id_event; ?>" >

                    <fieldset>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Lengkap</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="nama" class="form-control" placeholder="Nama Lengkap" value="<?= (isset($nama))? $telepon: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Email</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="email" class="form-control" placeholder="Email" value="<?= (isset($email))? $email: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">No HP</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="hp" class="form-control" placeholder="No HP" value="<?= (isset($hp))? $hp: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Nama Usaha / Brand</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <input type="text" name="usaha" class="form-control" placeholder="Nama Usaha / Brand" value="<?= (isset($usaha))? $usaha: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Asal TDA</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                        <input type="text" name="asaltda" class="form-control" placeholder="Asal TDA" value="<?= (isset($asaltda))? $asaltda: ''; ?>" required >
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Jenis Sponsor</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <select name="jenis" class="form-control">
                            <option value="">Pilih</option>
                            <option value="1">Premium : 5 Jt</option>
                            <option value="2">VIP : 2.5 Jt</option>
                            <option value="3">Super : 1.5 Jt</option>
                            <option value="7">Regurer Insert : 800 Rb</option>
                            <option value="4">Insert : 500 Rb</option>
                            <option value="5">Regurer : 300 Rb</option>
                            <option value="6">Tester : 20 Rb</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Metoda Pempayaran</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <select name="metoda" class="form-control">
                            <option value="">Pilih</option>
                            <option value="1">Kartu Kredit</option>
                            <option value="2">Virtual Account Bank</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <label class="control-label col-xs-12 col-sm-4 col-md-3">Keterangan</label>
                        <div class="col-xs-12 col-sm-8 col-md-9">
                          <textarea name="keterangan" class="form-control"><?= (isset($keterangan))? $keterangan : ""; ?></textarea>
                        </div>
                      </div>

                      <div class="form-group text-center m-t-40">
                        <div class="col-xs-12">
                          <button class="btn btn-success btn-block text-uppercase waves-effect waves-light" type="submit" name="tombol" value="1">
                            <b>DAFTAR</b>
                          </button>
                          <!-- <input type="submit" name="submit" value="Beli Tiket Sekarang!" class="btn btn-success" > -->
                        </div>
                      </div>
                    </fieldset>
                  </form>
                </div>
                <div class="col-xs-12 col-sm-5 col-md-4 col-lg-4">
                  <div class="tg-title">
                    <h2><?= $event[0]['nama']; ?></h2>
                    <div class="tg-decription">
                      <p></p>
                    </div>
                    <ul class="tg-address">
                      <li>
                        <img src="<?= base_url("/upload/event/".$event[0]['tahun']."/".$event[0]['gambar']); ?>" alt="image description">
                      </li>
                      <li>
                        <span style="padding-top: 5px;">Tanggal : </span>
                        <span><?= set_datetime($event[0]['tgl_mulai'])." - ".set_datetime($event[0]['tgl_selesai']); ?></span>
                      </li>
                      <li>
                        <span style="padding-top: 5px;">Kategori : </span>
                        <span><?= $event[0]['kategori_nama']; ?></span>
                      </li>
                      <li>
                        <span>Alamat : </span>
                        <span>
                          <em><?= $event[0]['lokasi']; ?></em>
                          <em><?= $event[0]['lokasi_lengkap']?></em>
                        </span>
                      </li>
                    </ul>
                  </div>
                </div>

                <?php } ?>
							</div>
						</div>
          </div>
        </div>
      </section>
    </main>
    <!--************************************
				Main End
		*************************************-->