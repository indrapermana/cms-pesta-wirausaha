<!-- 
  * ----------------------------------------------------------------------------
  * 
  *                             Pesta Wirausaha
  *
  *                          PT. Mobi Media Mandiri
  *                         All Model Pesta Wirausaha
  *                      Create By Muhammad Indra Permana
  * 
  * ----------------------------------------------------------------------------
-->

<!doctype html>
<!--[if lt IE 7]>		<html class="no-js lt-ie9 lt-ie8 lt-ie7" lang=""> <![endif]-->
<!--[if IE 7]>			<html class="no-js lt-ie9 lt-ie8" lang=""> <![endif]-->
<!--[if IE 8]>			<html class="no-js lt-ie9" lang=""> <![endif]-->
<!--[if gt IE 8]><!-->	
<html class="no-js" lang=""> 
  <!--<![endif]-->
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Pesta Wirausaha</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    
    <!-- Icon -->
    <link rel="shortcut icon" href="<?= base_url("assets/frontend/images/logo-pw.png"); ?>">
    
    <!-- Css -->
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/bootstrap.min.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/normalize.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/font-awesome.min.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/icomoon.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/customScrollbar.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/animate.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/owl.carousel.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/prettyPhoto.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/jquery.fullpage.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/transitions.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/main.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/color.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/responsive.css"); ?>">
    <link rel="stylesheet" href="<?= base_url("assets/frontend/css/customs.css"); ?>">
    
    <!-- Vendor JS -->
    <script src="<?= base_url("assets/frontend/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"); ?>"></script>
    <script>
      var site_url = "<?= site_url(); ?>";
    </script>
    <style>
      html,
      body{height: auto;}
    </style>
  </head>
  <body class="tg-home tg-homeone">
    <!--[if lt IE 8]>
      <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->

    <!--************************************
        Wrapper Start
    *************************************-->
    <div id="tg-wrapper" class="tg-wrapper tg-haslayout">
    
      <!--************************************
          Header Start
      *************************************-->
        <?= $header; ?>
      <!--************************************
          Header End
      *************************************-->

      <?= $content; ?>

      <!--************************************
          Footer Start
      *************************************-->
      <footer id="tg-footer" class="tg-footer tg-haslayout">
        <div class="tg-foorterbar">
          <div class="container"> 
            <div class="row">
              <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
                <p class="tg-copyrights">2018 All Rights Reserved © Komunitas Tangan Diatas</p>
                <ul class="tg-socialicons">
                  <li class="tg-facebook"><a href="javascript:void(0);"><i class="fa fa-facebook"></i></a></li>
                  <li class="tg-twitter"><a href="javascript:void(0);"><i class="fa fa-twitter"></i></a></li>
                  <li class="tg-linkedin"><a href="javascript:void(0);"><i class="fa fa-linkedin"></i></a></li>
                  <li class="tg-googleplus"><a href="javascript:void(0);"><i class="fa fa-google-plus"></i></a></li>
                  <li class="tg-rss"><a href="javascript:void(0);"><i class="fa fa-rss"></i></a></li>
                </ul>
                <strong class="tg-logo"><a href="https://tiket.pestawirausaha.com/"><img src="https://tiket.pestawirausaha.com/assets/landing/images/logo.png" alt="Komunitas Tangan Diatas" style="border-radius: 4px;"></a></strong>
              </div>
            </div>
          </div>
        </div> 
      </footer>
      <!--************************************
          Footer End
      *************************************-->
    </div>
    <!--************************************
        Wrapper End
    *************************************-->
    
    <script src="<?= base_url("assets/frontend/js/vendor/jquery-library.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/vendor/bootstrap.min.js"); ?>"></script>
    <script src="https://maps.google.com/maps/api/js?key=AIzaSyCR-KEWAVCn52mSdeVeTqZjtqbmVJyfSus&language=en"></script>
    <script src="<?= base_url("assets/frontend/js/customScrollbar.min.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/packery.pkgd.min.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/owl.carousel.min.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/jquery.hoverdir.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/jquery.vide.min.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/jquery.fullpage.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/isotope.pkgd.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/prettyPhoto.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/countdown.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/countTo.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/appear.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/gmap3.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/main.js"); ?>"></script>
    <script src="<?= base_url("assets/frontend/js/customs.js"); ?>"></script>
  </body>
</html>