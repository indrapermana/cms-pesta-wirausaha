		<header id="tg-header" class="tg-header tg-haslayout">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
						<strong class="tg-logo hidden-xs hidden-sm"><a href="#"><img src="<?= base_url("assets/frontend/images/logo.png"); ?>" style="border-radius: 3px;" alt="company logo here"></a></strong>
						<strong class="tg-logo hidden-md hidden-lg"><a href="#"><img src="<?= base_url("assets/frontend/images/logo-pw.png"); ?>" style="border-radius: 3px;" alt="company logo here"></a></strong>
						<div class="tg-navigationarea">
							<nav id="tg-nav" class="tg-nav">
								<div class="navbar-header">
									<button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#tg-navigation" aria-expanded="false">
										<span class="sr-only">Toggle navigation</span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
										<span class="icon-bar"></span>
									</button>
								</div>
								<div id="tg-navigation" class="collapse navbar-collapse tg-navigation">
									<ul>
										<li class="menu-item-has-children <?= isset($city_active)? "current-menu-item" : ""; ?>">
											<a href="<?= base_url("city"); ?>"><?= $this->session->userdata('city'); ?></a>
										</li>
										<li class="<?= isset($home_active)? "current-menu-item" : ""; ?>">
											<a href="<?= base_url("home"); ?>">Beranda</a>
										</li>
										<li class="<?= isset($event_active)? "current-menu-item" : ""; ?>">
											<a href="<?= base_url("event"); ?>">Kegiatan</a>
										</li>
										<li class="menu-item-has-children <?= isset($expo_active)? "current-menu-item" : ""; ?>">
											<a href="javascript:void(0);">Expo</a>
											<ul class="sub-menu">
												<li><a href="<?= base_url("expo/info"); ?>">Info</a></li>
												<li><a href="<?= base_url("expo/tatatertib"); ?>">Tata Tertib</a></li>
												<li><a href="<?= base_url("expo/peta"); ?>">Peta Expo</a></li>
												<li><a href="<?= base_url("expo/pesan"); ?>">Pesan Booth</a></li>
											</ul>
										</li>
										<li class="menu-item-has-children <?= isset($hotel_active)? "current-menu-item" : ""; ?>">
											<a href="javascript:void(0);">Hotel</a>
											<ul class="sub-menu">
												<li><a href="<?= base_url("hotel/info"); ?>">Info</a></li>
												<li><a href="<?= base_url("hotel/pesan"); ?>">Pesan Kamar</a></li>
											</ul>
										</li>
									</ul>
								</div>
							</nav>
							<a class="tg-btn tg-btnbookseat" href="<?= base_url("login"); ?>">Login</a>
						</div>
					</div>
				</div>
			</div>
		</header>