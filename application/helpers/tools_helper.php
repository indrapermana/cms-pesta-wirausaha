<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('getBulan')){
  function getBulan($bulan){
    $bulan = (int) $bulan;
    $months = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");
    return ($months[$bulan-1]);
  }
}

if ( ! function_exists('set_datetime')){
  function set_datetime($datetime){
    $date = date_create($datetime);
    
    $year = date_format($date,"Y");
    $month = (int) date_format($date,"m");
    $days = date_format($date,"d");
    $day = date_format($date,"w");
    $hour = date_format($date,"H");
    $minutes = date_format($date,"i");

    if ($year < 1000) {
      $year += 1900;
    }

    $dayarray = array("Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu");
    $montharray = array("Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember");

    return $dayarray[$day].",  ".$days." ".$montharray[$month-1]." ".$year." ".$hour.":".$minutes;
    // return $dayarray[$day].",  ".$days." ".$montharray[$month-1]." ".$year;
  }
}

if( ! function_exists('encode') ){
  function encode($data){
    $encode = base64_encode($data);
    return base64_encode($encode."|".$data."|".$encode);
  }
}

if( ! function_exists('decode') ){
  function decode($data){
    $decode = base64_decode($data);
    $data = explode("|",$decode);
    return $data[1];
  }
}

if( ! function_exists('get_weekend')){
  function get_weekend($now){
    $now = strtotime($now);
    $end_date = strtotime("+4 weeks");

    $dates = [];

    while (date("Y-m-d", $now) != date("Y-m-d", $end_date)) {
      $day_index = date("w", $now);
      if ($day_index == 0 || $day_index == 6) {
        // Print or store the weekends here
        $dates[] = date("Y-m-d",$now);
      }
      $now = strtotime(date("Y-m-d", $now) . "+1 day");
    }

    return($dates);
  }
}