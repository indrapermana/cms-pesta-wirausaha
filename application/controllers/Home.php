<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){

    $submit = $this->input->post('submit', true);
    if($submit){
      $search = $this->input->post('search', true);
      if($search!=""){
        redirect('search/'.$search);
      }
    }

    if($this->session->userdata('city')==""){
      $city= array(
        'id_city'   => '18',
        'city'   => 'Bandung',
        'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'home'
      );
      $this->session->set_userdata($city);
    }else{
      $city= array(
        'postion' => 'home'
      );
      $this->session->set_userdata($city);
    }

    $data['event'] = $this->PW_Model->get_event_by_status("1,2");
    
    $data['id_wilayah'] = "";
    $data['wilayah'] = $this->PW_Model->get_wilayah();
    $data['home_active'] = true;

    $this->template->frontend('home/index', $data);
  }

  public function search($search){
    // $search = $this->input->post('search');
    $data['event'] = $this->PW_Model->get_event_by_name($search);
    $data['search'] = $search;

    $this->template->frontend('home/search', $data);
  }

  public function search_by_wilayah(){
    $wilayah = $this->input->post('wilayah');
    if($wilayah!=""){
      $event = $this->PW_Model->get_event_by_wilayah($wilayah);
      $data = "";
      if(sizeof($event)>0){
        for($i=0; $i<sizeof($event); $i++){
          $id = $event[$i]['id'];
          $gambar = $event[$i]['gambar'];
          $judul = $event[$i]['nama_event'];
          $slug = $event[$i]['slug'];
          $tanggal = $event[$i]['tgl_mulai'];
          $tahun = $event[$i]['tahun'];
          $lokasi = $event[0]['lokasi'];
          $lokasi_lengakp = $event[0]['lokasi_lengkap'];

          if($gambar!=""){
            $img = "/upload/event/".$tahun."/".$gambar;
          }else{
            $img = "/upload/big/img3.jpg";
          }
          $data .= '<div class="tg-event">';
          $data .= '<div class="tg-eventspeaker">';
          $data .= '<figure class="tg-eventspeakerimg tg-themeimgborder">';
          $data .= '<img src='.base_url($img).' style="max-width: 200px;" width="200" alt="image description">';
          $data .= '</figure>';
          $data .= '<div class="tg-contentbox">';
          $data .= '<div class="tg-eventhead">';
          $data .= '<div class="tg-leftarea">';
          $data .= '<div class="tg-title">';
          $data .= '<h2>'.$judul.'</h2>';
          $data .= '</div>';
          $data .= '<time datetime="2017-12-12" style="margin: 9px 0 0;">'.set_datetime($tanggal).'</time>';
          $data .= '</div>';
          $data .= '<div class="tg-rightarea">';
          $data .= '<a class="tg-btnfarword" href="#"><i class="fa fa-mail-forward"></i></a>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '<div class="tg-description">';
          $data .= '<p>'.$lokasi."<br>".$lokasi_lengakp.'</p>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '</div>';
        }
      }else{
        $data = '<p class="text-center"><b>Tidak Ada Kegiatan</b></p>';
      }
      echo $data;
    }else{
      $event = $this->PW_Model->get_event_by_status("1,2", 6);
      $data = "";
      if(sizeof($event)>0){
        for($i=0; $i<sizeof($event); $i++){
          $id = $event[$i]['id'];
          $gambar = $event[$i]['gambar'];
          $judul = $event[$i]['nama_event'];
          $slug = $event[$i]['slug'];
          $tanggal = $event[$i]['tgl_mulai'];
          $tahun = $event[$i]['tahun'];
          $lokasi = $event[0]['lokasi'];
          $lokasi_lengakp = $event[0]['lokasi_lengkap'];

          if($gambar!=""){
            $img = "/upload/event/".$tahun."/".$gambar;
          }else{
            $img = "/upload/big/img3.jpg";
          }
          $data .= '<div class="tg-event">';
          $data .= '<div class="tg-eventspeaker">';
          $data .= '<figure class="tg-eventspeakerimg tg-themeimgborder">';
          $data .= '<img src='.base_url($img).' style="max-width: 200px;" width="200" alt="image description">';
          $data .= '</figure>';
          $data .= '<div class="tg-contentbox">';
          $data .= '<div class="tg-eventhead">';
          $data .= '<div class="tg-leftarea">';
          $data .= '<div class="tg-title">';
          $data .= '<h2>'.$judul.'</h2>';
          $data .= '</div>';
          $data .= '<time datetime="2017-12-12" style="margin: 9px 0 0;">'.set_datetime($tanggal).'</time>';
          $data .= '</div>';
          $data .= '<div class="tg-rightarea">';
          $data .= '<a class="tg-btnfarword" href="#"><i class="fa fa-mail-forward"></i></a>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '<div class="tg-description">';
          $data .= '<p>'.$lokasi."<br>".$lokasi_lengakp.'</p>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '</div>';
          $data .= '</div>';
        }
      }else{
        $data = '<p class="text-center"><b>Tidak Ada Kegiatan</b></p>';
      }
      echo $data;
    }
  }

  public function event($slug){
    $data['event'] = $this->PW_Model->get_event_byslug($slug);
    
    $this->template->frontend('event/index', $data);
  }
}