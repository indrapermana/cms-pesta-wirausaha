<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajak extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    $this->general->session_check();

    $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['ajak'] = $this->PW_Model->get_tiket_by_affid($this->user_id);

    $this->template->backend('ajak/index', $data);
  }

  public function peserta(){
    $this->general->session_check();

    $data['tiket'] = $this->PW_Model->totaltiket($this->user_level, $this->user_id);
    $this->template->backend('ajak/peserta', $data);
  }

  public function penjualan(){
    $this->general->session_check();

    $this->template->backend('ajak/penjualan');
  }

  public function pencairan(){
    $this->general->session_check();

    $this->template->backend('ajak/penjualan');
  }
}