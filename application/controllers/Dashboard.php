<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Dashboard extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
		$this->load->model('PW_Model');
		$this->general->session_check();
  }

  public function index(){

		// die($this->event_id);
    if($this->user_level=="1"){
      if($this->event_id!=""){
        $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
        $data['manage_event'] = true;
	
				$data['total_sales'] = $this->PW_Model->get_totaltiket_by_status($this->event_id, 1);
				$data['total_sponsor'] = $this->PW_Model->get_sponsor_by_status($this->event_id, 1);
				$data['total_user_registrasi'] = $this->PW_Model->get_tiket_user_registration($this->event_id);

        $data['dashboard_active'] = true;
        $this->template->backend('dashboard/index', $data);
			}else{
				redirect("kegiatan/manage");
			}
    }else{
      // die(date('t'));
      $peserta_id = $this->user_id;
      // $nama = $this->user_nama;
			// $role = $this->user_level;
			$reseller = count($this->PW_Model->get_peserta_by_event_and_affid($this->event_id, $peserta_id));
			$tanggal = $this->PW_Model->tanggal($role,$profileid);
			// $revdaily = $this->PW_Model->revdaily($role,$profileid);
			// $daudaily = $this->PW_Model->daudaily($role,$profileid);
			// $nrudaily = $this->PW_Model->nrudaily($role,$profileid);
			// // $dau = $this->PW_Model->dau($role,$profileid);
			// // $nau = $this->PW_Model->nau($role,$profileid);
			// $pendapatanbulanini = $this->PW_Model->pendapatanbulanini($role,$profileid);
			// $pendapatanbulanlalu = $this->PW_Model->pendapatanbulanlalu($role,$profileid);
			// $totalpendapatan = $this->PW_Model->totalpendapatan($role,$profileid);
			// $totalpoin = $this->PW_Model->totalpoin($role,$profileid);
			// $transaksibulanini = $this->PW_Model->transaksibulanini($role,$profileid);
			// $transaksihariini = $this->PW_Model->transaksihariini($role,$profileid);
			// $totalhariini = $this->PW_Model->totalhariini($role,$profileid);
			// //var_dump(count($transaksihariini));
			// $transaksikemaren = $this->PW_Model->transaksikemaren($role,$profileid);
			// // $trxterakhir = $this->PW_Model->transaksiterakhir($role,$profileid);
			// $totaltiket = $this->PW_Model->totaltiket($role,$profileid);
			// $tiketindent = $this->PW_Model->tiketindent($role,$profileid);
			$tiketlima = $this->PW_Model->get_tiket_by_affid($peserta_id, '5');
			// //$totalleads = $this->PW_Model->totalleads($role,$profileid);
			// $totaltrx = $this->PW_Model->totaltrx($role,$profileid);
			// $trxdibayar = $this->PW_Model->trxdibayar($role,$profileid);
			// $mytiket = $this->PW_Model->mytiket($profileid);
			// $leaderboard = $this->PW_Model->leaderboard();
			// //Setting
      // $datasetting = $this->Home_model->setting();
			// $event = $datasetting[0]->event;
			// $tglevent = $datasetting[0]->tgl;
      // $tempat = $datasetting[0]->tempat;

      // if ($role == '0'){
			// 	$komisidaerah = '';
			// 	$totalkomdaerah = '';
			// 	$komisiaff2 = '';
			// 	$totalkomaff = '';
			// }else{
			// 	if($role == '1'){
			// 		redirect('peserta');
			// 	}
			// 	$komisidaerah =  $this->PW_Model->komisidaerah();
			// 	$totalkomdaerah = $this->PW_Model->totalkomdaerah();
			// 	$komisiaff2 = $this->PW_Model->komisiaff();
			// 	$totalkomaff = $this->PW_Model->totalkomaff();
			// }
      
      $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
      $data['tiket'] = $this->PW_Model->get_peserta_by_event_and_gaffid($this->event_id, $peserta_id);
			$data['reseller'] = $reseller;
			$data['tiket_lima'] = $tiketlima;
			$data['dashboard_peserta'] = TRUE;
			
			$this->template->backend('dashboard/peserta', $data);
    }
	}
	
	public function detailtiket(){
		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['total_sales'] = $this->PW_Model->get_totaltiket_by_status($this->event_id, 1);

		$this->template->backend('dashboard/detail_tiket', $data);
	}

	public function detailsponsor(){
		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['total_sponsor'] = $this->PW_Model->get_sponsor_by_status($this->event_id, 1);

		$this->template->backend('dashboard/detail_sponsor', $data);
	}

	public function detailuser(){
		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['total_user_registrasi'] = $this->PW_Model->get_tiket_user_registration($this->event_id);

		$this->template->backend('dashboard/detail_user', $data);
	}
}
