<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Wilayah extends CI_Controller {
  protected $database;
  protected $auth;
  
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');

    $this->load->library('firebase');
    $firebase = $this->firebase->init();
    $this->database = $firebase->getDatabase();
    $this->auth = $firebase->getAuth();
  }

  public function index(){
    // echo "<pre>";print_r($this->get_daerah());die;
    $daerah = [];
    $wilayah = [];

    $data_daerah = $this->get_daerah();
    // echo $data_daerah['Balikpapan']['wilayah'];die;
    $i = 0;
    foreach($data_daerah as $key => $value){
      if(isset($value['wilayah'])){
        $daerah[] = $key;
        $wilayah[] = $value['wilayah'];

        $cek_wilayah = $this->PW_Model->get_wilayah_induk_by_wilayah_induk($value['wilayah']);
        if(count($cek_wilayah) == 0){
          $set_title_wilayah = strtolower($value['wilayah']);
          $slug_wilayah = url_title($set_title_wilayah);
          $id_wilayah = $this->PW_Model->wilayah_induk_insert($value['wilayah'], $slug_wilayah);
          
          $cek_daerah = $this->PW_Model->get_wilayah_by_wilayah($key);
          if(count($cek_daerah) == 0){
            $set_title_daerah = strtolower($key);
            $slug_daerah = url_title($set_title_daerah);

            $logo = isset($value['logo'])? $value['logo'] : "";

            $this->PW_Model->wilayah_insert($key, $slug_daerah, $logo, $id_wilayah);
          } else {
            $set_title_daerah = strtolower($key);
            $slug_daerah = url_title($set_title_daerah);

            $logo = isset($value['logo'])? $value['logo'] : "";

            $this->PW_Model->wilayah_update($cek_daerah[0]['id'], $key, $slug_daerah, $logo, $id_wilayah);
          }
        }else{
          $cek_daerah = $this->PW_Model->get_wilayah_by_wilayah($key);
          if(count($cek_daerah) == 0){
            $set_title_daerah = strtolower($key);
            $slug_daerah = url_title($set_title_daerah);

            $logo = isset($value['logo'])? $value['logo'] : "";

            $this->PW_Model->wilayah_insert($key, $slug_daerah, $logo, $cek_wilayah[0]['id']);
          } else {
            $set_title_daerah = strtolower($key);
            $slug_daerah = url_title($set_title_daerah);

            $logo = isset($value['logo'])? $value['logo'] : "";

            $this->PW_Model->wilayah_update($cek_daerah[0]['id'], $key, $slug_daerah, $logo, $cek_wilayah[0]['id']);
          }
        }
      }
      $i++;
    }

    echo "<pre>";print_r($wilayah);die;
  }

  private function get_daerah_by_id($daerah_id = NULL){
    if (empty($daerah_id) || !isset($daerah_id)) { return FALSE; }
    if ($this->database->getReference('daerah')->getSnapshot()->getChild($daerah_id)){
      return $this->database->getReference('daerah')->getChild($daerah_id)->getValue();
    } else {
      return FALSE;
    }
  }

  private function get_wilayah_by_id($wilayah_id = NULL){
    if (empty($wilayah_id) || !isset($wilayah_id)) { return FALSE; }
    if ($this->database->getReference('wilayah')->getSnapshot()->getChild($wilayah_id)){
      return $this->database->getReference('wilayah')->getChild($wilayah_id)->getValue();
    } else {
      return FALSE;
    }
  }

  private function get_daerah(){
    return $this->database->getReference('daerah')->getValue();
  }

  private function get_wilayah(){
    return $this->database->getReference('wilayah')->getValue();
  }

}