<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Event extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    // $data['event'] = $this->PW_Model->get_event_limit(4);
    // $data['wilayah'] = $this->PW_Model->get_wilayah();
    $data['kategori'] = $this->PW_Model->get_kategori();
    $data['event_active'] = true;

    if($this->session->userdata('city')==""){
      $city= array(
        'id_city'   => '18',
        'city'   => 'Bandung',
        'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'event'
      );
      $this->session->set_userdata($city);
    }else {
      $city= array(
        'postion' => 'event'
      );
      $this->session->set_userdata($city);
    }

    $this->template->frontend('event/index', $data);
  }

  public function wilayah($wilayah_id){
    $data['wilayah'] = $this->PW_Model->get_wilayah_by_id($wilayah_id);
    $data['event'] = $this->PW_Model->get_event_by_wilayah($wilayah_id, 4);

    if($this->session->userdata('city')==""){
      $city= array(
        'id_city'   => '18',
        'city'   => 'Bandung',
        'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'event/wilayah/'.$wilayah_id
      );
      $this->session->set_userdata($city);
    }else {
      $city= array(
        'postion' => 'event/wilayah/'.$wilayah_id
      );
      $this->session->set_userdata($city);
    }

    $this->template->frontend('event/wilayah', $data);
  }

  public function kategori($slug_kategori, $filter = ""){
    $data['kategori'] = $this->PW_Model->get_kategori_by_slug($slug_kategori);
    $data['event'] = $this->PW_Model->get_event_by_kategori_and_wilayah($data['kategori'][0]['id'], $this->session->userdata('id_city'));
    $data['event_active'] = true;
    $data['list_kategori'] = $this->PW_Model->get_kategori();
    // echo "<pre>";print_r($data['list_kategori']);die;

    if($this->session->userdata('city')==""){
      $city= array(
        'id_city'   => '18',
        'city'   => 'Bandung',
        'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'event/kategori/'.$slug_kategori
      );
      $this->session->set_userdata($city);
    }else {
      $city= array(
        'postion' => 'event/kategori/'.$slug_kategori
      );
      $this->session->set_userdata($city);
    }

    if($filter!=""){
      if($filter=="today" || $filter=="tomorrow") {
        $data['event'] = $this->PW_Model->get_event_by_tanggal(date("Y-m-d"), $filter);
      } else {
        $tanggal = get_weekend(date("Y-m-d"));
        $data['event'] = $this->PW_Model->get_event_by_tanggal($tanggal, $filter);
      }
    }

    $this->template->frontend('event/kategori', $data);
  }

  public function detail($tahun="", $slug=""){
    if($tahun=="" || $slug==""){
      redirect("home");
    }else{
      $data['event'] = $this->PW_Model->get_event_by_tahun_slug($tahun, $slug);
      $data['event_active'] = true;
      // echo "<pre>"; print_r($data['event']);die;

      if($this->session->userdata('city')==""){
        $city= array(
          'id_city'   => '18',
          'city'   => 'Bandung',
          'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'event/detail/'.$tahun.'/'.$slug
        );
        $this->session->set_userdata($city);
      }else {
        $city= array(
          'postion' => 'event/detail/'.$tahun.'/'.$slug
        );
        $this->session->set_userdata($city);
      }

      $this->session->set_userdata($city);
      $this->template->frontend('event/detail', $data);
    }
  }

  public function tiket($id_event="", $slug=""){
    if($id_event=="" || $slug==""){
      redirect("home");
    }else{
      if($this->session->userdata('city')==""){
        $city= array(
          'id_city'   => '18',
          'city'   => 'Bandung',
          'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'event/tiket/'.$id_event.'/'.$slug
        );
        $this->session->set_userdata($city);
      }else {
        $city= array(
          'postion' => 'event/tiket/'.$id_event.'/'.$slug
        );
        $this->session->set_userdata($city);
      }

      $affid = ""; $pesan = "";
      if(isset($_GET['aff'])){
        $affid = $_GET['aff'];
				$cookie = array(
          'name'   => 'data',
          'value'  => $affid,                            
          'expire' => 640000,
          'domain' => 'tiket.pestawirausaha.com',                                                                                  
          'secure' => false
			  );
        $this->input->set_cookie($cookie); 
        //var_dump($this->input->cookie('tdadata', false)); 
				$profile = $this->PW_Model->get_user_by_id($affid);
				$jmlprofile = count($profile);
				if ($jmlprofile == '0'){
					$pesan = 'Affiliate ID tidak ditemukan';
				}else{
					$nama = $profile[0]['nama'];
					$pesan = 'Anda akan membeli tiket dari '.$nama;
				}
      } else {
        $cookieData = $this->input->cookie('tdadata',true);
				if ($cookieData == NULL) {
					$affid = '0';
				}else{
					$affid = $cookieData;
				}		
				
				$pesan = '';
				//var_dump($affid);
      }

      $data['id_event'] = $id_event;
      $data['slug'] = $slug;
      $data['affid'] = $affid;
      $data['pesan'] = $pesan;

      $data['event'] = $this->PW_Model->get_event_by_id($id_event);
      $data['jenis_tiket'] = $this->PW_Model->get_layanan_tiket($id_event);
      $data['wilayah'] = $this->PW_Model->get_wilayah(1);
      $data['status_promo'] = ($this->PW_Model->check_kupon(date('Y-m-d H:i:s')) > 0)? TRUE : FALSE;

      $this->template->frontend('event/pembelian_tiket', $data);
    }
  }

  public function bayar(){
    // die($this->input->post('submit'));
    if(!$this->input->post('submit')){
      redirect("home");
    }

    $this->form_validation->set_rules('email', 'Email ID', 'trim|required|valid_email');
    $this->form_validation->set_rules('username', 'Username', 'trim|required');
    $this->form_validation->set_rules('usaha', 'Nama Usaha / Brand', 'trim|required|min_length[3]|max_length[50]|xss_clean');
    $this->form_validation->set_rules('nama', 'Nama Panggilan', 'trim|required|min_length[3]|max_length[50]|xss_clean');
    $this->form_validation->set_rules('password', 'Password', 'trim|required|md5');
    $this->form_validation->set_rules('telepon', 'No. Handphone', 'trim|required');	
    $this->form_validation->set_rules('layanan', 'Tiket', 'trim|required');
    $this->form_validation->set_rules('daerah', 'Kota', 'trim|required');		
    
    $layanan_pilihan =  $this->input->post('layanan');
		$user_email = $this->input->post('email');
		$display_name = $this->input->post('nama');
		$usaha = $this->input->post('usaha');
		$user_hp = $this->input->post('telepon');
		$user_nicename = $this->input->post('nama');
		$affid = $this->input->post('affid');
		$iddaerah = $this->input->post('daerah');
		$password = $this->input->post('password');
		$kode_promo = $this->input->post('kode_promo');
    $anggota = $this->input->post('anggota');
    $event_id = $this->input->post('event_id');
    $username = $this->input->post('username');
    $jumlah_tiket = $this->input->post('jumlah_tiket');
    
    $diskon = 0;
    if ($kode_promo == NULL){
			$diskon = 0;
			$kalimat = '';
		}else{
			$cek_kodepromo = $this->PW_Model->check_kupon_kode($kode_promo);
			$ada_promo = count($cek_kodepromo);
			if ($ada_promo > 0){
				$diskon = $cek_kodepromo[0]['diskon'];
				$kalimat = 'Selamat Anda mendapatkan diskon Rp.'.number_format($diskon).' dengan kode diskon '.$kode_promo.' <br>';
			}else{
				$diskon = 0;
				$kalimat = 'Mohon Maaf kode diskon yang anda gunakan tidak ditemukan. silahkan dicoba kembali klik back atau proses pembayaran ini <br>';
			}
    }
    
    $user_login = $user_email;
    $datalayanan = $this->PW_Model->get_layanan_by_id($layanan_pilihan);
    $layanan = $layanan_pilihan;
    $bulan = $datalayanan[0]['waktu'];
    $harga = $datalayanan[0]['harga'];
    $komisi = $datalayanan[0]['komisi'];
    $daerahkom = $datalayanan[0]['daerah'];
    $hadiahfix = $datalayanan[0]['hadiah'];
    $datadaerah = $this->PW_Model->get_wilayah_by_id($iddaerah);
    $daerah = $datadaerah[0]['wilayah'];
    //Setting
    $datasetting = $this->PW_Model->get_event_by_id($event_id);
    $event = $datasetting[0]['nama'];
    $tglevent = $datasetting[0]['tgl_mulai'];
    $tempat = $datasetting[0]['lokasi'];

    $rekening = $this->PW_Model->get_rekening($event_id);
    // $rekening = $datasetting[0]['rekening'];

    if ($this->form_validation->run() == FALSE) {
      $data = array(
				'error' => 'Silahkan isi form dengan benar '.validation_errors(),
				'jenis_tiket' => $this->PW_Model->get_layanan_tiket($event_id),
				'wilayah' => $this->PW_Model->get_wilayah(1),
				'layanan' => $layanan,				
				// 'harga'=> $harga,				
				'nama' => $display_name,
				'usaha' => $usaha,
				'telepon' => $user_hp,	
				'email' => $user_email,
				'password' => $password,
        'affid' => $affid,
        'id_event' => $event_id,
				'event' => $this->PW_Model->get_event_by_id($event_id),
        'status_promo' => ($this->PW_Model->check_kupon(date('Y-m-d H:i:s')) > 0)? TRUE : FALSE,
        'kode_promo' => $kode_promo,
        'username' => $username,	
			);
			//var_dump($layanan_pilihan);
      $this->template->frontend('event/pembelian_tiket', $data);

    } else {
      if($iddaerah == '0') {
        $data = array(
          'error' => 'Silahkan pilih Kota / Daerah TDA',
          'jenis_tiket' => $this->PW_Model->get_layanan_tiket($event_id),
				  'wilayah' => $this->PW_Model->get_wilayah(1),
          'layanan' => $layanan,				
          // 'harga'=> $harga,				
          'nama' => $display_name,
          'usaha' => $usaha,
          'telepon' => $user_hp,	
          'email' => $user_email,
          'password' => $password,
          'affid' => $affid,
          'id_event' => $event_id,
          'event' => $this->PW_Model->get_event_by_id($event_id),
          'status_promo' => ($this->PW_Model->check_kupon(date('Y-m-d H:i:s')) > 0)? TRUE : FALSE,
          'kode_promo' => $kode_promo,
          'username' => $username,
        );
        $this->template->frontend('event/pembelian_tiket', $data);

      } else {
        $this->load->helper('cookie');
				$affidnya = $this->input->post('affid');
				if ($affidnya == 0){
					if (get_cookie('refcookie')) {
					  $affid = $this->input->cookie('refcookie', TRUE);
					}else{
					  $affid = $this->input->post('affid');					
					}
					//$komisi = '0';
					//$diskon = '0';
					$hadiah = $hadiahfix;
					//$servicenya = 5 / 100;
					//$service = $harga * $servicenya;
					$hargafix = $harga;

				}else{
					$affid = $affidnya;
					$hadiahnya = 10 / 100;
					$hadiah = $hadiahfix;
					$hargafix = $harga;
        }
        
        // $user_email = $this->input->post('email');
				$cekemail = $this->PW_Model->check_email($user_email);
				if ($cekemail == FALSE) {
          // $data_user = array(
          //   'username' => $username,
          //   'passwd' => $password,
          //   'nama' => $display_name,
          //   'email' => $user_email,
          //   'telepon' => $user_hp,
          //   'reg_date' => date('Y-m-d H:i:s'),
          //   'kode_aktivasi' => substr(md5($email), 0, 10),
          //   'userlevel' => 2,
          //   'userstatus' => 0
          // );
          // $user_id = $this->PW_Model->user_insert($data_user);
          $this->PW_Model->user_insert($username, $password, $display_name, $user_email, $user_hp);       
          // insert form data into database	           		
        }
        
        $cekemailid = $this->PW_Model->check_email($user_email);
				foreach ($cekemailid as $dataemail){
					$user_status = $dataemail->userstatus;
					$user_id = $dataemail->id;
					//$user_email = $dataemail->user_email;
					//$display_name = $dataemail->display_name;
					//$tglendnya = strtotime($dataemail->tglend);
					$tglendnya = strtotime(date('Y-m-d'));
				}
				//insert table transaction
				$digits = 3;
				$tambahan = rand(pow(10, $digits-1), pow(10, $digits)-1);
				$totalharga = $hargafix - $diskon + $tambahan;
				$insert_id =  $this->db->insert_id();
        $invoice = $user_id.$tambahan;

        $data_trx = array(
          'event_id' => $event_id,
          'invoice' => $invoice,
          'tanggal' => date('Y-m-d H:i:s'),
          'tipe_tiket' => $layanan,
          'nama' => $display_name,
          'usaha' => $usaha,
          'nilai' => $totalharga,
          'no_hp' => $user_hp,
          'gaffid' => $user_id,
          'affid' => $affid,
          'daerah' => $daerah,
          'kode_bayar' => $invoice,
          'status_bayar' => 0,
          'email' => $user_email,
          'jml_tiket' => $jumlah_tiket,
        );
        // echo "<pre>";print_r($data_trx);die;
        $tiket_id = $this->PW_Model->tiket_insert($data_trx);

        $data_peserta = array(
          'event_id' => $event_id,
          'gaffid' => $user_id, 
          'username' => $username,
          'passwd' => $password,
          'nama' => $display_name
        );
        $this->PW_Model->peserta_insert($data_peserta);

        $data_user_tiket = array(
          'user_id' => $user_id,
          'tiket_id' => $tiket_id,
          'kode_tiket' => '',
          'tanggal' => date('Y-m-d H:i:s'),
          'harga' => $totalharga,
          'aff_id' => $affid,
          'status_bayar' => '0',
          'status_pakai' => '0'
        );

        $this->PW_Model->tiket_user_insert($data_user_tiket);
        
        $tgltrial = date('d M H:i:s', strtotime('+6 hours'));
				$tglend = date('d M H:i:s', strtotime('+6 hours'));							
        // send email
				$to_email = $user_email;	
				$nama = $display_name;	
				// $jmltiket = 1;

        $datamember = array(
          'user_email' => $user_email,
          'user_nicename' => $display_name, 
          'display_name' => $display_name,						
          'layanan' => $layanan,
          'totalharga' => $totalharga,
          'bulan' => $bulan,
          'invoice' => $invoice,
          'jmltiket' => $jumlah_tiket,
          'harga' => $harga,
          'tambahan' => $tambahan,

          'event' => $this->PW_Model->get_event_by_id($event_id),
          'rekening' => $rekening,
          'diskon' => $diskon,
          'kalimat' => $kalimat,
        );
        $this->template->frontend('event/pembayaran_tiket', $datamember);
      }
    }
  }

  function verify($hash=NULL){
    if ($this->PW_Model->verifyEmailID($hash)){
      //$this->session->set_flashdata('verify_msg','<div class="alert alert-success text-center">Your Email Address is successfully verified! Please login to access your account!</div>');
			$data = array(
				'success'=>'Email berhasil di verifikasi',
			);			
      $this->template->login('form_login', $data);
    } else {
      //$this->session->set_flashdata('verify_msg','<div class="alert alert-danger text-center">Sorry! There is error verifying your Email Address!</div>');
      // redirect('/login');
			$data = array(
				'error'=>'Email sudah diverifikasi',
			);			
      $this->template->login('form_login', $data);
    }
  }

  public function pgprocess(){
    //payment gateway nicepay
    //NICEPAY

    // Include Config File
    //include_once "../nicepay/lib/NicepayLib.php";
    require_once(APPPATH.'nicepay/lib/NicepayLib.php');
    $nicepay = new NicepayLib();

    function generateReference(){
      $micro_date = microtime();
      $date_array = explode(" ",$micro_date);
      $date = date("YmdHis",$date_array[1]);
      $date_array[0] = preg_replace('/[^\p{L}\p{N}\s]/u', '', $date_array[0]);
      return "Ref".$date.$date_array[0].rand(100,999);
    }
    /*
     * ____________________________________________________________
     *
     * Virtual Account Payment Method
     * ____________________________________________________________
     */
  
    if(isset($_GET['method'])){
      if($_GET['method']== '1'){
        $payMethod = '1';
      }else{
        echo 'Methode Undefinded';
      }
    }else{
      $payMethod = '2';
    }
  
    if($payMethod == '2'){
      $invoice = $this->input->post('invoice');
      $datatrx = $this->PW_Model->get_tiket_by_invoice($invoice);
      $display_name = $datatrx[0]->nama;
      $user_hp = $datatrx[0]->no_hp;
      $user_email = $datatrx[0]->email;
      $jumlah = $datatrx[0]->nilai;
      //$jumlah = '10000';
      //$bankCd         = $this->input->post('bankCd');
      $bankCd         = 'BBCA';
      $dateNow        = date('Ymd');
      $vaExpiryDate   = date('Ymd', strtotime($dateNow . ' +5 hours')); // Set VA expiry date +1 day (optional)

      // Populate Mandatory parameters to send
      $nicepay->set('payMethod', '02');
      $nicepay->set('currency', 'IDR');
      $nicepay->set('amt', $jumlah); // Total gross amount
      $nicepay->set('referenceNo', generateReference()); // Invoice Number or Referenc Number Generated by merchant
      $nicepay->set('description', 'Payment of Invoice No '.$nicepay->get('referenceNo')); // Transaction description
      $nicepay->set('bankCd', $bankCd);

      $nicepay->set('billingNm', $display_name); // Customer name
      $nicepay->set('billingPhone', $user_hp); // Customer phone number
      $nicepay->set('billingEmail', $user_email); //
      $nicepay->set('billingAddr', 'Jln. Default');
      $nicepay->set('billingCity', 'Jakarta');
      $nicepay->set('billingState', 'DKI Jakarta');
      $nicepay->set('billingPostCd', '17113');
      $nicepay->set('billingCountry', 'Indonesia');

      $nicepay->set('deliveryNm', $display_name); // Delivery name
      $nicepay->set('deliveryPhone', $user_hp);
      $nicepay->set('deliveryEmail', $user_email);
      $nicepay->set('deliveryAddr', 'Jln. Default');
      $nicepay->set('deliveryCity', 'Jakarta');
      $nicepay->set('deliveryState', 'DKI Jakarta');
      $nicepay->set('deliveryPostCd', '17113');
      $nicepay->set('deliveryCountry', 'Indonesia');

      $nicepay->set('vacctValidDt', $vaExpiryDate); // Set VA expiry date example: +1 day
      $nicepay->set('vacctValidTm', date('His')); // Set VA Expiry Time

      // Send Data
      // var_dump($nicepay->requestData);
      $response = $nicepay->requestVA();

      //Send Data
      //$response = $nicepay->requestVA();

      //Process response from NICEPAY
      if(isset($response->data->resultCd) && $response->data->resultCd == "0000"){
        header("Location: ".$response->data->requestURL."?tXid=".$response->tXid);
        //Please save your tXid in your database
      }elseif (isset($response->resultCd)) {
        // In this sample, we echo error message
        echo "<pre>";
        echo "result code    : ".$response->resultCd."\n";
        echo "result message : ".$response->resultMsg."\n";
        echo "</pre>";
      }else {
        // In this sample, we echo error message
        echo "<pre>Connection Timeout. Please Try Again.</pre>";
      }

      // if (isset($response->resultCd) && $response->resultCd == "0000") {
      // 	$responsetxid =  $response->tXid;
      // 	$dataupdate = array(
      // 		'respontxid' => $responsetxid,
      // 	);
      // 	$this->db->where('invoice',$invoice);
      // 	$this->db->update('tbl_transaksi',$dataupdate);
      // echo "<pre>";
      // echo "tXid              : $response->tXid (Save to your database to check status) \n";
      // echo "callbackUrl       : $response->callbackUrl\n";
      // echo "description       : $response->description\n";
      // echo "payment date      : $response->transDt\n"; // YYMMDD
      // echo "payment time      : $response->transTm\n"; // HH24MISS
      // echo "virtual account   : $response->bankVacctNo (For customer to transfer) \n";
      // echo "result code       : $response->resultCd\n";
      // echo "result message    : $response->resultMsg\n";
      // echo "reference no      : $response->referenceNo\n";
      // echo "payment method    : $response->payMethod";
      // echo "</pre>";
      // } elseif(isset($response->resultCd)) {
      //     // API data not correct, you can redirect back to checkout page or echo error message.
      //     // In this sample, we echo error message
      //     echo "<pre>";
      //     echo "result code       :".$response->resultCd."\n";
      //     echo "result message    :".$response->resultMsg."\n";
      //     echo "</pre>";
      // } else {
      //     // Timeout, you can redirect back to checkout page or echo error message.
      //     // In this sample, we echo error message
      //     echo "<pre>Connection Timeout. Please Try again.</pre>";
      // }

    }

    /*
     * ____________________________________________________________
     *
     * Credit Card Payment Method
     * ____________________________________________________________
     */

    elseif($payMethod == '01') {
      $invoice = $this->input->get('invoice');
      $datatrx = $this->PW_Model->get_tiket_by_invoice($invoice);
      $display_name = $datatrx[0]->nama;
      $user_hp = $datatrx[0]->no_hp;
      $user_email = $datatrx[0]->email;
      $jumlah = $datatrx[0]->nilai;
      //$jumlah = '10000';

      // Populate Mandatory parameters to send
      $nicepay->set('payMethod', '01');
      $nicepay->set('currency', 'IDR');
      $nicepay->set('amt', $jumlah); // Total gross amount
      $nicepay->set('referenceNo', generateReference()); // Invoice Number or Referenc Number Generated by merchant
      $nicepay->set('description', 'Payment of Invoice No '.$nicepay->get('referenceNo')); // Transaction description

      $nicepay->set('billingNm', $display_name); // Customer name
      $nicepay->set('billingPhone', $user_hp); // Customer phone number
      $nicepay->set('billingEmail', $user_email); //
      $nicepay->set('billingAddr', 'Jln. Default');
      $nicepay->set('billingCity', 'Jakarta');
      $nicepay->set('billingState', 'DKI Jakarta');
      $nicepay->set('billingPostCd', '17113');
      $nicepay->set('billingCountry', 'Indonesia');

      $nicepay->set('deliveryNm', $display_name); // Delivery name
      $nicepay->set('deliveryPhone', $user_hp);
      $nicepay->set('deliveryEmail', $user_email);
      $nicepay->set('deliveryAddr', 'Jln. Default');
      $nicepay->set('deliveryCity', 'Jakarta');
      $nicepay->set('deliveryState', 'DKI Jakarta');
      $nicepay->set('deliveryPostCd', '17113');
      $nicepay->set('deliveryCountry', 'Indonesia');

      // Send Data
      $response = $nicepay->chargeCard();
      $responsetxid =  $response->tXid;
      $dataupdate = array(
        'respontxid' => $responsetxid,
      );
      
      $this->db->where('invoice',$invoice);		        
      $this->db->update('tbl_transaksi',$dataupdate);

      // Response from NICEPAY
      // var_dump($response);
      // exit();
      if (isset($response->data->resultCd) && $response->data->resultCd == "0000") {
        header("Location: ".$response->data->requestURL."?tXid=".$response->tXid);
        // please save tXid in your database
        // echo "<pre>";
        // echo "tXid              : $response->tXid\n";
        // echo "API Type          : $response->apiType\n";
        // echo "Request Date      : $response->requestDate\n";
        // echo "Response Date     : $response->requestDate\n";
        // echo "</pre>";
      } elseif(isset($response->data->resultCd)) {
        // API data not correct or error happened in bank system, you can redirect back to checkout page or echo error message.
        // In this sample, we echo error message
        // header("Location: "."http://example.com/checkout.php");
        echo "<pre>";
        echo "result code       : ".$response->data->resultCd."\n";
        echo "result message    : ".$response->data->resultMsg."\n";
        // echo "requestUrl        : ".$response->data->requestURL."\n";
        echo "</pre>";
      } else {
        // Timeout, you can redirect back to checkout page or echo error message.
        // In this sample, we echo error message
        // header("Location: "."http://example.com/checkout.php");
        echo "<pre>Connection Timeout. Please Try again.</pre>";
      }
    }

    // Unknown
    else {
      echo "Unknown method";
    }
  }

  
}