<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    if( ! $this->session->userdata('login')){
      if($this->input->post('submit')){
        $this->load->library('form_validation');
        $this->form_validation->set_rules('username', 'Username', 'trim|xss_clean|strip_tags|required');
        $this->form_validation->set_rules('password', 'Password', 'trim|xss_clean|strip_tags|required');

        if($this->form_validation->run()){
          $username = $this->security->sanitize_filename($this->input->post('username'));
          $password = $this->security->sanitize_filename($this->input->post('password'));
          $check = $this->PW_Model->login($username, $password);
          $get = $check->row();
          $submit = ($this->session->userdata('submitlog'))? $this->session->userdata('submitlog') : 0;
          // echo "<pre>";print_r($get);die;
          if($check->num_rows() > 0){
            $sessdata = array(
					    'id' => $get->id,
					    'nama' => $get->nama,
              'ulevel' => $get->userlevel,
              'login' => TRUE,
					  );
            //echo "<pre>";print_r($sess);die;
            $this->session->set_userdata($sessdata);
            if($get->userlevel=="1") {
              redirect("kegiatan/manage");
            } elseif($get->userlevel=="2") {
              $data_peserta = $this->PW_Model->get_peserta_by_gaffid($get->id);

              $this->session->set_userdata(['eventid'=>$data_peserta->event_id]);

              redirect("dashboard");
            }
          }else{
            $this->input->set_cookie('msg_error', "Username atau Password Anda Salah", time()+3600);
            $this->session->sess_destroy();
            redirect("login");
          }
        }
      }

      $this->template->login('form_login');
    }else{
      if($this->session->userdata('ulevel')=="1") {
        redirect("kegiatan/manage");
      } elseif($this->session->userdata('ulevel')=="2") {
        redirect("dashboard");
      }
    }
  }

  public function logout(){
		if($this->session->userdata('login')) {
      $this->session->sess_destroy();
      redirect("login");
    }
	}
}
