<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sales extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

    $this->template->backend('sales/rekap', $data);
  }

  public function tiketonline($target = ""){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

    $tombol = $this->input->post("tombol",true);
    if ($tombol!="") {
      $nilai = $this->input->post("nilai",true);

      if ($nilai!="") {
        $this->PW_Model->upd_targettiket($nilai);
        $target = "";
      }
    }

    if ($target!="") {
      $data['data_tiket'] = $this->PW_Model->get_targettiket($this->event_id); 
    } else {
      $data['data_target'] = $this->PW_Model->get_targettiket($this->event_id);
      $data['data_tiket'] = $this->PW_Model->get_rekaptiket($this->event_id); 

      $data['data_daerah'] = $this->PW_Model->get_tiket_daerah($this->event_id); 
      $data['data_affiliate'] = $this->PW_Model->get_tiket_affiliate($this->event_id); 
      $data['data_kodeaff'] = $this->PW_Model->get_kode_affiliate(); 
    }

    $data['target'] = $target;
    $this->template->backend('sales/tiket_online', $data);
  }

  public function tiketgroup($tgid="0"){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$tiket = $this->input->post("tiket",true);
			$bayar = $this->input->post("bayar",true);
			$catatan = $this->input->post("catatan",true);
			
			$this->PW_Model->grouptiket_insert($tgid,$tiket,$bayar,$catatan);
		}
		
		if ($tgid!="0") {
			$data['data_tiket'] = $this->PW_Model->grouptiket_by_id($tgid);
			$data['data_tiket_trans'] = $this->PW_Model->get_grouptiket_trans($tgid);
			$data['site_form'] = "form";
		} else {
			$status = $this->input->post("status",true);
			if ($status=="") { $status = "0"; }
		
			$data['data_tiket'] = $this->PW_Model->get_grouptiket();
			$data['status'] = $status;
			$data['site_form'] = "list";
		}
		$data['tgid'] = $tgid;
		 
		$this->template->backend('sales/tiket_group', $data);
  }
  
  public function distribusitiket(){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		$urutan = $this->input->post("urutan",true);
		if ($urutan=="") { $urutan = "1"; }
    
    $data['urutan'] = $urutan;
		$data['data_wilayah'] = $this->PW_Model->get_tiket_wilayah(); 
		$data['data_daerah'] = $this->PW_Model->get_tiket_daerah($this->event_id); 
    $data['data_group'] = $this->PW_Model->get_grouptiket();
    
    $this->template->backend('sales/tiket_wilayah', $data);
	}
	
	public function jnstiket($aksi="",$jenisid="0"){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$nama = $this->input->post("nama",true);
			$harga = $this->input->post("harga",true);
			$status = $this->input->post("status",true);
			$waktu = $this->input->post("waktu",true);
			$komisi = $this->input->post("komisi",true);
			$daerah = $this->input->post("daerah",true);
			$hadiah = $this->input->post("hadiah",true);
			$deskripsi = $this->input->post("deskripsi",true);
			$url = " ";
			$reseller = 0;
			$harga_fix = $harga;
			$jumlah = $this->input->post("jumlah",true);

			if ($aksi=="tambah") { $this->PW_Model->layanan_insert($this->event_id, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah); }
			if ($aksi=="ubah") { $this->PW_Model->layanan_update($jenisid, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah); }
			if ($aksi=="hapus") { $this->PW_Model->layanan_delete($jenisid); }
			$aksi = "";
		}

		if ($aksi=="") {
			$data['site_form'] = "list";
			$data['jenis_tiket'] = $this->PW_Model->get_layanan($this->event_id);
    	} else {
			$data['site_form'] = "form";
			$data['data_jenis'] = $this->PW_Model->get_layanan_by_id($jenisid);
		}
		
		$data['aksi'] = $aksi;
		$data['jenisid'] = $jenisid;

		$this->template->backend('sales/tiket_jenis', $data);	
	}	
	
	public function rekon() {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$submit = 0; $data_rekon = array();
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			
			$txtbank = $this->input->post("txtbank",true);
			
			if ($txtbank!="") {
				$ltxtbank = explode("\r\n",$txtbank);
				if (sizeof($ltxtbank)>0) {
					for ($i=0;$i<sizeof($ltxtbank);$i++) {
						if (trim($ltxtbank[$i])!="") {
							$ktxtbank = explode("|",$ltxtbank[$i]);
							$tgl = ""; $jam = ""; $noreff = ""; $deskripsi = ""; $nilai = ""; $debet = ""; $kredit = ""; $saldo = "";
							if (sizeof($ktxtbank)>0) {
								$tgl = $ktxtbank[0];
								$jam = $ktxtbank[1];
								$tanggal = substr($tgl,0,4)."-".substr($tgl,4,2)."-".substr($tgl,6,2)." ".$jam;
								$noreff = $ktxtbank[2];
								$deskripsi = $ktxtbank[3];
								$nilai = $ktxtbank[4];
								$knilai = explode(" ",$nilai);
								if (sizeof($knilai)>0) {
									if ($knilai[0]=="D") { $debet = $knilai[1]; $kredit = "0"; }
									else { $kredit = $knilai[1]; $debet = "0"; }
								}
								$saldo = $ktxtbank[7];
							}
							if (($debet!="") && ($kredit!="")) {
								$data_rekon[] = array($tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo);
							}
						}
					}
				}
				$submit = 1;
			}
		}
		$data['data_tiket'] = $this->PW_Model->get_datatiket($this->event_id);
		$data['data_rekon'] = $data_rekon;
		$data['submit'] = $submit;
		
		$this->template->backend('sales/rekon', $data);	
	}	
	
	public function rekon2() {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['data_tiket'] = $this->PW_Model->get_datatiket_unsettle($this->event_id);
		$data['data_bank'] = $this->PW_Model->get_rekening_mutasi("1","");
		
		$this->template->backend('sales/rekon2', $data);
	}	
	
	public function pasang($tiketid,$bayarid) {
		$data_tiket = $this->PW_Model->get_tiket_byid($tiketid);
		$data_bayar = $this->PW_Model->get_rekening_mutasi_byid($bayarid);
		
		$invoice = ""; $tanggal = "";
		if ((sizeof($data_tiket)>0) && (sizeof($data_bayar)>0)) {
			$invoice = $data_tiket[0]["invoice"];
			$tanggal = $data_bayar[0]["tanggal"];
			
			$this->PW_Model->upd_mutasi($bayarid,"1",$invoice); 
			$this->PW_Model->upd_tiket_bayar($tiketid,$bayarid,$tanggal);
			echo "OK";
		} else {
			echo "Not OK";
		}
	}

  public function expo($param = ""){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		if ($param=="") {
			$data['data_expo'] = $this->PW_Model->get_dataexpo($this->event_id); 
			$data['data_booth'] = $this->PW_Model->get_expobooth(); 
			$data['data_expo1'] = $this->PW_Model->get_dataexpo1($this->event_id); 
			$data['data_expo2'] = $this->PW_Model->get_dataexpo3(); 
		} else if ($param=="harga") {
			$data['data_booth'] = $this->PW_Model->get_tipebooth($this->event_id);
		} else {
			$data['data_booth'] = $this->PW_Model->get_expobooth(); 
		}
    $data['param'] = $param;
    
    $this->template->backend('sales/expo', $data);
  }

  public function expowl(){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$status = $this->input->post("status",true);
		if ($status=="") { $status = "0"; }
		
		$data['data_expo'] = $this->PW_Model->get_dataexpo_wl($this->event_id, $status); 
		$data['data_expo2'] = $this->PW_Model->get_dataexpo3();
    $data['status'] = $status;
    
    $this->template->backend('sales/expo_follow_up', $data);
  }

  public function itemtambahan()
	{
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['data_expo'] = $this->PW_Model->get_dataexpo_tbh($this->event_id); 
		
		$this->template->backend('sales/item_tambahan', $data);
  }
  
  public function expotiket()
	{
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['data_expo'] = $this->PW_Model->get_dataexpo_tiket($this->event_id); 

		$this->template->backend('sales/expo_tiket', $data);
	}
	
	public function expofu($expoid="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data_booth = array();
		$step = $this->input->post("step",true); if ($step=="") { $step = "1"; }
		$tombol1 = $this->input->post("tombol1",true);
		if ($tombol1!="") {
			$ostatus = $this->input->post("ostat",true);
			$obooth = $this->input->post("obooth",true);
			$onilai = $this->input->post("onilai",true);
			$status = $this->input->post("status",true);
			$nobooth = $this->input->post("nobooth",true);
			$catatan = $this->input->post("catatan",true);
			$premium = $this->input->post("premium",true);
			$reguler = $this->input->post("reguler",true);
			$kuliner1 = $this->input->post("kuliner1",true);
			$kuliner2 = $this->input->post("kuliner2",true);
			$booth_valid = "1"; $nilai = 0;
			
			if ($step=="1") {
				if ($nobooth!=$obooth) {
					$xbooth = explode(",",$nobooth); 
					if (sizeof($xbooth)>0) {
						for ($i=0;$i<sizeof($xbooth);$i++) {
							$ibooth = $xbooth[$i];
							if (trim($ibooth)!="") {
								$status = "Tidak dikenal"; $tipe_booth = "-"; $harga = 0;
								$data_ibooth = $this->PW_Model->booth_id($ibooth);
								if (sizeof($data_ibooth)>0) {
									$tipe_booth = $data_ibooth[0]["nama_booth"];
									$tipeid_booth = $data_ibooth[0]["tipe_booth"];
									$istatus = $data_ibooth[0]["status_booth"];
									$harga = $data_ibooth[0]["harga"];
									if ($istatus=="0") { 
										$status = "Available";
										if ($tipeid_booth=="1") { $premium .= $ibooth . ","; }
										if ($tipeid_booth=="2") { $reguler .= $ibooth . ","; }
										if ($tipeid_booth=="3") { $kuliner2 .= $ibooth . ","; }
										if ($tipeid_booth=="8") { $kuliner1 .= $ibooth . ","; }
										$nilai += $harga;
									} else { $status = "Booked"; $booth_valid = "0"; }
								} else {
									$booth_valid = "0";  
								}
								$data_booth[] = array("no"=>$ibooth,"tipe"=>$tipe_booth,"status"=>$status,"harga"=>$harga);
							}
						} 
					}
					$step = "2";
				} else {
					$this->PW_Model->exponote_insert($expoid,$this->user_id,$catatan,$status,$nobooth);
					if ($ostatus!=$status) { 
						$this->PW_Model->expo_status($expoid,$status); 
						if ($status=="5") { $this->PW_Model->expo_cancel($expoid); }
					}
					$step = "3";
				}
				
			} else if ($step=="2") {
				$this->PW_Model->exponote_insert($expoid,$this->user_id,$catatan,$ostatus,$obooth);
				$this->PW_Model->expo_status($expoid,$ostatus); 
				$this->PW_Model->expo_booth($expoid,$obooth,$premium,$reguler,$kuliner1,$kuliner2,$onilai); 
				$step = "3";
			}

			$data['premium'] = $premium;
			$data['reguler'] = $reguler;
			$data['kuliner1'] = $kuliner1; 
			$data['kuliner2'] = $kuliner2;
			$data['nilai'] = $nilai;
			$data['status'] = $status;
			$data['nobooth'] = $nobooth;
			$data['catatan'] = $catatan;
			$data['booth_valid'] = $booth_valid;
		}
		
		$data['data_exponote'] = $this->PW_Model->exponote_list($expoid); 
		$data['data_expo'] = $this->PW_Model->get_dataexpo_byid($expoid); 
		$data['expoid'] = $expoid;
		$data['data_booth'] = $data_booth;
		$data['step'] = $step;
		
		$this->template->backend('sales/expo_fu', $data);
	}
	
	public function imgexpo() {
		$myImage = imagecreatefromjpeg(base_url("img/map_expo.jpg"));
		header("Content-type: image/jpeg");
		$red = imagecolorallocatealpha($myImage, 80, 80, 80, 40);
		imagefilledellipse($myImage, 160, 180, 20, 20, $red); 
		imagefilledellipse($myImage, 186, 180, 20, 20, $red); 
		imagefilledellipse($myImage, 160, 206, 20, 20, $red); 
		imagejpeg($myImage);
		imagedestroy($myImage);
	}
	
	public function expolist() {
		$this->general->session_check();
		$data['data_expo1'] = $this->PW_Model->get_dataexpo1($this->event_id);
		$data['data_expo2'] = $this->PW_Model->get_dataexpo2();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$this->template->backend('sales/expo_list', $data);
	}
	
	public function expo_invoice($expoid="0") {
		$this->general->session_check();
		$data['data_expo'] = $this->PW_Model->get_dataexpo_byid($expoid);
		$data['data_booth'] = $this->PW_Model->booth_expo($expoid);

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$this->load->view('backend/sales/email_expo_invoice',$data);
	}
	
	public function expo_waitinglist($expoid="0") {
		$this->general->session_check();
		$data['data_expo'] = $this->PW_Model->get_dataexpo_byid($expoid);

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$this->load->view('backend/sales/email_expo_waitinglist',$data); 
	}
	
	public function sponsor(){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$data['sponsor'] = $this->PW_Model->get_sponsor($this->event_id);

		$this->template->backend('sales/sponsor', $data);
	}

  public function hotel($id="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$status = $this->input->post("status",true);
			$jenis = $this->input->post("jenis",true);
			$cekin = $this->input->post("cekin",true);
			$cekout = $this->input->post("cekout",true);
			$numpax = $this->input->post("numpax",true);
			$roomtype = $this->input->post("roomtype",true);
			$jumlah = $this->input->post("jumlah",true);
			$keterangan = $this->input->post("keterangan",true);
			$keterangan2 = $this->input->post("keterangan2",true);
			$this->PW_Model->hotel_update($id,$status,$jenis,$cekin,$cekout,$numpax,$roomtype,$jumlah,$keterangan,$keterangan);
			$id = "0";
		}
		
		if ($id!="0") {
			$data['data_hotel'] = $this->PW_Model->get_hotel_byid($id); 
			$data['site_form'] = "form"; 
		} else {
			$status = $this->input->post("status",true);
			$data['data_rekap'] = $this->PW_Model->get_rekaphotel($this->event_id);
			$data['data_hotel'] = $this->PW_Model->get_hotel($this->event_id,$status); 
			$data['status'] = $status;
			$data['site_form'] = "list";
		}
		$data['id'] = $id;
		 
		$this->template->backend('sales/hotel', $data);
	}
	
	public function kamar($hotelid="0",$aksi="",$kamarid="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$themes_admin = $this->config->item('themes_admin');
		$data['themes_url'] = base_url($themes_admin); 
		$data['datepick'] = "0";
		
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$nama = $this->input->post("nama",true);
			$hp = $this->input->post("hp",true);
			$cekin = $this->input->post("cekin",true);
			$cekout = $this->input->post("cekout",true);
			$nokamar = $this->input->post("nokamar",true);
			$roomtype = $this->input->post("roomtype",true);
			$hal = $this->input->post("hal",true);

			if ($aksi=="add") { $this->PW_Model->kamarhotel_insert($hotelid,$nokamar,$roomtype,$nama,$hp,$cekin,$cekout,$hal); }
			if ($aksi=="edit") { $this->PW_Model->kamarhotel_update($kamarid,$nokamar,$roomtype,$nama,$hp,$cekin,$cekout,$hal); }
			if ($aksi=="rem") { $this->PW_Model->kamarhotel_delete($kamarid); }
			$aksi = "";
		}		
		
		$data['data_hotel'] = $this->PW_Model->get_hotel_byid($hotelid); 
		if ($aksi=="") {
			$data['data_kamar'] = $this->PW_Model->get_kamar_hotel_byhotelid($hotelid); 
			$data['site_form'] = "list";
		} else {
			$data['data_kamar'] = $this->PW_Model->get_kamar_hotel_byid($kamarid); 
			$data['site_form'] = "form";
		}
		$data['hotelid'] = $hotelid;
		$data['aksi'] = $aksi;
		$data['kamarid'] = $kamarid;
		
		$this->template->backend('sales/hotel_kamar', $data);
	}
  
  public function hotelrekap(){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$status = "1";
		if (isset($_POST["status"])) { $status = $this->input->post("status",true); }
		
		$data['status'] = $status;
		$data['data_hotel'] = $this->PW_Model->get_hotel_rekap($this->event_id,$status);
    $data['data_hotel2'] = $this->PW_Model->get_hotel_rekap3($this->event_id);
    
    $this->template->backend('sales/hotel_rekap', $data);	
	}
	
	public function hotelemail($tujuan) {
		$email = "novrand@gmail.com";
		$pesan = $this->load->view('backend/sales/email_hotel');	
		$this->kirim_email("Hotel","Akotrans Pesta Wirausaha","info@pestawirausaha.com",$tujuan,"[Pesta Wirausaha] Hotel Voucher Aston Marina Ancol",$pesan);
		//echo "done.";
	} 
	
	public function hotelblassemail() {
		$this->general->session_check();
		$data_hotel = $this->PW_Model->get_hotelemail_blass($this->event_id, 10);
		if (sizeof($data_hotel)>0) {
			for ($i=0;$i<sizeof($data_hotel);$i++) {
				$id = $data_hotel[$i]["id"];
				$nama = $data_hotel[$i]["nama_lengkap"];
				$email = $data_hotel[$i]["email"];
				
				$data['data_hotel'] = $this->PW_Model->get_hotel_byid($id);
				
				//$email = "novrand@gmail.com";
				$pesan = $this->load->view('backend/sales/email_hotel',$data,true);	
				$this->kirim_email("Hotel","Akotrans Pesta Wirausaha","info@pestawirausaha.com",$email,"[Pesta Wirausaha] Hotel Voucher Aston Marina Ancol",$pesan);
				
				$this->PW_Model->hotelemail_kirim($id);
				
				echo $id . " ". $nama . " " . $email . "<br>";
				
			} 
		}
	}
	
	
	public function hotelsms() {
		$this->general->session_check();
		$data_hotel = $this->PW_Model->get_hotel_blass($this->event_id);
		if (sizeof($data_hotel)>0) {
			for ($i=0;$i<sizeof($data_hotel);$i++) {
				$jenis = $data_hotel[$i]["jenis_kamar"];
				$kode = $data_hotel[$i]["kode_booking"];
				$cekin = substr($data_hotel[$i]["tgl_cekin"],8,2);
				$cekout = substr($data_hotel[$i]["tgl_cekout"],8,2); 
				$nama = $data_hotel[$i]["nama_lengkap"];
				$nohp = $data_hotel[$i]["nohp"];
				$jml = $data_hotel[$i]["jml_kamar"];
				$status = $data_hotel[$i]["status_bayar"];
				
				$nohp = str_replace("-","",$nohp);
				$nohp = str_replace(" ","",$nohp);
				$nohp = str_replace("+628","08",$nohp);
				
				$xjenis = ""; $harga = 0;
				if ($jenis=="1") { $xjenis = "3 Malam"; $harga = 1800000; }
				if ($jenis=="2") { $xjenis = "2 Malam"; $harga = 1200000; }
				
				if ($status=="0") {
					$nilai = $harga * $jml;
					$pesan = "Kak $nama, berikut data pemesanan paket $xjenis hotel aston tgl $cekin-$cekout sebanyak $jml kamar sebesar Rp. ".number_format($nilai).". Info lengkap di http://hotel.pestawirausaha.com/invoice/".$kode." Besok senin batas akhir pembayaran. Apabila sudah, bisa konfirmasi via WA ke 08112238737";
				} else {
					$pesan = "Kak $nama, berikut catatan paket $xjenis hotel aston tgl $cekin-$cekout sebanyak $jml kamar. Info lengkap di http://hotel.pestawirausaha.com/invoice/".$kode." Jika ada perubahan bisa konfirmasi via WA ke 0818915000";
				}
				
				if (($nohp!="") && (substr($nohp,0,2)=="08")) {
					echo ($i+1) . ". " . $kode . " " . $nama . " " . $nohp . " " . $cekin . " " . $cekout . " " . $jml . " " . $status . "<br>";
					echo $pesan . " " . strlen($pesan)."<br><br>";
					//$this->kirimSMS($nohp,$pesan);
				}
				
				
			} 
		}
	}
	
	public function hotellist() {
		$this->general->session_check();
		$data_hotel = $this->PW_Model->get_hotel_list($this->event_id);
		if (sizeof($data_hotel)>0) {
			echo "<table border=1>";
			echo "<tr><td>No</td><td>Kode Booking</td><td>Nama</td><td>Email</td><td>HP</td><td>KTP</td> <td>Asal TDA</td><td>Tanggal</td><td>Room Type</td><td>Num Room</td><td>Num Pax</td><td>Status</td><td>Note</td></tr>";
			
			for ($i=0;$i<sizeof($data_hotel);$i++) {
				$nama = $data_hotel[$i]["nama_lengkap"];
				$kode = $data_hotel[$i]["kode_booking"];
				$jenis = $data_hotel[$i]["jenis_kamar"];
				$noktp = $data_hotel[$i]["no_ktp"];
				$asaltda = $data_hotel[$i]["asal_tda"];
				$cekin = substr($data_hotel[$i]["tgl_cekin"],8,2);
				$cekout = substr($data_hotel[$i]["tgl_cekout"],8,2);
				$email = $data_hotel[$i]["email"];
				$nohp = $data_hotel[$i]["nohp"];
				$numpax = $data_hotel[$i]["numpax"];
				$breakfast = $data_hotel[$i]["breakfast"];
				$jmlkamar = $data_hotel[$i]["jml_kamar"];
				$roomtype = $data_hotel[$i]["room_type"];
				$status_bayar = $data_hotel[$i]["status_bayar"];
				$catatan = $data_hotel[$i]["keterangan"];
				 
				$xroomtype = "";
				if ($roomtype=="1") { $xroomtype = "R1 - 1 Bedroom"; }
				if ($roomtype=="2") { $xroomtype = "R2 - 2 Bedroom"; }
				
				$xstatus = "";
				if ($status_bayar=="1") { $xstatus = "Lunas"; }
				if ($status_bayar=="3") { $xstatus = "Expo"; }
				if ($status_bayar=="4") { $xstatus = "Pemateri"; }
				if ($status_bayar=="6") { $xstatus = "Sponsor"; }
				
				$xbreakfast = "";
				if ($breakfast=="1") { $xbreakfast = "Ya"; }
				
				echo "<tr><td>" . ($i+1) . "</td><td>" . $kode  ."</td><td>" . $nama  ."</td><td>".$email."</td><td>'".$nohp."</td><td>'".$noktp."</td><td>".$asaltda."</td><td>".$cekin."-".$cekout."</td><td>".$xroomtype."</td><td>".$jmlkamar."</td><td>".$numpax."</td><td>".$xbreakfast."</td><td>".$xstatus."</td><td>".$catatan."</td></tr>";
				
			}
			echo "</table>";
		}			
	}
	
	public function rekon_tiket() {
		$this->general->session_check();
		$data_bank = $this->PW_Model->get_trans_rekening("1");
		$data_tiket = $this->PW_Model->get_tiket_aktif($this->event_id);
		
		$arr_bank = array();
		if (sizeof($data_bank)>0) {
			$jml = 0; $total = 0;
			for ($i=0;$i<sizeof($data_bank);$i++) {
				$nilai = $data_bank[$i]["kredit"];
				$keterangan = $data_bank[$i]["keterangan"];
				$xket = explode(",",$keterangan);
				if (sizeof($xket)>0) {
					for ($j=0;$j<sizeof($xket);$j++) {
						if (!isset($arr_bank[$xket[$j]])) { 
							$arr_bank[trim($xket[$j])] = $nilai;
							$jml++; 
						}
						//else { echo $xket[$j] . " " . $nilai . "<br>"; }
					}
				}
				$total += $nilai;
				
			} 
			echo "Jumlah : $jml, Nilai Transfer : ".number_format($total)."<br><br>";
		} 
		//print_r($arr_bank);
		
		$arr_tiket = array();
		//print_r($data_tiket);
		if (sizeof($data_tiket)>0) {
			echo "Yg sudah terbit tiket tapi belum bayar : <br>";
			$j=1; $total1 = 0; $total2 = 0;
			for ($i=0;$i<sizeof($data_tiket);$i++) {
				$invoice = $data_tiket[$i]["invoice"];
				$nilai = $data_tiket[$i]["n_nilai"];
				if (!isset($arr_bank[$invoice])) {
					$nama = $data_tiket[$i]["nama"];
					$hp = $data_tiket[$i]["no_hp"];
					$usaha = $data_tiket[$i]["usaha"];
					$aksi = $data_tiket[$i]["aksi_bayar"];
					$approver = $data_tiket[$i]["approver"];
					$total1 += $nilai;
					echo "$j,$invoice,$nama,$usaha,$hp,$aksi,$approver,$nilai<br>";
					$j++;
				}
				$total2 += $nilai;
				$arr_tiket[trim($invoice)] = $nilai;
			}
			echo "Total Selisih : ".number_format($total1) . "<br>";
			echo "Total Nilai Tiket : ".number_format($total2) . "<br>";
			echo "Jumlah Tiket : ".sizeof($data_tiket) . "<br><br>";
		}
		//print_r($arr_tiket);
		
		if (sizeof($data_bank)>0) {
			echo "Yg sudah bayar tapi tidak ada tiket : <br>";
			$k = 1; $total = 0;
			for ($i=0;$i<sizeof($data_bank);$i++) {
				$nilai = $data_bank[$i]["kredit"];
				$keterangan = $data_bank[$i]["keterangan"];
				$xket = explode(",",$keterangan);
				if (sizeof($xket)>0) {
					for ($j=0;$j<sizeof($xket);$j++) {
						if (!isset($arr_tiket[trim($xket[$j])])) {
							echo $k . ". " . $xket[$j] . " " . number_format($nilai) . "<br>";
							$k++; $total += $nilai;
						}
					}
				}
			} 
			echo "Jumlah : $k, Nilai : ".number_format($total). "<br>";
		} 		
	}
	
	// update t_tiket set dtanggal = str_to_date(tanggal,"%d %b %Y %H:%i:%s") 
	public function salesTiketBelumBayar() {
		$this->general->session_check();
		$data_tiket = $this->PW_Model->get_tiket_unpaid_date('2018-12-10 00:00:00', $this->event_id);
		if (sizeof($data_tiket)>0) {
			$j = 1; $arrPhone = array();
			for ($i=0;$i<sizeof($data_tiket);$i++) {
				//id, invoice, tanggal, nama, no_hp, usaha, email, nilai, n_nilai
				$id = $data_tiket[$i]["id"];
				$invoice = $data_tiket[$i]["invoice"];
				$tanggal = $data_tiket[$i]["tanggal"];
				$nama = $data_tiket[$i]["nama"];
				$no_hp = $data_tiket[$i]["no_hp"];
				$usaha = $data_tiket[$i]["usaha"];
				$email = $data_tiket[$i]["email"];
				$nilai = $data_tiket[$i]["nilai"];
				$n_nilai = $data_tiket[$i]["n_nilai"];
				
				$sudah_bayar = "0";
				$data_bayar = $this->PW_Model->get_tikethp_bayar($no_hp, $this->event_id);
				if (sizeof($data_bayar)>0) { $sudah_bayar = "1"; }
				
				if (in_array($no_hp,$arrPhone)) { $sudah_bayar = "1"; } else { $arrPhone[] = $no_hp; }
				
				$f_nilai = floor($n_nilai / 1000);
				
				$pesan = "Hai kak $nama. Hari ini hari terakhir promo HARBOLNAS tiket PW, besok harga sudah kembali normal 600 rb. Silahkan melakukan pembayaran sebesar $nilai ke BSM 7110041009 a.n Komunitas Tangan Di Atas";
				$pesan2 = "Hai kak $nama. Hari ini hari terakhir promo HARBOLNAS untuk tiket PW, besok sudah kembali normal 600 rb. Jangan lupa gunakan kode promo KOLABORAKSI untuk mendapatkan harga spesial.";
				
				if ($sudah_bayar=="0") {
					echo "$j. $id,$invoice,$tanggal,$nama,$no_hp,$nilai,$n_nilai,$f_nilai, $sudah_bayar <Br>";
					if ($f_nilai==450) {
						echo $pesan . " " . strlen($pesan) . "<br><br>";
					} else {
						echo $pesan2 . " " . strlen($pesan2). "<br><br>";
						$pesan = $pesan2;
					}
					//$this->kirimSMS($no_hp,$pesan);
					
					$j++;
				}
			}
		}
	}
	
	public function salesTiketBelumBayarSebelum() {
		$this->general->session_check();
		$data_tiket = $this->PW_Model->get_tiket_unpaid_sebelum_date('2019-01-18', $this->event_id);
		if (sizeof($data_tiket)>0) {
			$j = 1; $arrPhone = array();
			for ($i=0;$i<sizeof($data_tiket);$i++) {
				//id, invoice, tanggal, nama, no_hp, usaha, email, nilai, n_nilai
				$id = $data_tiket[$i]["id"];
				$invoice = $data_tiket[$i]["invoice"];
				$tanggal = $data_tiket[$i]["tanggal"];
				$nama = $data_tiket[$i]["nama"];
				$no_hp = $data_tiket[$i]["no_hp"];
				$usaha = $data_tiket[$i]["usaha"];
				$email = $data_tiket[$i]["email"];
				$nilai = $data_tiket[$i]["nilai"];
				$n_nilai = $data_tiket[$i]["n_nilai"];
				
				$sudah_bayar = "0";
				$data_bayar = $this->PW_Model->get_tikethp_bayar($no_hp, $this->event);
				if (sizeof($data_bayar)>0) { $sudah_bayar = "1"; }
				
				if (in_array($no_hp,$arrPhone)) { $sudah_bayar = "1"; } else { $arrPhone[] = $no_hp; }
				
				
				
				/*$pesan = "Hai kak $nama. Terima kasih telah memesan tiket PW pada tanggal $tanggal dengan nomor invoice #$invoice. Silahkan melakukan pembayaran sebesar $nilai ke BSM 7110041009 a.n Komunitas Tangan Di Atas";
				$pesan2 = "Hai kak $nama. Dapatkan tiket PW dengan harga 450 rb di program HARBOLNAS sampai tanggal 13 desember dengan kode Promo KOLABORAKSI. Segera pesan di http://tiket.pestawirausaha.com ya kak.  ";*/
				
				/*$pesan = "Hai kak $nama. Hari ini hari terakhir promo HARBOLNAS tiket PW, besok harga sudah kembali normal 600 rb. Silahkan melakukan pembayaran sebesar $nilai ke BSM 7110041009 a.n Komunitas Tangan Di Atas";
				$pesan2 = "Hai kak $nama. Hari ini hari terakhir promo HARBOLNAS untuk tiket PW, besok sudah kembali normal 600 rb. Jangan lupa gunakan kode promo KOLABORAKSI di http://tiket.pestawirausaha.com/pesan ya kak";*/
				
				$pesan = "Hai kak $nama. Sayang sekali program DISKONTERAKHIR telah berakhir. Namun ada penawaran khusus untuk kakak dan hanya hari ini untuk tiket PWN di harga 450 rb dengan membayar di http://hotel.pestawirausaha.com/tiket/$invoice . Atas kerjasama dengan Nicepay. ";
				
				$f_nilai = floor($n_nilai / 1000);
				
				if ($sudah_bayar=="0") {
					echo "$j. $id,$invoice,$tanggal,$nama,$no_hp,$nilai,$n_nilai,$f_nilai, $sudah_bayar <Br>";
					/*if ($f_nilai==450) {
						echo $pesan . " " . strlen($pesan) . "<br><br>";
					} else {
						echo $pesan2 . " " . strlen($pesan2). "<br><br>";
						$pesan = $pesan2;
					}*/
					//echo $pesan . " " . strlen($pesan) . "<br><Br>";
					//$this->kirimSMS($no_hp,$pesan);
					
					$j++;
				}
			}
		}
	}
	
	public function salesTiketAktif() {
		$this->general->session_check();
		$data_tiket = $this->PW_Model->get_tiket_aktif($this->event_id);
		if (sizeof($data_tiket)>0) {
			$j = 1; $arrPhone = array();
			for ($i=0;$i<sizeof($data_tiket);$i++) {
				$id = $data_tiket[$i]["id"];
				$nama = $data_tiket[$i]["nama"];
				$hp = $data_tiket[$i]["no_hp"];
				$usaha = $data_tiket[$i]["usaha"];
				$daerah = $data_tiket[$i]["daerah"];
				$gaffid = $data_tiket[$i]["gaffid"];
				
				if ($gaffid=="0") {
					$data_affid = $this->PW_Model->get_gaffid($nama,$usaha,$daerah);
					if (sizeof($data_affid)>0) { $gaffid = $data_affid[0]["affid"]; }
					if ($gaffid=="Belum Aktif") { $gaffid = "-1"; }
					
					$this->PW_Model->update_gaffid($id,$gaffid);
				}
				
				echo "$j. $id,$nama,$hp,$usaha,$daerah,$gaffid <Br>";
				$j++;
			}
		}
	}
	
	public function salesTiketGAffid() {
		$this->general->session_check();
		$data_tiket = $this->PW_Model->get_tiket_gaffid_aktif($this->event_id);
		if (sizeof($data_tiket)>0) {
			$j = 1; $arrPhone = array();
			for ($i=0;$i<sizeof($data_tiket);$i++) {
				$id = $data_tiket[$i]["id"];
				$nama = $data_tiket[$i]["nama"];
				$hp = $data_tiket[$i]["no_hp"];
				$usaha = $data_tiket[$i]["usaha"];
				$daerah = $data_tiket[$i]["daerah"];
				$gaffid = $data_tiket[$i]["gaffid"];
				
				$pesan = "Hai kak $nama. Hanya mengingatkan kalau promo Harbolnas PW berakhir malam ini. Jadi ajak teman anda dengan membagikan link  https://tiket.pestawirausaha.com/beli/?aff=".$gaffid." dengan kode promo KOLABORAKSI. Dan dapatkan komisi 100 rb."; 
				
				echo "$j. $id,$nama,$hp,$usaha,$daerah,$gaffid <Br>";
				echo $pesan . " " . strlen($pesan) . "<br><Br>";
				//$this->kirimSMS($hp,$pesan);
				
				$j++;
			}
		}
	}
	
	public function hotelBelumBayar() {
		$data_hotel = $this->PW_Model->get_hotel("0");
		if (sizeof($data_hotel)>0) {
			for ($i=0;$i<sizeof($data_hotel);$i++) {
				$id = $data_hotel[$i]["id"];
				$tanggal = $data_hotel[$i]["tanggal"];
				$nama = $data_hotel[$i]["nama_lengkap"];
				$jenis = $data_hotel[$i]["jenis_kamar"];
				$hp = $data_hotel[$i]["nohp"];
				$jml = $data_hotel[$i]["jml_kamar"];
				
				$hp = str_replace("+62","0",$hp);
				$hp = str_replace(" ","",$hp);
				$hp = str_replace("-","",$hp);
				
				$jeniskamar = ""; $nilai = 0;
				if ($jenis=="1") { $jeniskamar = "Paket 3 Malam Aston Marina"; $nilai = $jml * 1800000; }
				if ($jenis=="2") { $jeniskamar = "Paket 2 Malam Aston Marina"; $nilai = $jml * 1200000; }
				
				$pesan = "Hai kak $nama. Mengingatkan untuk pembayaran pesanan kamar pada tanggal $tanggal untuk $jeniskamar dengan jumlah $jml kamar. Nilai tagihan sebesar Rp. ".number_format($nilai). ". Konfirmasi pembayaran ke 08112238737";
				echo $hp . " : " . $pesan . "<br>";
				echo strlen($pesan); echo "<br>";
				//$this->kirimSMS($hp,$pesan);
			}
		}
	}
	
	public function expoBayarSebagian() {
		$data_expo = $this->PW_Model->get_dataexpo_wl("3");
		if (sizeof($data_expo)>0) {
			for ($i=0;$i<sizeof($data_expo);$i++) {
				$id = $data_expo[$i]["id"];
				$tanggal = $data_expo[$i]["tanggal"];
				$nama = $data_expo[$i]["nama"];
				$email = $data_expo[$i]["email"];
				$hp = $data_expo[$i]["no_hp"];
				$multi_premium = $data_expo[$i]["premium"];
				$multi_reguler = $data_expo[$i]["reguler"];
				$kuli_premium = $data_expo[$i]["kuliner"];
				$kuli_reguler = $data_expo[$i]["kuliner2"];
				$nilai = $data_expo[$i]["nilai_bayar"];
				$bayar1 = $data_expo[$i]["bayar1"];

				$stand = $multi_premium . $multi_reguler . $kuli_premium . $kuli_reguler;

				$pesan = "Hai kak $nama. Mengingatkan untuk pelunasan stand expo Pesta Wirausaha 2019 nomor $stand dengan total tagihan Rp. ".number_format($nilai).". Pembayaran pertama sebesar Rp. ".number_format($bayar1).", kekurangan pembayaran Rp. ".number_format($nilai-$bayar1).". Konfirmasi dan pertanyaan bisa hubungi kak Sony di 08995708330";

				echo $hp . " | " . $pesan . " " . strlen($pesan) . "<br>"; 
				//$this->kirimSMS($hp,$pesan);
			}
		}
	}

	public function expoPesan() {
		$data_expo = $this->PW_Model->get_dataexpo_wl("2");
		if (sizeof($data_expo)>0) {
			for ($i=0;$i<sizeof($data_expo);$i++) {
				$id = $data_expo[$i]["id"];
				$tanggal = $data_expo[$i]["tanggal"];
				$nama = $data_expo[$i]["nama"];
				$email = $data_expo[$i]["email"];
				$hp = $data_expo[$i]["no_hp"];
				$multi_premium = $data_expo[$i]["premium"];
				$multi_reguler = $data_expo[$i]["reguler"];
				$kuli_premium = $data_expo[$i]["kuliner"];
				$kuli_reguler = $data_expo[$i]["kuliner2"];
				$nilai = $data_expo[$i]["nilai_bayar"];
				$bayar1 = $data_expo[$i]["bayar1"];

				$stand = $multi_premium . $multi_reguler . $kuli_premium . $kuli_reguler;

				$pesan = "Hai kak $nama. Mengingatkan untuk pembayaran stand expo Pesta Wirausaha 2019 nomor $stand dengan total tagihan Rp. ".number_format($nilai).". Konfirmasi dan pertanyaan bisa hubungi kak Sony di 08995708330";

				if ($hp!="082115397374") {
					echo $hp . " | " . $pesan . " " . strlen($pesan) . "<br>"; 
					//$this->kirimSMS($hp,$pesan);
				}
			}
		}
	}	
	
	public function kirimSMS($nohp,$pesan) {
		$userkey = 'ic4wb9fucskr67d9y1g7';
		$passkey = '6hrzzzkus65vdskutg14';
		$url = 'http://mobimedia.zenziva.co.id/api/sendsms/';
		$curlHandle = curl_init();
		curl_setopt($curlHandle, CURLOPT_URL, $url);
		curl_setopt($curlHandle, CURLOPT_HEADER, 0);
		curl_setopt($curlHandle, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYHOST, 2);
		curl_setopt($curlHandle, CURLOPT_SSL_VERIFYPEER, 0);
		curl_setopt($curlHandle, CURLOPT_TIMEOUT,30);
		curl_setopt($curlHandle, CURLOPT_POST, 1);
		curl_setopt($curlHandle, CURLOPT_POSTFIELDS, array(
			'userkey' => $userkey,
			'passkey' => $passkey,
			'nohp' => $nohp,
			'pesan' => $pesan
		));
		$results = json_decode(curl_exec($curlHandle), true);
		curl_close($curlHandle);
		print_r($results);
	}
	
	public function kirim_email($emailgroup,$nama,$pengirim,$tujuan,$subject,$pesan) {
		$this->load->library('email');
		/*$config['protocol'] = "smtp";
		$config['smtp_host'] = "ssl://mail.pestawirausaha.com";
		$config['smtp_port'] = "465";
		$config['smtp_user'] = "info@pestawirausaha.com";
		$config['smtp_pass'] = "SendokGarpu2017";*/
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		$this->email->from($pengirim, $nama);
		$this->email->to($tujuan);

		$this->email->subject($subject);
		$this->email->message($pesan);
		if (!$this->email->send()) {
			show_error($this->email->print_debugger());
		}
		
		$kode = ""; $kkar = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		for ($i=0;$i<30;$i++) { $kode .= $kkar[rand(0,strlen($kkar)-1)]; }
		
		$this->PW_Model->email_log($emailgroup,$kode,$nama,$pengirim,$tujuan,$subject,$pesan,"1");	
	}

	public function gift(){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;
		// $data['user_level'] = $this->user_level;

		$this->template->backend('sales/gift', $data);
	}
}