<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Keuangan extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
	}

  public function anggaran($aksi="",$anggaranid="0"){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$induk = $this->input->post("induk",true);
			$kode = $this->input->post("kode",true);
			$anggaran = $this->input->post("anggaran",true);
			$keterangan = $this->input->post("keterangan",true);

			if ($aksi=="tambah") { $this->PW_Model->budget_insert($this->event_id, $induk,$kode,$anggaran,$keterangan); }
			if ($aksi=="ubah") { $this->PW_Model->budget_update($anggaranid,$induk,$kode,$anggaran,$keterangan); }
			if ($aksi=="hapus") { $this->PW_Model->budget_delete($anggaranid); }
			$aksi = "";
		}

		if ($aksi=="") {
			$data['site_form'] = "list";
			$data['data_anggaran'] = $this->PW_Model->get_budget($this->event_id);
			$data['data_item'] = $this->PW_Model->get_budgetitem();
		} else {
			$data['site_form'] = "form";
			$data['data_anggaran'] = $this->PW_Model->get_budget_byid($anggaranid);
			$data['data_induk'] = $this->PW_Model->get_budget($this->event_id);
			$data['data_item'] = $this->PW_Model->get_budgetitem_bybudgetid($anggaranid);
		}

		$data['aksi'] = $aksi;
    $data['anggaranid'] = $anggaranid;
    
    $this->template->backend('keuangan/anggaran', $data);
  }

  public function itemanggaran($anggaranid="0",$aksi="",$itemid="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$jenis = $this->input->post("jenis",true);
			$jumlah = $this->input->post("jumlah",true);
			$harga = $this->input->post("harga",true);
			$ppn = $this->input->post("ppn",true);
			$jmlhari = $this->input->post("jml_hari",true);

			if ($aksi=="tambah") { $this->PW_Model->budgetitem_insert($anggaranid,$jenis,$jumlah,$harga,$ppn,$jmlhari); }
			if ($aksi=="ubah") { $this->PW_Model->budgetitem_update($itemid,$jenis,$jumlah,$harga,$ppn,$jmlhari); }
			if ($aksi=="hapus") { $this->PW_Model->budgetitem_delete($itemid); }
      $aksi = "";
		} 

    $data['data_anggaran'] = $this->PW_Model->get_budget_byid($anggaranid);
		if ($aksi=="") {
			$data['site_form'] = "list";
			$data['data_item'] = $this->PW_Model->get_budgetitem_bybudgetid($anggaranid);
		} else {
			$data['site_form'] = "form";
			$data['data_item'] = $this->PW_Model->get_budgetitem_byid($itemid);
		}

    $data['aksi'] = $aksi;
		$data['anggaranid'] = $anggaranid;		
		$data['itemid'] = $itemid;		

    $this->template->backend('keuangan/item_anggaran', $data);	
  }
  
  public function realisasi() {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

    $this->template->backend('keuangan/realisasi', $data);
  }
  
  public function pengeluaran(){
		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$this->template->backend('keuangan/pengeluaran');
  }

  public function nicepay($ttrans="0",$transid="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		if ($ttrans=="hapus") {
			$this->PW_Model->nicepay_hapus($transid);
			$ttrans = "0"; $tipetrans = "0";
		}

		if (($ttrans!="0") && ($ttrans!="100")) {
			$data['tipetrans'] = $ttrans;
			$data['transid'] = $transid;
			if ($ttrans=="1") { // tiket
				$data_trans = $this->PW_Model->get_tiket_byid($transid);
			}
			$data['data_trans'] = $data_trans;
			$this->load->view('backend/keuangan/nicepay_redirect',$data);		
		} else {
			$tipetrans = "0"; 
			if ($ttrans=="100") {
				$tipetrans = "0"; 
			} else {
				//$tipetrans = "1"; 
				$data_trans = array();
				$tipe = $this->input->post("tipe",true);
				if ($tipe!="") { $tipetrans = $tipe; } 
			}

			$data['tipetrans'] = $tipetrans;
			if ($tipetrans=="0") { // Data Nicepay
				$data_trans = $this->PW_Model->get_nicepay_pay();
			} else if ($tipetrans=="1") { // Tiket Online
				$data_trans = $this->PW_Model->get_tiket_aktif();
			} else if ($tipetrans=="3") { // Expo
				$data_trans = $this->PW_Model->get_expo_aktif();
			} else if ($tipetrans=="6") { // Hotel
				$data_trans = $this->PW_Model->get_hotel_aktif(); 
			}

      $data['data_trans'] = $data_trans;
			$data['data_nicepay'] = $this->PW_Model->get_nicepay_pay();

      $this->template->backend('keuangan/nicepay', $data);

		}
  }
  
  public function nicepaytiket() {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;

		$data['data_trans'] = $this->PW_Model->get_nicepay_jenis("11");

    $this->template->backend('keuangan/nicepay_tiket', $data);
	}

	public function kupon($aksi="",$kuponid="0"){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;
		
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$kode = $this->input->post("kode",true);
			$type = $this->input->post("type",true);
			$diskon = $this->input->post("diskon",true);
			$komisi = $this->input->post("komisi",true);
			$status = $this->input->post("status",true);
			$tgl_buat = date("Y-m-d H:i:s");
			$tgl_exp = $this->input->post("tgl_exp",true);

			if ($aksi=="tambah") { $this->PW_Model->kupon_insert($kode, $type, $diskon, $komisi, $status, $this->user_id, $tgl_buat, $tgl_exp); }
			if ($aksi=="ubah") { $this->PW_Model->kupon_update($kuponid, $kode, $type, $diskon, $komisi, $status, $tgl_buat, $tgl_exp); }
			if ($aksi=="hapus") { $this->PW_Model->kupon_delete($kuponid); }
			$aksi = "";
		}

		if ($aksi=="") {
			$data['site_form'] = "list";
			$data['data_kupon'] = $this->PW_Model->get_kupon();
		} else {
			$data['site_form'] = "form";
			$data['data_kupon'] = $this->PW_Model->get_kupon_by_id($kuponid);
		}

		$data['aksi'] = $aksi;
    $data['kuponid'] = $kuponid;

		$this->template->backend('keuangan/kupon', $data);
	}

	public function layanan($aksi="",$layananid="0"){
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;
		
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$nama = $this->input->post("nama",true);
			$harga = $this->input->post("harga",true);
			$status = ($this->input->post("status", true)=="")? "0" : "1";
			$waktu = $this->input->post("waktu", true);
			$komisi = $this->input->post("komisi",true);
			$daerah = $this->input->post("daerah",true);
			$hadiah = $this->input->post("hadiah",true);
			$url = $this->input->post("url",true);
			$reseller = ($this->input->post("reseller", true)=="")? "0" : "1";
			$harga_fix = $this->input->post("harga",true);
			$jumlah = $this->input->post("jumlah",true);
			$deskripsi = $this->input->post("deskripsi",true);

			if ($aksi=="tambah") { $this->PW_Model->layanan_insert($this->event_id, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah); }
			if ($aksi=="ubah") { $this->PW_Model->layanan_update($layananid, $nama, $harga, $status, $waktu, $komisi, $daerah, $hadiah, $deskripsi, $url, $reseller, $harga_fix, $jumlah); }
			if ($aksi=="hapus") { $this->PW_Model->layanan_delete($layananid); }
			$aksi = "";
		}

		if ($aksi=="") {
			$data['site_form'] = "list";
			$data['data_layanan'] = $this->PW_Model->get_layanan($this->event_id);
		} else {
			$data['site_form'] = "form";
			$data['data_layanan'] = $this->PW_Model->get_layanan_by_id($layananid);
		}

		$data['aksi'] = $aksi;
    $data['layananid'] = $layananid;

		$this->template->backend('keuangan/layanan', $data);
	}

}