<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Email extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){

  }

  public function kirim(){
    set_time_limit(0);
    $this->general->session_check();

    $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;
    
    $submit = 0; $target = "0"; $pesan = "";
    $tombol = $this->input->post("tombol",true);
    if ($tombol!="") {
      $target = $this->input->post("target",true);
      $tujuan = $this->input->post("tujuan",true);
      $isi = $this->input->post("isi",true);
      
      $arr_tiket = array();
      if ($target=="2") {
        //$xisi = str_replace("{NAMA}","Abrahamsyah",$isi);
        //$this->kirim_email("abrahamsyah@gmail.com",$tujuan,$xisi);
        
        $xisi = str_replace("{NAMA}","Deddy",$isi);
        $this->kirim_email("novrand@gmail.com",$tujuan,$xisi);
        $pesan = "Pengiriman email test telah dikirimkan";
      } else {
        $jml_email = 0;
        $data_tiket = $this->PW_Model->get_datatiket($this->event_id);
        if (sizeof($data_tiket)>0) {
          for ($i=0;$i<sizeof($data_tiket);$i++) {
            $nama = $data_tiket[$i]["nama"];
            $email = $data_tiket[$i]["email"];
            $kode_bayar = $data_tiket[$i]["kode_bayar"];
        
            if ($kode_bayar==$target) {
              $arr_tiket[] = array("nama"=>$nama,"email"=>$email);
              if (($tujuan!="") && ($isi!="")) {
                $xisi = str_replace("{NAMA}",$nama,$isi);
                $this->kirim_email($email,$tujuan,$xisi);
                $jml_email++;
              }
            }
          }
        }

        $pesan = "Pengiriman ke $jml_email email telah dikirimkan";
      }

      $data['pesan'] = $pesan;
      $data['arr_tiket'] = $arr_tiket;
      $submit = 1;
    }
    $data['target'] = $target;
    $data['submit'] = $submit;

    $this->template->backend('email/kirim', $data);
  }

  public function kirim_email($tujuan,$subject,$pesan) {
		$this->load->library('email');
		/*$config['protocol'] = "smtp";
		$config['smtp_host'] = "mail.pestawirausaha.com";
		$config['smtp_port'] = "587";
		$config['smtp_user'] = "info@pestawirausaha.com";
		$config['smtp_pass'] = "SendokGarpu2017";*/
		$config['charset'] = "utf-8";
		$config['mailtype'] = "html";
		$config['newline'] = "\r\n";
		$this->email->initialize($config);

		$this->email->from('info@pestawirausaha.com', 'Panitia Pesta Wirausaha 2019');
		$this->email->to($tujuan);

		$this->email->subject($subject);
		$this->email->message($pesan);
		$this->email->send();
		
		//$this->PW_model->email_log($tujuan,$subject,$pesan);
	}

  public function test_email() {
		$this->kirim_email("novrand@gmail.com","tujuan","isi");
  }
  
  public function kirem_test() {
		$curl = curl_init();

		$username = "pwdaerah5";
		$token = "G1QbEcUkOrVJ8ShoaRuXHBF4W9iPTMzDpwdaerah5";
		$time = time();
		$generated_token = hash_hmac("sha256",$username."::".$token."::".$time,$token);
		curl_setopt_array($curl, array(
			CURLOPT_URL => "https://api.kirim.email/v3/list/53383",
			CURLOPT_RETURNTRANSFER => true,
			CURLOPT_ENCODING => "",
			CURLOPT_MAXREDIRS => 10,
			CURLOPT_TIMEOUT => 0,
			CURLOPT_FOLLOWLOCATION => false,
			CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
			CURLOPT_CUSTOMREQUEST => "GET",
			CURLOPT_HTTPHEADER => array(
				"Auth-Id: ".$username,
				"Auth-Token: ".$generated_token,
				"Timestamp: ".$time
			),
		));

		$response = curl_exec($curl);
		$err = curl_error($curl);
		curl_close($curl);
		if ($err) {
			echo "cURL Error #:" . $err;
		} else {
			echo $response;
		}
	}

}