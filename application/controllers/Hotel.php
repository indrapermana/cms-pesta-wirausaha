<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Hotel extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');

    $this->nicepay_url = 'https://www.nicepay.co.id/nicepay/api/';
		// $this->nicepay_url = 'https://qa.nicepay.co.id/nicepay/api/';

    // dev
    //$this->nicepay_merchid = "IONPAYTEST";
    //$this->nicepay_merchkey = "33F49GnCMS1mFYlGXisbUDzVf2ATWCl9k3R++d5hDd3Frmuos/XLx8XhXpe+LDYAbpGKZYSwtlyyLOtS/8aD7A==";

    // production
    $this->nicepay_merchid = "HOTELPWP4Y";
    $this->nicepay_merchkey = "+XRXEqlfHC2SLejErvjPEcYRF9HDBxR4xklzJPMJwUCNVaq09zn7YvhzpCr2Fe6xh+xKACS7lPkCFUAALXpOoA==";
    $this->timeout_connect = 0;	
  }

  public function index(){
    redirect("hotel/info");
  }

  public function info(){
    $data['hotel_active'] = true;
    $this->template->frontend("hotel/info", $data);
  }

  public function pesan(){
    $data['hotel_active'] = true;
    $this->template->frontend("hotel/pesan", $data);
  }

  public function pesan1(){
    $page = "0";
    $pesan = "";
    
    $event_id = $this->input->post("event_id",true);
    $tombol = $this->input->post("tombol",true);
    if($tombol=="0"){
      if($event_id!=""){
        $page = "1";
      }else{
        $pesan = "Event Harus Dipilih !!!";
        $page = "0";
      }
    } else if($tombol=="1"){
      $nama = $this->input->post("nama",true);
			$email = $this->input->post("email",true);
			$hp = $this->input->post("hp",true);
			$ktp = $this->input->post("ktp",true);
			$tiket = $this->input->post("tiket",true);
			$asal = $this->input->post("asaltda",true);
			$jumlah = $this->input->post("jumlah",true);
			$jenis = "1";
			$harga = 1800000;
			$nilai = $jumlah * $harga;
			$status = 0;
      $keterangan = $this->input->post("keterangan",true);
      
      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = time().$_FILES["foto"]['name'].".".$ext_foto;
      $path_img = "./upload/hotel/";

      if($this->valid_upload($fn_foto, $path_img)){
        if ($jenis=="1") {
          $cekin = "2019-01-24";
          $cekout = "2019-01-27";
        } else {
          $cekin = "2019-01-25";
          $cekout = "2019-01-27";
        }
  
        $kode = $this->PW_Model->hotelkode_get();
        $hotelid = 0;
        $data_hotel = $this->PW_Model->hotel_insert($event_id,$jenis,$kode,$cekin,$cekout,$nama,$email,$hp,$tiket,$asal,$ktp,$fn_foto,$keterangan,$jumlah,$nilai,$status);

        if (sizeof($data_hotel)>0) { 
          $hotelid = $data_hotel[0]["id"];
        }

        $data['nama'] = $nama;
        $data['jumlah'] = $jumlah;
        $data['harga'] = $harga;
        $data['nilai'] = $nilai;
        $data['invoice'] = $hotelid;
        $data['waktu'] = date('d M Y H:i',mktime(date("H"),date("i"),0,date("m"),date("d")+1,date("Y")));

			$page = "2";	

      }else{
        $pesan = "Upload Gagal";
      }
    }

    $data['data_jumlah'] = $this->PW_Model->jml_book_hotel();
    $data['event'] = $this->PW_Model->get_event_by_status('1');
		$data['pesan'] = $pesan;
    $data['page'] = $page;
    $data['id_event'] = $event_id;
    $data['hotel_active'] = true;

    $this->template->frontend("hotel/pesan1", $data);
  }

  public function pesan2(){
    $page = "0";
		$pesan = "";

		$tombol = $this->input->post("tombol",true);
		if($tombol=="0"){
      $event_id = $this->input->post("event_id",true);
      if($event_id!=""){
        $page = "1";
      }else{
        $pesan = "Event Harus Dipilih !!!";
        $page = "0";
      }
    } else if ($tombol=="1") {
			$nama = $this->input->post("nama",true);
			$email = $this->input->post("email",true);
			$hp = $this->input->post("hp",true);
			$ktp = $this->input->post("ktp",true);
			$tiket = $this->input->post("tiket",true);
			$asal = $this->input->post("asaltda",true);
			$jumlah = $this->input->post("jumlah",true);
			$jenis = $this->input->post("jenis",true);
      $metoda = $this->input->post("metoda",true);

      $harga = 1800000; $namapaket = "Paket 3 Malam Aston Marina ";
			if ($jenis=="2") { $harga = 1200000; $namapaket = "Paket 2 Malam Aston Marina "; }

			$nilai = $jumlah * $harga;
			$status = 0;
      $keterangan = $this->input->post("keterangan",true);

      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = time().$_FILES["foto"]['name'].".".$ext_foto;
      $path_img = "./upload/hotel/";

      if($this->valid_upload($fn_foto, $path_img)){
        if ($jenis=="1") {
          $cekin = "2019-01-24";
          $cekout = "2019-01-27";
        } else {
          $cekin = "2019-01-25";
          $cekout = "2019-01-27";
        }

        $kode = $this->PW_Model->hotelkode_get();
        $hotelid = 0;
			  // $data_hotel = $this->PW_Model->hotel_insert($jenis,$nama,$email,$hp,$tiket,$asal,$ktp,$fn_foto,$keterangan,$jumlah,$metoda,$nilai,$status);
        $data_hotel = $this->PW_Model->hotel_insert($event_id,$jenis,$kode,$cekin,$cekout,$nama,$email,$hp,$tiket,$asal,$ktp,$fn_foto,$keterangan,$jumlah,$nilai,$status,$metoda);

        if (sizeof($data_hotel)>0) { $hotelid = $data_hotel[0]["id"]; }
        $data['nama'] = $nama;
        $data['jumlah'] = $jumlah;
        $data['harga'] = $harga;
        $data['nilai'] = $nilai;
        $data['invoice'] = $hotelid;
        $data['waktu'] = date('d M Y H:i',mktime(date("H"),date("i"),0,date("m"),date("d")+1,date("Y")));

        $metode = $this->input->post("slmetode",true);
        $metode = "00";

        if ($metoda=="1") { $metode = "01"; }
        if (($metoda=="2") || ($metoda=="3") || ($metoda=="4") || ($metoda=="5") || ($metoda=="6") || ($metoda=="7")) { $metode = "02"; }
        if (($metoda=="8") || ($metoda=="9")) { $metode = "03"; }

        $requestData['iMid'] = $this->nicepay_merchid;
        $requestData['payMethod'] = $metode;
        $requestData['currency'] = "IDR";
        $requestData['merchantKey'] = $this->nicepay_merchkey;
        $requestData['amt'] = $nilai;
        $requestData['referenceNo'] = $hotelid;
        $requestData['goodsNm'] = $namapaket . "#".$hotelid;
        $requestData['billingNm'] = $nama;
        $requestData['billingPhone']  = $hp;
        $requestData['billingEmail'] = $email;
        $requestData['callBackUrl'] = base_url('hotel/callback');
        $requestData['dbProcessUrl'] = base_url('hotel/dbprocess');
        $requestData['description'] = $namapaket . " No." .$requestData['referenceNo'];
        $requestData['merchantToken'] = hash('sha256', $requestData['iMid'].$requestData['referenceNo'].$requestData['amt'].$requestData['merchantKey']);
        $requestData['cartData'] = '{}';

        if ($metode == '01') {
          $requestData['instmntType'] = 1;
          $requestData['instmntMon'] = 1;
          $requestData['billingAddr'] = $asal;
          $requestData['billingCity'] = $asal;
          $requestData['billingState'] = $asal;
          $requestData['billingPostCd'] = '12520';
          $requestData['billingCountry'] = 'Indonesia';
          $requestData['userIP'] = $_SERVER['REMOTE_ADDR'];
        } else if ($metode == '02') {
          if ($metoda=="2") { $bankCode = "BMRI"; } // Bank Mandiri
          else if ($metoda=="3") { $bankCode = "CENA"; } // Bank BCA
          else if ($metoda=="4") { $bankCode = "BBBA"; } // Bank Permata
          else if ($metoda=="5") { $bankCode = "BNIN"; } // Bank BNI
          else if ($metoda=="6") { $bankCode = "BRIN"; } // Bank BRI
          else if ($metoda=="7") { $bankCode = "BNIA"; } // Bank Niaga
          $requestData['bankCd'] = $bankCode;
        } else if ($metode == '04' or $metode == '05') {
          $requestData['userIP'] = $_SERVER['REMOTE_ADDR'];
        }

        //print_r($requestData);

        $server_url = $this->nicepay_url. "orderRegist.do";
        $response = json_decode(substr($this->curl_post($server_url,$requestData),4));

        //Process response from NICEPAY
        if(isset($response->data->resultCd) && $response->data->resultCd == "0000"){
          $txid = $response->tXid;
          $this->PW_Model->hotel_np1($hotelid,$txid);

          header("Location: ".$response->data->requestURL."?tXid=".$txid);
        } elseif (isset($response->resultCd)) {
          $error = '<div>'.
            'Result Code    : '.$response->resultCd.'<br/>'.
            'Result Message : '.$response->resultMsg.'<br/>'.
            '</div>';
          $data['error'] = $error;
          $data['id'] = $id;
          $this->session->set_flashdata('error', $data);

          redirect(base_url('/pesan2'));
        } else {
          $error = 'Connection API Nicepay Timeout. Please Try Again';
          $data['error'] = $error;
          $data['id'] = $id;
          $this->session->set_flashdata('error', $data);

          redirect(base_url('/pesan2'));
        }

        $page = "2";
      }else{
        $pesan = "Upload Gagal";
      }
    }

    $data['data_jumlah'] = $this->PW_Model->jml_book_hotel();
    $data['event'] = $this->PW_Model->get_event_by_status('1');
		$data['pesan'] = $pesan;
    $data['page'] = $page;
    $data['id_event'] = $event_id;
    $data['hotel_active'] = true;

    $this->template->frontend("hotel/pesan2", $data);
  }

  private function valid_upload($fn_foto, $path_img){
    // Upload Images
    if( ! empty($_FILES['foto']['name'])){
      if(!is_dir($path_img)){
        if (!mkdir($path_img, 0777, true)) {
          die('Failed to create folders...');
        }else{
          if(file_exists($path_img)){
            chmod($path_img, 0777);
            if( ! $this->uploads($fn_foto, $path_img)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              die($error);
              // return [];
            }
            return true;
          }
        }
      }else{
        if(file_exists($path_img)){
          if( ! $this->uploads($fn_foto, $path_img)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            // $this->input->set_cookie('msg_error', $error, time()+3600);
            die($error);
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path){
    //$config['upload_path'] = './assets/images/content/';
    $config['upload_path']   = $path;
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['max_size']      = 204800;
    $config['remove_space']  = TRUE;
    $config['file_name']     = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload('foto')){
      $img = $this->upload->data();
      $this->img = $img['file_name'];
      return TRUE;
    }else{
      $this->error = $this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }

  public function hotelkode() {
		/*$k = 0;
		$kar = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
		for ($i=0;$i<200;$i++) {
			$kode = "";
			for ($j=0;$j<7;$j++) { $kode .= $kar[rand(0,strlen($kar)-1)]; }

			$data_cek = $this->Hotel_model->hotelkode_cek($kode);
			if (sizeof($data_cek)==0) {
				$this->Hotel_model->hotelkode_insert($kode);
				echo ($k+1) . ". " . $kode . "<br>";
				$k++;
			}
		} */

		$data_hotel = $this->PW_Model->get_hotel_by_no_kode();
		if (sizeof($data_hotel)>0) {
			for ($i=0;$i<sizeof($data_hotel);$i++) {
				$id = $data_hotel[$i]["id"];
				$nama = $data_hotel[$i]["nama_lengkap"];
				$jenis = $data_hotel[$i]["jenis_kamar"];

				if ($jenis=="1") {
					$cekin = "2019-01-24";
					$cekout = "2019-01-27";
				} else {
					$cekin = "2019-01-25";
					$cekout = "2019-01-27";
				}

				$kode = $this->PW_Model->hotelkode_get();
				$this->PW_Model->hotelkode_update($id,$kode,$cekin,$cekout);

				echo ($i+1) . " " . $id . " " . $nama . " " . $jenis . " " . $kode . "<br>";
			}
		}
	}

	public function invoice($kodebooking="0") {
		$data['kodebooking'] = $kodebooking;
		$data['data_hotel'] = $this->PW_Model->get_hotel_by_kode($kodebooking);
		$this->load->view('frontend/hotel/email_hotel_invoice',$data);			
	}

	public function track($kodebooking="0") {
		$this->PW_Model->hotel_track($kodebooking);
	}

	public function callback() {
		$this->nicelog("callback",json_encode($_GET),json_encode($_POST));

		$hotelid = $_GET['referenceNo'];
    $requestData['amt'] = $_GET['amount'];
    $requestData['referenceNo'] = $hotelid;
    $requestData['tXid'] = $_GET['tXid'];
    $requestData['iMid'] = $this->nicepay_merchid;
    $requestData['merchantKey'] = $this->nicepay_merchkey;
    $requestData['merchantToken'] = hash('sha256', $requestData['iMid'].$hotelid.$requestData['amt'].$requestData['merchantKey']);

    $server_url = $this->nicepay_url. "onePassStatus.do";
    $result = json_decode($this->curl_post($server_url,$requestData));

    //Process Response Nicepay
		$this->PW_Model->hotel_npcode($hotelid,$result->resultCd,$result->resultMsg,json_encode($result));

    if (isset($result->resultCd) && $result->resultCd == '0000') {
			if ($result->payMethod=="01") {
				$this->Hotel_model->hotel_np2($hotelid,$result->cardNo,$result->cardExpYymm,$result->status);
			} else if ($result->payMethod=="02") {
				$this->Hotel_model->hotel_np3($hotelid,$result->bankCd,$result->vacctNo,$result->vacctValidDt,$result->vacctValidTm,$result->status);
			} else if ($result->payMethod=="03") {
				$this->Hotel_model->hotel_np3($hotelid,$result->mitraCd,$result->payNo,"","",$result->status);
			}

      if ($result->status=="0") { $this->Hotel_model->hotel2_status($hotelid,"1"); }

      header("Location: ".site_url("trans/".$hotelid));

      /*$data['hotel'] = $this->Hotel_model->get_hotel_byid($hotelid);
      $data['result'] = $result;
			$this->load->view('pesan_info',$data);*/

    } elseif (isset($result->resultCd)) {
      $error = '<div>'.
        'Result Code    : '.$result->resultCd.'<br/>'.
        'Result Message : '.$result->resultMsg.'<br/>'.
        '</div>';

      $data['error'] = $error;
      $data['id'] = $hotelid;

      $this->session->set_flashdata('error', $data);
      redirect(base_url('/pesan2'));

    } else {

      $error = 'Timeout When Checking Payment Status in Nicepay';
      $data['error'] = $error;
      $data['id'] = $hotelid;
      $this->session->set_flashdata('error', $data);
      redirect(base_url('/pesan2'));
    }
	}

	public function dbprocess() {
		$this->nicelog("dbprocess",json_encode($_GET),json_encode($_POST));
	}

	

	public function nicelog($metoda,$dataget,$datapost) {
		$this->Hotel_model->nice_log("Hotel",$metoda,$dataget,$datapost);
	}

  public function curl_post($server_url,$param) {
    $sparam = "";
    foreach($param as $key=>$value) { $sparam .= $key.'='.$value.'&'; }
    rtrim($sparam, '&');

    //open connection
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $server_url);
    curl_setopt($ch,CURLOPT_HEADER, 0);
    curl_setopt($ch,CURLOPT_POST, count($param));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $sparam);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_TIMEOUT, $this->timeout_connect);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $this->timeout_connect);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  public function trans($transid="0") {
		if ($transid!="0") {
			$data_hotel = $this->PW_Model->get_hotel_byid($transid);
			if (sizeof($data_hotel)>0) {
				$data["transid"] = $transid;
				$data["hotel"] = $data_hotel;
				$this->template->frontend('hotel/pesan_info',$data);
			} else {
				header("Location: ".site_url("pesan2"));
			}
		} else {
			header("Location: ".site_url("pesan2"));
		}
  }

}