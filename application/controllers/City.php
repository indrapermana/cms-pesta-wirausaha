<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class City extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    $data['wilayah'] = $this->PW_Model->get_wilayah_induk();
    $data['city_active'] = true;

    if($this->session->userdata('city')==""){
      $city= array(
        'id_city' => '18',
        'city'    => 'Bandung',
        'postion' => ($this->session->userdata('postion')!="") ? $this->session->userdata('postion') : 'home'
      );
      $this->session->set_userdata($city);
    }

    $this->template->frontend('city/index', $data);
  }

  public function redirect($slug){
    $wilayah = $this->PW_Model->get_wilayah_by_slug($slug);
    $city= array(
      'id_city' => $wilayah[0]['id'],
      'city'    => $wilayah[0]['wilayah'],
    );
    $this->session->set_userdata($city);

    redirect($this->session->userdata('postion'));
  }
}