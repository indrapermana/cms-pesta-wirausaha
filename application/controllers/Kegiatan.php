<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Kegiatan extends CI_Controller {
  private $img, $video;

  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    // $time = time()+3600;
    // echo $time."<br>".date("Y-m-d H:i:s", $time);die;
    $this->manage();
  }

  public function manage($aksi="", $eventid=0){
    if($aksi!="ubah"){
      $this->session->set_userdata(['eventid'=>'']);
    }
    $this->general->session_check();
    // die("wew");

    $tombol = $this->input->post("tombol",true);
		if ($tombol!="") {

      $user_id = $this->user_id;
      $kategori_id = $this->input->post("kategori", true);
      $wilayah_id = $this->input->post("wilayah", true);
      $nama = $this->input->post("judul", true);
      $status_event = ($this->input->post("status_event", true)=="")? "0" : "1";
      $tahun = $this->input->post("tahun", true);
      $lokasi = $this->input->post("lokasi", true);
      $lokasi_lengkap = $this->input->post("lokasi_lengkap", true);
      $tgl_mulai = $this->input->post("tgl_mulai", true);
      $tgl_selesai = $this->input->post("tgl_selesai", true);
      $materi_event = $this->input->post("materi_event", true);
      $lok_lat = $this->input->post("lok_lat", true);
      $lok_long = $this->input->post("lok_long", true);

      $set_title = strtolower($nama);
      $slug = url_title($set_title);

      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = $slug.".".$ext_foto;
      $path_img = "./upload/event/".$tahun."/";

      // die($status_event);
      // echo $aksi;
      // die("wew");

			if ($aksi=="tambah") {
        if($this->valid_upload($fn_foto, $path_img)){
          $this->PW_Model->event_insert($user_id, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $this->img, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long); 
          $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
        }else{
          die("Upload gagal");
        }
      }

      if ($aksi=="ubah") {
        // die($ext_foto);
        if($ext_foto!=""){
          if($this->valid_upload($fn_foto, $path_img)){
            $this->PW_Model->event_update_gambar($eventid, $fn_foto);
            $this->PW_Model->event_update($eventid, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long);
            $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
            
          } //else{
            // $this->input->set_cookie('msg_error', "Data kegiatan gagal disimpan", time()+3600);
            // die("Upload Gagal");
          // }
        }else{
          // die($materi_event);
          $this->PW_Model->event_update($eventid, $kategori_id, $tahun, $wilayah_id, $nama, $slug, $status_event, $lokasi, $lokasi_lengkap, $tgl_mulai, $tgl_selesai, $materi_event, $lok_lat, $lok_long);
          $this->input->set_cookie('msg_success', "Data kegiatan berhasil disimpan", time()+3600);
        }
      }

      if ($aksi=="hapus") { 
        $this->PW_Model->event_delete($eventid); 
        $this->input->set_cookie('msg_success', "Data kegiatan berhasil dihapus", time()+3600);
        redirect("kegiatan/manage");
      }

      if($aksi != "ubah"){
        $aksi = "";
      }
    }
    
    if($aksi==""){ 
      $data['site_form'] = "list";
      if($this->user_level=="1"){
        // $data['data_event'] = $this->PW_Model->get_event();
        $data['data_event_publish'] = $this->PW_Model->get_event_by_status(1);
        $data['data_event_non_publish'] = $this->PW_Model->get_event_by_status(0);
        $data['data_event_past'] = $this->PW_Model->get_event_by_status(2);
      }else{
        // $data['data_event'] = $this->PW_Model->get_event_by_user_id($this->user_id);
        $data['data_event_publish'] = $this->PW_Model->get_event_by_user_id_and_status($this->user_id, 1);
        $data['data_event_non_publish'] = $this->PW_Model->get_event_by_user_id_and_status($this->user_id, 0);
        $data['data_event_past'] = $this->PW_Model->get_event_by_user_id_and_status($this->user_id, 2);
      }

    }else{
      $data['site_form'] = "form";
      $data['data_event'] = $this->PW_Model->get_event_by_id($eventid);
      $data['data_kategori'] = $this->PW_Model->get_kategori();
      $data['data_wilayah'] = $this->PW_Model->get_wilayah();

      $data['edit_event'] = true;

      if($aksi=="ubah"){
        $this->session->set_userdata(['eventid'=>$eventid]);
        $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
      }
    }

    $data['aksi'] = $aksi;
    $data['eventid'] = $eventid;

    $this->template->backend('kegiatan/manage', $data, false);
  }

  private function valid_upload($fn_foto, $path_img){
    // Upload Images
    if( ! empty($_FILES['foto']['name'])){
      if(!is_dir($path_img)){
        if (!mkdir($path_img, 0777, true)) {
          // die('Failed to create folders...');
          $this->input->set_cookie('msg_error', "Failed to create folders...", time()+3600);
        }else{
          if(file_exists($path_img)){
            chmod($path_img, 0777);
            if( ! $this->uploads($fn_foto, $path_img)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              $this->input->set_cookie('msg_error', $error, time()+3600);
              // die($error);
              // return [];
            }
            return true;
          }
        }
      }else{
        if(file_exists($path_img)){
          if( ! $this->uploads($fn_foto, $path_img)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            $this->input->set_cookie('msg_error', $error, time()+3600);
            // die($error);
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path){
    //$config['upload_path'] = './assets/images/content/';
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['max_size']	= '1000';
    $config['remove_space'] = TRUE;
    $config['file_name'] = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload('foto')){
      $img = $this->upload->data();
      $this->img = $img['file_name'];
      return TRUE;
    }else{
      $this->error = $this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }

  public function manages($eventid=0){
    $this->general->session_check();
    if($eventid > 0){
      $this->session->set_userdata(['eventid'=>$eventid]);
      // die($this->event_id);
      redirect("dashboard");
    }else{
      redirect("kegiatan/manage");
    }
  }
}