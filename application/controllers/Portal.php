<?php defined('BASEPATH') OR exit('No direct script access allowed');

class Portal extends CI_Controller {
  private $img, $video;

  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  // public function kegiatan($aksi="", $eventid=0){
    
  // }

  public function berita($aksi="", $beritaid=0){
    $this->general->session_check();

    $data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

    $tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
      $tanggal = $this->input->post("tanggal", true);
      $judul = $this->input->post("judul",true);
			// $gambar = $this->input->post("gambar",true);
      $berita = $this->input->post("berita",true);
      
      $ext_foto = strtolower(pathinfo($_FILES['foto']['name'], PATHINFO_EXTENSION));
      $fn_foto = $judul.".".$ext_foto;
      $path_img = "./upload/event/berita/".$this->$event_id."/";
			
			if ($aksi=="tambah") {
        if($this->valid_upload($fn_foto, $path_img)){
          $this->PW_Model->berita_insert($this->event_id, $tanggal,$judul,$fn_foto,$berita); 
        }else{
          die("Upload gagal");
        }
      }
			if ($aksi=="ubah") { 
        if($fn_foto!=""){
          if($this->valid_upload($fn_foto, $path_img)){
            $this->PW_Model->berita_update_gambar($beritaid, $fn_foto);
            $this->PW_Model->berita_update($beritaid,$tanggal,$judul,$fn_foto,$berita); 
          }else{
            die("Upload Gagal");
          }
        }else{
          $this->PW_Model->berita_update($beritaid,$tanggal,$judul,$fn_foto,$berita); 
          }
      }
			if ($aksi=="hapus") { $this->PW_Model->berita_delete($beritaid); }
			$aksi = "";
    }

    if($aksi==""){ 
      $data['site_form'] = "list";
      $data['data_berita'] = $this->PW_Model->get_berita($this->event_id);
    }else{
      $data['site_form'] = "form";
      $data['data_berita'] = $this->PW_Model->get_berita_byid($beritaid);
    }

    $data['aksi'] = $aksi;
    $data['beritaid'] = $beritaid;

    $this->template->backend('portal/berita', $data);
  }

  private function valid_upload($fn_foto, $path_img){
    // Upload Images
    if( ! empty($_FILES['foto']['name'])){
      if(!is_dir($path_img)){
        if (!mkdir($path_img, 0777, true)) {
          die('Failed to create folders...');
        }else{
          if(file_exists($path_img)){
            chmod($path_img, 0777);
            if( ! $this->uploads($fn_foto, $path_img)){
              $err = explode("<|>", str_replace("<p>", "", $this->error));
              $error = $err[0];
              die($error);
              // return [];
            }
            return true;
          }
        }
      }else{
        if(file_exists($path_img)){
          if( ! $this->uploads($fn_foto, $path_img)){
            $err = explode("<|>", str_replace("<p>", "", $this->error));
            $error = $err[0];
            // $this->input->set_cookie('msg_error', $error, time()+3600);
            die($error);
          }
          return true;
        }
      }
    }
  }

  private function uploads($fn, $path){
    //$config['upload_path'] = './assets/images/content/';
    $config['upload_path'] = $path;
    $config['allowed_types'] = 'jpg|png|jpeg';
    $config['max_size']	= '1000';
    $config['remove_space'] = TRUE;
    $config['file_name'] = $fn;
    //echo "<pre>";print_r($config);die;

    $this->load->library('upload');
    $this->upload->initialize($config, FALSE);
    if($this->upload->do_upload('foto')){
      $img = $this->upload->data();
      $this->img = $img['file_name'];
      return TRUE;
    }else{
      $this->error = $this->upload->display_errors('', '<|>');
      return FALSE;
    }
  }

  public function kategori($aksi="", $kategoriid=0){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

    $tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
      $nama = $this->input->post("nama", true);
      $slug = $this->input->post("slug",true);
	
			if ($aksi=="tambah") { $this->PW_Model->kategori_insert($this->event_id, $nama,$slug); }
			if ($aksi=="ubah") { $this->PW_Model->kategori_update($kategoriid,$nama,$slug); }
			if ($aksi=="hapus") { $this->PW_Model->kategori_delete($kategoriid); }
			$aksi = "";
		}
    
    if($aksi==""){ 
      $data['site_form'] = "list";
      $data['data_kategori'] = $this->PW_Model->get_kategori($this->event_id);
    }else{
      $data['site_form'] = "form";
      $data['data_kategori'] = $this->PW_Model->get_kategori_byid($kategoriid);
    }

    $data['aksi'] = $aksi;
    $data['kategoriid'] = $kategoriid;

    $this->template->backend('portal/kategori', $data);
  }
}