<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pencairan extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index(){
    $this->general->session_check();

    $this->template->backend('pencairan/index');
  }
}