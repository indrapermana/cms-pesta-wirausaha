<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sponsor extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');

    $this->nicepay_url = 'https://www.nicepay.co.id/nicepay/api/';
		// $this->nicepay_url = 'https://qa.nicepay.co.id/nicepay/api/';

    // dev
    //$this->nicepay_merchid = "IONPAYTEST";
    //$this->nicepay_merchkey = "33F49GnCMS1mFYlGXisbUDzVf2ATWCl9k3R++d5hDd3Frmuos/XLx8XhXpe+LDYAbpGKZYSwtlyyLOtS/8aD7A==";

    // production
    $this->nicepay_merchid = "IONPAY";
    $this->nicepay_merchkey = "+XRXEqlfHC2SLejErvjPEcYRF9HDBxR4xklzJPMJwUCNVaq09zn7YvhzpCr2Fe6xh+xKACS7lPkCFUAALXpOoA==";
    $this->timeout_connect = 0;	
  }

  public function pesan(){
    $page = "0";
    $pesan = "";
    $event_id = "";

    $tombol = $this->input->post("tombol",true);
    // die($tombol);
    if ($tombol=="0") {
      $event_id = $this->input->post("event_id",true);
      if($event_id!=""){
        $page = "1";
      }else{
        $pesan = "Event Harus Dipilih !!!";
        $page = "0";
      }
    } elseif($tombol=="1") {
      $nama = $this->input->post("nama",true);
      $email = $this->input->post("email",true);
      $hp = $this->input->post("hp",true);
      $usaha = $this->input->post("usaha",true);
      $asal = $this->input->post("asaltda",true);
      $jenis = $this->input->post("jenis",true);
      $metoda = $this->input->post("metoda",true);
      $keterangan = $this->input->post("keterangan",true);
      $event_id = $this->input->post("event_id",true);

      $nilai = 0; $namapaket = "";
      if ($jenis=="1") { $nilai = 5000000; $namapaket = "Paket Sponsor Internal Premium"; }
      if ($jenis=="2") { $nilai = 2500000; $namapaket = "Paket Sponsor Internal VIP"; }
      if ($jenis=="3") { $nilai = 1500000; $namapaket = "Paket Sponsor Internal Super"; }
      if ($jenis=="4") { $nilai = 500000; $namapaket = "Paket Sponsor Internal Insert"; }
      if ($jenis=="5") { $nilai = 300000; $namapaket = "Paket Sponsor Internal Reguler"; }
      if ($jenis=="6") { $nilai = 20000; $namapaket = "Paket Sponsor Internal Test"; }
      if ($jenis=="7") { $nilai = 800000; $namapaket = "Paket Sponsor Internal Reguler Insert"; }

      $sponsorid = 0;
      $data_sponsor = $this->PW_Model->sponsor_insert($event_id,"00",$nama,$usaha,$email,$hp,$asal,$jenis,"0",$keterangan,$nilai,$metoda);
      if (sizeof($data_sponsor)>0) { $sponsorid = $data_sponsor[0]["id"]; }

      $metode = "00";
      if ($metoda=="1") { $metode = "01"; }
      if (($metoda=="2") || ($metoda=="3") || ($metoda=="4") || ($metoda=="5") || ($metoda=="6") || ($metoda=="7")) { $metode = "02"; }
      if (($metoda=="8") || ($metoda=="9")) { $metode = "03"; }

      $requestData['iMid'] = $this->nicepay_merchid;
      $requestData['payMethod'] = $metode;
      $requestData['currency'] = "IDR";
      $requestData['merchantKey'] = $this->nicepay_merchkey;
      $requestData['amt'] = $nilai;
      $requestData['referenceNo'] = "S".$sponsorid;
      $requestData['goodsNm'] = $namapaket . " #S".$sponsorid;
      $requestData['billingNm'] = $nama;
      $requestData['billingPhone']  = $hp;
      $requestData['billingEmail'] = $email;
      $requestData['callBackUrl'] = base_url('sponsor/scallback');
      $requestData['dbProcessUrl'] = base_url('sponsor/sdbprocess');
      $requestData['description'] = $namapaket . " No." .$requestData['referenceNo'];
      $requestData['merchantToken'] = hash('sha256', $requestData['iMid'].$requestData['referenceNo'].$requestData['amt'].$requestData['merchantKey']);
      $requestData['cartData'] = '{}';
      if ($metode == '01') {
        $requestData['instmntType'] = 1;
        $requestData['instmntMon'] = 1;
        $requestData['billingAddr'] = $asal;
        $requestData['billingCity'] = $asal;
        $requestData['billingState'] = $asal;
        $requestData['billingPostCd'] = '12520';
        $requestData['billingCountry'] = 'Indonesia';
        $requestData['userIP'] = $_SERVER['REMOTE_ADDR'];
      } else if ($metode == '02') {
        if ($metoda=="2") { $bankCode = "BMRI"; } // Bank Mandiri
          else if ($metoda=="3") { $bankCode = "CENA"; } // Bank BCA
          else if ($metoda=="4") { $bankCode = "BBBA"; } // Bank Permata
          else if ($metoda=="5") { $bankCode = "BNIN"; } // Bank BNI
          else if ($metoda=="6") { $bankCode = "BRIN"; } // Bank BRI
          else if ($metoda=="7") { $bankCode = "BNIA"; } // Bank Niaga

        $requestData['bankCd'] = $bankCode;
      } else if ($metode == '04' or $metode == '05') {
        $requestData['userIP'] = $_SERVER['REMOTE_ADDR'];
      }

      $server_url = $this->nicepay_url. "orderRegist.do";
      $response = json_decode(substr($this->curl_post($server_url,$requestData),4));

      //Process response from NICEPAY
      if(isset($response->data->resultCd) && $response->data->resultCd == "0000"){
        $txid = $response->tXid;
        $this->PW_Model->sponsor_np1($sponsorid,$txid,json_encode($requestData),json_encode($response));

        header("Location: ".$response->data->requestURL."?tXid=".$txid);
      } elseif (isset($response->resultCd)) {
        $error = '<div>'.
            'Result Code    : '.$response->resultCd.'<br/>'.
            'Result Message : '.$response->resultMsg.'<br/>'.
            '</div>';
        $data['pesan'] = $error;
        // $data['id'] = $id;
        $data['nama'] = $nama;
        $data['email'] = $email;
        $data['hp'] = $hp;
        $data['usaha'] = $usaha;
        $data['asaltda'] = $asal;
        $data['jenis'] = $jenis;
        $data['metoda'] = $metoda;
        $data['keterangan'] = $keterangan;

        // $this->session->set_flashdata('error', $data);
        // redirect("sponsor/pesan");

      } else {
        $error = 'Connection API Nicepay Timeout. Please Try Again';
        $data['pesan'] = $error;
        // $data['id'] = $id;
        $data['nama'] = $nama;
        $data['email'] = $email;
        $data['hp'] = $hp;
        $data['usaha'] = $usaha;
        $data['asaltda'] = $asal;
        $data['jenis'] = $jenis;
        $data['metoda'] = $metoda;
        $data['keterangan'] = $keterangan;

        // $this->session->set_flashdata('error', $data);
        // redirect("sponsor/pesan");
      }
    }

    if($event_id==""){
      $data['event'] = $this->PW_Model->get_event_by_status('1');
    }else{
      $data['event'] = $this->PW_Model->get_event_by_id($event_id);
    }
    $data['pesan'] = $pesan;
    $data['page'] = $page;
    $data['id_event'] = $event_id;

    $this->template->frontend("sponsor/pesan", $data);
  }

  public function scallback() {
		$this->snicelog("callback",json_encode($_GET),json_encode($_POST));

		$sponsorid = substr($_GET['referenceNo'],1);
    $requestData['amt'] = $_GET['amount'];
    $requestData['referenceNo'] = "S".$sponsorid;
    $requestData['tXid'] = $_GET['tXid'];
    $requestData['iMid'] = $this->nicepay_merchid;
    $requestData['merchantKey'] = $this->nicepay_merchkey;
    $requestData['merchantToken'] = hash('sha256', $requestData['iMid']."S".$sponsorid.$requestData['amt'].$requestData['merchantKey']);

    $server_url = $this->nicepay_url. "onePassStatus.do";
    $result = json_decode($this->curl_post($server_url,$requestData));

        //Process Response Nicepay
		$this->PW_Model->sponsor_npcode($sponsorid,$result->resultCd,$result->resultMsg,json_encode($requestData),json_encode($result));
    if (isset($result->resultCd) && $result->resultCd == '0000') {
      if ($result->payMethod=="01") {
        $this->PW_Model->sponsor_np2($sponsorid,$result->cardNo,$result->cardExpYymm,$result->status);
      } else if ($result->payMethod=="02") {
        $this->PW_Model->sponsor_np3($sponsorid,$result->bankCd,$result->vacctNo,$result->vacctValidDt,$result->vacctValidTm,$result->status);
      } else if ($result->payMethod=="03") {
        $this->PW_Model->sponsor_np3($sponsorid,$result->mitraCd,$result->payNo,"","",$result->status);
      }
      if ($result->status=="0") { $this->PW_Model->sponsor_status($sponsorid,"1"); }

      header("Location: http://sponsor.pestawirausaha.com/trans/".$sponsorid);

    } elseif (isset($result->resultCd)) {
      $error = '<div>'.
          'Result Code    : '.$result->resultCd.'<br/>'.
          'Result Message : '.$result->resultMsg.'<br/>'.
          '</div>';
      $data['error'] = $error;
      // $data['id'] = $hotelid;
      $this->session->set_flashdata('error', $data);
      redirect("http://sponsor.pestawirausaha.com/pesan");
    } else {
      $error = 'Timeout When Checking Payment Status in Nicepay';
      $data['error'] = $error;
      // $data['id'] = $hotelid;
      $this->session->set_flashdata('error', $data);
      redirect("http://sponsor.pestawirausaha.com/pesan");
    }
	}

	public function sdbprocess() {
		$this->snicelog("dbprocess",json_encode($_GET),json_encode($_POST));

		$sponsorid = substr($_POST['referenceNo'],1);
		$status = $_POST['status'];
		if ($status=="0") { $this->PW_Model->sponsor_status($sponsorid,"1"); }
	}

	public function snicelog($metoda,$dataget,$datapost) {
		$this->PW_Model->nice_log("Sponsor",$metoda,$dataget,$datapost);
  }
  
  public function curl_post($server_url,$param) {
    $sparam = "";
    foreach($param as $key=>$value) { $sparam .= $key.'='.$value.'&'; }
    rtrim($sparam, '&');

    //open connection
    $ch = curl_init();
    curl_setopt($ch,CURLOPT_URL, $server_url);
    curl_setopt($ch,CURLOPT_HEADER, 0);
    curl_setopt($ch,CURLOPT_POST, count($param));
    curl_setopt($ch,CURLOPT_POSTFIELDS, $sparam);
    curl_setopt($ch,CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch,CURLOPT_TIMEOUT, $this->timeout_connect);
    curl_setopt($ch,CURLOPT_CONNECTTIMEOUT, $this->timeout_connect);
    curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
    curl_setopt($ch,CURLOPT_SSL_VERIFYPEER, 0);

    $result = curl_exec($ch);
    curl_close($ch);
    return $result;
  }

  public function trans($transid="0") {
		if ($transid!="0") {
			$data_sponsor = $this->PW_Model->get_sponsor_by_id($transid);
			if (sizeof($data_sponsor)>0) {
				$data["transid"] = $transid;
				$data["sponsor"] = $data_sponsor;
				$this->template->frontend('sponsor/pesan_info',$data);
			} else {
				header("Location: ".site_url("sponsor/pesan"));
			}
		} else {
			header("Location: ".site_url("sponsor/pesan"));
		}
  }
}