<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Bank extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
    // $this->load->library('template');
    $this->load->model('PW_Model');
  }

  public function index($kodebank="",$mutasiid="0"){
    $this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;

		$periode = $this->input->post("periode",true);
		if ($periode=="") { $periode = date("my"); } 
		
		$data['periode'] = $periode;
		$data['kodebank'] = $kodebank;
		$data['mutasiid'] = $mutasiid;
		
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$tipetrans = $this->input->post("tipetrans",true);
			$keterangan = $this->input->post("keterangan",true);
			$this->PW_Model->upd_mutasi($mutasiid,$tipetrans,$keterangan);
			
			$mutasiid = "0";
		}
		
		$rekening_id = "0";
		$data_bank = $this->PW_Model->get_bank($kodebank);
		if (sizeof($data_bank)>0) { $rekening_id = $data_bank[0]["id"]; }
		$data['data_bank'] = $data_bank;
		if ($mutasiid!="0") {
			$data['data_rekening'] = $this->PW_Model->get_rekening_mutasi_byid($mutasiid);
			$data['site_form'] = "form";
		} else {
			$data['data_rekap_rekening'] = $this->PW_Model->get_rekap_rekening($rekening_id);
			$data['data_rekening'] = $this->PW_Model->get_rekening_mutasi($rekening_id,$periode);
			$data['site_form'] = "list";
		} 

    $this->template->backend('bank/rekening', $data);
	}
	
	public function upload() {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
		$data['manage_event'] = true;
		
		$data['rekening'] = $this->PW_Model->get_rekening($this->event_id);
		
		$submit = 0; $jmldata = 0;
		$tombol = $this->input->post("tombol",true);
		if ($tombol!="") {
			$txtbank = $this->input->post("txtbank",true);
			$rekening = $this->input->post("rekening_id",true);
			
			if ($txtbank!="") {
				$ltxtbank = explode("\r\n",$txtbank);
				if (sizeof($ltxtbank)>0) {
					for ($i=0;$i<sizeof($ltxtbank);$i++) {
						if (trim($ltxtbank[$i])!="") {
							$ktxtbank = explode("|",$ltxtbank[$i]);
							$tgl = ""; $jam = ""; $noreff = ""; $deskripsi = ""; $nilai = ""; $debet = ""; $kredit = ""; $saldo = "";
							if (sizeof($ktxtbank)>0) {
								$tgl = $ktxtbank[0];
								$jam = $ktxtbank[1];
								$tanggal = substr($tgl,0,4)."-".substr($tgl,4,2)."-".substr($tgl,6,2)." ".$jam;
								$noreff = $ktxtbank[2];
								$deskripsi = $ktxtbank[3];
								$nilai = $ktxtbank[4];
								$knilai = explode(" ",$nilai);
								if (sizeof($knilai)>0) {
									if ($knilai[0]=="D") { $debet = $knilai[1]; $kredit = "0"; }
									else { $kredit = $knilai[1]; $debet = "0"; }
								}
								$saldo = $ktxtbank[7];
							}
							if (($debet!="") && ($kredit!="")) {
								// $rekening = "1"; // bsm
								$data_cek = $this->PW_Model->cek_rekening_mutasi($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo);
								if (sizeof($data_cek)==0) {
									$this->PW_Model->rekening_mutasi_insert($rekening,$tanggal,$noreff,$deskripsi,$debet,$kredit,$saldo);
									
									//rekening_sync($rekmutid);
									$jmldata++;
								}
							}
						}
					}
				}
				$submit = 1;
			}
		}
		$data['submit'] = $submit;
		$data['jmldata'] = $jmldata;
		
		$this->template->backend('bank/upload', $data);	
	}

	public function rekap($mutasiid="0",$settle_code="0") {
		$this->general->session_check();

		$data['event'] = $this->PW_Model->get_event_by_id($this->event_id);
    $data['manage_event'] = true;
		
		$tipetrans = "1";
		$tipe = $this->input->post("tipe",true);
		if ($tipe!="") { $tipetrans = $tipe; } 
		
		if (($mutasiid!="0") && ($settle_code!="0")) {
			$this->PW_Model->set_rekenening_settlecode($mutasiid,$settle_code);
		}
		
		$data['tipetrans'] = $tipetrans;
		$data['data_rekening'] = $this->PW_Model->get_trans_rekening($tipetrans);
		
		$this->template->backend('bank/rekap', $data);
	}
}