<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Expo extends CI_Controller {
  public function __construct(){
    parent::__construct();
    
		// $this->load->library('template');
		
		$this->load->library('botdetect/BotDetectCaptcha', array( 
			'captchaConfig' => 'ExampleCaptcha' 
		)); 
		
    $this->load->model('PW_Model');
  }

  public function index(){
    redirect("expo/info");
  }

  public function pesan($invoice="0", $kode=""){
		$nama = $this->input->post("nama",true);
		$usaha = $this->input->post("usaha",true);
		$email = $this->input->post("email",true);
		$hp = $this->input->post("hp",true);
		$premium = $this->input->post("premium",true);
		$reguler = $this->input->post("reguler",true);
		$kuliner1 = $this->input->post("kuliner1",true);
		$kuliner2 = $this->input->post("kuliner2",true);
		$spremium = $this->input->post("spremium",true); if ($premium=="") { $premium = explode(",",$spremium); }
		$sreguler = $this->input->post("sreguler",true); if ($reguler=="") { $reguler = explode(",",$sreguler); }
		$skuliner1 = $this->input->post("skuliner1",true); if ($kuliner1=="") { $kuliner1 = explode(",",$skuliner1); }		
		$skuliner2 = $this->input->post("skuliner2",true); if ($kuliner2=="") { $kuliner2 = explode(",",$skuliner2); }		
    $event_id = $this->input->post("event_id",true);
    
		//if (time()<strtotime('2018/11/01')) {
			$harga_premium = 4000000;
			$harga_reguler = 3000000;
			$harga_kuliner1 = 4000000;
			$harga_kuliner2 = 3000000; 
		/*} else {
			$harga_premium = 5000000; 
			$harga_reguler = 3500000;
			$harga_kuliner1 = 5000000;
			$harga_kuliner2 = 3500000; 
		}*/

		$error = "";
		$nilai_bayar = 0;
		$xpremium = ""; 
		if (sizeof($premium)>0) { 
			for ($i=0;$i<sizeof($premium);$i++) { 
				if (trim($premium[$i])!="") {
					$xpremium .= $premium[$i] . ","; 
					$nilai_bayar += $harga_premium; 
				}
			} 
		}
		$xreguler = ""; 
		if (sizeof($reguler)>0) { 
			for ($i=0;$i<sizeof($reguler);$i++) { 
				if (trim($reguler[$i])!="") {
					$xreguler .= $reguler[$i] . ","; 
					$nilai_bayar += $harga_reguler; 
				}
			} 
		}
		$xkuliner1 = ""; 
		if (sizeof($kuliner1)>0) { 
			for ($i=0;$i<sizeof($kuliner1);$i++) { 
				if (trim($kuliner1[$i])!="") {
					$xkuliner1 .= $kuliner1[$i] . ","; 
					$nilai_bayar += $harga_kuliner1; 
				}
			} 
		}
		$xkuliner2 = ""; 
		if (sizeof($kuliner2)>0) { 
			for ($i=0;$i<sizeof($kuliner2);$i++) { 
				if (trim($kuliner2[$i])!="") {
					$xkuliner2 .= $kuliner2[$i] . ","; 
					$nilai_bayar += $harga_kuliner2; 
				}
			} 
		}
		
		$page = "0"; $img_captcha = ""; $pesan = "";
    $tombol = $this->input->post("tombol",true);
    if($tombol=="0") {
      // die(($event_id=="")? "wew": "waw");
      if($event_id!=""){
        $page = "1";
      }else{
        $page = "0";
      }
    }
    // die($page);

		if ($tombol=="1") { 
			if (($nama!="") && ($usaha!="") && ($email!="") && ($hp!="")) {
				$page = "2"; 
			} else {
				$pesan = "Semua parameter harus diisi";
				$page = "1";
			}
		}
		if ($tombol=="2") { 
			if ($xpremium.$xreguler.$xkuliner1.$xkuliner2!="") {
				// load the BotDetect Captcha library and set its parameter 
			
				// make Captcha Html accessible to View code 
				$data['captchaHtml'] = $this->botdetectcaptcha->Html(); 
				
				// $kode_captcha = "";
				// for ($i=0;$i<5;$i++) { $kode_captcha .= rand(0,9); }
				// $vals = array(
				// 	'word' => $kode_captcha,
				// 	'img_path' => './captcha_images/',
				// 	'img_url' => base_url('captcha_images/'),
				// 	'img_width' => 348,
				// 	'img_height' => 40,
				// 	'expiration' => 7200,
				// );
				// $captcha = create_captcha($vals); 
				// die(print_r($vals));
				// $this->session->set_userdata('str_captcha', $captcha['word']);
				// $img_captcha = $captcha['image'];
		
				$page = "3"; 
			} else {
				$page = "2"; 
			}
		}
		if ($tombol=="3") {
			$t_tatatertib = trim($this->input->post("tatatertib",true));
			$t_captcha = trim($this->input->post("captcha",true));
			// $s_captcha = trim($this->session->userdata('str_captcha'));
			$isHuman = $this->botdetectcaptcha->Validate($t_captcha);
			// if ((strcmp($t_captcha,$s_captcha)==0) && ($t_captcha!="") && ($t_tatatertib!="")) { 
			if(($isHuman) && $t_tatatertib!=""){
				$invoice = 3001;
				$data_invoice = $this->PW_Model->get_expo_invoice();
				if (sizeof($data_invoice)>0) { $invoice = $data_invoice[0]["invoice"]; }
			
				$kodepesan = ""; $acak = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
				for ($j=0;$j<30;$j++) { $kodepesan .= $acak[rand(0,strlen($acak)-1)]; }
			
				$expoid = 0;
				$data_expo = $this->PW_Model->expo_pesan($event_id,$invoice,$kodepesan,$nama,$usaha,$email,$hp,$spremium,$sreguler,$skuliner1,$skuliner2,$nilai_bayar); 
				if (sizeof($data_expo)>0) { $expoid = $data_expo[0]["id"]; }
			
				$nobooth = "";
				if ($spremium!="") {
					$epremium = explode(",",$spremium);
					if (sizeof($epremium)>0) {
						for ($i=0;$i<sizeof($epremium);$i++) {
							if (trim($epremium[$i])!="") {
								$this->PW_Model->booth_booked($epremium[$i],$expoid,$harga_premium);
								$nobooth .= $epremium[$i] . ",";
								//$nilai_bayar += $harga_premium;
							}
						}
					}
				}
			
				if ($sreguler!="") {
					$ereguler = explode(",",$sreguler);
					if (sizeof($ereguler)>0) {
						for ($i=0;$i<sizeof($ereguler);$i++) {
							if (trim($ereguler[$i])!="") {
								$this->PW_Model->booth_booked($ereguler[$i],$expoid,$harga_reguler);
								$nobooth .= $ereguler[$i] . ",";
								//$nilai_bayar += $harga_reguler;
							}
						}
					}
				}

				if ($skuliner1!="") {
					$ekuliner1 = explode(",",$skuliner1);
					if (sizeof($ekuliner1)>0) {
						for ($i=0;$i<sizeof($ekuliner1);$i++) {
							if (trim($ekuliner1[$i])!="") {
								$this->PW_Model->booth_booked($ekuliner1[$i],$expoid,$harga_kuliner1);
								$nobooth .= $ekuliner1[$i] . ",";
								//$nilai_bayar += $harga_kuliner1;
							}
						}
					}
				}
				if ($skuliner2!="") {
					$ekuliner2 = explode(",",$skuliner2);
					if (sizeof($ekuliner2)>0) {
						for ($i=0;$i<sizeof($ekuliner2);$i++) {
							if (trim($ekuliner2[$i])!="") {
								$this->PW_Model->booth_booked($ekuliner2[$i],$expoid,$harga_kuliner2);
								$nobooth .= $ekuliner2[$i] . ",";
								//$nilai_bayar += $harga_kuliner2;
							}
						}
					}
				}
				$kodebayar = $this->PW_Model->kodeexpo_get();
				$nilai1 = $nilai_bayar/2 - $kodebayar;
				$nilai2 = $nilai_bayar/2 + $kodebayar;
			
				$this->PW_Model->exponote_insert($expoid,0,"Pemesanan Booth $nobooth","2",$nobooth);
				$this->PW_Model->expo_nilaibayar($expoid,$nobooth,$nilai_bayar,$kodebayar,$nilai1,$nilai2);
				
				$this->email_inv($expoid);
			
				$page = "4";				
			} else {
				$pesan = "Maaf! Kode Captcha tidak sesuai. Tata Tertib harus disetujui. ";
				
				// $kode_captcha = "";
				// for ($i=0;$i<5;$i++) { $kode_captcha .= rand(0,9); }
				// $vals = array(
				// 	'word' => $kode_captcha,
				// 	'img_path' => './captcha_images/',
				// 	'img_url' => base_url('captcha_images/'),
				// 	'img_width' => 348,
				// 	'img_height' => 40,
				// 	'expiration' => 7200,
				// );
				// $captcha = create_captcha($vals); 
				// $this->session->set_userdata('str_captcha', $captcha['word']);
				// $img_captcha = $captcha['image'];
				
				$page = "3";
			}

		}
		
    $data['error'] = $error;
    $data['event_id'] = $event_id;
		$data['nama'] = $nama;
		$data['usaha'] = $usaha;
		$data['email'] = $email;
		$data['hp'] = $hp;
		$data['premium'] = $xpremium;
		$data['reguler'] = $xreguler;
		$data['kuliner1'] = $xkuliner1;
		$data['kuliner2'] = $xkuliner2;
		$data['harga_premium'] = $harga_premium;
		$data['harga_reguler'] = $harga_reguler;
		$data['harga_kuliner1'] = $harga_kuliner1;
		$data['harga_kuliner2'] = $harga_kuliner2;
		$data['nilai_bayar'] = $nilai_bayar;
		$data['img_captcha'] = $img_captcha;
		$data['pesan'] = $pesan;
		
    $data['data_booth'] = $this->PW_Model->get_expobooth();
    $data['event'] = $this->PW_Model->get_event_by_status('1');
		
		$data['page'] = $page;
		$data['expo_active'] = true;

		$this->template->frontend('expo/pesan',$data);
	}

  public function info(){
	$data['expo_active'] = true;
    $this->template->frontend('expo/info', $data);
  }

  public function tatatertib(){
	$data['expo_active'] = true;
    $this->template->frontend('expo/tata_tertib', $data);
  }

  public function peta(){
	$data['expo_active'] = true;
    $data['data_booth'] = $this->PW_Model->get_expobooth2();
    $this->template->frontend('expo/peta', $data);
  }

  public function maplayout($maptype="1") {
    $nmfile = "assets/frontend/images/expo/expo1.jpg";
    if ($maptype=="2") { $nmfile = "assets/frontend/images/expo/kuliner1.jpg"; }
    $myImage = imagecreatefromjpeg(base_url($nmfile));
    header("Content-type: image/jpeg");

    $red = imagecolorallocatealpha($myImage, 80, 80, 80, 40);
    $data_booth = $this->PW_Model->booth_tipe($maptype);
    if (sizeof($data_booth)>0) {
      for ($i=0;$i<sizeof($data_booth);$i++) {
        $map_x = $data_booth[$i]["map_x"];
        $map_y = $data_booth[$i]["map_y"];
        $status = $data_booth[$i]["status_booth"];

        if ($status!="0") { imagefilledellipse($myImage, $map_x, $map_y, 30, 30, $red); } 
      }
    }
    imagejpeg($myImage);
    imagedestroy($myImage);
  }
}