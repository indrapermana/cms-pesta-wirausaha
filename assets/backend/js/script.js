jQuery(document).ready(function() {
  // jQuery('#date-range').datepicker({
  //   toggleActive: true,
  //   format: "yyyy-mm-dd",
  //   autoclose: true,
  // });

  $(".knob").knob();

  $('#datetime-start').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });
  $('#datetime-end').datetimepicker({
    format: 'YYYY-MM-DD HH:mm'
  });

  $(":file").filestyle({input: false});

  // var placeholders = $('.select2').attr("placeholder");
  // alert(placeholder);

  $(".select2").select2({
    placeholder : "Pilih"
  });

  if($("#elm1").length > 0){
    tinymce.init({
        selector: "textarea#elm1",
        theme: "modern",
        height:300,
        plugins: [
            "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
            "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
            "save table contextmenu directionality emoticons template paste textcolor"
        ],
        toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons", 
        style_formats: [
            {title: 'Bold text', inline: 'b'},
            {title: 'Red text', inline: 'span', styles: {color: '#ff0000'}},
            {title: 'Red header', block: 'h1', styles: {color: '#ff0000'}},
            {title: 'Example 1', inline: 'span', classes: 'example1'},
            {title: 'Example 2', inline: 'span', classes: 'example2'},
            {title: 'Table styles'},
            {title: 'Table row 1', selector: 'tr', classes: 'tablerow1'}
        ]
    });    
  }

  var handleDataTableButtons = function() {
    "use strict";
    0 !== $("#datatable-buttons").length && $("#datatable-buttons").DataTable({
      dom: "Bfrtip",
      buttons: [{
        extend: "copy",
        className: "btn-sm"
      }, {
        extend: "csv",
        className: "btn-sm"
      }, {
        extend: "excel",
        className: "btn-sm"
      }, {
        extend: "pdf",
        className: "btn-sm"
      }, {
        extend: "print",
        className: "btn-sm"
      }],
      responsive: !0
    })
  },
  TableManageButtons = function() {
      "use strict";
      return {
          init: function() {
              handleDataTableButtons()
          }
      }
  }();
});

TableManageButtons.init();

// !function($) {
//   "use strict";

//   var GoogleMap = function() {};

//   GoogleMap.prototype.getLocation = function($container) {
//     var centerCoordinates = new google.maps.LatLng(37.6, -95.665);
// 		var map = new GMaps($container, {
// 			center : centerCoordinates,
// 			zoom : 4
// 		});
// 		var card = document.getElementById('pac-card');
// 		var input = document.getElementById('pac-input');
// 		var infowindowContent = document.getElementById('infowindow-content');
 
// 		map.controls[google.maps.ControlPosition.TOP_RIGHT].push(card);
 
// 		var autocomplete = new google.maps.places.Autocomplete(input);
// 		var infowindow = new google.maps.InfoWindow();
// 		infowindow.setContent(infowindowContent);
 
// 		var marker = new google.maps.Marker({
// 			map : map
// 		});
 
// 		autocomplete.addListener('place_changed',function() {
// 			document.getElementById("location-error").style.display = 'none';
// 			infowindow.close();
// 			marker.setVisible(false);
// 			var place = autocomplete.getPlace();
// 			if (!place.geometry) {
// 				document.getElementById("location-error").style.display = 'inline-block';
// 				document.getElementById("location-error").innerHTML = "Cannot Locate '" + input.value + "' on map";
// 				return;
// 			}
 
// 			map.fitBounds(place.geometry.viewport);
// 			marker.setPosition(place.geometry.location);
// 			marker.setVisible(true);
 
// 			infowindowContent.children['place-icon'].src = place.icon;
// 			infowindowContent.children['place-name'].textContent = place.name;
// 			infowindowContent.children['place-address'].textContent = input.value;
// 			infowindow.open(map, marker);
// 		});
//   },

//   GoogleMap.prototype.init = function() {
//     var $this = this;
//     $(document).ready(function(){
//       $this.getLocation();
//     });
//   },

//   //init
//   $.GoogleMap = new GoogleMap, $.GoogleMap.Constructor = GoogleMap

// }(window.jQuery),

// //initializing
// function($) {
//   "use strict";
//   $.GoogleMap.init()
// }(window.jQuery);