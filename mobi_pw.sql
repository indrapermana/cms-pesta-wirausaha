-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jul 12, 2019 at 08:37 AM
-- Server version: 10.3.15-MariaDB
-- PHP Version: 7.2.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `mobi_pw`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_aff_pw2019`
--

CREATE TABLE `t_aff_pw2019` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `affid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `daerah` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_aff_rotasi`
--

CREATE TABLE `t_aff_rotasi` (
  `affid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `daerah` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `jumlah` bigint(21) NOT NULL DEFAULT 0,
  `status_link` int(11) NOT NULL,
  `date_link` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_api_log`
--

CREATE TABLE `t_api_log` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `partner_id` int(11) NOT NULL,
  `api_type` varchar(20) NOT NULL,
  `statusCode` int(11) NOT NULL,
  `statusMsg` varchar(100) NOT NULL,
  `api_request` varchar(200) NOT NULL,
  `api_response` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_berita`
--

CREATE TABLE `t_berita` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `judul` varchar(100) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `berita` varchar(5000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_berita`
--

INSERT INTO `t_berita` (`id`, `event_id`, `tanggal`, `judul`, `gambar`, `berita`) VALUES
(1, 9, '2019-01-23 17:50:00', 'Pengunjung makan gratis di Pesta Wirausaha 2019 Ancol', 'https://img.antaranews.com/cache/730x487/2019/01/IMG-20190123-WA0216.jpg', 'Pengunjung Ancol pada akhir pekan ini memiliki kesempatan untuk bisa menikmati kuliner gratis dalam acara Pesta Wirausaha Nasional 2019 yang diselenggarakan oleh Komunitas Tangan Di Atas (TDA).\r\n\r\nAcara dengan tema Kolaboraksi ini akan digelar di Ecovention Hall, Allianz Ecopark Ancol pada tanggal 25-27 Januari 2019.\r\n\r\n\"Pengunjung cukup menunjukkan KTP kepada petugas. Setelah itu, pengunjung akan mendapatkan satu kupon yang dapat ditukarkan dengan menu favoritnya? kata Direktur Pestawirausaha Nasional 2019, Ferdian di Jakarta Barat, Rabu.\r\n\r\nKuliner yang disajikan pun tidak sedikit, panitia telah menyiapkan 1.000 porsi per hari untuk program yang diberi judul Berkah Pemilu 2019 ini. Menunya pun beragam mulai dari soto, ayam geprek, nasi pecel, sate dan lainnya.\r\n\r\n\"Syaratnya adalah pengunjung memiliki nama mirip dengan Presiden atau Wakil Presiden Indonesia,\" katanya.\r\n\r\nPada hari pertama atau tanggal 25 Januari 2019 nama yang berhak untuk mendapatkan makanan gratis adalah yang bernama Soekarno (Sukarno), Soeharto (Suharto), BJ Habibie (Habibi), Megawati (Mega, Wati), Abdurahman Wahid (Abdurahman, Wahid), Susilo Bambang Yudoyono (Susilo, Bambang, Yudoyono) dan Joko Widodo (Joko, Widodo).\r\n\r\nNama di hari kedua Mohammad Hatta (Mohammad, Hatta), Hamengkubuwono IX (Buwono), Adam Malik (Adam, Malik), Umar Wirahadikusumah, Sudarmono, Try Sutrisno, BJ Habibie Hamzah Haz, Boediono, Jusuf Kalla.\r\n\r\nNama di Hari terakhir adalah Joko Widodo (Joko, Widodo), Ma`ruf Amin (Ma`ruf, Amin), Prabowo Subianto (Prabowo), Sandiaga Uno (Sandi).\r\n\r\nSelain itu, pada Pesta Wirausaha kali ini, para peserta kegiatan juga akan mendapatkan promo menarik di unit-unit rekreasi yang terdapat di kawasan Ancol Taman Impian seperti Atlantis Water Adventure, Dunia Fantasi, Sea World Ancol dan Ocean Dream Samudra.\r\n\r\n\"Ancol ingin turut serta mendukung pengembangan UMKM Indonesia, oleh karena itu kami menyambut baik Pesta Wirausaha ini dan mendukung dengan memberikan promo khusus peserta selama penyelenggaraan acara,\" kata VP Corporate Secretary Ancol Taman Impian, Agung Praptono.\r\n'),
(2, 11, '2019-01-25 14:00:00', 'Kemenkop UKM Harap Pelaku Usaha Tumbuh dalam Koperasi', 'https://foto.wartaekonomi.co.id/files/arsip_foto_2019_01_25/entrepeneur_2019_01_25_223846_big.jpg', 'Di era Revolusi Industri 4.0 ini kolaborasi atau kerja sama di antara para pelaku usaha dan stakeholder menjadi faktor penting dari sekedar bersaing satu sama lain. Terlebih, kolaborasi sangat sejalan dengan nilai-nilai dan prinsip koperasi.\r\n\r\nHal tersebut disampaikan Deputi Bidang Pengembangan SDM, Kementerian Koperasi dan UKM, Rulli Nuryanto pada pembukaan acara Pesta Wirausaha 2019; KolaborAksi yang diselenggarakan oleh Komunitas Tangan Di Atas pada tanggal 25-27 Januari 2019 bertempat di Econvention Ancol Jakarta.\r\n\r\n\"Dalam wadah koperasi kita bisa bekerjasama dengan saling menguntungkan dan untuk kesejahteraan bersama,\" kata Rulli, Jumat (25/1/2019).\r\n\r\nUntuk itu Rulli mengharapkan para peserta kegiatan Pesta Wirausaha yang merupakan para pelaku usaha dapat Ikut mengembangkan dan memberdayakan koperasi diantara mereka.\r\n\r\nRulli juga mengapresiasi kegiatan ini karena mendukung upaya menumbuhkan wirausaha baru dan memberdayakan pelaku UKM.\r\n\r\nAcara Pembukaan Pesta Wirausaha 2019 ini juga dihadiri oleh Adi Aryantara (Kepala Dinas Koperasi, UKM dan Perdagangan Provinsi DKI Jakarta, Bertho Darmo Poedjo (Direktur PT. Pembangunan Jaya Ancol), Bimo Prasetio (Presiden TDA 5.1) H. Nuzri Arismal (Founder TDA), Musofa Ramdoni (Majelis Wali Amanah TDA), para anggota & wirausaha dari seuruh Indonesia.\r\n\r\nDalam sambutannya Presiden Komunitas Tangan Di Atas, Bimo Prasetio menyampaikan bahwa peserta pesta wirausaha kali ini adalah anggota-anggota TDA dari segala pelosok tanah air untuk  menimba ilmu dan jejaring. Mereka semua bersama dengan peserta lain akan mengikuti mini workshop terkait teknik-teknik digital marketing terkini.\r\n\r\nSementara Kepala Dinas Koperasi, UKM dan Perdagangan Provinsi DKI Jakarta dalam sambutannya mengatakan menyambut baik kegiatan Pesta Wirausaha ini & akan mendukung terus melalui berbagai kebijakan-kebijakan yang mendukung wirausaha.\r\n\r\nKegiatan Pesta Wirausaha 2019; KolaborAksi ini akan diselenggarakan selama tiga hari dari tanggal 25-27 januari 2019 dan akan diisi dengan berbagai agenda kegiatan seperti kelas digital, panggung inspirasi, panggung expo dan panggung kuliner. Pesta wirausaha 2019 ini akan menghadirkan 30 pembicara, 1.000 mentor, 250 booth pameran dan 3.500 org peserta.');

-- --------------------------------------------------------

--
-- Table structure for table `t_blog`
--

CREATE TABLE `t_blog` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `judul` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `slug` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `gambar` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `short_content` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `content` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_booth_pw2019`
--

CREATE TABLE `t_booth_pw2019` (
  `id` int(11) NOT NULL,
  `no_booth` int(11) NOT NULL,
  `tipe_booth` int(11) NOT NULL,
  `map_type` int(11) NOT NULL,
  `map_x` int(11) NOT NULL,
  `map_y` int(11) NOT NULL,
  `status_booth` int(11) NOT NULL,
  `booked_date` datetime NOT NULL,
  `booked_by` int(11) NOT NULL,
  `nilai_booth` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_booth_pw2019`
--

INSERT INTO `t_booth_pw2019` (`id`, `no_booth`, `tipe_booth`, `map_type`, `map_x`, `map_y`, `status_booth`, `booked_date`, `booked_by`, `nilai_booth`, `nama`, `usaha`) VALUES
(424, 77, 4, 1, 985, 399, 1, '2019-01-15 13:01:52', 215, 0, '', ''),
(423, 76, 4, 1, 945, 399, 1, '2019-01-15 13:01:52', 215, 0, '', ''),
(422, 75, 4, 1, 905, 399, 1, '2019-01-16 10:48:50', 225, 0, '', ''),
(421, 74, 4, 1, 865, 399, 1, '2019-01-09 20:02:47', 199, 0, '', ''),
(420, 203, 4, 1, 85, 856, 1, '2019-01-04 23:32:22', 189, 0, '', ''),
(419, 202, 4, 1, 125, 856, 1, '2019-01-04 23:32:22', 189, 0, '', ''),
(418, 201, 4, 1, 165, 856, 1, '2019-01-04 23:32:22', 189, 0, '', ''),
(417, 200, 4, 1, 205, 856, 1, '2019-01-04 23:32:22', 189, 0, '', ''),
(416, 199, 4, 1, 245, 856, 1, '2019-01-16 10:43:15', 223, 0, '', ''),
(415, 198, 4, 1, 285, 856, 1, '2019-01-16 10:46:00', 224, 0, '', ''),
(414, 197, 4, 1, 325, 856, 1, '2019-01-16 10:46:00', 224, 0, '', ''),
(413, 196, 4, 1, 365, 856, 1, '2019-01-16 10:39:51', 222, 0, '', ''),
(412, 195, 4, 1, 405, 856, 1, '2019-01-16 10:52:12', 226, 0, '', ''),
(411, 194, 4, 1, 445, 856, 1, '2019-01-16 15:32:02', 233, 0, '', ''),
(410, 193, 4, 1, 485, 856, 1, '2019-01-09 19:52:40', 198, 0, '', ''),
(409, 192, 4, 1, 525, 856, 1, '2019-01-09 19:52:40', 198, 0, '', ''),
(408, 191, 4, 1, 565, 856, 1, '2019-01-16 10:37:53', 221, 0, '', ''),
(407, 190, 4, 1, 605, 856, 1, '2019-01-16 10:35:33', 220, 0, '', ''),
(406, 189, 4, 1, 645, 856, 1, '2019-01-16 15:54:57', 235, 0, '', ''),
(405, 188, 4, 1, 685, 856, 1, '2019-01-11 19:42:17', 253, 0, '', ''),
(404, 187, 4, 1, 725, 856, 1, '2019-01-16 15:35:46', 251, 0, '', ''),
(403, 113, 4, 1, 85, 538, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(402, 112, 4, 1, 125, 538, 0, '2019-01-21 18:07:34', 0, 3000000, '', ''),
(401, 111, 4, 1, 165, 538, 1, '2019-01-16 10:52:12', 226, 0, '', ''),
(400, 59, 4, 1, 85, 299, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(399, 58, 4, 1, 125, 299, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(398, 57, 4, 1, 165, 299, 1, '2019-01-21 18:16:43', 252, 0, '', ''),
(397, 239, 8, 2, 384, 398, 1, '2018-11-08 23:59:30', 113, 4000000, '', ''),
(396, 222, 8, 2, 1183, 398, 1, '2018-09-20 19:20:19', 84, 4000000, '', ''),
(395, 221, 8, 2, 1183, 93, 1, '2018-10-08 18:54:49', 93, 4000000, '', ''),
(394, 204, 8, 2, 384, 93, 1, '2018-10-31 18:51:29', 106, 4000000, '', ''),
(393, 186, 1, 1, 765, 856, 1, '2018-09-26 10:41:51', 92, 4000000, '', ''),
(392, 185, 2, 1, 85, 816, 1, '2019-01-04 23:32:22', 189, 3000000, '', ''),
(391, 184, 2, 1, 125, 816, 1, '2019-01-04 23:32:22', 189, 3000000, '', ''),
(390, 183, 2, 1, 165, 816, 1, '2019-01-04 23:32:22', 189, 3000000, '', ''),
(389, 182, 2, 1, 205, 816, 1, '2019-01-04 23:32:22', 189, 3000000, '', ''),
(388, 181, 2, 1, 245, 816, 1, '2019-01-04 18:25:23', 188, 3000000, '', ''),
(387, 180, 2, 1, 285, 816, 1, '2019-01-04 18:25:23', 188, 3000000, '', ''),
(386, 179, 2, 1, 325, 816, 1, '2019-01-17 12:54:58', 239, 3000000, '', ''),
(385, 178, 2, 1, 365, 816, 1, '2019-01-17 12:54:58', 239, 3000000, '', ''),
(384, 177, 2, 1, 405, 816, 1, '2019-01-17 11:11:37', 238, 3000000, '', ''),
(383, 176, 2, 1, 445, 816, 1, '2019-01-16 13:27:09', 232, 3000000, '', ''),
(382, 175, 2, 1, 485, 816, 1, '2019-01-09 19:52:40', 198, 3000000, '', ''),
(381, 174, 2, 1, 525, 816, 1, '2019-01-09 19:52:40', 198, 3000000, '', ''),
(380, 173, 2, 1, 565, 816, 1, '2019-01-09 16:39:39', 195, 2400000, '', ''),
(379, 172, 2, 1, 605, 816, 1, '2019-01-09 16:39:39', 195, 2400000, '', ''),
(378, 171, 2, 1, 645, 816, 1, '2019-01-09 16:36:57', 194, 2400000, '', ''),
(377, 170, 2, 1, 685, 816, 1, '2019-01-09 16:36:57', 194, 2400000, '', ''),
(376, 169, 2, 1, 725, 816, 1, '2019-01-09 16:36:57', 194, 2400000, '', ''),
(375, 168, 1, 1, 765, 816, 1, '2018-09-21 14:30:42', 87, 4000000, '', ''),
(374, 167, 2, 1, 827, 717, 1, '2019-01-16 12:16:46', 229, 3000000, '', ''),
(373, 166, 2, 1, 787, 717, 1, '2019-01-03 19:35:32', 184, 3000000, '', ''),
(372, 165, 2, 1, 747, 717, 1, '2019-01-16 11:10:20', 227, 2500000, '', ''),
(371, 164, 2, 1, 707, 717, 1, '2019-01-16 11:10:20', 227, 2500000, '', ''),
(370, 163, 2, 1, 667, 717, 1, '2019-01-17 17:54:22', 241, 3000000, '', ''),
(369, 162, 2, 1, 627, 717, 1, '2019-01-14 12:47:46', 209, 2700000, '', ''),
(368, 161, 2, 1, 587, 717, 1, '2019-01-14 15:51:12', 211, 3000000, '', ''),
(367, 160, 2, 1, 547, 717, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(366, 159, 2, 1, 507, 717, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(365, 158, 2, 1, 467, 717, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(364, 157, 2, 1, 427, 717, 1, '2019-01-21 12:42:24', 248, 3000000, '', ''),
(363, 156, 2, 1, 387, 717, 1, '2019-01-24 07:22:55', 260, 3000000, '', ''),
(362, 155, 2, 1, 347, 717, 1, '2019-01-22 10:36:13', 255, 3000000, '', ''),
(361, 154, 2, 1, 307, 717, 1, '2019-01-23 11:44:50', 257, 3000000, '', ''),
(360, 153, 2, 1, 267, 717, 1, '2019-01-23 11:44:50', 257, 3000000, '', ''),
(359, 152, 2, 1, 227, 717, 1, '2019-01-23 11:44:08', 256, 3000000, '', ''),
(358, 151, 2, 1, 187, 717, 1, '2019-01-02 07:12:13', 179, 3000000, '', ''),
(357, 150, 1, 1, 147, 717, 1, '0000-00-00 00:00:00', 19, 4000000, '', ''),
(356, 149, 2, 1, 827, 677, 1, '2018-11-09 08:16:30', 114, 3000000, '', ''),
(355, 148, 2, 1, 787, 677, 1, '2018-11-09 08:16:30', 114, 3000000, '', ''),
(354, 147, 2, 1, 747, 677, 1, '2018-12-31 00:27:42', 176, 3000000, '', ''),
(353, 146, 2, 1, 707, 677, 1, '2019-01-13 13:05:40', 207, 3000000, '', ''),
(352, 145, 2, 1, 667, 677, 1, '2018-10-19 15:09:16', 97, 3000000, '', ''),
(351, 144, 2, 1, 627, 677, 1, '2019-01-15 14:09:53', 217, 3000000, '', ''),
(350, 143, 2, 1, 587, 677, 1, '2019-01-16 12:12:15', 228, 2500000, '', ''),
(349, 142, 2, 1, 547, 677, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(348, 141, 2, 1, 507, 677, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(347, 140, 2, 1, 467, 677, 1, '2019-01-15 13:01:52', 215, 3000000, '', ''),
(346, 139, 2, 1, 427, 677, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(345, 138, 2, 1, 387, 677, 1, '2019-01-25 12:32:10', 267, 3000000, '', ''),
(344, 137, 2, 1, 347, 677, 1, '2019-01-25 06:55:42', 266, 3000000, '', ''),
(343, 136, 2, 1, 307, 677, 1, '2019-01-24 18:59:19', 265, 3000000, '', ''),
(342, 135, 2, 1, 267, 677, 1, '2019-01-24 18:59:19', 265, 3000000, '', ''),
(341, 134, 2, 1, 227, 677, 1, '2019-01-19 15:07:19', 244, 2400000, '', ''),
(340, 133, 2, 1, 187, 677, 1, '2019-01-10 12:42:33', 201, 3000000, '', ''),
(339, 132, 1, 1, 147, 677, 1, '0000-00-00 00:00:00', 19, 4000000, '', ''),
(338, 131, 2, 1, 85, 578, 1, '2019-01-02 17:50:32', 181, 3000000, '', ''),
(337, 130, 2, 1, 125, 578, 1, '2019-01-02 17:50:32', 181, 3000000, '', ''),
(336, 129, 2, 1, 165, 578, 1, '2019-01-15 14:51:38', 218, 3000000, '', ''),
(335, 128, 2, 1, 205, 578, 1, '2018-12-20 13:53:56', 162, 3000000, '', ''),
(334, 127, 2, 1, 245, 578, 1, '2019-01-19 21:30:25', 245, 3000000, '', ''),
(333, 126, 2, 1, 285, 578, 1, '2019-01-21 17:08:25', 249, 3000000, '', ''),
(332, 125, 2, 1, 325, 578, 1, '2019-01-24 07:36:20', 264, 3000000, '', ''),
(331, 124, 2, 1, 365, 578, 1, '2019-01-24 07:36:20', 264, 3000000, '', ''),
(330, 123, 2, 1, 405, 578, 1, '2019-01-13 22:29:37', 208, 3000000, '', ''),
(329, 122, 2, 1, 445, 578, 1, '2019-01-13 22:29:37', 208, 3000000, '', ''),
(328, 121, 2, 1, 485, 578, 1, '2019-01-13 22:29:37', 208, 3000000, '', ''),
(327, 120, 2, 1, 525, 578, 1, '2019-01-02 16:16:30', 180, 3000000, '', ''),
(326, 119, 2, 1, 565, 578, 1, '2019-01-20 10:34:11', 246, 3000000, '', ''),
(325, 118, 2, 1, 605, 578, 1, '2019-01-17 10:51:28', 237, 3000000, '', ''),
(324, 117, 2, 1, 645, 578, 1, '2019-01-02 18:52:58', 182, 3000000, '', ''),
(323, 116, 2, 1, 685, 578, 1, '2018-12-31 19:31:12', 177, 3000000, '', ''),
(322, 115, 2, 1, 725, 578, 1, '2018-12-10 15:32:40', 138, 3000000, '', ''),
(321, 114, 1, 1, 765, 578, 1, '2018-09-20 13:12:57', 79, 4000000, '', ''),
(320, 110, 2, 1, 205, 538, 1, '2018-12-20 13:53:56', 162, 3000000, '', ''),
(319, 109, 2, 1, 245, 538, 1, '2018-12-05 11:33:27', 132, 3000000, '', ''),
(318, 108, 2, 1, 285, 538, 1, '0000-00-00 00:00:00', 37, 3000000, '', ''),
(317, 107, 2, 1, 325, 538, 1, '2019-01-16 15:54:57', 235, 3000000, '', ''),
(316, 106, 2, 1, 365, 538, 1, '2019-01-19 08:05:09', 243, 3000000, '', ''),
(315, 105, 2, 1, 405, 538, 1, '2018-12-20 11:18:06', 161, 3000000, 'cindera', 'Madani pakaian muslim'),
(314, 104, 2, 1, 445, 538, 1, '2018-12-20 11:18:06', 161, 3000000, 'Masyarakat Tanpa Riba', 'Masyarakat Tanpa Riba'),
(313, 103, 2, 1, 485, 538, 1, '2018-12-20 11:18:06', 161, 3000000, 'Masyarakat Tanpa Riba', 'Masyarakat Tanpa Riba'),
(312, 102, 2, 1, 525, 538, 1, '2019-01-09 22:20:40', 200, 3000000, '', ''),
(311, 101, 2, 1, 565, 538, 1, '2018-11-02 15:46:12', 107, 3000000, '', ''),
(310, 100, 2, 1, 605, 538, 1, '2018-11-02 15:28:58', 107, 3000000, '', ''),
(309, 99, 2, 1, 645, 538, 1, '2018-12-04 18:32:11', 130, 3000000, '', ''),
(308, 98, 2, 1, 685, 538, 1, '2018-09-21 13:52:05', 23, 3000000, '', ''),
(307, 97, 2, 1, 725, 538, 1, '2018-09-21 13:52:05', 23, 3000000, '', ''),
(306, 96, 1, 1, 765, 538, 1, '2018-09-20 13:12:57', 79, 4000000, '', ''),
(305, 91, 2, 1, 825, 439, 1, '2018-09-20 16:16:31', 81, 3000000, '', ''),
(304, 90, 2, 1, 785, 439, 1, '2018-11-09 10:07:23', 43, 3000000, '', ''),
(303, 89, 2, 1, 745, 439, 1, '0000-00-00 00:00:00', 43, 3000000, '', ''),
(302, 88, 2, 1, 705, 439, 1, '2018-11-12 17:26:01', 117, 3000000, '', ''),
(301, 87, 2, 1, 665, 439, 1, '2018-12-05 13:08:46', 133, 3000000, '', ''),
(300, 86, 2, 1, 625, 439, 1, '2018-12-11 10:52:01', 140, 3000000, '', ''),
(299, 85, 2, 1, 585, 439, 1, '2019-01-10 19:43:09', 203, 3000000, '', ''),
(298, 84, 2, 1, 545, 439, 1, '2019-01-18 14:16:52', 242, 3000000, '', ''),
(297, 83, 2, 1, 505, 439, 1, '2019-01-18 14:16:52', 242, 2500000, '', ''),
(296, 82, 2, 1, 465, 439, 1, '2019-01-17 14:13:39', 240, 3000000, '', ''),
(295, 81, 2, 1, 425, 439, 1, '2019-01-17 14:13:39', 240, 3000000, '', ''),
(294, 80, 2, 1, 385, 439, 1, '2019-01-17 14:13:39', 240, 3000000, '', ''),
(293, 79, 2, 1, 345, 439, 1, '2018-09-21 13:52:05', 86, 3000000, '', ''),
(292, 78, 1, 1, 305, 439, 1, '2018-09-20 17:19:36', 83, 4000000, '', ''),
(291, 73, 2, 1, 825, 399, 1, '2018-12-04 19:33:13', 131, 3000000, '', ''),
(290, 72, 2, 1, 785, 399, 1, '2018-12-04 19:33:13', 131, 3000000, '', ''),
(289, 71, 2, 1, 745, 399, 1, '2018-12-13 11:43:29', 147, 3000000, '', ''),
(288, 70, 2, 1, 705, 399, 1, '2018-12-20 10:24:06', 160, 3000000, '', ''),
(287, 69, 2, 1, 665, 399, 1, '2019-01-16 13:23:03', 231, 3000000, '', ''),
(286, 68, 2, 1, 625, 399, 1, '2018-12-11 10:52:01', 140, 3000000, '', ''),
(285, 67, 2, 1, 585, 399, 1, '2018-12-19 18:33:27', 159, 3000000, '', ''),
(284, 66, 2, 1, 545, 399, 1, '2019-01-23 17:50:55', 259, 3000000, '', ''),
(283, 65, 2, 1, 505, 399, 1, '2019-01-21 11:12:14', 247, 3000000, '', ''),
(282, 64, 2, 1, 465, 399, 1, '2019-01-21 11:12:14', 247, 3000000, '', ''),
(281, 63, 2, 1, 425, 399, 1, '2019-01-21 11:12:14', 247, 3000000, '', ''),
(280, 62, 2, 1, 385, 399, 1, '2018-09-23 19:32:05', 91, 3000000, '', ''),
(279, 61, 2, 1, 345, 399, 1, '2018-09-20 16:53:18', 82, 3000000, '', ''),
(278, 60, 1, 1, 305, 399, 1, '2018-09-20 16:53:18', 82, 4000000, '', ''),
(277, 56, 2, 1, 205, 299, 1, '2019-01-11 19:42:17', 205, 3000000, '', ''),
(276, 55, 2, 1, 245, 299, 1, '2019-01-05 01:12:32', 190, 3000000, '', ''),
(275, 54, 2, 1, 285, 299, 1, '2018-09-21 19:50:55', 89, 3000000, '', ''),
(274, 53, 2, 1, 325, 299, 1, '2018-12-21 06:50:19', 163, 3000000, '', ''),
(273, 52, 2, 1, 365, 299, 1, '2019-01-23 15:34:04', 258, 3000000, '', ''),
(272, 51, 2, 1, 405, 299, 1, '2018-12-12 12:39:02', 143, 3000000, '', ''),
(271, 50, 2, 1, 445, 299, 1, '2019-01-15 20:36:40', 219, 3000000, '', ''),
(270, 49, 2, 1, 485, 299, 1, '2018-11-08 21:10:22', 112, 3000000, '', ''),
(269, 48, 2, 1, 525, 299, 1, '2018-12-12 21:43:01', 145, 3000000, '', ''),
(268, 47, 2, 1, 565, 299, 1, '2018-12-19 17:40:41', 158, 3000000, '', ''),
(267, 46, 2, 1, 605, 299, 1, '2018-12-19 17:40:41', 158, 3000000, '', ''),
(266, 45, 2, 1, 645, 299, 1, '2018-12-19 17:40:41', 158, 3000000, '', ''),
(265, 44, 2, 1, 685, 299, 1, '2018-12-20 11:18:06', 161, 3000000, '', ''),
(264, 43, 2, 1, 725, 299, 1, '2018-12-20 11:18:06', 161, 3000000, 'Fenni rinanda', 'Rinanda skin care center'),
(263, 42, 1, 1, 765, 299, 1, '2018-09-12 09:11:41', 53, 4000000, '', ''),
(262, 41, 2, 1, 85, 259, 1, '2018-12-15 13:44:22', 154, 3000000, '', ''),
(261, 40, 2, 1, 125, 259, 1, '2018-12-18 16:16:27', 157, 3000000, '', ''),
(260, 39, 2, 1, 165, 259, 1, '2019-01-01 19:30:46', 178, 3000000, '', ''),
(259, 38, 2, 1, 205, 259, 1, '2019-01-14 15:26:51', 210, 3000000, '', ''),
(258, 37, 2, 1, 245, 259, 1, '2019-01-07 00:26:37', 191, 3000000, '', ''),
(257, 36, 2, 1, 285, 259, 1, '2019-01-03 15:06:32', 183, 3000000, '', ''),
(256, 35, 2, 1, 325, 259, 1, '2018-12-23 15:28:00', 168, 3000000, '', ''),
(255, 34, 2, 1, 365, 259, 1, '2019-01-24 07:27:11', 261, 3000000, '', ''),
(254, 33, 2, 1, 405, 259, 1, '2019-01-16 16:29:42', 236, 3000000, '', ''),
(253, 32, 2, 1, 445, 259, 1, '2019-01-16 16:29:42', 236, 3000000, '', ''),
(252, 31, 2, 1, 485, 259, 1, '2019-01-24 07:30:52', 263, 3000000, '', ''),
(251, 30, 2, 1, 525, 259, 1, '2019-01-22 09:58:45', 254, 3000000, '', ''),
(250, 29, 2, 1, 565, 259, 1, '2019-01-16 12:48:42', 230, 3000000, '', ''),
(249, 28, 2, 1, 605, 259, 1, '2019-01-04 17:32:26', 187, 3000000, '', ''),
(248, 27, 2, 1, 645, 259, 1, '2019-01-24 07:28:26', 262, 3000000, '', ''),
(247, 26, 2, 1, 685, 259, 1, '2018-11-12 19:41:25', 118, 3000000, '', ''),
(246, 25, 2, 1, 725, 259, 1, '2018-11-30 14:31:48', 71, 3000000, '', ''),
(245, 24, 1, 1, 765, 259, 1, '2018-09-19 11:10:09', 71, 4000000, '', ''),
(244, 23, 1, 1, 667, 159, 1, '2018-09-20 15:33:07', 80, 4000000, '', ''),
(243, 22, 1, 1, 627, 159, 1, '2018-10-25 01:20:21', 99, 4000000, '', ''),
(242, 21, 1, 1, 587, 159, 1, '2018-10-12 19:31:28', 94, 4000000, '', ''),
(241, 20, 1, 1, 547, 159, 1, '2018-10-25 05:52:18', 100, 4000000, '', ''),
(240, 19, 1, 1, 507, 159, 1, '2018-10-30 09:52:00', 104, 4000000, '', ''),
(239, 18, 1, 1, 467, 159, 1, '2018-10-30 09:52:00', 104, 4000000, '', ''),
(238, 17, 1, 1, 427, 159, 1, '2018-11-30 17:52:43', 123, 4000000, '', ''),
(237, 16, 1, 1, 387, 159, 1, '2018-11-09 14:19:38', 116, 4000000, '', ''),
(236, 15, 1, 1, 347, 159, 1, '2018-12-23 06:30:39', 166, 4000000, '', ''),
(235, 14, 1, 1, 307, 159, 1, '2018-10-31 13:04:47', 105, 4000000, '', ''),
(234, 13, 1, 1, 267, 159, 1, '0000-00-00 00:00:00', 95, 4000000, 'Lina Kartikasari', 'HIJAB.ID'),
(233, 12, 1, 1, 227, 159, 1, '2018-09-11 14:29:12', 95, 4000000, 'Lina Kartikasari', 'HIJAB.ID'),
(232, 11, 1, 1, 187, 159, 1, '2018-10-15 11:24:11', 95, 4000000, 'Tonton Taufik', 'RajaBacklink.com'),
(231, 10, 1, 1, 147, 159, 1, '2018-10-15 11:19:54', 95, 4000000, 'Tonton Taufik', 'RajaBacklink.com'),
(196, 205, 3, 2, 431, 93, 1, '2018-12-12 16:56:20', 144, 3000000, '', ''),
(197, 206, 3, 2, 478, 93, 1, '2018-12-12 16:56:20', 144, 3000000, '', ''),
(198, 207, 3, 2, 525, 93, 1, '2018-12-27 07:20:31', 173, 3000000, '', ''),
(199, 208, 3, 2, 572, 93, 1, '2018-12-22 13:12:11', 136, 3000000, '', ''),
(200, 209, 3, 2, 619, 93, 1, '2019-01-10 17:21:56', 202, 3000000, '', ''),
(201, 210, 3, 2, 666, 93, 1, '2018-10-31 18:51:29', 106, 3000000, '', ''),
(202, 211, 3, 2, 713, 93, 1, '2018-12-03 19:28:39', 206, 3000000, '', ''),
(203, 212, 3, 2, 760, 93, 1, '2018-11-24 15:42:47', 206, 3000000, 'raja uduk', 'mandiri - raja uduk'),
(204, 213, 3, 2, 807, 93, 1, '2018-11-25 10:26:42', 120, 3000000, '', ''),
(205, 214, 3, 2, 854, 93, 1, '2018-12-12 05:52:43', 141, 3000000, '', ''),
(206, 215, 3, 2, 901, 93, 1, '2019-01-14 19:09:29', 212, 2700000, '', ''),
(207, 216, 3, 2, 948, 93, 1, '2019-01-14 19:09:29', 212, 2700000, '', ''),
(208, 217, 3, 2, 995, 93, 1, '2018-10-28 18:58:43', 103, 3000000, '', ''),
(209, 218, 3, 2, 1042, 93, 1, '2019-01-07 01:28:52', 222, 3000000, '', ''),
(210, 219, 3, 2, 1089, 93, 1, '2018-12-27 22:04:52', 174, 3000000, '', ''),
(211, 220, 3, 2, 1136, 93, 1, '2018-09-19 14:50:22', 77, 3000000, '', ''),
(214, 223, 3, 2, 1136, 398, 1, '2019-01-14 19:56:20', 250, 3000000, '', ''),
(215, 224, 3, 2, 1089, 398, 1, '2018-12-28 06:27:34', 175, 3000000, '', ''),
(216, 225, 3, 2, 1042, 398, 1, '2018-12-07 12:48:44', 172, 3000000, '', ''),
(217, 226, 3, 2, 995, 398, 1, '2018-12-12 22:25:05', 146, 3000000, '', ''),
(218, 227, 3, 2, 948, 398, 1, '2018-12-15 13:27:39', 152, 3000000, 'enny kasim', 'pempek enni'),
(219, 228, 3, 2, 901, 398, 1, '2018-12-15 13:25:28', 152, 3000000, 'dewi rachmawati', 'kedai hijau'),
(220, 229, 3, 2, 854, 398, 1, '2018-12-11 08:09:58', 139, 3000000, '', ''),
(221, 230, 3, 2, 807, 398, 1, '2018-09-21 14:31:58', 87, 3000000, '', ''),
(222, 231, 3, 2, 760, 398, 1, '2018-11-26 09:00:37', 121, 3000000, '', ''),
(223, 232, 3, 2, 713, 398, 1, '2018-12-15 13:29:15', 152, 3000000, 'revina indah sari', 'poopa'),
(224, 233, 3, 2, 666, 398, 1, '2018-12-15 13:30:49', 152, 3000000, 'lina', 'gogo mango & thai'),
(225, 234, 3, 2, 619, 398, 1, '2018-12-15 13:32:09', 152, 3000000, 'debby', 'kedai debby'),
(226, 235, 3, 2, 572, 398, 1, '2018-12-27 06:13:23', 171, 3000000, '', ''),
(227, 236, 3, 2, 525, 398, 1, '2019-01-15 14:05:39', 216, 3000000, '', ''),
(228, 237, 3, 2, 478, 398, 1, '2019-01-07 01:28:52', 222, 3000000, '', ''),
(229, 238, 3, 2, 431, 398, 0, '2019-01-11 14:57:15', 204, 3000000, '', ''),
(425, 92, 4, 1, 865, 439, 1, '2019-01-09 20:02:47', 199, 0, '', ''),
(426, 93, 4, 1, 905, 439, 1, '2019-01-16 10:48:50', 225, 0, '', ''),
(427, 94, 4, 1, 945, 439, 1, '2019-01-16 10:52:12', 226, 0, '', ''),
(428, 95, 4, 1, 985, 439, 1, '2019-01-15 13:01:52', 215, 0, '', ''),
(429, 1, 4, 1, 186, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(430, 2, 4, 1, 226, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(431, 3, 4, 1, 266, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(432, 4, 4, 1, 306, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(433, 5, 4, 1, 346, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(434, 6, 4, 1, 386, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(435, 7, 4, 1, 426, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(436, 8, 4, 1, 466, 64, 0, '0000-00-00 00:00:00', 0, 0, '', ''),
(437, 9, 4, 1, 506, 64, 0, '0000-00-00 00:00:00', 0, 0, '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_budget`
--

CREATE TABLE `t_budget` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `kode_budget` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `item_budget` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `budget1` int(11) NOT NULL DEFAULT 0,
  `budget2` int(11) NOT NULL DEFAULT 0,
  `budget3` int(11) NOT NULL DEFAULT 0,
  `realisasi` int(11) NOT NULL DEFAULT 0,
  `urutan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_budget`
--

INSERT INTO `t_budget` (`id`, `event_id`, `parent_id`, `kode_budget`, `item_budget`, `keterangan`, `budget1`, `budget2`, `budget3`, `realisasi`, `urutan`) VALUES
(1, 9, 0, 'INDUK', 'Anggaran 2019', 'Budget Anggaran 2019', 0, 19800000, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_budget_item`
--

CREATE TABLE `t_budget_item` (
  `id` int(11) NOT NULL,
  `budget_id` int(11) NOT NULL,
  `jenis_budget` int(11) NOT NULL,
  `jml` int(11) NOT NULL,
  `harga` int(11) NOT NULL,
  `ppn` int(11) NOT NULL,
  `jml_hari` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_budget_item`
--

INSERT INTO `t_budget_item` (`id`, `budget_id`, `jenis_budget`, `jml`, `harga`, `ppn`, `jml_hari`) VALUES
(3, 1, 2, 20, 900000, 1, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_device_log`
--

CREATE TABLE `t_device_log` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `partner_id` int(11) NOT NULL,
  `tiket_id` int(11) NOT NULL,
  `device_id` varchar(20) NOT NULL,
  `cert` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_email_comingsoon`
--

CREATE TABLE `t_email_comingsoon` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_email_log`
--

CREATE TABLE `t_email_log` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `email_group` varchar(20) NOT NULL,
  `kode` varchar(30) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `pengirim` varchar(100) NOT NULL,
  `tujuan` varchar(100) NOT NULL,
  `subject` varchar(100) NOT NULL,
  `pesan` text NOT NULL,
  `status_email` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_email_pw2019_log`
--

CREATE TABLE `t_email_pw2019_log` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `tujuan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `judul` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `isi` text COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_email_pw2019_log`
--

INSERT INTO `t_email_pw2019_log` (`id`, `tanggal`, `tujuan`, `judul`, `isi`) VALUES
(1, '2018-08-21 11:50:17', 'abrahamsyah@gmail.com', 'test email', 'hai {NAMA}, ini pesan email'),
(2, '2018-08-21 11:50:17', 'novrand@gmail.com', 'test email', 'hai {NAMA}, ini pesan email'),
(3, '2018-08-21 11:54:18', 'novrand@gmail.com', 'test', 'test'),
(4, '2018-08-21 11:56:55', 'abrahamsyah@gmail.com', 'test email', 'halo Abrahamsyah'),
(5, '2018-08-21 11:56:55', 'novrand@gmail.com', 'test email', 'halo Deddy');

-- --------------------------------------------------------

--
-- Table structure for table `t_entrun_peserta`
--

CREATE TABLE `t_entrun_peserta` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `pin` varchar(100) NOT NULL,
  `spin` varchar(100) NOT NULL,
  `hp` varchar(100) NOT NULL,
  `hp2` varchar(100) NOT NULL,
  `tgllahir` varchar(50) NOT NULL,
  `jenis_kelamin` int(11) NOT NULL,
  `golongan_darah` int(11) NOT NULL,
  `jersey` int(11) NOT NULL,
  `catatan` varchar(1000) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `payfee` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `np_requestOrder` varchar(1000) NOT NULL,
  `np_responseOrder` varchar(1000) NOT NULL,
  `np_requestOnePass` varchar(1000) NOT NULL,
  `np_responseOnePass` varchar(1000) NOT NULL,
  `np_txid` varchar(50) NOT NULL,
  `np_resultCode` varchar(20) NOT NULL,
  `np_resultMsg` varchar(50) NOT NULL,
  `np_resultText` varchar(1000) NOT NULL,
  `np_authNo` varchar(10) NOT NULL,
  `np_creditcard` varchar(50) NOT NULL,
  `np_expdate` varchar(10) NOT NULL,
  `np_bankcode` varchar(10) NOT NULL,
  `np_vanum` varchar(50) NOT NULL,
  `np_vavaliddate` varchar(50) NOT NULL,
  `np_vavalidtime` varchar(50) NOT NULL,
  `np_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_event`
--

CREATE TABLE `t_event` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `update_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `tahun` varchar(4) NOT NULL,
  `periode` varchar(5) NOT NULL,
  `wilayah_id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `status_event` int(11) NOT NULL DEFAULT 0 COMMENT '0 = not publish, 1 = publish, 2 = sudah selesai',
  `gambar` varchar(50) NOT NULL,
  `lokasi` varchar(100) NOT NULL,
  `lokasi_lengkap` varchar(500) NOT NULL,
  `tgl_mulai` datetime NOT NULL,
  `tgl_selesai` datetime NOT NULL,
  `flag_commingsoon` int(11) NOT NULL,
  `materi_event` text NOT NULL,
  `lok_lat` varchar(20) NOT NULL,
  `lok_long` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_event`
--

INSERT INTO `t_event` (`id`, `user_id`, `kategori_id`, `update_date`, `tahun`, `periode`, `wilayah_id`, `nama`, `slug`, `status_event`, `gambar`, `lokasi`, `lokasi_lengkap`, `tgl_mulai`, `tgl_selesai`, `flag_commingsoon`, `materi_event`, `lok_lat`, `lok_long`) VALUES
(1, 2, 1, '2019-04-01 08:08:45', '2017', '', 5, 'Pesta Wirausaha\n Batam 2017', 'batam2017', 2, 'logo-batam.jpg', 'Gedung Bank Indonesia - Batam Center', '', '2017-11-16 09:30:00', '2017-11-19 08:00:00', 0, '<p><b>Pesta Wirausaha Batam 2017 - Go Scale Up</b></p>  <p>Pesta Wirausaha Batam 2017 ini juga menyelenggarakan kegiatan utama yang tentunya sangat mendukung bisnis para wirausaha Batam diantaranya adalah:</p>  <p> 1. Expo/pameran/bazaar<br> 2. Seminar<br> 3. Klinik Bisnis<br> 4. Kelas Teknis<br> </p>  <p>Pesta Wirausaha Batam 2017 akan diselenggarakan pada tanggal 16 – 19 November 2017.</p>  <p>Kegiatan ini akan dipusatkan di dua tempat yaitu:</p>  <p>Seinar : Ruang Pertemuan Lt.3 Bank Indonesia, Batam Center.<br> Expo / Pameran : Lapangan Alun-Alun Engku Putri, Batam Center.</p>  <p>Adapun detail kegiatannya adalah:</p>  <p><b>SEMINAR WIRAUSAHA</b></p>  <p>Peningkatan kapasitas dan wawasan melalui pembahasan topik-topik yang berhubungan langsung dengan kebutuhan para pengusaha yang akan dibawakan langsung oleh para pembicara yang sudah terbukti sukses di bisnisnya masing-masing.</p>  <p><b>KELAS TEKNIS</b></p>  <p>Sebuah kelas kecil yang berisi materi teknis dalam menjalankan serta mengelola usaha yang dibawakan oleh praktisi bisnis di bidangnya.</p>  <p><b>KLINIK BISNIS</b></p>  <p>Ajang bertemunya peserta dengan mentor menurut jenis usahanya masing-masing dan belajar dari pengalaman para mentor secara privat.</p>  <p><b>EXPO</b></p>  <p>Ajang bagi para peserta pameran untuk mempromosikan produk-produknya, meningkatkan penjualan melalui transaksi retail dan peluang para peserta untuk mencari mitra kerjasama.</p>', '1.127776', '104.056701'),
(2, 2, 1, '2019-04-01 08:08:45', '2017', '', 1, 'Pesta Wirausaha Gresik 2017', 'gresik2017', 3, 'logo-gresik.jpg', 'Gelora Joko Samudro', '', '2017-12-16 09:30:00', '2017-12-17 12:00:00', 0, '', '', ''),
(3, 2, 1, '2019-07-10 07:34:07', '2017', '', 3, 'Bogor', 'bogor2017', 1, 'logo-pw.jpg', 'Bogor', '', '2017-12-22 10:00:00', '2017-12-23 10:00:00', 1, '', '', ''),
(4, 2, 2, '2019-07-10 07:43:54', '2018', '', 6, 'Kediri', 'kediri2017', 1, 'logo-pw.jpg', 'Kediri', '', '2017-12-16 10:00:00', '2017-12-17 11:00:00', 0, '', '', ''),
(5, 2, 2, '2019-07-10 07:34:02', '2018', '', 8, 'Padang', 'padang2018', 1, 'logo-pw.jpg', 'Padang', '', '2018-01-12 07:30:00', '2018-01-13 10:30:00', 1, '', '', ''),
(6, 2, 1, '2019-07-10 07:33:53', '2018', '', 11, 'Palu', 'palu2018', 1, 'logo-pw.jpg', 'Palu', '', '2018-01-19 08:00:00', '2018-01-20 09:00:00', 1, '', '', ''),
(7, 2, 1, '2019-07-10 07:33:55', '2018', '', 12, 'Solo', 'solo2018', 1, 'logo-pw.jpg', 'Solo', '', '2018-02-02 09:00:00', '2018-02-03 22:00:00', 1, '', '', ''),
(8, 2, 1, '2019-07-10 07:43:43', '2019', '', 1, 'Pesta Wirausaha Nasional 2019', '2019', 1, '', 'Ecovention Hall', 'Taman Impian Jaya Ancol', '2019-01-25 10:00:00', '2019-01-27 10:00:00', 1, '', '-6.127519', '106.837385'),
(9, 1, 2, '2019-07-10 07:33:26', '2019', '', 18, 'coba event admin 1', 'coba-event-admin-1', 1, 'coba-event-admin-14.png', 'Kantor Pusat PT. Swamedia Informatika', 'Jl. Sido Mulyo No.29, Sukaluyu, Cibeunying Kaler, Kota Bandung, Jawa Barat 40123', '2019-04-10 10:00:00', '2019-04-25 10:00:00', 0, '<div class=\"tg-box\">\r\n									<h2>Introduction:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur sint occaecat cupidatat non proident.</p>\r\n										<p>Sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Topics:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Ceserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>\r\n										<ul class=\"tg-liststyle\">\r\n											<li>Sed quia consequuntur magni dolores</li>\r\n											<li>Ratione voluptatem sequi nesciunt</li>\r\n											<li>Neque porro quisquam est</li>\r\n											<li>Qui dolorem ipsum quia dolor sit amet</li>\r\n											<li>Consectetur adipisci velit</li>\r\n											<li>Sed quia non numquam eius</li>\r\n										</ul>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Targeted Audience:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n										<ul class=\"tg-liststyle\">\r\n											<li>Sed quia consequuntur magni dolores</li>\r\n											<li>Ratione voluptatem sequi nesciunt</li>\r\n											<li>Neque porro quisquam est</li>\r\n											<li>Qui dolorem ipsum quia dolor sit amet</li>\r\n										</ul>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Conclusion:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur sint occaecat cupidatat non proident.</p>\r\n									</div>\r\n								</div>', '-6.8934444', '107.6328061'),
(11, 1, 2, '2019-07-10 07:33:29', '2019', '', 2, 'coba 1', 'coba-1', 1, 'coba-11.png', 'Bandung', 'jl. taman sari no.12', '2019-04-04 08:00:00', '2019-04-05 18:00:00', 0, '<div class=\"tg-box\">\r\n									<h2>Introduction:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur sint occaecat cupidatat non proident.</p>\r\n										<p>Sunt in culpa qui officia deserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Topics:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Ceserunt mollit anim id est laborum. Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit aspernatur aut odit aut fugit.</p>\r\n										<ul class=\"tg-liststyle\">\r\n											<li>Sed quia consequuntur magni dolores</li>\r\n											<li>Ratione voluptatem sequi nesciunt</li>\r\n											<li>Neque porro quisquam est</li>\r\n											<li>Qui dolorem ipsum quia dolor sit amet</li>\r\n											<li>Consectetur adipisci velit</li>\r\n											<li>Sed quia non numquam eius</li>\r\n										</ul>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Targeted Audience:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.</p>\r\n										<ul class=\"tg-liststyle\">\r\n											<li>Sed quia consequuntur magni dolores</li>\r\n											<li>Ratione voluptatem sequi nesciunt</li>\r\n											<li>Neque porro quisquam est</li>\r\n											<li>Qui dolorem ipsum quia dolor sit amet</li>\r\n										</ul>\r\n									</div>\r\n								</div>\r\n								<div class=\"tg-box\">\r\n									<h2>Conclusion:</h2>\r\n									<div class=\"tg-description\">\r\n										<p>Sed quia consequuntur magni dolores eos qui ratione voluptatem sequi nesciunt. Neque porro quisquam est, qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit, sed quia non numquam eius modi tempora incidunt ut labore et dolore magnam aliquam quaerat voluptatem.</p>\r\n										<p>Consectetur adipisicing elit sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam quisud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur sint occaecat cupidatat non proident.</p>\r\n									</div>\r\n								</div>', '12321', '234324');

-- --------------------------------------------------------

--
-- Table structure for table `t_event_carousel`
--

CREATE TABLE `t_event_carousel` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `gambar` varchar(100) NOT NULL,
  `alternate` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_event_carousel`
--

INSERT INTO `t_event_carousel` (`id`, `event_id`, `gambar`, `alternate`) VALUES
(1, 1, 'batam-1.jpg', ''),
(2, 1, 'batam-2.jpg', ''),
(3, 1, 'batam-3.jpg', ''),
(4, 1, 'batam-4.jpg', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_event_tiket`
--

CREATE TABLE `t_event_tiket` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `keterangan` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `harga` int(11) NOT NULL,
  `quota` int(11) NOT NULL,
  `terjual` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_kodebayar`
--

CREATE TABLE `t_expo_kodebayar` (
  `id` int(11) NOT NULL,
  `kode` int(11) NOT NULL,
  `status_kode` int(11) NOT NULL,
  `tanggal` datetime NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_expo_kodebayar`
--

INSERT INTO `t_expo_kodebayar` (`id`, `kode`, `status_kode`, `tanggal`) VALUES
(1, 813, 1, '2018-09-19 13:55:02'),
(2, 764, 1, '2018-09-19 14:43:23'),
(3, 100, 1, '2018-09-19 14:43:56'),
(4, 254, 1, '2018-09-19 14:46:32'),
(5, 434, 1, '2018-09-19 14:48:14'),
(6, 184, 1, '2018-09-19 14:50:22'),
(7, 7, 1, '2018-09-19 14:53:08'),
(8, 898, 1, '2018-09-20 13:12:57'),
(9, 382, 1, '2018-09-20 15:33:07'),
(10, 466, 1, '2018-09-20 16:16:31'),
(11, 449, 1, '2018-09-20 16:53:18'),
(12, 485, 1, '2018-09-20 17:19:36'),
(13, 561, 1, '2018-09-20 19:20:01'),
(14, 710, 1, '2018-09-20 19:20:19'),
(15, 779, 1, '2018-09-21 13:52:05'),
(16, 447, 1, '2018-09-21 14:30:42'),
(17, 86, 1, '2018-09-21 14:31:58'),
(18, 174, 1, '2018-09-21 19:50:55'),
(19, 659, 1, '2018-09-23 18:09:42'),
(20, 379, 1, '2018-09-23 19:32:05'),
(21, 58, 1, '2018-09-26 10:41:51'),
(22, 106, 1, '2018-10-08 18:54:49'),
(23, 168, 1, '2018-10-12 19:31:28'),
(24, 897, 1, '2018-10-15 11:19:54'),
(25, 130, 1, '2018-10-15 11:24:11'),
(26, 396, 1, '2018-10-19 15:09:16'),
(27, 736, 1, '2018-10-24 03:14:37'),
(28, 598, 1, '2018-10-25 01:20:21'),
(29, 515, 1, '2018-10-25 05:52:18'),
(30, 129, 1, '2018-10-25 15:18:45'),
(31, 580, 1, '2018-10-26 12:29:24'),
(32, 225, 1, '2018-10-28 18:58:43'),
(33, 104, 1, '2018-10-30 09:52:00'),
(34, 929, 1, '2018-10-31 13:04:47'),
(35, 517, 1, '2018-10-31 18:51:29'),
(36, 139, 1, '2018-11-02 15:28:58'),
(37, 295, 1, '2018-11-02 15:46:12'),
(38, 540, 1, '2018-11-05 17:45:34'),
(39, 282, 1, '2018-11-07 19:56:32'),
(40, 939, 1, '2018-11-08 15:02:59'),
(41, 279, 1, '2018-11-08 21:10:22'),
(42, 911, 1, '2018-11-08 23:59:30'),
(43, 959, 1, '2018-11-09 08:16:30'),
(44, 800, 1, '2018-11-09 10:07:23'),
(45, 308, 1, '2018-11-09 14:19:38'),
(46, 171, 1, '2018-11-12 17:26:01'),
(47, 509, 1, '2018-11-12 19:41:25'),
(48, 412, 1, '2018-11-24 15:42:47'),
(49, 880, 1, '2018-11-25 10:26:42'),
(50, 189, 1, '2018-11-26 09:00:37'),
(51, 698, 1, '2018-11-30 14:31:48'),
(52, 304, 1, '2018-11-30 17:52:43'),
(53, 395, 1, '2018-12-03 14:58:51'),
(54, 270, 1, '2018-12-03 19:28:39'),
(55, 443, 1, '2018-12-04 13:27:00'),
(56, 307, 1, '2018-12-04 13:28:14'),
(57, 814, 1, '2018-12-04 18:32:02'),
(58, 675, 1, '2018-12-04 18:32:05'),
(59, 546, 1, '2018-12-04 18:32:11'),
(60, 512, 1, '2018-12-04 19:33:13'),
(61, 583, 1, '2018-12-05 11:33:27'),
(62, 791, 1, '2018-12-05 13:08:46'),
(63, 510, 1, '2018-12-07 12:09:51'),
(64, 133, 1, '2018-12-07 12:11:11'),
(65, 715, 1, '2018-12-07 12:48:44'),
(66, 728, 1, '2018-12-10 14:13:42'),
(67, 795, 1, '2018-12-10 15:32:40'),
(68, 523, 1, '2018-12-11 08:09:58'),
(69, 63, 1, '2018-12-11 10:52:01'),
(70, 673, 1, '2018-12-12 05:52:43'),
(71, 72, 1, '2018-12-12 08:00:50'),
(72, 832, 1, '2018-12-12 12:39:02'),
(73, 252, 1, '2018-12-12 16:56:20'),
(74, 888, 1, '2018-12-12 21:43:01'),
(75, 754, 1, '2018-12-12 22:25:05'),
(76, 841, 1, '2018-12-13 11:43:29'),
(77, 251, 1, '2018-12-15 13:25:28'),
(78, 127, 1, '2018-12-15 13:27:39'),
(79, 223, 1, '2018-12-15 13:29:15'),
(80, 96, 1, '2018-12-15 13:30:49'),
(81, 916, 1, '2018-12-15 13:32:09'),
(82, 39, 1, '2018-12-15 13:44:19'),
(83, 439, 1, '2018-12-15 13:44:22'),
(84, 609, 1, '2018-12-15 16:44:57'),
(85, 457, 1, '2018-12-17 10:09:06'),
(86, 16, 1, '2018-12-18 16:16:27'),
(87, 103, 1, '2018-12-19 17:40:41'),
(88, 542, 1, '2018-12-19 18:33:27'),
(89, 368, 1, '2018-12-20 10:24:06'),
(90, 928, 1, '2018-12-20 11:18:06'),
(91, 976, 1, '2018-12-20 13:53:56'),
(92, 394, 1, '2018-12-21 06:50:19'),
(93, 686, 1, '2018-12-21 20:15:54'),
(94, 595, 1, '2018-12-22 13:12:11'),
(95, 458, 1, '2018-12-23 06:30:39'),
(96, 88, 1, '2018-12-23 15:20:22'),
(97, 548, 1, '2018-12-23 15:28:00'),
(98, 3, 1, '2018-12-23 17:53:33'),
(99, 652, 1, '2018-12-26 23:04:38'),
(100, 239, 1, '2018-12-27 06:13:23'),
(101, 947, 1, '2018-12-27 06:23:09'),
(102, 285, 1, '2018-12-27 07:20:31'),
(103, 935, 1, '2018-12-27 22:04:52'),
(104, 242, 1, '2018-12-28 06:27:34'),
(105, 638, 1, '2018-12-31 00:27:42'),
(106, 921, 1, '2018-12-31 19:31:12'),
(107, 78, 1, '2019-01-01 19:30:46'),
(108, 566, 1, '2019-01-02 07:12:13'),
(109, 573, 1, '2019-01-02 16:16:30'),
(110, 835, 1, '2019-01-02 17:50:32'),
(111, 966, 1, '2019-01-02 18:52:58'),
(112, 873, 1, '2019-01-03 15:06:32'),
(113, 477, 1, '2019-01-03 19:35:32'),
(114, 153, 1, '2019-01-03 21:56:57'),
(115, 614, 1, '2019-01-04 11:02:43'),
(116, 876, 1, '2019-01-04 17:32:26'),
(117, 643, 1, '2019-01-04 18:25:23'),
(118, 190, 1, '2019-01-04 23:32:22'),
(119, 705, 1, '2019-01-05 01:12:32'),
(120, 771, 1, '2019-01-07 00:26:37'),
(121, 181, 1, '2019-01-07 01:28:52'),
(122, 556, 1, '2019-01-07 10:59:59'),
(123, 703, 1, '2019-01-09 16:36:57'),
(124, 480, 1, '2019-01-09 16:39:39'),
(125, 402, 1, '2019-01-09 18:05:24'),
(126, 314, 1, '2019-01-09 18:08:09'),
(127, 796, 1, '2019-01-09 19:52:40'),
(128, 378, 1, '2019-01-09 20:02:47'),
(129, 235, 1, '2019-01-09 22:20:40'),
(130, 158, 1, '2019-01-10 12:42:33'),
(131, 837, 1, '2019-01-10 17:21:56'),
(132, 526, 1, '2019-01-10 19:43:09'),
(133, 177, 1, '2019-01-11 14:57:15'),
(134, 662, 1, '2019-01-11 19:42:17'),
(135, 74, 1, '2019-01-12 12:50:20'),
(136, 803, 1, '2019-01-13 13:05:40'),
(137, 676, 1, '2019-01-13 22:29:37'),
(138, 907, 1, '2019-01-14 12:47:46'),
(139, 707, 1, '2019-01-14 15:26:51'),
(140, 456, 1, '2019-01-14 15:51:12'),
(141, 29, 1, '2019-01-14 19:09:29'),
(142, 255, 1, '2019-01-14 19:56:20'),
(143, 132, 1, '2019-01-14 23:35:52'),
(144, 979, 1, '2019-01-15 13:01:52'),
(145, 162, 1, '2019-01-15 14:05:39'),
(146, 113, 1, '2019-01-15 14:09:53'),
(147, 547, 1, '2019-01-15 14:51:38'),
(148, 315, 1, '2019-01-15 20:36:40'),
(149, 775, 1, '2019-01-16 10:35:33'),
(150, 768, 1, '2019-01-16 10:37:53'),
(151, 549, 1, '2019-01-16 10:39:51'),
(152, 753, 1, '2019-01-16 10:43:15'),
(153, 722, 1, '2019-01-16 10:46:00'),
(154, 968, 1, '2019-01-16 10:48:50'),
(155, 961, 1, '2019-01-16 10:52:12'),
(156, 383, 1, '2019-01-16 11:10:20'),
(157, 648, 1, '2019-01-16 12:12:15'),
(158, 927, 1, '2019-01-16 12:16:46'),
(159, 604, 1, '2019-01-16 12:48:42'),
(160, 428, 1, '2019-01-16 13:23:03'),
(161, 975, 1, '2019-01-16 13:27:09'),
(162, 59, 1, '2019-01-16 15:32:03'),
(163, 150, 1, '2019-01-16 15:35:46'),
(164, 950, 1, '2019-01-16 15:54:57'),
(165, 208, 1, '2019-01-16 16:29:42'),
(166, 855, 1, '2019-01-17 10:51:28'),
(167, 357, 1, '2019-01-17 11:11:37'),
(168, 692, 1, '2019-01-17 12:54:58'),
(169, 115, 1, '2019-01-17 14:13:39'),
(170, 197, 1, '2019-01-17 17:54:22'),
(171, 712, 1, '2019-01-18 14:16:52'),
(172, 729, 1, '2019-01-19 08:05:09'),
(173, 265, 1, '2019-01-19 15:07:19'),
(174, 25, 1, '2019-01-19 21:30:25'),
(175, 117, 1, '2019-01-20 10:34:11'),
(176, 499, 1, '2019-01-21 11:12:14'),
(177, 256, 1, '2019-01-21 12:42:24'),
(178, 780, 1, '2019-01-21 17:08:25'),
(179, 134, 1, '2019-01-21 17:09:16'),
(180, 120, 1, '2019-01-21 18:07:34'),
(181, 27, 1, '2019-01-21 18:16:43'),
(182, 403, 1, '2019-01-21 18:32:56'),
(183, 562, 1, '2019-01-22 09:58:45'),
(184, 237, 1, '2019-01-22 10:36:13'),
(185, 300, 1, '2019-01-23 11:44:08'),
(186, 815, 1, '2019-01-23 11:44:50'),
(187, 475, 1, '2019-01-23 15:34:04'),
(188, 99, 1, '2019-01-23 17:50:55'),
(189, 896, 1, '2019-01-24 07:22:55'),
(190, 207, 1, '2019-01-24 07:27:11'),
(191, 468, 1, '2019-01-24 07:28:26'),
(192, 68, 1, '2019-01-24 07:30:52'),
(193, 840, 1, '2019-01-24 07:36:20'),
(194, 850, 1, '2019-01-24 18:59:19'),
(195, 33, 1, '2019-01-25 06:55:42'),
(196, 905, 1, '2019-01-25 12:32:10'),
(197, 899, 0, '0000-00-00 00:00:00'),
(198, 92, 0, '0000-00-00 00:00:00'),
(199, 105, 0, '0000-00-00 00:00:00'),
(200, 455, 0, '0000-00-00 00:00:00'),
(201, 199, 0, '0000-00-00 00:00:00'),
(202, 551, 0, '0000-00-00 00:00:00'),
(203, 962, 0, '0000-00-00 00:00:00'),
(204, 388, 0, '0000-00-00 00:00:00'),
(205, 525, 0, '0000-00-00 00:00:00'),
(206, 495, 0, '0000-00-00 00:00:00'),
(207, 411, 0, '0000-00-00 00:00:00'),
(208, 808, 0, '0000-00-00 00:00:00'),
(209, 341, 0, '0000-00-00 00:00:00'),
(210, 259, 0, '0000-00-00 00:00:00'),
(211, 258, 0, '0000-00-00 00:00:00'),
(212, 894, 0, '0000-00-00 00:00:00'),
(213, 964, 0, '0000-00-00 00:00:00'),
(214, 318, 0, '0000-00-00 00:00:00'),
(215, 175, 0, '0000-00-00 00:00:00'),
(216, 922, 0, '0000-00-00 00:00:00'),
(217, 385, 0, '0000-00-00 00:00:00'),
(218, 211, 0, '0000-00-00 00:00:00'),
(219, 453, 0, '0000-00-00 00:00:00'),
(220, 565, 0, '0000-00-00 00:00:00'),
(221, 723, 0, '0000-00-00 00:00:00'),
(222, 487, 0, '0000-00-00 00:00:00'),
(223, 872, 0, '0000-00-00 00:00:00'),
(224, 291, 0, '0000-00-00 00:00:00'),
(225, 241, 0, '0000-00-00 00:00:00'),
(226, 594, 0, '0000-00-00 00:00:00'),
(227, 987, 0, '0000-00-00 00:00:00'),
(228, 391, 0, '0000-00-00 00:00:00'),
(229, 26, 0, '0000-00-00 00:00:00'),
(230, 425, 0, '0000-00-00 00:00:00'),
(231, 135, 0, '0000-00-00 00:00:00'),
(232, 220, 0, '0000-00-00 00:00:00'),
(233, 529, 0, '0000-00-00 00:00:00'),
(234, 34, 0, '0000-00-00 00:00:00'),
(235, 410, 0, '0000-00-00 00:00:00'),
(236, 717, 0, '0000-00-00 00:00:00'),
(237, 274, 0, '0000-00-00 00:00:00'),
(238, 206, 0, '0000-00-00 00:00:00'),
(239, 797, 0, '0000-00-00 00:00:00'),
(240, 992, 0, '0000-00-00 00:00:00'),
(241, 960, 0, '0000-00-00 00:00:00'),
(242, 13, 0, '0000-00-00 00:00:00'),
(243, 196, 0, '0000-00-00 00:00:00'),
(244, 55, 0, '0000-00-00 00:00:00'),
(245, 879, 0, '0000-00-00 00:00:00'),
(246, 309, 0, '0000-00-00 00:00:00'),
(247, 292, 0, '0000-00-00 00:00:00'),
(248, 128, 0, '0000-00-00 00:00:00'),
(249, 193, 0, '0000-00-00 00:00:00'),
(250, 630, 0, '0000-00-00 00:00:00'),
(251, 114, 0, '0000-00-00 00:00:00'),
(252, 152, 0, '0000-00-00 00:00:00'),
(253, 463, 0, '0000-00-00 00:00:00'),
(254, 268, 0, '0000-00-00 00:00:00'),
(255, 488, 0, '0000-00-00 00:00:00'),
(256, 903, 0, '0000-00-00 00:00:00'),
(257, 658, 0, '0000-00-00 00:00:00'),
(258, 328, 0, '0000-00-00 00:00:00'),
(259, 75, 0, '0000-00-00 00:00:00'),
(260, 148, 0, '0000-00-00 00:00:00'),
(261, 21, 0, '0000-00-00 00:00:00'),
(262, 730, 0, '0000-00-00 00:00:00'),
(263, 861, 0, '0000-00-00 00:00:00'),
(264, 932, 0, '0000-00-00 00:00:00'),
(265, 697, 0, '0000-00-00 00:00:00'),
(266, 195, 0, '0000-00-00 00:00:00'),
(267, 301, 0, '0000-00-00 00:00:00'),
(268, 912, 0, '0000-00-00 00:00:00'),
(269, 599, 0, '0000-00-00 00:00:00'),
(270, 9, 0, '0000-00-00 00:00:00'),
(271, 76, 0, '0000-00-00 00:00:00'),
(272, 321, 0, '0000-00-00 00:00:00'),
(273, 591, 0, '0000-00-00 00:00:00'),
(274, 783, 0, '0000-00-00 00:00:00'),
(275, 46, 0, '0000-00-00 00:00:00'),
(276, 66, 0, '0000-00-00 00:00:00'),
(277, 969, 0, '0000-00-00 00:00:00'),
(278, 404, 0, '0000-00-00 00:00:00'),
(279, 492, 0, '0000-00-00 00:00:00'),
(280, 418, 0, '0000-00-00 00:00:00'),
(281, 110, 0, '0000-00-00 00:00:00'),
(282, 842, 0, '0000-00-00 00:00:00'),
(283, 826, 0, '0000-00-00 00:00:00'),
(284, 345, 0, '0000-00-00 00:00:00'),
(285, 311, 0, '0000-00-00 00:00:00'),
(286, 178, 0, '0000-00-00 00:00:00'),
(287, 769, 0, '0000-00-00 00:00:00'),
(288, 343, 0, '0000-00-00 00:00:00'),
(289, 553, 0, '0000-00-00 00:00:00'),
(290, 250, 0, '0000-00-00 00:00:00'),
(291, 462, 0, '0000-00-00 00:00:00'),
(292, 363, 0, '0000-00-00 00:00:00'),
(293, 653, 0, '0000-00-00 00:00:00'),
(294, 592, 0, '0000-00-00 00:00:00'),
(295, 586, 0, '0000-00-00 00:00:00'),
(296, 650, 0, '0000-00-00 00:00:00'),
(297, 297, 0, '0000-00-00 00:00:00'),
(298, 271, 0, '0000-00-00 00:00:00'),
(299, 569, 0, '0000-00-00 00:00:00'),
(300, 567, 0, '0000-00-00 00:00:00'),
(301, 94, 0, '0000-00-00 00:00:00'),
(302, 895, 0, '0000-00-00 00:00:00'),
(303, 494, 0, '0000-00-00 00:00:00'),
(304, 602, 0, '0000-00-00 00:00:00'),
(305, 844, 0, '0000-00-00 00:00:00'),
(306, 15, 0, '0000-00-00 00:00:00'),
(307, 380, 0, '0000-00-00 00:00:00'),
(308, 384, 0, '0000-00-00 00:00:00'),
(309, 176, 0, '0000-00-00 00:00:00'),
(310, 810, 0, '0000-00-00 00:00:00'),
(311, 98, 0, '0000-00-00 00:00:00'),
(312, 476, 0, '0000-00-00 00:00:00'),
(313, 409, 0, '0000-00-00 00:00:00'),
(314, 949, 0, '0000-00-00 00:00:00'),
(315, 124, 0, '0000-00-00 00:00:00'),
(316, 204, 0, '0000-00-00 00:00:00'),
(317, 883, 0, '0000-00-00 00:00:00'),
(318, 306, 0, '0000-00-00 00:00:00'),
(319, 823, 0, '0000-00-00 00:00:00'),
(320, 555, 0, '0000-00-00 00:00:00'),
(321, 164, 0, '0000-00-00 00:00:00'),
(322, 185, 0, '0000-00-00 00:00:00'),
(323, 40, 0, '0000-00-00 00:00:00'),
(324, 532, 0, '0000-00-00 00:00:00'),
(325, 706, 0, '0000-00-00 00:00:00'),
(326, 210, 0, '0000-00-00 00:00:00'),
(327, 889, 0, '0000-00-00 00:00:00'),
(328, 424, 0, '0000-00-00 00:00:00'),
(329, 581, 0, '0000-00-00 00:00:00'),
(330, 461, 0, '0000-00-00 00:00:00'),
(331, 559, 0, '0000-00-00 00:00:00'),
(332, 83, 0, '0000-00-00 00:00:00'),
(333, 534, 0, '0000-00-00 00:00:00'),
(334, 858, 0, '0000-00-00 00:00:00'),
(335, 882, 0, '0000-00-00 00:00:00'),
(336, 508, 0, '0000-00-00 00:00:00'),
(337, 576, 0, '0000-00-00 00:00:00'),
(338, 776, 0, '0000-00-00 00:00:00'),
(339, 514, 0, '0000-00-00 00:00:00'),
(340, 322, 0, '0000-00-00 00:00:00'),
(341, 49, 0, '0000-00-00 00:00:00'),
(342, 361, 0, '0000-00-00 00:00:00'),
(343, 504, 0, '0000-00-00 00:00:00'),
(344, 585, 0, '0000-00-00 00:00:00'),
(345, 165, 0, '0000-00-00 00:00:00'),
(346, 672, 0, '0000-00-00 00:00:00'),
(347, 635, 0, '0000-00-00 00:00:00'),
(348, 107, 0, '0000-00-00 00:00:00'),
(349, 951, 0, '0000-00-00 00:00:00'),
(350, 332, 0, '0000-00-00 00:00:00'),
(351, 851, 0, '0000-00-00 00:00:00'),
(352, 294, 0, '0000-00-00 00:00:00'),
(353, 918, 0, '0000-00-00 00:00:00'),
(354, 838, 0, '0000-00-00 00:00:00'),
(355, 413, 0, '0000-00-00 00:00:00'),
(356, 726, 0, '0000-00-00 00:00:00'),
(357, 161, 0, '0000-00-00 00:00:00'),
(358, 572, 0, '0000-00-00 00:00:00'),
(359, 917, 0, '0000-00-00 00:00:00'),
(360, 375, 0, '0000-00-00 00:00:00'),
(361, 651, 0, '0000-00-00 00:00:00'),
(362, 329, 0, '0000-00-00 00:00:00'),
(363, 448, 0, '0000-00-00 00:00:00'),
(364, 867, 0, '0000-00-00 00:00:00'),
(365, 737, 0, '0000-00-00 00:00:00'),
(366, 136, 0, '0000-00-00 00:00:00'),
(367, 640, 0, '0000-00-00 00:00:00'),
(368, 179, 0, '0000-00-00 00:00:00'),
(369, 102, 0, '0000-00-00 00:00:00'),
(370, 247, 0, '0000-00-00 00:00:00'),
(371, 280, 0, '0000-00-00 00:00:00'),
(372, 218, 0, '0000-00-00 00:00:00'),
(373, 743, 0, '0000-00-00 00:00:00'),
(374, 24, 0, '0000-00-00 00:00:00'),
(375, 656, 0, '0000-00-00 00:00:00'),
(376, 277, 0, '0000-00-00 00:00:00'),
(377, 445, 0, '0000-00-00 00:00:00'),
(378, 351, 0, '0000-00-00 00:00:00'),
(379, 537, 0, '0000-00-00 00:00:00'),
(380, 937, 0, '0000-00-00 00:00:00'),
(381, 323, 0, '0000-00-00 00:00:00'),
(382, 420, 0, '0000-00-00 00:00:00'),
(383, 974, 0, '0000-00-00 00:00:00'),
(384, 236, 0, '0000-00-00 00:00:00'),
(385, 685, 0, '0000-00-00 00:00:00'),
(386, 965, 0, '0000-00-00 00:00:00'),
(387, 806, 0, '0000-00-00 00:00:00'),
(388, 944, 0, '0000-00-00 00:00:00'),
(389, 173, 0, '0000-00-00 00:00:00'),
(390, 627, 0, '0000-00-00 00:00:00'),
(391, 245, 0, '0000-00-00 00:00:00'),
(392, 287, 0, '0000-00-00 00:00:00'),
(393, 884, 0, '0000-00-00 00:00:00'),
(394, 618, 0, '0000-00-00 00:00:00'),
(395, 574, 0, '0000-00-00 00:00:00'),
(396, 84, 0, '0000-00-00 00:00:00'),
(397, 908, 0, '0000-00-00 00:00:00'),
(398, 312, 0, '0000-00-00 00:00:00'),
(399, 714, 0, '0000-00-00 00:00:00'),
(400, 539, 0, '0000-00-00 00:00:00'),
(401, 554, 0, '0000-00-00 00:00:00'),
(402, 278, 0, '0000-00-00 00:00:00'),
(403, 138, 0, '0000-00-00 00:00:00'),
(404, 313, 0, '0000-00-00 00:00:00'),
(405, 122, 0, '0000-00-00 00:00:00'),
(406, 348, 0, '0000-00-00 00:00:00'),
(407, 48, 0, '0000-00-00 00:00:00'),
(408, 747, 0, '0000-00-00 00:00:00'),
(409, 846, 0, '0000-00-00 00:00:00'),
(410, 214, 0, '0000-00-00 00:00:00'),
(411, 552, 0, '0000-00-00 00:00:00'),
(412, 619, 0, '0000-00-00 00:00:00'),
(413, 828, 0, '0000-00-00 00:00:00'),
(414, 996, 0, '0000-00-00 00:00:00'),
(415, 511, 0, '0000-00-00 00:00:00'),
(416, 625, 0, '0000-00-00 00:00:00'),
(417, 269, 0, '0000-00-00 00:00:00'),
(418, 166, 0, '0000-00-00 00:00:00'),
(419, 53, 0, '0000-00-00 00:00:00'),
(420, 349, 0, '0000-00-00 00:00:00'),
(421, 358, 0, '0000-00-00 00:00:00'),
(422, 338, 0, '0000-00-00 00:00:00'),
(423, 478, 0, '0000-00-00 00:00:00'),
(424, 843, 0, '0000-00-00 00:00:00'),
(425, 376, 0, '0000-00-00 00:00:00'),
(426, 756, 0, '0000-00-00 00:00:00'),
(427, 829, 0, '0000-00-00 00:00:00'),
(428, 217, 0, '0000-00-00 00:00:00'),
(429, 372, 0, '0000-00-00 00:00:00'),
(430, 261, 0, '0000-00-00 00:00:00'),
(431, 119, 0, '0000-00-00 00:00:00'),
(432, 377, 0, '0000-00-00 00:00:00'),
(433, 296, 0, '0000-00-00 00:00:00'),
(434, 982, 0, '0000-00-00 00:00:00'),
(435, 347, 0, '0000-00-00 00:00:00'),
(436, 141, 0, '0000-00-00 00:00:00'),
(437, 901, 0, '0000-00-00 00:00:00'),
(438, 688, 0, '0000-00-00 00:00:00'),
(439, 724, 0, '0000-00-00 00:00:00'),
(440, 701, 0, '0000-00-00 00:00:00'),
(441, 77, 0, '0000-00-00 00:00:00'),
(442, 10, 0, '0000-00-00 00:00:00'),
(443, 757, 0, '0000-00-00 00:00:00'),
(444, 200, 0, '0000-00-00 00:00:00'),
(445, 821, 0, '0000-00-00 00:00:00'),
(446, 232, 0, '0000-00-00 00:00:00'),
(447, 984, 0, '0000-00-00 00:00:00'),
(448, 702, 0, '0000-00-00 00:00:00'),
(449, 126, 0, '0000-00-00 00:00:00'),
(450, 839, 0, '0000-00-00 00:00:00'),
(451, 421, 0, '0000-00-00 00:00:00'),
(452, 809, 0, '0000-00-00 00:00:00'),
(453, 392, 0, '0000-00-00 00:00:00'),
(454, 221, 0, '0000-00-00 00:00:00'),
(455, 626, 0, '0000-00-00 00:00:00'),
(456, 303, 0, '0000-00-00 00:00:00'),
(457, 144, 0, '0000-00-00 00:00:00'),
(458, 680, 0, '0000-00-00 00:00:00'),
(459, 904, 0, '0000-00-00 00:00:00'),
(460, 700, 0, '0000-00-00 00:00:00'),
(461, 431, 0, '0000-00-00 00:00:00'),
(462, 956, 0, '0000-00-00 00:00:00'),
(463, 334, 0, '0000-00-00 00:00:00'),
(464, 57, 0, '0000-00-00 00:00:00'),
(465, 470, 0, '0000-00-00 00:00:00'),
(466, 862, 0, '0000-00-00 00:00:00'),
(467, 60, 0, '0000-00-00 00:00:00'),
(468, 91, 0, '0000-00-00 00:00:00'),
(469, 398, 0, '0000-00-00 00:00:00'),
(470, 999, 0, '0000-00-00 00:00:00'),
(471, 273, 0, '0000-00-00 00:00:00'),
(472, 45, 0, '0000-00-00 00:00:00'),
(473, 257, 0, '0000-00-00 00:00:00'),
(474, 505, 0, '0000-00-00 00:00:00'),
(475, 770, 0, '0000-00-00 00:00:00'),
(476, 812, 0, '0000-00-00 00:00:00'),
(477, 54, 0, '0000-00-00 00:00:00'),
(478, 201, 0, '0000-00-00 00:00:00'),
(479, 444, 0, '0000-00-00 00:00:00'),
(480, 172, 0, '0000-00-00 00:00:00'),
(481, 408, 0, '0000-00-00 00:00:00'),
(482, 834, 0, '0000-00-00 00:00:00'),
(483, 877, 0, '0000-00-00 00:00:00'),
(484, 910, 0, '0000-00-00 00:00:00'),
(485, 360, 0, '0000-00-00 00:00:00'),
(486, 568, 0, '0000-00-00 00:00:00'),
(487, 755, 0, '0000-00-00 00:00:00'),
(488, 4, 0, '0000-00-00 00:00:00'),
(489, 320, 0, '0000-00-00 00:00:00'),
(490, 909, 0, '0000-00-00 00:00:00'),
(491, 789, 0, '0000-00-00 00:00:00'),
(492, 446, 0, '0000-00-00 00:00:00'),
(493, 438, 0, '0000-00-00 00:00:00'),
(494, 682, 0, '0000-00-00 00:00:00'),
(495, 786, 0, '0000-00-00 00:00:00'),
(496, 400, 0, '0000-00-00 00:00:00'),
(497, 2, 0, '0000-00-00 00:00:00'),
(498, 531, 0, '0000-00-00 00:00:00'),
(499, 988, 0, '0000-00-00 00:00:00'),
(500, 513, 0, '0000-00-00 00:00:00'),
(501, 212, 0, '0000-00-00 00:00:00'),
(502, 56, 0, '0000-00-00 00:00:00'),
(503, 741, 0, '0000-00-00 00:00:00'),
(504, 182, 0, '0000-00-00 00:00:00'),
(505, 156, 0, '0000-00-00 00:00:00'),
(506, 528, 0, '0000-00-00 00:00:00'),
(507, 14, 0, '0000-00-00 00:00:00'),
(508, 393, 0, '0000-00-00 00:00:00'),
(509, 600, 0, '0000-00-00 00:00:00'),
(510, 186, 0, '0000-00-00 00:00:00'),
(511, 464, 0, '0000-00-00 00:00:00'),
(512, 125, 0, '0000-00-00 00:00:00'),
(513, 671, 0, '0000-00-00 00:00:00'),
(514, 95, 0, '0000-00-00 00:00:00'),
(515, 550, 0, '0000-00-00 00:00:00'),
(516, 719, 0, '0000-00-00 00:00:00'),
(517, 486, 0, '0000-00-00 00:00:00'),
(518, 665, 0, '0000-00-00 00:00:00'),
(519, 874, 0, '0000-00-00 00:00:00'),
(520, 816, 0, '0000-00-00 00:00:00'),
(521, 362, 0, '0000-00-00 00:00:00'),
(522, 541, 0, '0000-00-00 00:00:00'),
(523, 953, 0, '0000-00-00 00:00:00'),
(524, 31, 0, '0000-00-00 00:00:00'),
(525, 822, 0, '0000-00-00 00:00:00'),
(526, 496, 0, '0000-00-00 00:00:00'),
(527, 557, 0, '0000-00-00 00:00:00'),
(528, 683, 0, '0000-00-00 00:00:00'),
(529, 467, 0, '0000-00-00 00:00:00'),
(530, 381, 0, '0000-00-00 00:00:00'),
(531, 246, 0, '0000-00-00 00:00:00'),
(532, 38, 0, '0000-00-00 00:00:00'),
(533, 718, 0, '0000-00-00 00:00:00'),
(534, 18, 0, '0000-00-00 00:00:00'),
(535, 353, 0, '0000-00-00 00:00:00'),
(536, 645, 0, '0000-00-00 00:00:00'),
(537, 998, 0, '0000-00-00 00:00:00'),
(538, 830, 0, '0000-00-00 00:00:00'),
(539, 869, 0, '0000-00-00 00:00:00'),
(540, 442, 0, '0000-00-00 00:00:00'),
(541, 690, 0, '0000-00-00 00:00:00'),
(542, 661, 0, '0000-00-00 00:00:00'),
(543, 848, 0, '0000-00-00 00:00:00'),
(544, 340, 0, '0000-00-00 00:00:00'),
(545, 793, 0, '0000-00-00 00:00:00'),
(546, 774, 0, '0000-00-00 00:00:00'),
(547, 615, 0, '0000-00-00 00:00:00'),
(548, 750, 0, '0000-00-00 00:00:00'),
(549, 601, 0, '0000-00-00 00:00:00'),
(550, 405, 0, '0000-00-00 00:00:00'),
(551, 264, 0, '0000-00-00 00:00:00'),
(552, 490, 0, '0000-00-00 00:00:00'),
(553, 634, 0, '0000-00-00 00:00:00'),
(554, 642, 0, '0000-00-00 00:00:00'),
(555, 97, 0, '0000-00-00 00:00:00'),
(556, 958, 0, '0000-00-00 00:00:00'),
(557, 422, 0, '0000-00-00 00:00:00'),
(558, 342, 0, '0000-00-00 00:00:00'),
(559, 773, 0, '0000-00-00 00:00:00'),
(560, 5, 0, '0000-00-00 00:00:00'),
(561, 344, 0, '0000-00-00 00:00:00'),
(562, 180, 0, '0000-00-00 00:00:00'),
(563, 763, 0, '0000-00-00 00:00:00'),
(564, 275, 0, '0000-00-00 00:00:00'),
(565, 993, 0, '0000-00-00 00:00:00'),
(566, 820, 0, '0000-00-00 00:00:00'),
(567, 731, 0, '0000-00-00 00:00:00'),
(568, 641, 0, '0000-00-00 00:00:00'),
(569, 137, 0, '0000-00-00 00:00:00'),
(570, 971, 0, '0000-00-00 00:00:00'),
(571, 613, 0, '0000-00-00 00:00:00'),
(572, 942, 0, '0000-00-00 00:00:00'),
(573, 253, 0, '0000-00-00 00:00:00'),
(574, 415, 0, '0000-00-00 00:00:00'),
(575, 727, 0, '0000-00-00 00:00:00'),
(576, 451, 0, '0000-00-00 00:00:00'),
(577, 536, 0, '0000-00-00 00:00:00'),
(578, 977, 0, '0000-00-00 00:00:00'),
(579, 684, 0, '0000-00-00 00:00:00'),
(580, 121, 0, '0000-00-00 00:00:00'),
(581, 664, 0, '0000-00-00 00:00:00'),
(582, 584, 0, '0000-00-00 00:00:00'),
(583, 230, 0, '0000-00-00 00:00:00'),
(584, 740, 0, '0000-00-00 00:00:00'),
(585, 123, 0, '0000-00-00 00:00:00'),
(586, 989, 0, '0000-00-00 00:00:00'),
(587, 940, 0, '0000-00-00 00:00:00'),
(588, 154, 0, '0000-00-00 00:00:00'),
(589, 226, 0, '0000-00-00 00:00:00'),
(590, 596, 0, '0000-00-00 00:00:00'),
(591, 628, 0, '0000-00-00 00:00:00'),
(592, 213, 0, '0000-00-00 00:00:00'),
(593, 116, 0, '0000-00-00 00:00:00'),
(594, 355, 0, '0000-00-00 00:00:00'),
(595, 759, 0, '0000-00-00 00:00:00'),
(596, 915, 0, '0000-00-00 00:00:00'),
(597, 699, 0, '0000-00-00 00:00:00'),
(598, 44, 0, '0000-00-00 00:00:00'),
(599, 111, 0, '0000-00-00 00:00:00'),
(600, 593, 0, '0000-00-00 00:00:00'),
(601, 365, 0, '0000-00-00 00:00:00'),
(602, 914, 0, '0000-00-00 00:00:00'),
(603, 794, 0, '0000-00-00 00:00:00'),
(604, 588, 0, '0000-00-00 00:00:00'),
(605, 151, 0, '0000-00-00 00:00:00'),
(606, 471, 0, '0000-00-00 00:00:00'),
(607, 163, 0, '0000-00-00 00:00:00'),
(608, 654, 0, '0000-00-00 00:00:00'),
(609, 216, 0, '0000-00-00 00:00:00'),
(610, 346, 0, '0000-00-00 00:00:00'),
(611, 369, 0, '0000-00-00 00:00:00'),
(612, 970, 0, '0000-00-00 00:00:00'),
(613, 818, 0, '0000-00-00 00:00:00'),
(614, 991, 0, '0000-00-00 00:00:00'),
(615, 799, 0, '0000-00-00 00:00:00'),
(616, 118, 0, '0000-00-00 00:00:00'),
(617, 926, 0, '0000-00-00 00:00:00'),
(618, 644, 0, '0000-00-00 00:00:00'),
(619, 397, 0, '0000-00-00 00:00:00'),
(620, 65, 0, '0000-00-00 00:00:00'),
(621, 460, 0, '0000-00-00 00:00:00'),
(622, 169, 0, '0000-00-00 00:00:00'),
(623, 560, 0, '0000-00-00 00:00:00'),
(624, 778, 0, '0000-00-00 00:00:00'),
(625, 101, 0, '0000-00-00 00:00:00'),
(626, 936, 0, '0000-00-00 00:00:00'),
(627, 305, 0, '0000-00-00 00:00:00'),
(628, 713, 0, '0000-00-00 00:00:00'),
(629, 955, 0, '0000-00-00 00:00:00'),
(630, 249, 0, '0000-00-00 00:00:00'),
(631, 354, 0, '0000-00-00 00:00:00'),
(632, 781, 0, '0000-00-00 00:00:00'),
(633, 831, 0, '0000-00-00 00:00:00'),
(634, 503, 0, '0000-00-00 00:00:00'),
(635, 23, 0, '0000-00-00 00:00:00'),
(636, 900, 0, '0000-00-00 00:00:00'),
(637, 777, 0, '0000-00-00 00:00:00'),
(638, 17, 0, '0000-00-00 00:00:00'),
(639, 666, 0, '0000-00-00 00:00:00'),
(640, 194, 0, '0000-00-00 00:00:00'),
(641, 219, 0, '0000-00-00 00:00:00'),
(642, 352, 0, '0000-00-00 00:00:00'),
(643, 290, 0, '0000-00-00 00:00:00'),
(644, 240, 0, '0000-00-00 00:00:00'),
(645, 836, 0, '0000-00-00 00:00:00'),
(646, 19, 0, '0000-00-00 00:00:00'),
(647, 42, 0, '0000-00-00 00:00:00'),
(648, 864, 0, '0000-00-00 00:00:00'),
(649, 544, 0, '0000-00-00 00:00:00'),
(650, 582, 0, '0000-00-00 00:00:00'),
(651, 335, 0, '0000-00-00 00:00:00'),
(652, 885, 0, '0000-00-00 00:00:00'),
(653, 80, 0, '0000-00-00 00:00:00'),
(654, 902, 0, '0000-00-00 00:00:00'),
(655, 784, 0, '0000-00-00 00:00:00'),
(656, 61, 0, '0000-00-00 00:00:00'),
(657, 423, 0, '0000-00-00 00:00:00'),
(658, 82, 0, '0000-00-00 00:00:00'),
(659, 64, 0, '0000-00-00 00:00:00'),
(660, 997, 0, '0000-00-00 00:00:00'),
(661, 330, 0, '0000-00-00 00:00:00'),
(662, 857, 0, '0000-00-00 00:00:00'),
(663, 527, 0, '0000-00-00 00:00:00'),
(664, 995, 0, '0000-00-00 00:00:00'),
(665, 767, 0, '0000-00-00 00:00:00'),
(666, 41, 0, '0000-00-00 00:00:00'),
(667, 827, 0, '0000-00-00 00:00:00'),
(668, 262, 0, '0000-00-00 00:00:00'),
(669, 981, 0, '0000-00-00 00:00:00'),
(670, 205, 0, '0000-00-00 00:00:00'),
(671, 860, 0, '0000-00-00 00:00:00'),
(672, 11, 0, '0000-00-00 00:00:00'),
(673, 288, 0, '0000-00-00 00:00:00'),
(674, 399, 0, '0000-00-00 00:00:00'),
(675, 946, 0, '0000-00-00 00:00:00'),
(676, 52, 0, '0000-00-00 00:00:00'),
(677, 497, 0, '0000-00-00 00:00:00'),
(678, 36, 0, '0000-00-00 00:00:00'),
(679, 721, 0, '0000-00-00 00:00:00'),
(680, 482, 0, '0000-00-00 00:00:00'),
(681, 590, 0, '0000-00-00 00:00:00'),
(682, 660, 0, '0000-00-00 00:00:00'),
(683, 202, 0, '0000-00-00 00:00:00'),
(684, 963, 0, '0000-00-00 00:00:00'),
(685, 281, 0, '0000-00-00 00:00:00'),
(686, 222, 0, '0000-00-00 00:00:00'),
(687, 633, 0, '0000-00-00 00:00:00'),
(688, 734, 0, '0000-00-00 00:00:00'),
(689, 69, 0, '0000-00-00 00:00:00'),
(690, 913, 0, '0000-00-00 00:00:00'),
(691, 32, 0, '0000-00-00 00:00:00'),
(692, 866, 0, '0000-00-00 00:00:00'),
(693, 952, 0, '0000-00-00 00:00:00'),
(694, 577, 0, '0000-00-00 00:00:00'),
(695, 972, 0, '0000-00-00 00:00:00'),
(696, 871, 0, '0000-00-00 00:00:00'),
(697, 43, 0, '0000-00-00 00:00:00'),
(698, 782, 0, '0000-00-00 00:00:00'),
(699, 441, 0, '0000-00-00 00:00:00'),
(700, 426, 0, '0000-00-00 00:00:00'),
(701, 519, 0, '0000-00-00 00:00:00'),
(702, 209, 0, '0000-00-00 00:00:00'),
(703, 606, 0, '0000-00-00 00:00:00'),
(704, 500, 0, '0000-00-00 00:00:00'),
(705, 694, 0, '0000-00-00 00:00:00'),
(706, 985, 0, '0000-00-00 00:00:00'),
(707, 267, 0, '0000-00-00 00:00:00'),
(708, 691, 0, '0000-00-00 00:00:00'),
(709, 231, 0, '0000-00-00 00:00:00'),
(710, 824, 0, '0000-00-00 00:00:00'),
(711, 620, 0, '0000-00-00 00:00:00'),
(712, 631, 0, '0000-00-00 00:00:00'),
(713, 260, 0, '0000-00-00 00:00:00'),
(714, 933, 0, '0000-00-00 00:00:00'),
(715, 248, 0, '0000-00-00 00:00:00'),
(716, 994, 0, '0000-00-00 00:00:00'),
(717, 668, 0, '0000-00-00 00:00:00'),
(718, 579, 0, '0000-00-00 00:00:00'),
(719, 276, 0, '0000-00-00 00:00:00'),
(720, 155, 0, '0000-00-00 00:00:00'),
(721, 370, 0, '0000-00-00 00:00:00'),
(722, 859, 0, '0000-00-00 00:00:00'),
(723, 489, 0, '0000-00-00 00:00:00'),
(724, 646, 0, '0000-00-00 00:00:00'),
(725, 663, 0, '0000-00-00 00:00:00'),
(726, 8, 0, '0000-00-00 00:00:00'),
(727, 6, 0, '0000-00-00 00:00:00'),
(728, 187, 0, '0000-00-00 00:00:00'),
(729, 790, 0, '0000-00-00 00:00:00'),
(730, 516, 0, '0000-00-00 00:00:00'),
(731, 266, 0, '0000-00-00 00:00:00'),
(732, 711, 0, '0000-00-00 00:00:00'),
(733, 339, 0, '0000-00-00 00:00:00'),
(734, 543, 0, '0000-00-00 00:00:00'),
(735, 484, 0, '0000-00-00 00:00:00'),
(736, 157, 0, '0000-00-00 00:00:00'),
(737, 611, 0, '0000-00-00 00:00:00'),
(738, 81, 0, '0000-00-00 00:00:00'),
(739, 131, 0, '0000-00-00 00:00:00'),
(740, 350, 0, '0000-00-00 00:00:00'),
(741, 623, 0, '0000-00-00 00:00:00'),
(742, 893, 0, '0000-00-00 00:00:00'),
(743, 875, 0, '0000-00-00 00:00:00'),
(744, 941, 0, '0000-00-00 00:00:00'),
(745, 416, 0, '0000-00-00 00:00:00'),
(746, 954, 0, '0000-00-00 00:00:00'),
(747, 146, 0, '0000-00-00 00:00:00'),
(748, 374, 0, '0000-00-00 00:00:00'),
(749, 356, 0, '0000-00-00 00:00:00'),
(750, 79, 0, '0000-00-00 00:00:00'),
(751, 845, 0, '0000-00-00 00:00:00'),
(752, 203, 0, '0000-00-00 00:00:00'),
(753, 159, 0, '0000-00-00 00:00:00'),
(754, 436, 0, '0000-00-00 00:00:00'),
(755, 881, 0, '0000-00-00 00:00:00'),
(756, 299, 0, '0000-00-00 00:00:00'),
(757, 925, 0, '0000-00-00 00:00:00'),
(758, 366, 0, '0000-00-00 00:00:00'),
(759, 804, 0, '0000-00-00 00:00:00'),
(760, 149, 0, '0000-00-00 00:00:00'),
(761, 687, 0, '0000-00-00 00:00:00'),
(762, 751, 0, '0000-00-00 00:00:00'),
(763, 817, 0, '0000-00-00 00:00:00'),
(764, 761, 0, '0000-00-00 00:00:00'),
(765, 538, 0, '0000-00-00 00:00:00'),
(766, 263, 0, '0000-00-00 00:00:00'),
(767, 805, 0, '0000-00-00 00:00:00'),
(768, 286, 0, '0000-00-00 00:00:00'),
(769, 856, 0, '0000-00-00 00:00:00'),
(770, 605, 0, '0000-00-00 00:00:00'),
(771, 491, 0, '0000-00-00 00:00:00'),
(772, 639, 0, '0000-00-00 00:00:00'),
(773, 70, 0, '0000-00-00 00:00:00'),
(774, 708, 0, '0000-00-00 00:00:00'),
(775, 852, 0, '0000-00-00 00:00:00'),
(776, 677, 0, '0000-00-00 00:00:00'),
(777, 469, 0, '0000-00-00 00:00:00'),
(778, 693, 0, '0000-00-00 00:00:00'),
(779, 892, 0, '0000-00-00 00:00:00'),
(780, 47, 0, '0000-00-00 00:00:00'),
(781, 637, 0, '0000-00-00 00:00:00'),
(782, 945, 0, '0000-00-00 00:00:00'),
(783, 521, 0, '0000-00-00 00:00:00'),
(784, 801, 0, '0000-00-00 00:00:00'),
(785, 819, 0, '0000-00-00 00:00:00'),
(786, 302, 0, '0000-00-00 00:00:00'),
(787, 316, 0, '0000-00-00 00:00:00'),
(788, 886, 0, '0000-00-00 00:00:00'),
(789, 720, 0, '0000-00-00 00:00:00'),
(790, 934, 0, '0000-00-00 00:00:00'),
(791, 167, 0, '0000-00-00 00:00:00'),
(792, 401, 0, '0000-00-00 00:00:00'),
(793, 920, 0, '0000-00-00 00:00:00'),
(794, 518, 0, '0000-00-00 00:00:00'),
(795, 192, 0, '0000-00-00 00:00:00'),
(796, 145, 0, '0000-00-00 00:00:00'),
(797, 228, 0, '0000-00-00 00:00:00'),
(798, 986, 0, '0000-00-00 00:00:00'),
(799, 147, 0, '0000-00-00 00:00:00'),
(800, 319, 0, '0000-00-00 00:00:00'),
(801, 575, 0, '0000-00-00 00:00:00'),
(802, 324, 0, '0000-00-00 00:00:00'),
(803, 414, 0, '0000-00-00 00:00:00'),
(804, 792, 0, '0000-00-00 00:00:00'),
(805, 931, 0, '0000-00-00 00:00:00'),
(806, 669, 0, '0000-00-00 00:00:00'),
(807, 674, 0, '0000-00-00 00:00:00'),
(808, 90, 0, '0000-00-00 00:00:00'),
(809, 533, 0, '0000-00-00 00:00:00'),
(810, 745, 0, '0000-00-00 00:00:00'),
(811, 766, 0, '0000-00-00 00:00:00'),
(812, 865, 0, '0000-00-00 00:00:00'),
(813, 327, 0, '0000-00-00 00:00:00'),
(814, 589, 0, '0000-00-00 00:00:00'),
(815, 678, 0, '0000-00-00 00:00:00'),
(816, 493, 0, '0000-00-00 00:00:00'),
(817, 890, 0, '0000-00-00 00:00:00'),
(818, 807, 0, '0000-00-00 00:00:00'),
(819, 234, 0, '0000-00-00 00:00:00'),
(820, 430, 0, '0000-00-00 00:00:00'),
(821, 772, 0, '0000-00-00 00:00:00'),
(822, 1, 0, '0000-00-00 00:00:00'),
(823, 563, 0, '0000-00-00 00:00:00'),
(824, 530, 0, '0000-00-00 00:00:00'),
(825, 170, 0, '0000-00-00 00:00:00'),
(826, 317, 0, '0000-00-00 00:00:00'),
(827, 854, 0, '0000-00-00 00:00:00'),
(828, 417, 0, '0000-00-00 00:00:00'),
(829, 188, 0, '0000-00-00 00:00:00'),
(830, 833, 0, '0000-00-00 00:00:00'),
(831, 419, 0, '0000-00-00 00:00:00'),
(832, 440, 0, '0000-00-00 00:00:00'),
(833, 244, 0, '0000-00-00 00:00:00'),
(834, 474, 0, '0000-00-00 00:00:00'),
(835, 387, 0, '0000-00-00 00:00:00'),
(836, 62, 0, '0000-00-00 00:00:00'),
(837, 108, 0, '0000-00-00 00:00:00'),
(838, 87, 0, '0000-00-00 00:00:00'),
(839, 333, 0, '0000-00-00 00:00:00'),
(840, 967, 0, '0000-00-00 00:00:00'),
(841, 870, 0, '0000-00-00 00:00:00'),
(842, 298, 0, '0000-00-00 00:00:00'),
(843, 227, 0, '0000-00-00 00:00:00'),
(844, 50, 0, '0000-00-00 00:00:00'),
(845, 140, 0, '0000-00-00 00:00:00'),
(846, 696, 0, '0000-00-00 00:00:00'),
(847, 825, 0, '0000-00-00 00:00:00'),
(848, 570, 0, '0000-00-00 00:00:00'),
(849, 429, 0, '0000-00-00 00:00:00'),
(850, 938, 0, '0000-00-00 00:00:00'),
(851, 326, 0, '0000-00-00 00:00:00'),
(852, 364, 0, '0000-00-00 00:00:00'),
(853, 749, 0, '0000-00-00 00:00:00'),
(854, 386, 0, '0000-00-00 00:00:00'),
(855, 739, 0, '0000-00-00 00:00:00'),
(856, 520, 0, '0000-00-00 00:00:00'),
(857, 35, 0, '0000-00-00 00:00:00'),
(858, 758, 0, '0000-00-00 00:00:00'),
(859, 709, 0, '0000-00-00 00:00:00'),
(860, 607, 0, '0000-00-00 00:00:00'),
(861, 71, 0, '0000-00-00 00:00:00'),
(862, 746, 0, '0000-00-00 00:00:00'),
(863, 198, 0, '0000-00-00 00:00:00'),
(864, 545, 0, '0000-00-00 00:00:00'),
(865, 73, 0, '0000-00-00 00:00:00'),
(866, 93, 0, '0000-00-00 00:00:00'),
(867, 289, 0, '0000-00-00 00:00:00'),
(868, 990, 0, '0000-00-00 00:00:00'),
(869, 943, 0, '0000-00-00 00:00:00'),
(870, 868, 0, '0000-00-00 00:00:00'),
(871, 610, 0, '0000-00-00 00:00:00'),
(872, 878, 0, '0000-00-00 00:00:00'),
(873, 853, 0, '0000-00-00 00:00:00'),
(874, 765, 0, '0000-00-00 00:00:00'),
(875, 20, 0, '0000-00-00 00:00:00'),
(876, 215, 0, '0000-00-00 00:00:00'),
(877, 437, 0, '0000-00-00 00:00:00'),
(878, 498, 0, '0000-00-00 00:00:00'),
(879, 906, 0, '0000-00-00 00:00:00'),
(880, 748, 0, '0000-00-00 00:00:00'),
(881, 980, 0, '0000-00-00 00:00:00'),
(882, 501, 0, '0000-00-00 00:00:00'),
(883, 667, 0, '0000-00-00 00:00:00'),
(884, 142, 0, '0000-00-00 00:00:00'),
(885, 28, 0, '0000-00-00 00:00:00'),
(886, 608, 0, '0000-00-00 00:00:00'),
(887, 738, 0, '0000-00-00 00:00:00'),
(888, 325, 0, '0000-00-00 00:00:00'),
(889, 472, 0, '0000-00-00 00:00:00'),
(890, 957, 0, '0000-00-00 00:00:00'),
(891, 22, 0, '0000-00-00 00:00:00'),
(892, 502, 0, '0000-00-00 00:00:00'),
(893, 973, 0, '0000-00-00 00:00:00'),
(894, 143, 0, '0000-00-00 00:00:00'),
(895, 407, 0, '0000-00-00 00:00:00'),
(896, 481, 0, '0000-00-00 00:00:00'),
(897, 762, 0, '0000-00-00 00:00:00'),
(898, 507, 0, '0000-00-00 00:00:00'),
(899, 689, 0, '0000-00-00 00:00:00'),
(900, 524, 0, '0000-00-00 00:00:00'),
(901, 535, 0, '0000-00-00 00:00:00'),
(902, 371, 0, '0000-00-00 00:00:00'),
(903, 616, 0, '0000-00-00 00:00:00'),
(904, 112, 0, '0000-00-00 00:00:00'),
(905, 389, 0, '0000-00-00 00:00:00'),
(906, 670, 0, '0000-00-00 00:00:00'),
(907, 109, 0, '0000-00-00 00:00:00'),
(908, 930, 0, '0000-00-00 00:00:00'),
(909, 621, 0, '0000-00-00 00:00:00'),
(910, 704, 0, '0000-00-00 00:00:00'),
(911, 433, 0, '0000-00-00 00:00:00'),
(912, 191, 0, '0000-00-00 00:00:00'),
(913, 802, 0, '0000-00-00 00:00:00'),
(914, 367, 0, '0000-00-00 00:00:00'),
(915, 919, 0, '0000-00-00 00:00:00'),
(916, 229, 0, '0000-00-00 00:00:00'),
(917, 284, 0, '0000-00-00 00:00:00'),
(918, 506, 0, '0000-00-00 00:00:00'),
(919, 811, 0, '0000-00-00 00:00:00'),
(920, 597, 0, '0000-00-00 00:00:00'),
(921, 522, 0, '0000-00-00 00:00:00'),
(922, 571, 0, '0000-00-00 00:00:00'),
(923, 603, 0, '0000-00-00 00:00:00'),
(924, 479, 0, '0000-00-00 00:00:00'),
(925, 89, 0, '0000-00-00 00:00:00'),
(926, 310, 0, '0000-00-00 00:00:00'),
(927, 983, 0, '0000-00-00 00:00:00'),
(928, 695, 0, '0000-00-00 00:00:00'),
(929, 558, 0, '0000-00-00 00:00:00'),
(930, 160, 0, '0000-00-00 00:00:00'),
(931, 578, 0, '0000-00-00 00:00:00'),
(932, 624, 0, '0000-00-00 00:00:00'),
(933, 283, 0, '0000-00-00 00:00:00'),
(934, 785, 0, '0000-00-00 00:00:00'),
(935, 847, 0, '0000-00-00 00:00:00'),
(936, 30, 0, '0000-00-00 00:00:00'),
(937, 432, 0, '0000-00-00 00:00:00'),
(938, 272, 0, '0000-00-00 00:00:00'),
(939, 733, 0, '0000-00-00 00:00:00'),
(940, 647, 0, '0000-00-00 00:00:00'),
(941, 617, 0, '0000-00-00 00:00:00'),
(942, 725, 0, '0000-00-00 00:00:00'),
(943, 406, 0, '0000-00-00 00:00:00'),
(944, 183, 0, '0000-00-00 00:00:00'),
(945, 788, 0, '0000-00-00 00:00:00'),
(946, 798, 0, '0000-00-00 00:00:00'),
(947, 679, 0, '0000-00-00 00:00:00'),
(948, 465, 0, '0000-00-00 00:00:00'),
(949, 744, 0, '0000-00-00 00:00:00'),
(950, 564, 0, '0000-00-00 00:00:00'),
(951, 459, 0, '0000-00-00 00:00:00'),
(952, 51, 0, '0000-00-00 00:00:00'),
(953, 752, 0, '0000-00-00 00:00:00'),
(954, 649, 0, '0000-00-00 00:00:00'),
(955, 243, 0, '0000-00-00 00:00:00'),
(956, 233, 0, '0000-00-00 00:00:00'),
(957, 587, 0, '0000-00-00 00:00:00'),
(958, 454, 0, '0000-00-00 00:00:00'),
(959, 238, 0, '0000-00-00 00:00:00'),
(960, 732, 0, '0000-00-00 00:00:00'),
(961, 760, 0, '0000-00-00 00:00:00'),
(962, 887, 0, '0000-00-00 00:00:00'),
(963, 742, 0, '0000-00-00 00:00:00'),
(964, 452, 0, '0000-00-00 00:00:00'),
(965, 629, 0, '0000-00-00 00:00:00'),
(966, 336, 0, '0000-00-00 00:00:00'),
(967, 390, 0, '0000-00-00 00:00:00'),
(968, 863, 0, '0000-00-00 00:00:00'),
(969, 293, 0, '0000-00-00 00:00:00'),
(970, 681, 0, '0000-00-00 00:00:00'),
(971, 948, 0, '0000-00-00 00:00:00'),
(972, 891, 0, '0000-00-00 00:00:00'),
(973, 636, 0, '0000-00-00 00:00:00'),
(974, 787, 0, '0000-00-00 00:00:00'),
(975, 923, 0, '0000-00-00 00:00:00'),
(976, 612, 0, '0000-00-00 00:00:00'),
(977, 373, 0, '0000-00-00 00:00:00'),
(978, 450, 0, '0000-00-00 00:00:00'),
(979, 12, 0, '0000-00-00 00:00:00'),
(980, 37, 0, '0000-00-00 00:00:00'),
(981, 67, 0, '0000-00-00 00:00:00'),
(982, 85, 0, '0000-00-00 00:00:00'),
(983, 224, 0, '0000-00-00 00:00:00'),
(984, 331, 0, '0000-00-00 00:00:00'),
(985, 337, 0, '0000-00-00 00:00:00'),
(986, 359, 0, '0000-00-00 00:00:00'),
(987, 427, 0, '0000-00-00 00:00:00'),
(988, 435, 0, '0000-00-00 00:00:00'),
(989, 473, 0, '0000-00-00 00:00:00'),
(990, 483, 0, '0000-00-00 00:00:00'),
(991, 622, 0, '0000-00-00 00:00:00'),
(992, 632, 0, '0000-00-00 00:00:00'),
(993, 655, 0, '0000-00-00 00:00:00'),
(994, 657, 0, '0000-00-00 00:00:00'),
(995, 716, 0, '0000-00-00 00:00:00'),
(996, 735, 0, '0000-00-00 00:00:00'),
(997, 849, 0, '0000-00-00 00:00:00'),
(998, 924, 0, '0000-00-00 00:00:00'),
(999, 978, 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_loginlog`
--

CREATE TABLE `t_expo_loginlog` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `email` varchar(100) NOT NULL,
  `pwd` varchar(100) NOT NULL,
  `login_text` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_note_pw2019`
--

CREATE TABLE `t_expo_note_pw2019` (
  `id` int(11) NOT NULL,
  `expo_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `catatan` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `status_pesan` int(11) NOT NULL,
  `nobooth` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_pw2019`
--

CREATE TABLE `t_expo_pw2019` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `invoice` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `kode_pesan` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `bio` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `web` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `socmed` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `upin` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `status_pesan` int(11) NOT NULL,
  `minat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `premium` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `reguler` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kuliner` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kuliner2` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `catatan` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `nilai_bayar` int(11) NOT NULL,
  `kode_bayar` int(11) NOT NULL,
  `bayar1` int(11) NOT NULL,
  `tgl_bayar1` datetime NOT NULL,
  `bayar2` int(11) NOT NULL,
  `tgl_bayar2` datetime NOT NULL,
  `team_fu` int(11) NOT NULL,
  `status_fu` int(11) NOT NULL,
  `nobooth` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `last_update` datetime NOT NULL,
  `tbh_tanggal` datetime NOT NULL,
  `tbh_titipbrosur` int(11) NOT NULL,
  `tbh_penjaga` int(11) NOT NULL,
  `tbh_daya` int(11) NOT NULL,
  `tbh_meja` int(11) NOT NULL,
  `tbh_kursi` int(11) NOT NULL,
  `tbh_tiket` int(11) NOT NULL,
  `tbh_tv` int(11) NOT NULL,
  `tbh_lainnya` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `tbh_penanggungjawab` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `tbh_status` int(11) NOT NULL,
  `tbh_nilai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_tambahan`
--

CREATE TABLE `t_expo_tambahan` (
  `id` int(11) NOT NULL,
  `expo_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `item_value` int(11) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_expo_tiket`
--

CREATE TABLE `t_expo_tiket` (
  `id` int(11) NOT NULL,
  `expo_id` int(11) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `hp` varchar(50) NOT NULL,
  `status_tiket` int(11) NOT NULL,
  `no_tiket` varchar(20) NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_fnc_pwd_akun`
--

CREATE TABLE `t_fnc_pwd_akun` (
  `id` int(11) NOT NULL,
  `kode` varchar(5) COLLATE utf8_unicode_ci NOT NULL,
  `group_akun` int(11) NOT NULL COMMENT '1 = pendapatan, 2 = pengeluaran',
  `keterangan` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `seq` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_fnc_pwd_akun`
--

INSERT INTO `t_fnc_pwd_akun` (`id`, `kode`, `group_akun`, `keterangan`, `seq`) VALUES
(1, '41', 1, 'Penjualan Tiket', 1),
(2, '42', 1, 'Sponsorship', 2),
(3, '43', 1, 'Donasi', 3),
(4, '44', 1, 'Pendapatan Lainnya', 4),
(5, '51', 2, 'Marketing', 1),
(6, '52', 2, 'Administrasi & Sekretariat', 2),
(7, '53', 2, 'Gedung & Tenda', 3),
(8, '54', 2, 'Logistik', 4);

-- --------------------------------------------------------

--
-- Table structure for table `t_hotel`
--

CREATE TABLE `t_hotel` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `kode_booking` varchar(10) NOT NULL,
  `jenis_kamar` int(11) NOT NULL,
  `tgl_cekin` date NOT NULL,
  `tgl_cekout` date NOT NULL,
  `numpax` int(11) NOT NULL,
  `room_type` int(11) NOT NULL,
  `breakfast` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `no_tiket` varchar(100) NOT NULL,
  `asal_tda` varchar(100) NOT NULL,
  `no_ktp` varchar(100) NOT NULL,
  `file_ktp` varchar(100) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `keterangan2` varchar(1000) NOT NULL,
  `jml_kamar` int(11) NOT NULL,
  `metoda_bayar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `nilai_bf` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL COMMENT '1=Waiting List, 2=Pemesanan Booth, 3.Pembayaran Sebagian, 4=Pembayaran Lunas, 6=Sponsorship, 5=Batal',
  `np_requestOrder` varchar(1000) NOT NULL,
  `np_responseOrder` varchar(1000) NOT NULL,
  `np_requestOnePass` varchar(1000) NOT NULL,
  `np_responseOnePass` varchar(1000) NOT NULL,
  `np_txid` varchar(50) NOT NULL,
  `np_resultCode` varchar(20) NOT NULL,
  `np_resultMsg` varchar(50) NOT NULL,
  `np_resultText` varchar(1000) NOT NULL,
  `np_authNo` varchar(10) NOT NULL,
  `np_creditcard` varchar(50) NOT NULL,
  `np_expdate` varchar(10) NOT NULL,
  `np_bankcode` varchar(10) NOT NULL,
  `np_vanum` varchar(50) NOT NULL,
  `np_vavaliddate` varchar(50) NOT NULL,
  `np_vavalidtime` varchar(50) NOT NULL,
  `np_status` int(11) NOT NULL,
  `email_kirim` int(11) NOT NULL,
  `email_status` int(11) NOT NULL,
  `email_kode` varchar(20) NOT NULL,
  `bayar_tgl` varchar(100) NOT NULL,
  `bayar_note` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_hotel`
--

INSERT INTO `t_hotel` (`id`, `event_id`, `tanggal`, `kode_booking`, `jenis_kamar`, `tgl_cekin`, `tgl_cekout`, `numpax`, `room_type`, `breakfast`, `nama_lengkap`, `email`, `nohp`, `no_tiket`, `asal_tda`, `no_ktp`, `file_ktp`, `keterangan`, `keterangan2`, `jml_kamar`, `metoda_bayar`, `nilai`, `nilai_bf`, `status_bayar`, `np_requestOrder`, `np_responseOrder`, `np_requestOnePass`, `np_responseOnePass`, `np_txid`, `np_resultCode`, `np_resultMsg`, `np_resultText`, `np_authNo`, `np_creditcard`, `np_expdate`, `np_bankcode`, `np_vanum`, `np_vavaliddate`, `np_vavalidtime`, `np_status`, `email_kirim`, `email_status`, `email_kode`, `bayar_tgl`, `bayar_note`) VALUES
(1, 0, '2019-04-18 11:11:03', 'NNWASAQ', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1555560663Screenshot_2019-01-09_16-38-01.png.png', 'wew', '', 1, 0, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(2, 0, '2019-04-18 11:14:06', 'AYJTX7N', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1555560846Screenshot_2019-01-09_16-38-01.png.png', 'wew', '', 1, 0, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(3, 0, '2019-04-18 11:14:53', 'OSFJJEE', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1555560893Screenshot_2019-01-09_16-38-01.png.png', 'wew', '', 1, 0, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(4, 0, '2019-04-18 11:15:05', 'ISTGRX8', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1555560905Screenshot_2019-01-09_16-38-01.png.png', 'wew', '', 1, 0, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(5, 0, '2019-04-18 11:15:31', 'S3E5G15', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1555560931Screenshot_2019-01-09_16-38-01.png.png', 'wew', '', 1, 0, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(6, 9, '2019-04-23 14:08:05', 'L75TQP0', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1556003285avatar-1.jpg.jpg', '', '', 1, 2, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(7, 9, '2019-04-23 14:10:05', '7DYQ63L', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1556003405avatar-1.jpg.jpg', '', '', 1, 2, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(8, 9, '2019-04-23 14:12:20', 'DUH2LTT', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1556003540avatar-1.jpg.jpg', '', '', 1, 2, 1800000, 0, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', ''),
(9, 9, '2019-04-23 14:12:33', '1F35YQN', 1, '2019-01-24', '2019-01-27', 0, 0, 0, 'Indra', 'indra.starts.permana@gmail.com', '08976533578', '001', 'Bandung', '3204281404950009', '1556003553avatar-1.jpg.jpg', '', '', 1, 2, 1800000, 0, 0, '', '', '', '', 'HOTELPWP4Y02201904231412344714', '', '', '', '', '', '', '', '', '', '', 0, 0, 0, '', '', '');

-- --------------------------------------------------------

--
-- Table structure for table `t_hotel_kamar`
--

CREATE TABLE `t_hotel_kamar` (
  `id` int(11) NOT NULL,
  `hotel_id` int(11) NOT NULL,
  `no_kamar` varchar(20) NOT NULL,
  `tipe_kamar` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `telepon` varchar(100) NOT NULL,
  `cekin` int(11) NOT NULL,
  `cekout` int(11) NOT NULL,
  `hal` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_hotel_kode`
--

CREATE TABLE `t_hotel_kode` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `status_kode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_hotel_kode`
--

INSERT INTO `t_hotel_kode` (`id`, `kode`, `status_kode`) VALUES
(1, '9X8E3C7', 1),
(2, '1GXNIGV', 1),
(3, 'L6O0KQ5', 1),
(4, 'EF5OIV3', 1),
(5, 'ERWEFVI', 1),
(6, '9YG144F', 1),
(7, 'DA0O7EF', 1),
(8, 'HVAL150', 1),
(9, '9RTE8GI', 1),
(10, 'E2REQX5', 1),
(11, 'KRAOS02', 1),
(12, 'P58WQ98', 1),
(13, 'I5YHMIL', 1),
(14, 'LPTPHBT', 1),
(15, 'XZP8GPM', 1),
(16, 'YGFECE0', 1),
(17, 'TDY18N8', 1),
(18, 'KVKVB4B', 1),
(19, 'I5V6UA5', 1),
(20, '1QIQXOU', 1),
(21, 'ZSLJW9A', 1),
(22, 'UN959JR', 1),
(23, 'AETJ9EG', 1),
(24, 'UFCLWL2', 1),
(25, 'JZM9IXI', 1),
(26, '4XJPAJL', 1),
(27, 'AT3AXMK', 1),
(28, 'XRQHWST', 1),
(29, 'J3LSTXS', 1),
(30, '2L1W8AC', 1),
(31, '9KO93HA', 1),
(32, 'QTLDB1L', 1),
(33, 'XK46EFP', 1),
(34, 'X38QEZC', 1),
(35, 'D0FDATD', 1),
(36, '31EKLPO', 1),
(37, 'MGZ9RT6', 1),
(38, 'VYMJRKZ', 1),
(39, 'W92ZZ83', 1),
(40, '0R6TJA4', 1),
(41, 'UPI6W76', 1),
(42, 'DQ2ZFF9', 1),
(43, 'WPYJOR8', 1),
(44, 'EP247YO', 1),
(45, 'GZI1ERX', 1),
(46, '1OT54MU', 1),
(47, 'ARTW7I6', 1),
(48, 'M05QGXL', 1),
(49, 'DL0KA9C', 1),
(50, 'FQZ74J2', 1),
(51, 'ZWMZD6M', 1),
(52, 'BFIN6E4', 1),
(53, 'C1GGC6R', 1),
(54, 'D5TJMIG', 1),
(55, 'HS86ELW', 1),
(56, 'II9JNIW', 1),
(57, 'JMRMDXS', 1),
(58, 'GU9KQTT', 1),
(59, '2209KZ6', 1),
(60, 'OATXIS6', 1),
(61, 'W1T6DBI', 1),
(62, 'HY1NJ1Y', 1),
(63, 'ZKHSC8R', 1),
(64, 'MXO1X7O', 1),
(65, '6PLTG5P', 1),
(66, 'K6YRUP5', 1),
(67, '4HTTR1B', 1),
(68, 'TZT6N7Y', 1),
(69, 'B4D7KOR', 1),
(70, 'RJ72FVK', 1),
(71, '0BFUIZE', 1),
(72, '0RGJHZG', 1),
(73, 'UX4VS7T', 1),
(74, '3MBLV8D', 1),
(75, '1TNSVTD', 1),
(76, '4JHU0O4', 1),
(77, '7DBS15E', 1),
(78, 'J3XDGYO', 1),
(79, '2XSTG6C', 1),
(80, '2PFWZNH', 1),
(81, 'P1BN4C6', 1),
(82, 'W8A52Y9', 1),
(83, '8NN0A5K', 1),
(84, 'H1M9HRW', 1),
(85, '743NV50', 1),
(86, 'Q8XD7X8', 1),
(87, '0L8YYLP', 1),
(88, 'ZH069C6', 1),
(89, 'HUSEPMR', 1),
(90, 'BIISG5V', 1),
(91, 'ETU44ST', 1),
(92, 'T48IBZF', 1),
(93, 'A1BIMUM', 1),
(94, '1643FDL', 1),
(95, 'L97QSRK', 1),
(96, 'M94633F', 1),
(97, '5SK6JME', 1),
(98, 'W6RO3MH', 1),
(99, '8PTKOQ0', 1),
(100, '67BJ75F', 1),
(101, '1YKWGUT', 1),
(102, 'Q7XC3FR', 1),
(103, 'WRYV6H5', 1),
(104, 'LXWHVXR', 1),
(105, 'STWJH66', 1),
(106, 'ORP5PD8', 1),
(107, 'IJP50DQ', 1),
(108, 'XLM888G', 1),
(109, 'TWXCGKM', 1),
(110, 'NHI2ZYX', 1),
(111, 'E2VMBAI', 1),
(112, '1EZPPBO', 1),
(113, 'OAU8XIA', 1),
(114, '3SMHZV9', 1),
(115, 'OKXSCI5', 1),
(116, 'EJD6O3L', 1),
(117, '34ZI5KG', 1),
(118, 'SSGMBTT', 1),
(119, '0ETFOGY', 1),
(120, 'RPUVZYR', 1),
(121, 'DR37MSF', 1),
(122, 'H2L0LSC', 1),
(123, 'MBWDGFJ', 1),
(124, 'VM7C1SY', 1),
(125, 'QGGUX9R', 1),
(126, '9RXHK87', 1),
(127, 'WRA9SWC', 1),
(128, 'Z2MKEKN', 1),
(129, '62CN8I7', 1),
(130, 'WHPWZC3', 1),
(131, 'AA1WS1V', 1),
(132, 'BOY0HAB', 1),
(133, 'MKPIDRW', 1),
(134, 'BZTY79K', 1),
(135, 'WBEWC5J', 1),
(136, 'UW4VBTL', 1),
(137, 'ITNV423', 1),
(138, '7JQ99AX', 1),
(139, '698TACF', 1),
(140, 'C7OWUJH', 1),
(141, 'W2T4M7P', 1),
(142, 'GZJE9ZD', 1),
(143, '9015Z0O', 1),
(144, 'Z3U109O', 1),
(145, 'LIW7BG2', 1),
(146, 'NDIU3RY', 1),
(147, '2H227UX', 1),
(148, 'WKCMDXE', 1),
(149, '4WSG5FD', 1),
(150, '6L6KPE4', 1),
(151, 'IWSB4L3', 1),
(152, '15ROGU1', 1),
(153, 'KH5E3OL', 1),
(154, 'ZTPV5L5', 1),
(155, 'LPZTCIV', 1),
(156, '6TPYP7D', 1),
(157, 'VR45ZZA', 1),
(158, 'TELIX04', 1),
(159, 'TCZ4RPO', 1),
(160, 'UYAQIPE', 1),
(161, 'YNHJ5BF', 1),
(162, 'U1GE5SM', 1),
(163, 'TJGCL66', 1),
(164, '3MLNBL3', 1),
(165, 'K188EFI', 1),
(166, '9HOU9UY', 1),
(167, '4CBNLIQ', 1),
(168, 'XFNQRY3', 1),
(169, 'T9X30V1', 1),
(170, '41948NP', 1),
(171, '78E2BFG', 1),
(172, 'NNWASAQ', 1),
(173, 'AYJTX7N', 1),
(174, 'OSFJJEE', 1),
(175, 'ISTGRX8', 1),
(176, 'S3E5G15', 1),
(177, 'Z1M0PVK', 1),
(178, 'DTXSB21', 1),
(179, 'L75TQP0', 1),
(180, '7DYQ63L', 1),
(181, 'DUH2LTT', 1),
(182, '1F35YQN', 1),
(183, '0JEBGAV', 0),
(184, 'XPLVSAB', 0),
(185, 'P3NSOUL', 0),
(186, '0E4RKYN', 0),
(187, '9F0ZO51', 0),
(188, 'V5NILY3', 0),
(189, '4Z5JSI2', 0),
(190, '63DW88E', 0),
(191, 'IWRH1I7', 0),
(192, 'GDY19BA', 0),
(193, 'L04FPZP', 0),
(194, '78H41LR', 0),
(195, '0JV86DG', 0),
(196, 'YLD4O1V', 0),
(197, 'O3WZTQ5', 0),
(198, '8FK6DR1', 0),
(199, '52IVC3U', 0),
(200, '860XH3R', 0),
(201, 'L2TD8R4', 0),
(202, 'GHC1VJI', 0),
(203, 'ND58E73', 0),
(204, '5RNNBS4', 0),
(205, 'PN10FK4', 0),
(206, 'D1YK91C', 0),
(207, 'UBK8EF6', 0),
(208, 'JCZEUDS', 0),
(209, 'WVNB8E2', 0),
(210, 'EOXIGLS', 0),
(211, 'GCV0D6Y', 0),
(212, 'ICVSFLX', 0),
(213, '0OFMATO', 0),
(214, '9YHDD4M', 0),
(215, 'JF5QIQG', 0),
(216, 'MM5VPQD', 0),
(217, 'U20KG6X', 0),
(218, 'GPBGDJJ', 0),
(219, 'GDVQJR6', 0),
(220, 'S7D4K9P', 0),
(221, 'ZPTKIJU', 0),
(222, 'OGIVVK1', 0),
(223, 'ZTB6XXM', 0),
(224, '7EJPCMK', 0),
(225, 'ML0C1JN', 0),
(226, '9T8OZG9', 0),
(227, 'KQ1AA26', 0),
(228, 'XPJVTSA', 0),
(229, 'W5L8HBB', 0),
(230, '9LO94MN', 0),
(231, 'TSN49F4', 0),
(232, '971XNAI', 0),
(233, '7TJTPUS', 0),
(234, 'XWTW78V', 0),
(235, '2L9L3NG', 0),
(236, '3SA2Q2P', 0),
(237, '33Y1M7K', 0),
(238, '2R2PEMC', 0),
(239, 'CLX4WXG', 0),
(240, 'QBMJTNC', 0),
(241, '9FR38FU', 0),
(242, 'LD5DVYT', 0),
(243, 'ZBV1MJW', 0),
(244, '872O8EX', 0),
(245, 'RR0RXHL', 0),
(246, 'VN66R2A', 0),
(247, 'CQU1SFT', 0),
(248, '4PG3M8H', 0),
(249, 'KD52UVK', 0),
(250, 'H3V3HR0', 0),
(251, 'YJ110LS', 0),
(252, 'IRCD7I6', 0),
(253, 'JHEUKAM', 0),
(254, '5VWDPI7', 0),
(255, 'X0XMAOD', 0),
(256, '00W9HZC', 0),
(257, 'F79OFD8', 0),
(258, 'PDLLZ8O', 0),
(259, 'FGM269F', 0),
(260, '6OIXE5X', 0),
(261, 'MU0RSZ6', 0),
(262, 'X34D6GO', 0),
(263, 'WE32LFV', 0),
(264, 'IF0FT93', 0),
(265, 'X4QAOGR', 0),
(266, '76NUZIY', 0),
(267, 'WPCIU6B', 0),
(268, '5CWEHMJ', 0),
(269, '0MCOGSP', 0),
(270, 'VZ6SVKD', 0),
(271, 'KT1684F', 0),
(272, 'T0HO33T', 0),
(273, 'AG21S5G', 0),
(274, 'YOVKDS3', 0),
(275, 'Y269MY6', 0),
(276, 'LSC4JJJ', 0),
(277, 'CC2DIV5', 0),
(278, '0QBP4W0', 0),
(279, '7ETW7QV', 0),
(280, 'JERUXUO', 0),
(281, '73YA5QD', 0),
(282, 'EB84RAK', 0),
(283, 'LWAJ1U5', 0),
(284, 'YAR8E9S', 0),
(285, '2T7ZMVZ', 0),
(286, 'IC3MD2H', 0),
(287, 'V2R7PSH', 0),
(288, 'GCC5CU3', 0),
(289, 'HTMADKZ', 0),
(290, 'P6PY8JA', 0),
(291, 'CBHY3ZV', 0),
(292, 'JI2PK5L', 0),
(293, 'NPEV9RV', 0),
(294, 'C1LSX1G', 0),
(295, 'VAHYBOM', 0),
(296, '5E7EM0U', 0),
(297, 'XW5BB9W', 0),
(298, 'BRIEIUW', 0),
(299, '5L3RLAF', 0),
(300, 'MPRIUPM', 0),
(301, '6G7U22V', 0),
(302, '42I5JQA', 0),
(303, 'RAXNLQ4', 0),
(304, 'XRAA6RI', 0),
(305, 'Q7VNDS8', 0),
(306, '6KT0D1W', 0),
(307, 'MIW4IJH', 0),
(308, 'U0CHHCH', 0),
(309, 'EUPVRA9', 0),
(310, 'VT7R3QI', 0),
(311, '6I4JQQD', 0),
(312, 'Z0LJQNR', 0),
(313, 'YPY29EO', 0),
(314, 'RFNDYLV', 0),
(315, 'S23PAXY', 0),
(316, 'RE2H4DR', 0),
(317, 'KQ886X1', 0),
(318, '52GN7TR', 0),
(319, 'W5CFX5U', 0),
(320, 'YTJFXBN', 0),
(321, 'SE43V22', 0),
(322, 'RPTNHZ1', 0),
(323, 'FJI2EL7', 0),
(324, '2HSQ01W', 0),
(325, 'O297730', 0),
(326, 'TWSBCCO', 0),
(327, 'K2GQCOI', 0),
(328, 'HZF97YQ', 0),
(329, 'XPCCICA', 0),
(330, 'G60ZSJ0', 0),
(331, 'ULF5ELL', 0),
(332, 'G0TOQZN', 0),
(333, 'NN4B36D', 0),
(334, 'B9EI548', 0),
(335, 'OEY9P44', 0),
(336, 'UFF16ZF', 0),
(337, 'MPTZ2N0', 0),
(338, 'WK4YK86', 0),
(339, 'F34U7TT', 0),
(340, 'NNO7TTZ', 0),
(341, 'PJ41YOR', 0),
(342, 'R2IDCC1', 0),
(343, 'MBYS4SC', 0),
(344, '2BWFPAD', 0),
(345, '8U2O3XG', 0),
(346, 'SCX945D', 0),
(347, '785K9T3', 0),
(348, '4B6WDS2', 0),
(349, 'ST5RDY5', 0),
(350, '7MBPOZP', 0),
(351, 'JUTGSOQ', 0),
(352, 'S7JM9F9', 0),
(353, 'CY2VHXC', 0),
(354, 'LM7IZ9Y', 0),
(355, 'DYEMJXT', 0),
(356, '2C9KAJX', 0),
(357, '9PXCEPX', 0),
(358, 'LDZXQX6', 0),
(359, 'FXUILZV', 0),
(360, 'VNENQEY', 0),
(361, 'QOMQ49S', 0),
(362, '8OGKS67', 0),
(363, '8U3DHOM', 0),
(364, 'TE8ERDS', 0),
(365, '7IGOWS4', 0),
(366, 'QSNP6TZ', 0),
(367, 'PQWNAQQ', 0),
(368, 'H431816', 0),
(369, 'Q4OOCU2', 0),
(370, 'ZDXGVAV', 0),
(371, 'SUK7B7L', 0),
(372, 'BO1JJVA', 0),
(373, 'IM6YHLC', 0),
(374, 'K6599SF', 0),
(375, 'VT1DECB', 0),
(376, 'F9MGOEQ', 0),
(377, 'XZR5COT', 0),
(378, 'JZWTVST', 0),
(379, 'VAYGUPK', 0),
(380, 'YRL3RYA', 0),
(381, '62RTS8O', 0),
(382, 'UN84CVO', 0),
(383, 'YD7KDVQ', 0),
(384, 'XB1MTDG', 0),
(385, 'B1G7UXQ', 0),
(386, 'DW5X93S', 0),
(387, 'CO61S3B', 0),
(388, 'VPSJRJW', 0),
(389, 'BN2CE8A', 0),
(390, 'ZWR2JMQ', 0),
(391, 'JG8MV5D', 0),
(392, 'DZFZFX9', 0),
(393, 'W7VYKN0', 0),
(394, 'PM1E9I7', 0),
(395, 'JVNS1M5', 0),
(396, 'NH8R7DG', 0),
(397, 'C1FYY1N', 0),
(398, '8FENR5S', 0),
(399, 'REP0Z2J', 0),
(400, 'RFF4NEL', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_hotel_test`
--

CREATE TABLE `t_hotel_test` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jenis_kamar` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `no_tiket` varchar(100) NOT NULL,
  `asal_tda` varchar(100) NOT NULL,
  `no_ktp` varchar(100) NOT NULL,
  `file_ktp` varchar(100) NOT NULL,
  `keterangan` varchar(1000) NOT NULL,
  `jml_kamar` int(11) NOT NULL,
  `metoda_bayar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `np_requestOrder` varchar(1000) NOT NULL,
  `np_responseOrder` varchar(1000) NOT NULL,
  `np_requestOnePass` varchar(1000) NOT NULL,
  `np_responseOnePass` varchar(1000) NOT NULL,
  `np_txid` varchar(50) NOT NULL,
  `np_resultCode` varchar(20) NOT NULL,
  `np_resultMsg` varchar(50) NOT NULL,
  `np_resultText` varchar(1000) NOT NULL,
  `np_authNo` varchar(10) NOT NULL,
  `np_creditcard` varchar(50) NOT NULL,
  `np_expdate` varchar(10) NOT NULL,
  `np_bankcode` varchar(10) NOT NULL,
  `np_vanum` varchar(50) NOT NULL,
  `np_vavaliddate` varchar(50) NOT NULL,
  `np_vavalidtime` varchar(50) NOT NULL,
  `np_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_hotel_test`
--

INSERT INTO `t_hotel_test` (`id`, `tanggal`, `jenis_kamar`, `nama_lengkap`, `email`, `nohp`, `no_tiket`, `asal_tda`, `no_ktp`, `file_ktp`, `keterangan`, `jml_kamar`, `metoda_bayar`, `nilai`, `status_bayar`, `np_requestOrder`, `np_responseOrder`, `np_requestOnePass`, `np_responseOnePass`, `np_txid`, `np_resultCode`, `np_resultMsg`, `np_resultText`, `np_authNo`, `np_creditcard`, `np_expdate`, `np_bankcode`, `np_vanum`, `np_vavaliddate`, `np_vavalidtime`, `np_status`) VALUES
(1, '2018-12-14 11:07:32', 1, 'test', 'test@test.com', '2342343', '234', '2342', '23423', '', '23423', 2, 1, 3600000, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(2, '2018-12-14 11:10:20', 1, 'test', 'test@test.com', '2342343', '234', '2342', '23423', '', '23423', 2, 1, 3600000, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(3, '2018-12-14 11:11:46', 1, 'test', 'test@test.com', '2342343', '234', '2342', '23423', '', '23423', 2, 1, 3600000, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(4, '2018-12-14 11:35:44', 1, 'sdfasd', 'test@test.com', 'sdfsadf', 'sdfsad', '2342', 'sdfsadf', '', 'dsfsad', 2, 1, 3600000, 0, '', '', '', '', 'IONPAYTEST01201812141035445354', '', '', '', '', '', '', '', '', '', '', 0),
(5, '2018-12-14 12:02:09', 1, 'test', 'test@test.com', '23423423', 'sdfsad', 'sdfsd', '32423423', '', 'asfadf', 2, 2, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141102092162', '', '', '', '', '', '', '', '', '', '', 0),
(6, '2018-12-14 12:12:13', 1, 'test', 'test@test.com', '23423423', 'sdfsad', 'sdfsd', '32423423', '', 'asfadf', 2, 2, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141112138281', '', '', '', '', '', '', '', '', '', '', 0),
(7, '2018-12-14 12:13:28', 1, 'test', 'test@test.com', '23423423', 'sdfsad', 'sdfsd', '32423423', '', 'asfadf', 2, 3, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141113287540', '', '', '', '', '', '', '', '', '', '', 0),
(8, '2018-12-14 12:15:07', 1, 'test', 'test@test.com', '234234', 'sdfsad', '234', '234234', '', '234234', 2, 3, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141115089488', '', '', '', '', '', '', '', '', '', '', 0),
(9, '2018-12-14 14:02:35', 1, 'sdfasdf', 'sdfsd@afdsafasd.com', 'sdf', 'sdf', 'sdf', 'sdf', '', 'sdfsd', 3, 3, 5400000, 0, '', '', '', '', 'IONPAYTEST02201812141302357725', '', '', '', '', '', '', '1302357725', '', '', '', 0),
(10, '2018-12-14 14:05:47', 1, 'sdfasdf', 'test@test.com', '08112238737', 'sdfsad', 'sdfsd', '32423423', '', 'sdfsad', 2, 2, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141305479875', '', '', '', '', '', '', 'BMRI', '1305479875', '20181221', '235959', 0),
(11, '2018-12-14 14:08:19', 1, 'asfasdf', 'sdfsd@afdsafasd.com', '08112238737', 'sdfsad', 'sdfsd', '32423423', '', 'fasdfas', 2, 2, 3600000, 0, '', '', '', '', 'IONPAYTEST02201812141308201912', '', '', '', '', '', '', '', '', '', '', 0),
(12, '2018-12-18 15:36:30', 2, 'sdfasdf', 'test@test.com', '08112238737', 'sdfsad', 'sdfsd', '32423423', '', 'test', 2, 1, 2400000, 0, '', '', '', '', 'IONPAYTEST01201812181436303860', '', '', '', '', '', '', '', '', '', '', 0),
(13, '2018-12-18 15:36:57', 2, 'sdfasdf', 'test@test.com', '08112238737', 'sdfsad', 'sdfsd', '32423423', '', 'test', 2, 1, 2400000, 0, '', '', '', '', 'IONPAYTEST01201812181436574194', '', '', '', '', '441111******1118', '1912', '', '', '', '', 0),
(14, '2018-12-18 16:08:20', 1, 'asdfasd', 'test@test.com', '08112238737', 'sdfsdf', 'sfasd', '', '', 'sfasdf', 2, 1, 3600000, 0, '', '', '', '', 'IONPAYTEST01201812181508200470', '0000', 'paid', '', '', '441111******1118', '1912', '', '', '', '', 0),
(15, '2018-12-18 18:04:35', 2, 'sfasd', 'admin@admin.com', 'sdafasdf', 'sdfsad', 'sdfsd', '324234', '', 'sdfad', 1, 1, 1200000, 0, '', '', '', '', 'IONPAYTEST01201812181704357005', '0000', 'paid', '{\"reqTm\":\"170435\",\"ccTransType\":\"1\",\"acquStatus\":\"00\",\"fee\":\"0\",\"tXid\":\"IONPAYTEST01201812181704357005\",\"amt\":\"1200000\",\"cancelAmt\":null,\"notaxAmt\":\"0\",\"depositTm\":null,\"cardNo\":\"441111******1118\",\"issuBankCd\":\"OTHR\",\"preauthToken\":null,\"cardExpYymm\":\"2111\",\"acquBankNm\":\"Mandiri\",\"payMethod\":\"01\",\"currency\":\"IDR\",\"instmntMon\":\"1\",\"issuBankNm\":\"OTHER\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 2 Malam Aston Marina #15\",\"referenceNo\":\"15\",\"transTm\":\"170435\",\"authNo\":\"357005\",\"recurringToken\":null,\"vat\":\"0\",\"instmntType\":\"1\",\"resultMsg\":\"paid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"sfasd\",\"acquBankCd\":\"BMRI\",\"depositDt\":null,\"reqDt\":\"20181218\",\"transDt\":\"20181218\",\"status\":\"0\"}', '', '441111******1118', '2111', '', '', '', '', 0),
(16, '2018-12-18 19:21:09', 2, 'test', 'sdfsd@afdsafasd.com', 'sdfsd', 'sdfsad', 'sdfsd', '324234', '', 'test', 1, 1, 1200000, 1, '', '', '', '', 'IONPAYTEST01201812181821100606', '0000', 'paid', '{\"reqTm\":\"182110\",\"ccTransType\":\"1\",\"acquStatus\":\"00\",\"fee\":\"0\",\"tXid\":\"IONPAYTEST01201812181821100606\",\"amt\":\"1200000\",\"cancelAmt\":null,\"notaxAmt\":\"0\",\"depositTm\":null,\"cardNo\":\"441111******1118\",\"issuBankCd\":\"OTHR\",\"preauthToken\":null,\"cardExpYymm\":\"2111\",\"acquBankNm\":\"Mandiri\",\"payMethod\":\"01\",\"currency\":\"IDR\",\"instmntMon\":\"1\",\"issuBankNm\":\"OTHER\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 2 Malam Aston Marina #16\",\"referenceNo\":\"16\",\"transTm\":\"182110\",\"authNo\":\"100606\",\"recurringToken\":null,\"vat\":\"0\",\"instmntType\":\"1\",\"resultMsg\":\"paid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"test\",\"acquBankCd\":\"BMRI\",\"depositDt\":null,\"reqDt\":\"20181218\",\"transDt\":\"20181218\",\"status\":\"0\"}', '', '441111******1118', '2111', '', '', '', '', 0),
(17, '2018-12-18 19:24:07', 2, 'sdfasdf', 'test@test.com', '23423423', 'sdfsad', '2342', '', '', 'test', 1, 1, 1200000, 1, '', '', '', '', 'IONPAYTEST01201812181824072499', '0000', 'paid', '{\"reqTm\":\"182407\",\"ccTransType\":\"1\",\"acquStatus\":\"00\",\"fee\":\"0\",\"tXid\":\"IONPAYTEST01201812181824072499\",\"amt\":\"1200000\",\"cancelAmt\":null,\"notaxAmt\":\"0\",\"depositTm\":null,\"cardNo\":\"441111******1118\",\"issuBankCd\":\"OTHR\",\"preauthToken\":null,\"cardExpYymm\":\"2111\",\"acquBankNm\":\"Mandiri\",\"payMethod\":\"01\",\"currency\":\"IDR\",\"instmntMon\":\"1\",\"issuBankNm\":\"OTHER\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 2 Malam Aston Marina #17\",\"referenceNo\":\"17\",\"transTm\":\"182407\",\"authNo\":\"072499\",\"recurringToken\":null,\"vat\":\"0\",\"instmntType\":\"1\",\"resultMsg\":\"paid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"sdfasdf\",\"acquBankCd\":\"BMRI\",\"depositDt\":null,\"reqDt\":\"20181218\",\"transDt\":\"20181218\",\"status\":\"0\"}', '', '441111******1118', '2111', '', '', '', '', 0),
(18, '2018-12-18 19:35:53', 2, 'test', 'sdfsadf', '23423423', '123123', 'sdfsd', '32423423', '', 'test', 1, 2, 1200000, 0, '', '', '', '', '', '', '', '', '', '', '', '', '', '', '', 0),
(19, '2018-12-18 19:36:14', 2, 'test', 'test@test.com', '23423423', 'sdfsad', 'sdfsd', '', '', 'sasadfsd', 1, 2, 1200000, 0, '', '', '', '', 'IONPAYTEST02201812181836148027', '0000', 'unpaid', '{\"reqTm\":\"183614\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 2 Malam Aston Marina #19\",\"referenceNo\":\"19\",\"transTm\":\"183614\",\"tXid\":\"IONPAYTEST02201812181836148027\",\"amt\":\"1200000\",\"cancelAmt\":null,\"depositTm\":null,\"vacctNo\":\"1836148027\",\"resultMsg\":\"unpaid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"test\",\"vacctValidDt\":\"20181225\",\"depositDt\":null,\"payMethod\":\"02\",\"reqDt\":\"20181218\",\"bankCd\":\"BMRI\",\"currency\":\"IDR\",\"transDt\":\"20181218\",\"vacctValidTm\":\"235959\",\"status\":\"3\"}', '', '', '', 'BMRI', '1836148027', '20181225', '235959', 3),
(20, '2018-12-18 19:46:41', 1, 'test', 'test@test.com', '23423423', 'sdfsdf', '2342', 'sadfasd', '', 'sdfasdf', 1, 8, 1800000, 0, '', '', '', '', 'IONPAYTEST03201812181846423034', '0000', 'unpaid', '{\"reqTm\":\"184642\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 3 Malam Aston Marina #20\",\"referenceNo\":\"20\",\"transTm\":\"184642\",\"mitraCd\":\"ALMA\",\"tXid\":\"IONPAYTEST03201812181846423034\",\"amt\":\"1800000\",\"cancelAmt\":null,\"depositTm\":null,\"resultMsg\":\"unpaid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"test\",\"payNo\":\"181846423034\",\"payValidTm\":\"235959\",\"depositDt\":null,\"payMethod\":\"03\",\"reqDt\":\"20181218\",\"currency\":\"IDR\",\"payValidDt\":\"20181225\",\"transDt\":\"20181218\",\"status\":\"3\"}', '', '', '', 'ALMA', '181846423034', '', '', 3),
(21, '2018-12-19 16:41:49', 2, 'test', 'test@test.com', '23423423', 'sdfsad', 'asdfsad', '32423423', '', 'sdfsd', 1, 2, 1200000, 0, '', '', '', '', 'IONPAYTEST02201812191541491614', '0000', 'unpaid', '{\"reqTm\":\"154149\",\"resultCd\":\"0000\",\"goodsNm\":\"Paket 2 Malam Aston Marina #21\",\"referenceNo\":\"21\",\"transTm\":\"154149\",\"tXid\":\"IONPAYTEST02201812191541491614\",\"amt\":\"1200000\",\"cancelAmt\":null,\"depositTm\":null,\"vacctNo\":\"1541491614\",\"resultMsg\":\"unpaid\",\"iMid\":\"IONPAYTEST\",\"billingNm\":\"test\",\"vacctValidDt\":\"20181226\",\"depositDt\":null,\"payMethod\":\"02\",\"reqDt\":\"20181219\",\"bankCd\":\"BMRI\",\"currency\":\"IDR\",\"transDt\":\"20181219\",\"vacctValidTm\":\"235959\",\"status\":\"3\"}', '', '', '', 'BMRI', '1541491614', '20181226', '235959', 3);

-- --------------------------------------------------------

--
-- Table structure for table `t_jadwal`
--

CREATE TABLE `t_jadwal` (
  `id` int(11) NOT NULL,
  `lokasi` int(11) NOT NULL,
  `tanggal` date NOT NULL,
  `harike` int(11) NOT NULL,
  `jam_mulai` int(11) NOT NULL,
  `jam_akhir` int(11) NOT NULL,
  `sjam_mulai` varchar(10) NOT NULL,
  `sjam_akhir` varchar(10) NOT NULL,
  `durasi` int(11) NOT NULL,
  `pemateri` varchar(200) NOT NULL,
  `pemateri_id` int(11) NOT NULL,
  `deskripsi` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jadwal`
--

INSERT INTO `t_jadwal` (`id`, `lokasi`, `tanggal`, `harike`, `jam_mulai`, `jam_akhir`, `sjam_mulai`, `sjam_akhir`, `durasi`, `pemateri`, `pemateri_id`, `deskripsi`) VALUES
(1, 1, '2019-01-25', 1, 540, 660, '09.00', '11.00', 120, 'Kementrian', 0, 'Opening Ceremonial'),
(2, 1, '2019-01-25', 1, 660, 780, '11.00', '13.00', 120, 'Break Sholat Jumat', 0, 'Sholat Jumat'),
(3, 1, '2019-01-25', 1, 780, 825, '13.00', '13.45', 45, 'Coach Rene Suhardono (Komisaris Utama Ancol)', 0, 'Membangun Mindset Kolaboraksi'),
(4, 1, '2019-01-25', 1, 825, 885, '13.45', '14.45', 60, 'Safri Helmi - Erita Z, Teguh Atmajaya - Agustina, Ronny Anggono - Sonya Chaterina', 0, 'Couplepreneur : Cinta Dengan Kolaboraksi - Sukses Usaha Sukses Keluarga'),
(5, 1, '2019-01-25', 1, 885, 930, '14.45', '15.30', 45, 'Nofi Bayu Darmawan - Kampung Marketer', 0, 'Inspirasi Kolaboraksi'),
(6, 1, '2019-01-25', 1, 930, 975, '15.30', '16.15', 0, 'Adrian A Gunadi (CEO Investree), Denni Gautama (VP Traveloka)', 0, 'Kolaboraksi Bersama Teknologi'),
(7, 1, '2019-01-25', 1, 975, 1020, '16.15', '17.00', 0, 'Adhitya Chandra (Chief Marketing Officer Lion Parcel)', 0, 'Transformasi Logistik dalam memenuhi pasar di era digital'),
(8, 1, '2019-01-25', 1, 1020, 1080, '17.00', '18.00', 60, 'Dewa Eka (CEO Billionare)', 0, 'Trik Kolaborasi dengan yang terbaik'),
(9, 1, '2019-01-26', 2, 600, 660, '10.00', '11.00', 60, 'Didi Diarsa, Sapta Nirwandar (Ketua Halal Lifestyle Center)', 0, 'Industri Kreatif dan Pariwisata Go Global dengan Kolaboraksi'),
(10, 1, '2019-01-26', 2, 660, 720, '11.00', '12.00', 60, 'Iman Firmansyah (Ketua TDA Bandung), Syahril (Ketua TDA Palu), Agoeng Wibowo (Sekjen Genpro)\r\n', 0, 'KUBIS - Kupas Kisah Kolaboraksi dalam komunitas'),
(11, 1, '2019-01-26', 2, 720, 780, '12.00', '13.00', 60, 'Break', 0, 'Break'),
(12, 1, '2019-01-26', 2, 780, 840, '13.00', '14.00', 60, 'Christian Sugiono (jalan2men), Donny Kris Puriyono (Malang Strudel), Rex Marindo (Upnormal)', 0, 'Sukses dengan Kolaboraksi'),
(13, 1, '2019-01-26', 2, 840, 845, '14.00', '14.05', 5, 'Kick off TDA - Foodiz', 0, 'Tanda Tangan MoU'),
(14, 1, '2019-01-26', 2, 845, 855, '14.05', '14.15', 10, 'MTI Launching', 0, 'Team MTI'),
(15, 1, '2019-01-26', 2, 855, 915, '14.15', '15.15', 60, 'Ambawani Restu Widi (Kepala Tim Pengembangan Ekonomi BI DKI), Wisnu Dewobroto (TDA), Dewi Melsari Haryanti (UKMIndonesia.org)', 0, 'Peran BI Prov DKI Jakarta Dalam Pengembangan UKM'),
(16, 1, '2019-01-26', 2, 915, 960, '15.15', '16.00', 45, 'CEO Rumah Zakat', 0, 'Happiness Collaboration'),
(17, 1, '2019-01-26', 2, 960, 1005, '16.00', '16.45', 45, 'Yudistira Nugroho (Google Indonesia)', 0, 'Google Analyst for UKM'),
(18, 1, '2019-01-26', 2, 1005, 1080, '16.45', '18.00', 75, 'TDA Gathering', 0, 'Merajut nilai nilai Komunitas TDA'),
(19, 1, '2019-01-26', 2, 1080, 1110, '18.00', '18.30', 30, 'Break', 0, 'Break'),
(20, 1, '2019-01-26', 2, 1110, 1290, '18.30', '21.30', 120, 'Konser Kemanusiaan ', 0, 'Konser Kemanusiaan');

-- --------------------------------------------------------

--
-- Table structure for table `t_jadwal_lokasi`
--

CREATE TABLE `t_jadwal_lokasi` (
  `id` int(11) NOT NULL,
  `lokasi` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_jadwal_lokasi`
--

INSERT INTO `t_jadwal_lokasi` (`id`, `lokasi`) VALUES
(1, 'Panggung Inspirasi'),
(2, 'Panggung Expo'),
(3, 'Panggung Kuliner'),
(4, 'Kelas Digital'),
(5, 'TDA Lounge');

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE `t_kategori` (
  `id` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`id`, `nama`, `slug`) VALUES
(1, 'Pesta Wirausaha', 'pesta-wirausaha'),
(2, 'Seminar', 'seminar');

-- --------------------------------------------------------

--
-- Table structure for table `t_konfirmasi`
--

CREATE TABLE `t_konfirmasi` (
  `id` int(11) NOT NULL,
  `invoice` varchar(10) NOT NULL,
  `email` varchar(30) NOT NULL,
  `user_id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `nominal` int(10) NOT NULL,
  `bank` varchar(15) NOT NULL,
  `norek` int(20) NOT NULL,
  `tanggal` date NOT NULL,
  `status` int(5) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kupon`
--

CREATE TABLE `t_kupon` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `type` int(5) NOT NULL COMMENT '1=regurer, 2=mahasiswa',
  `diskon` int(12) NOT NULL,
  `komisi` int(5) NOT NULL,
  `status` int(1) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tgl_buat` datetime NOT NULL,
  `tgl_exp` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kupon`
--

INSERT INTO `t_kupon` (`id`, `kode`, `type`, `diskon`, `komisi`, `status`, `user_id`, `tgl_buat`, `tgl_exp`) VALUES
(1, 'SINGLELILLAH', 1, 401000, 0, 1, 1, '2019-04-09 17:27:04', '2019-04-30 23:59:00'),
(2, 'TDACAMPUS', 2, 150000, 0, 1, 1, '2019-04-09 17:28:23', '2019-04-30 23:59:00');

-- --------------------------------------------------------

--
-- Table structure for table `t_kwitansi`
--

CREATE TABLE `t_kwitansi` (
  `id` int(11) NOT NULL,
  `no_surat` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `terima_dari` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nilai` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `terbilang` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `tujuan_pembayaran` varchar(500) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_layanan`
--

CREATE TABLE `t_layanan` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `nama` varchar(25) NOT NULL,
  `harga` int(12) NOT NULL DEFAULT 0,
  `status` int(5) NOT NULL DEFAULT 0,
  `waktu` int(25) NOT NULL DEFAULT 0,
  `komisi` int(11) NOT NULL DEFAULT 0,
  `daerah` int(7) NOT NULL DEFAULT 0,
  `hadiah` int(7) NOT NULL DEFAULT 0,
  `deskripsi` text DEFAULT NULL,
  `url` varchar(150) DEFAULT NULL,
  `reseller` int(5) NOT NULL DEFAULT 0,
  `harga_fix` int(20) NOT NULL DEFAULT 0,
  `jumlah` int(5) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_layanan`
--

INSERT INTO `t_layanan` (`id`, `event_id`, `nama`, `harga`, `status`, `waktu`, `komisi`, `daerah`, `hadiah`, `deskripsi`, `url`, `reseller`, `harga_fix`, `jumlah`) VALUES
(1, 9, 'Regular', 900000, 1, 1, 1, 50000, 50000, '- Tiket Masuk 3 Hari Event di Eco Convention Ancol<br>- TIDAK TERMASUK TIKET ANCOL.', ' ', 0, 900000, 0),
(2, 9, 'Mahasiswa', 300000, 1, 1, 150000, 50000, 50000, '- Tiket Masuk 3 Hari Event <br> \r\n - Menunjukan Kartu Mahasiswa Aktif saat masuk<br>\r\n - Hanya untuk 1000 Pendaftar <br>', '', 0, 300000, 0),
(3, 9, 'Premium', 500000, 1, 10, 100000, 250000, 150000, '- Tiket Masuk 3 Hari Event di Eco Convention Ancol<br>\r\n- TIDAK TERMASUK TIKET ANCOL.', ' ', 0, 500000, 200);

-- --------------------------------------------------------

--
-- Table structure for table `t_nicepay_log`
--

CREATE TABLE `t_nicepay_log` (
  `id` int(11) NOT NULL,
  `ngroup` varchar(10) NOT NULL,
  `tanggal` datetime NOT NULL,
  `metoda` varchar(100) NOT NULL,
  `data_get` varchar(1000) NOT NULL,
  `data_post` varchar(1000) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_nicepay_log`
--

INSERT INTO `t_nicepay_log` (`id`, `ngroup`, `tanggal`, `metoda`, `data_get`, `data_post`) VALUES
(1, 'Hotel', '2019-04-16 11:45:58', 'callback', '[]', '[]');

-- --------------------------------------------------------

--
-- Table structure for table `t_nicepay_sajah`
--

CREATE TABLE `t_nicepay_sajah` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_invoice` varchar(10) NOT NULL,
  `id_trans` int(11) NOT NULL,
  `jenis_trans` int(11) NOT NULL,
  `nama_lengkap` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `nohp` varchar(100) NOT NULL,
  `note_trans` varchar(100) NOT NULL,
  `metoda_bayar` int(11) NOT NULL,
  `nilai` int(11) NOT NULL,
  `payfee` int(11) NOT NULL,
  `total` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `np_requestOrder` varchar(1000) NOT NULL,
  `np_responseOrder` varchar(1000) NOT NULL,
  `np_requestOnePass` varchar(1000) NOT NULL,
  `np_responseOnePass` varchar(1000) NOT NULL,
  `np_txid` varchar(50) NOT NULL,
  `np_resultCode` varchar(20) NOT NULL,
  `np_resultMsg` varchar(50) NOT NULL,
  `np_resultText` varchar(1000) NOT NULL,
  `np_authNo` varchar(10) NOT NULL,
  `np_creditcard` varchar(50) NOT NULL,
  `np_expdate` varchar(10) NOT NULL,
  `np_bankcode` varchar(10) NOT NULL,
  `np_vanum` varchar(50) NOT NULL,
  `np_vavaliddate` varchar(50) NOT NULL,
  `np_vavalidtime` varchar(50) NOT NULL,
  `np_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_param_pw2019`
--

CREATE TABLE `t_param_pw2019` (
  `id` int(11) NOT NULL,
  `param` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_param_pw2019`
--

INSERT INTO `t_param_pw2019` (`id`, `param`, `nilai`) VALUES
(1, 'TIKET', 1000);

-- --------------------------------------------------------

--
-- Table structure for table `t_partner`
--

CREATE TABLE `t_partner` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `partner_key` varchar(50) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_partner`
--

INSERT INTO `t_partner` (`id`, `kode`, `nama`, `partner_key`) VALUES
(1, 'PWAPPS', 'Pesta Wirausaha Apps', '123456');

-- --------------------------------------------------------

--
-- Table structure for table `t_peserta`
--

CREATE TABLE `t_peserta` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `gaffid` int(11) NOT NULL,
  `affid` int(11) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_peserta`
--

INSERT INTO `t_peserta` (`id`, `event_id`, `gaffid`, `affid`, `username`, `passwd`, `nama`) VALUES
(1, 9, 4, 0, 'asep', 'asep123', 'Asep'),
(2, 9, 4, 0, 'asep', 'asep123', 'Asep');

-- --------------------------------------------------------

--
-- Table structure for table `t_rekening`
--

CREATE TABLE `t_rekening` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `kode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `nomor` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `bank` varchar(100) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_rekening_mutasi`
--

CREATE TABLE `t_rekening_mutasi` (
  `id` int(11) NOT NULL,
  `rekening_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `no_referensi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `deskripsi` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `debet` double NOT NULL,
  `kredit` double NOT NULL,
  `saldo` double NOT NULL,
  `kode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tipe_trans` int(11) NOT NULL,
  `keterangan` varchar(200) COLLATE utf8_unicode_ci NOT NULL,
  `sync_status` int(11) NOT NULL DEFAULT 0,
  `settle_code` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_rekening_text`
--

CREATE TABLE `t_rekening_text` (
  `id` int(11) NOT NULL,
  `rekening_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `data_text` varchar(20000) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_spoin_pw2019`
--

CREATE TABLE `t_spoin_pw2019` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `invoice` varchar(6) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `asal_tda` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `jenis_paket` int(11) NOT NULL,
  `status_pesan` int(11) NOT NULL,
  `catatan` varchar(300) COLLATE utf8_unicode_ci NOT NULL,
  `nilai_bayar` int(11) NOT NULL,
  `metode_bayar` int(11) NOT NULL,
  `np_requestOrder` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `np_responseOrder` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `np_requestOnePass` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `np_responseOnePass` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `np_txid` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_resultCode` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `np_resultMsg` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_resultText` varchar(1000) COLLATE utf8_unicode_ci NOT NULL,
  `np_authNo` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `np_creditcard` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_expdate` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `np_bankcode` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `np_vanum` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_vavaliddate` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_vavalidtime` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `np_status` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_spoin_pw2019`
--

INSERT INTO `t_spoin_pw2019` (`id`, `event_id`, `tanggal`, `invoice`, `nama`, `usaha`, `email`, `no_hp`, `asal_tda`, `jenis_paket`, `status_pesan`, `catatan`, `nilai_bayar`, `metode_bayar`, `np_requestOrder`, `np_responseOrder`, `np_requestOnePass`, `np_responseOnePass`, `np_txid`, `np_resultCode`, `np_resultMsg`, `np_resultText`, `np_authNo`, `np_creditcard`, `np_expdate`, `np_bankcode`, `np_vanum`, `np_vavaliddate`, `np_vavalidtime`, `np_status`) VALUES
(1, 9, '2019-04-25 10:13:17', '00', 'Indra', 'maju mundur', 'indra.starts.permana@gmail.com', '08976533578', 'Bandung', 1, 0, 'wew', 5000000, 1, '{\"iMid\":\"HOTELPWP4Y\",\"payMethod\":\"01\",\"currency\":\"IDR\",\"merchantKey\":\"+XRXEqlfHC2SLejErvjPEcYRF9HDBxR4xklzJPMJwUCNVaq09zn7YvhzpCr2Fe6xh+xKACS7lPkCFUAALXpOoA==\",\"amt\":5000000,\"referenceNo\":\"S1\",\"goodsNm\":\"Paket Sponsor Internal Premium #S1\",\"billingNm\":\"Indra\",\"billingPhone\":\"08976533578\",\"billingEmail\":\"indra.starts.permana@gmail.com\",\"callBackUrl\":\"http:\\/\\/localhost\\/pesta_wirausaha\\/sponsor\\/scallback\",\"dbProcessUrl\":\"http:\\/\\/localhost\\/pesta_wirausaha\\/sponsor\\/sdbprocess\",\"description\":\"Paket Sponsor Internal Premium No.S1\",\"merchantToken\":\"b751a4f6060420b2732032cd3bca5bf227876dc0681024599683b069c4e62c87\",\"cartData\":\"{}\",\"instmntType\":1,\"instmntMon\":1,\"billingAddr\":\"Bandung\",\"billingCity\":\"Bandung\",\"billingState\":\"Bandung\",\"billingPostCd\":\"12520\",\"billingCountry\":\"Indonesia\",\"userIP\":\"::1\"}', '{\"apiType\":\"M0\",\"tXid\":\"HOTELPWP4Y01201904251013198858\",\"requestDate\":\"20190425101311\",\"responseDate\":\"20190425101311\",\"data\":{\"tXid\":\"HOTELPWP4Y01201904251013198858\",\"resultCd\":\"0000\",\"resultMsg\":\"SUCCESS\",\"requestURL\":\"https:\\/\\/www.nicepay.co.id\\/nicepay\\/api\\/orderInquiry.do\"}}', '', '', 'HOTELPWP4Y01201904251013198858', '', '', '', '', '', '', '', '', '', '', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_sponsor`
--

CREATE TABLE `t_sponsor` (
  `id` int(11) NOT NULL,
  `s_level` int(11) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `logo` varchar(100) NOT NULL,
  `bio` varchar(1000) NOT NULL,
  `expo_id` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sponsor`
--

INSERT INTO `t_sponsor` (`id`, `s_level`, `nama`, `logo`, `bio`, `expo_id`) VALUES
(1, 1, 'Sponsor', 'logo', 'bio', 0),
(2, 1, 'sponsor 2', 'logo ', 'bio', 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_sponsor_level`
--

CREATE TABLE `t_sponsor_level` (
  `id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `slevel` varchar(100) NOT NULL,
  `urutan` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_sponsor_level`
--

INSERT INTO `t_sponsor_level` (`id`, `parent_id`, `slevel`, `urutan`) VALUES
(1, 0, 'Sponsor', 1),
(2, 0, 'Sponsor Inernal', 2),
(3, 2, 'Premium', 3),
(4, 2, 'VIP', 4),
(5, 2, 'Super', 5),
(6, 2, 'Insert', 6),
(7, 2, 'Reguler', 7);

-- --------------------------------------------------------

--
-- Table structure for table `t_tiket`
--

CREATE TABLE `t_tiket` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `invoice` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dtanggal` datetime NOT NULL,
  `tipe_tiket` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `usaha` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nilai` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `gaffid` int(11) NOT NULL,
  `affid` int(11) NOT NULL,
  `no_hp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `daerah` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kode_bayar` int(11) NOT NULL,
  `status_bayar` int(1) NOT NULL COMMENT '0=belum bayar, 1=udah bayar',
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `aksi_bayar` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `approver` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `id_bayar` int(11) NOT NULL,
  `tgl_bayar` datetime NOT NULL,
  `jml_tiket` int(11) NOT NULL,
  `n_nilai` int(11) NOT NULL,
  `n_pw` int(11) NOT NULL,
  `n_hadiah` int(11) NOT NULL,
  `n_daerah` int(11) NOT NULL,
  `n_affiliate` int(11) NOT NULL,
  `n_kode` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_tiket`
--

INSERT INTO `t_tiket` (`id`, `event_id`, `invoice`, `tanggal`, `dtanggal`, `tipe_tiket`, `nama`, `usaha`, `nilai`, `gaffid`, `affid`, `no_hp`, `daerah`, `kode_bayar`, `status_bayar`, `email`, `aksi_bayar`, `approver`, `id_bayar`, `tgl_bayar`, `jml_tiket`, `n_nilai`, `n_pw`, `n_hadiah`, `n_daerah`, `n_affiliate`, `n_kode`) VALUES
(6, 9, '4465', '2019-04-12 14:42:48', '0000-00-00 00:00:00', '1', 'Asep', 'mundur terus', '499465', 4, 3, '0898765432122', 'Bali', 4465, 1, 'asep@gmail.com', '', '', 1, '2019-04-14 08:15:25', 1, 0, 0, 0, 0, 0, 0),
(5, 9, '4717', '2019-04-12 14:41:36', '0000-00-00 00:00:00', '1', 'Asep', 'mundur terus', '499717', 4, 3, '0898765432122', 'Bali', 4717, 0, 'asep@gmail.com', '', '', 0, '0000-00-00 00:00:00', 1, 0, 0, 0, 0, 0, 0),
(4, 9, '3744', '2019-04-11 11:25:44', '0000-00-00 00:00:00', '1', 'Indra', 'maju mundur', '499744', 3, 0, '08987654321', 'Bali', 3744, 1, 'indra.starts.permana@gmail.com', '', '', 2, '2019-04-15 09:09:20', 1, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_tiketgroup_pw2019`
--

CREATE TABLE `t_tiketgroup_pw2019` (
  `id` int(11) NOT NULL,
  `invoice` varchar(10) COLLATE utf8_unicode_ci NOT NULL,
  `affid` int(11) NOT NULL,
  `tanggal` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `kontak` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `no_hp` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `nilai` int(11) NOT NULL,
  `nilai_lunas` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `jml_tiket` int(11) NOT NULL,
  `jml_terjual` int(11) NOT NULL,
  `n_nilai` int(11) NOT NULL,
  `n_pw` int(11) NOT NULL,
  `n_hadiah` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_tiketgroup_trans`
--

CREATE TABLE `t_tiketgroup_trans` (
  `id` int(11) NOT NULL,
  `tiketgroup_id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `jml_tiket` int(11) NOT NULL,
  `nilai_bayar` int(11) NOT NULL,
  `catatan` varchar(500) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_tiket_issued`
--

CREATE TABLE `t_tiket_issued` (
  `id` int(11) NOT NULL,
  `kode` varchar(10) NOT NULL,
  `jenis` varchar(20) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `usaha` varchar(100) NOT NULL,
  `asal_tda` varchar(100) NOT NULL,
  `no_hp` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `qrcode` varchar(20) NOT NULL,
  `idacc` varchar(20) NOT NULL,
  `role_id` int(11) NOT NULL DEFAULT 1
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_tiket_scanqr`
--

CREATE TABLE `t_tiket_scanqr` (
  `id` int(11) NOT NULL,
  `tanggal` datetime NOT NULL,
  `partner_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `dataqr` varchar(200) NOT NULL,
  `qr_type` varchar(20) NOT NULL,
  `qr_status` int(11) NOT NULL,
  `qr_msg` varchar(200) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_tiket_wilayah`
--

CREATE TABLE `t_tiket_wilayah` (
  `id` int(11) NOT NULL,
  `daerah` varchar(100) NOT NULL,
  `wilayah` varchar(100) NOT NULL,
  `jml_anggota` int(11) NOT NULL,
  `tiket_online` varchar(100) NOT NULL,
  `tiket_group_affid` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_tipebooth_pw2019`
--

CREATE TABLE `t_tipebooth_pw2019` (
  `id` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `nama` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `jml_booth` int(11) NOT NULL,
  `harga` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `t_user`
--

CREATE TABLE `t_user` (
  `id` int(11) NOT NULL,
  `kode` varchar(20) NOT NULL,
  `username` varchar(100) NOT NULL,
  `passwd` varchar(100) NOT NULL,
  `nama` varchar(100) NOT NULL,
  `email` varchar(50) NOT NULL,
  `telepon` varchar(50) NOT NULL,
  `reg_date` datetime NOT NULL,
  `act_date` datetime NOT NULL,
  `kode_aktivasi` varchar(10) NOT NULL,
  `userlevel` int(11) NOT NULL,
  `userstatus` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_user`
--

INSERT INTO `t_user` (`id`, `kode`, `username`, `passwd`, `nama`, `email`, `telepon`, `reg_date`, `act_date`, `kode_aktivasi`, `userlevel`, `userstatus`) VALUES
(1, '1', 'admin', 'db96de53f52ba6a51c01341e6f2260a6', 'indra', 'indra@gmail.com', '0897654321', '2019-03-20 07:17:24', '2019-03-20 13:19:27', '1', 1, 1),
(2, '2', 'budi', 'b29887e3798fa47bd7a3cca63cab3716', 'Budi Anduk', 'budi@gmail.com', '0879654342123', '2019-03-25 11:29:06', '2019-03-26 10:30:12', '123', 2, 1),
(3, '', 'indra', '5c26097303c740fa732b05c65351bd85', 'Indra Permana', 'indra.starts.permana@gmail.com', '08987654321', '2019-04-11 11:22:08', '0000-00-00 00:00:00', 'e8bcceeff7', 2, 1),
(4, '', 'asep', 'ae47e02b9e2dcce41e14503028dee69e', 'Asep', 'asep@gmail.com', '0898765432122', '2019-04-12 14:39:52', '0000-00-00 00:00:00', 'a4f2d65b3e', 2, 1);

-- --------------------------------------------------------

--
-- Table structure for table `t_user_group`
--

CREATE TABLE `t_user_group` (
  `id` int(11) NOT NULL,
  `nama` varchar(50) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_user_group`
--

INSERT INTO `t_user_group` (`id`, `nama`) VALUES
(1, 'Administrator'),
(2, 'Peserta'),
(3, 'TDA Daerah'),
(4, 'Expo PW'),
(5, 'Finance PW'),
(6, 'BOD TDA'),
(7, 'Akotrans PW');

-- --------------------------------------------------------

--
-- Table structure for table `t_user_tiket`
--

CREATE TABLE `t_user_tiket` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `tiket_id` int(11) NOT NULL,
  `kode_tiket` varchar(30) COLLATE utf8_unicode_ci NOT NULL,
  `tanggal` datetime NOT NULL,
  `harga` int(11) NOT NULL,
  `aff_id` int(11) NOT NULL,
  `status_bayar` int(11) NOT NULL,
  `status_pakai` int(11) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `t_user_tiket`
--

INSERT INTO `t_user_tiket` (`id`, `user_id`, `tiket_id`, `kode_tiket`, `tanggal`, `harga`, `aff_id`, `status_bayar`, `status_pakai`) VALUES
(1, 4, 6, '', '2019-04-12 14:42:48', 499465, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `t_wilayah`
--

CREATE TABLE `t_wilayah` (
  `id` int(11) NOT NULL,
  `wilayah` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL,
  `gambar` varchar(255) NOT NULL,
  `wilayah_induk_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_wilayah`
--

INSERT INTO `t_wilayah` (`id`, `wilayah`, `slug`, `gambar`, `wilayah_induk_id`) VALUES
(1, 'Gresik', 'gresik', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fgresik.jpg?alt=media&token=bd90687b-732a-4bb8-933a-cf67aed55fc1', 7),
(2, 'Tanjung Pinang', 'tanjung-pinang', 'Logo-Tanjung-Pinang.png', 15),
(3, 'Bogor', 'bogor', 'Logo-Bogor.png', 5),
(4, 'Garut', 'garut', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fgarut.jpg?alt=media&token=c162ff74-d751-4edb-b54b-3e9024ca9131', 5),
(5, 'Batam', 'batam', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbatam.jpg?alt=media&token=dd1e0f4f-1c43-42ce-9395-31bbc6cf1b83', 12),
(6, 'Kediri', 'kediri', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fkediri.jpg?alt=media&token=fee157f4-875a-4f1b-baa1-71d4ab0b349f', 8),
(7, 'Parepare', 'parepare', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fparepare.jpg?alt=media&token=d5344be3-397f-465e-9a24-f954a39701be', 16),
(8, 'Padang', 'padang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpadang.jpg?alt=media&token=3692e546-6e50-4fa3-a807-4b86668b5a52', 15),
(9, 'Kab Bogor', 'kab-bogor', 'Logo-Kab-Bogor.png', 5),
(10, 'Jakarta Raya', 'jakarta-raya', 'Logo-Jakarta.png', 4),
(11, 'Palu', 'palu', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpalu.jpg?alt=media&token=854266e9-cc63-4eff-9a9b-88bdb96b9d27', 17),
(12, 'Solo', 'solo', 'Logo-Solo.png', 9),
(13, 'Palembang', 'palembang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpalembang.jpg?alt=media&token=b03be0e0-4822-42a9-b719-5a82916ecf40', 18),
(14, 'Malang', 'malang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmalang.jpg?alt=media&token=d41007da-98cf-40b4-87e5-fc6608d21bc9', 8),
(15, 'Balikpapan', 'balikpapan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbalikpapan.jpg?alt=media&token=ead6dc9c-8963-48a8-877d-0ce4f3405e85', 11),
(16, 'Samarinda', 'samarinda', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsamarinda.jpg?alt=media&token=43f5298e-6074-4baf-8ca1-32817e9c3c77', 11),
(17, 'Bali', 'bali', 'Logo-Bali.png', 2),
(18, 'Bandung', 'bandung', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbandung.jpg?alt=media&token=12598718-f693-4644-a698-bcec600d5eab', 5),
(19, 'Bekasi', 'bekasi', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbekasi.jpg?alt=media&token=4518a013-fbb6-44e7-9ca5-97d3a3bde0ab', 6),
(20, 'Depok', 'depok', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fdepok.jpg?alt=media&token=b8e763de-22a4-4f11-810a-aa5564397a04', 6),
(21, 'Surabaya', 'surabaya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsurabaya.jpg?alt=media&token=a1ca8163-af1c-44f3-93dd-35bf0f19b01b', 7),
(22, 'Mataram', 'mataram', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmataram.jpg?alt=media&token=6fdfa52b-86fb-48b7-8634-5d8bb8533e4e', 2),
(23, 'Makassar', 'makassar', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmakassar.jpg?alt=media&token=d591ef3e-5f23-46dd-9858-8c221d57fcc1', 16),
(24, 'Blitar', 'blitar', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fblitar.jpg?alt=media&token=e52a7314-3c33-4087-b29d-85fc367c58d3', 8),
(25, 'Lampung', 'lampung', 'Logo-Lampung.png', 13),
(26, 'Banjarmasin', 'banjarmasin', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbanjarmasin.jpg?alt=media&token=d09815ae-ad45-486d-86f1-565cadd58027', 10),
(27, 'Pekanbaru', 'pekanbaru', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpekanbaru.jpg?alt=media&token=e717d082-3c74-48b8-a868-21c113492b20', 15),
(28, 'Medan', 'medan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmedan.jpg?alt=media&token=f690280a-b650-4afd-9fc9-516fbb22e31a', 1),
(29, 'Tasik', 'tasik', 'Logo-Tasik.png', 6),
(30, 'Semarang', 'semarang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsemarang.jpg?alt=media&token=bfbc0f2d-0b23-4bb8-89fe-8abb6fcdd7ab', 9),
(31, 'Banda Aceh', 'banda-aceh', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbanda_aceh.jpg?alt=media&token=36d695f1-75d8-4fa6-8aef-fa97a9c583e4', 1),
(32, 'Bandar Lampung', 'bandar-lampung', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbandar_lampung.jpg?alt=media&token=6718ebf8-8ddd-4b8c-9b9b-62c9148bfb6c', 13),
(33, 'Banjarbaru', 'banjarbaru', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbanjarbaru.jpg?alt=media&token=83726d52-c795-452f-a204-a20e5d5e5454', 10),
(34, 'Banyumas', 'banyumas', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbanjarmasin.jpg?alt=media&token=d09815ae-ad45-486d-86f1-565cadd58027', 9),
(35, 'Belitung', 'belitung', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbitung.jpg?alt=media&token=9af3f943-e061-4277-9e86-3a161e7a654a', 18),
(36, 'Bengkulu', 'bengkulu', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbengkulu.jpg?alt=media&token=bb365d31-d9ae-47f6-8b84-a062485ef0fa', 13),
(37, 'Besemah Raya', 'besemah-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsulsel.jpg?alt=media&token=fd96ccb1-87fa-4d0c-9cbb-f5fd8b5a676a', 16),
(38, 'Bima', 'bima', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbima.jpg?alt=media&token=8bdf15ab-cd53-4df6-a89c-8dbc1ecfd092', 2),
(39, 'Bogor Kabupaten', 'bogor-kabupaten', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbogor.jpg?alt=media&token=28c9b3f3-e04c-4763-8fde-b53be10f734b', 6),
(40, 'Bogor Raya', 'bogor-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbogor.jpg?alt=media&token=28c9b3f3-e04c-4763-8fde-b53be10f734b', 6),
(41, 'Bojonegoro', 'bojonegoro', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F120px-Logo_kabupaten_Bojonegoro.png?alt=media&token=85f60e33-2def-46fb-9095-8980a57bd961', 7),
(42, 'Bulukumba', 'bulukumba', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fbulukumba.jpg?alt=media&token=52e0e7bb-eaef-437d-9582-63b7ce921361', 16),
(43, 'Ciamis', 'ciamis', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fciamis.jpg?alt=media&token=ec82e6c1-4197-4cb4-a3d3-162d597996f3', 5),
(44, 'Cianjur', 'cianjur', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fcianjur.jpg?alt=media&token=463cc739-63df-439d-966b-fc277ee2ecb2', 5),
(45, 'Cilacap', 'cilacap', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F150px-Logo-Cilacap.png?alt=media&token=34f3a800-662b-4f35-9bcd-3876857e8a30', 9),
(46, 'Cilegon', 'cilegon', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fcilegon.jpg?alt=media&token=2de82fbc-85a9-4e09-b570-15241d69da48', 3),
(47, 'Cirebon', 'cirebon', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fcirebon.jpg?alt=media&token=3b336b4d-a969-4029-a946-f8ee62c55588', 5),
(48, 'Deli Serdang', 'deli-serdang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fdeliserdang.jpg?alt=media&token=d0603021-33a6-4044-a56b-5df7e1b47d92', 1),
(49, 'Denpasar Raya', 'denpasar-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fdenpasar.jpg?alt=media&token=b9a515a2-02c2-4977-931f-f757e61391bc', 2),
(50, 'Dompu', 'dompu', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fdompu.jpg?alt=media&token=62ca441e-8f31-4bad-9ef5-38813d3810a8', 2),
(51, 'Gorontalo', 'gorontalo', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fgorontalo.jpg?alt=media&token=15b151d5-c53a-4d24-b6ca-469592e847cf', 17),
(52, 'Grobogan', 'grobogan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fgrobogan.jpg?alt=media&token=9858014e-1cae-4599-803c-ba5dcea7da5c', 9),
(53, 'Hong Kong', 'hong-kong', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fhongkong.jpg?alt=media&token=c1fc908b-b355-4693-b076-f2bd16815d7e', 14),
(54, 'Hulu Sungai Utara', 'hulu-sungai-utara', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fhulu_sungai_utara.jpg?alt=media&token=95f66375-1149-4d86-921e-ec5bba232629', 10),
(55, 'Jakarta Barat', 'jakarta-barat', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjakarta.jpg?alt=media&token=a3004dbb-8d91-4141-854d-4b0f7d0fe2ea', 4),
(56, 'Jakarta Pusat', 'jakarta-pusat', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjakarta.jpg?alt=media&token=a3004dbb-8d91-4141-854d-4b0f7d0fe2ea', 4),
(57, 'Jakarta Selatan', 'jakarta-selatan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjakarta.jpg?alt=media&token=a3004dbb-8d91-4141-854d-4b0f7d0fe2ea', 4),
(58, 'Jakarta Timur', 'jakarta-timur', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjakarta.jpg?alt=media&token=a3004dbb-8d91-4141-854d-4b0f7d0fe2ea', 4),
(59, 'Jakarta Utara', 'jakarta-utara', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjakarta.jpg?alt=media&token=a3004dbb-8d91-4141-854d-4b0f7d0fe2ea', 4),
(60, 'Jambi', 'jambi', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjambi.jpg?alt=media&token=0dd344b8-7bad-4304-b649-c416e6dfd8ce', 12),
(61, 'Jayapura Raya', 'jayapura-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjayapura.jpeg?alt=media&token=a0be9d67-38db-497a-8610-dfd9cda0df58', 16),
(62, 'Jember', 'jember', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjember.jpg?alt=media&token=c154462d-11d8-41b4-8be7-a5ab24773ce9', 7),
(63, 'Jentago', 'jentago', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsulsel.jpg?alt=media&token=fd96ccb1-87fa-4d0c-9cbb-f5fd8b5a676a', 16),
(64, 'Jepara', 'jepara', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjepara.jpg?alt=media&token=9b8b6a67-8389-43a9-adcf-207238ea4c43', 9),
(65, 'Jombang', 'jombang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fjepara.jpg?alt=media&token=9b8b6a67-8389-43a9-adcf-207238ea4c43', 8),
(66, 'Karawang', 'karawang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fkarawang.jpg?alt=media&token=79c2b957-72b5-4c95-afbd-85eaf34a8038', 6),
(67, 'Karimun', 'karimun', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Funnamed.gif?alt=media&token=03105329-96b6-4127-b633-ef046f244650', 12),
(68, 'Kendari', 'kendari', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2FLambang_Kota_Kendari.png?alt=media&token=d2bcf13a-6597-4164-a0bd-ccc7afb28452', 16),
(69, 'Klaten', 'klaten', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fklaten.jpg?alt=media&token=b9b50890-b905-495b-96fc-bd940484d334', 9),
(70, 'Korea Selatan', 'korea-selatan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2FkoreaSelatan.jpg?alt=media&token=878aa88f-e37a-4211-92ba-1b10d929db9a', 14),
(71, 'Kutai Barat', 'kutai-barat', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fkutaibarat.jpg?alt=media&token=deed372b-b6cb-4f37-a361-fed71e7a42af', 11),
(72, 'Kutai Kartanegara', 'kutai-kartanegara', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fkutai_kartanegara.jpg?alt=media&token=8369a260-ebe1-4f8a-a4a9-a43932d77241', 11),
(73, 'Lain - Lain', 'lain-lain', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftda.png?alt=media&token=8a25e045-e392-4c04-8949-a59bd538efb6', 4),
(74, 'Lamongan', 'lamongan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Flamongan.jpg?alt=media&token=eb8dd656-b7a7-4e0d-ab9b-8762d87b1ec1', 7),
(75, 'Lampung Timur', 'lampung-timur', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Flampung.jpg?alt=media&token=a8204b7a-4dd4-4132-9cf7-54044869b04e', 13),
(76, 'Langsa', 'langsa', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Flangsa.jpg?alt=media&token=fea9d2fe-325c-4723-b9e5-c73ea5144b85', 1),
(77, 'Lingga', 'lingga', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F240px-Logo-lingga-new.png?alt=media&token=454c9ec3-59b8-4a6b-bbcd-c1b0210bee98', 12),
(78, 'Lombok Timur', 'lombok-timur', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Flombok_timur.jpg?alt=media&token=bd77b215-435d-4568-baa1-d15b7b74f730', 2),
(79, 'Lubuklinggau', 'lubuklinggau', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Flubuklinggau.jpg?alt=media&token=51a70792-35d9-44dd-8c30-2da5702a64a7', 18),
(80, 'Luwu Raya', 'luwu-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fluwu.jpg?alt=media&token=2ba68b11-5d98-496c-847a-9a8f5123a09a', 16),
(81, 'Madiun', 'madiun', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmadiun.jpg?alt=media&token=5e9385dd-4c92-4af4-b66b-33ee372b8256', 8),
(82, 'Magelang', 'magelang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmagelang.jpg?alt=media&token=068296c3-1f87-4e77-a4b0-66c1f4c5fb59', 9),
(83, 'Magetan', 'magetan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F150px-Logo_Kabupaten_Magetan_Vector.jpg?alt=media&token=a057b9aa-1789-4989-922e-39225fe882f1', 8),
(84, 'Mamuju', 'mamuju', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmamuju.jpg?alt=media&token=2b9fa50a-4885-41e8-a153-7816e7ed4d69', 17),
(85, 'Manado', 'manado', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmanado.jpg?alt=media&token=e4f08f59-0649-45dc-8fd4-90bffc0824d7', 17),
(86, 'Merangin', 'merangin', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F100px-Lambang_Kabupaten_Merangin.png?alt=media&token=2222de84-5547-4fc6-929e-eea1f7dc5ee6', 12),
(87, 'Mesir', 'mesir', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmesir.jpg?alt=media&token=27176ad1-c00e-4f85-827e-f3f879100149', 14),
(88, 'Metro Lampung', 'metro-lampung', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmetro.jpg?alt=media&token=4aa78998-fe70-4d5d-abc3-2d49acd566ed', 13),
(89, 'Mojokerto', 'mojokerto', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fmojokerto.jpg?alt=media&token=126fc12e-48bb-46e2-b171-8059832a20bc', 7),
(90, 'Muara Bungo', 'muara-bungo', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F140px-Lambang_Kabupaten_Bungo.png?alt=media&token=01cff37e-a79d-4de3-9bf3-d6659054ee3a', 12),
(91, 'Nabire', 'nabire', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fnabire.jpg?alt=media&token=478bdace-fe52-4d7c-a9d5-b22e9eccd99d', 16),
(92, 'Nganjuk', 'nganjuk', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fnganjuk.jpg?alt=media&token=f8fbca7d-e1da-48a8-a2af-bba4c15f2471', 8),
(93, 'OKU Raya', 'oku-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Foku_raya.jpg?alt=media&token=c2529557-4161-4df4-82b2-102d29cd9201', 18),
(94, 'PPU Paser', 'ppu-paser', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fppu-paser.jpg?alt=media&token=e5be92ca-6ea5-4951-883a-4886a10bc7e8', 11),
(95, 'Pacitan', 'pacitan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpacitan.jpg?alt=media&token=91b1725f-ab3b-4cce-b3d3-34a6b5968be7', 8),
(96, 'Pamekasan', 'pamekasan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpamekasan.jpg?alt=media&token=75cbf062-9424-467b-8567-40a108d64fd6', 7),
(97, 'Pandeglang', 'pandeglang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpandeglang.jpg?alt=media&token=4ab68f38-6aa1-437f-8398-477c6da6b111', 3),
(98, 'Pasuruan', 'pasuruan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpasuruan.jpg?alt=media&token=863d7a1a-b38d-4f37-a3e8-8e349285f5a1', 7),
(99, 'Pati', 'pati', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpati.jpg?alt=media&token=117b8182-c44f-4c9c-b169-cc5b38e06622', 9),
(100, 'Payakumbuh', 'payakumbuh', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpayakumbuh.jpg?alt=media&token=5a537ef0-2d97-4b57-93f4-dd6fb2e0b519', 15),
(101, 'Pemalang', 'pemalang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpemalang.jpg?alt=media&token=49fa0074-a41b-412d-bff9-5ad84c876d97', 9),
(102, 'Perth', 'perth', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fperth.jpg?alt=media&token=748f9f19-3f2b-48d8-b14e-905fc98a879f', 14),
(103, 'Pontianak', 'pontianak', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpontianak.jpg?alt=media&token=3d9471ba-f337-44e6-8163-e3921f6c031d', 11),
(104, 'Pringsewu', 'pringsewu', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpringsewu.jpg?alt=media&token=e29794e2-74f9-4bf1-bd8b-6619afe01275', 13),
(105, 'Purwakarta', 'purwakarta', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fpurwakarta.jpg?alt=media&token=7e0aa0b4-1e39-4bc6-aa11-95e594e5e27a', 5),
(106, 'Rokan Hilir', 'rokan-hilir', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Frokan_hilir.png?alt=media&token=6f8b8bfb-7289-491c-9084-389ecb97bed6', 15),
(107, 'Salatiga', 'salatiga', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsalatiga.jpg?alt=media&token=9ffacf1f-39d1-4712-8746-88d5de73a068', 9),
(108, 'Sangatta', 'sangatta', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsangatta.jpg?alt=media&token=1f42a069-9351-4ef4-956a-4d626700dee1', 11),
(109, 'Serang', 'serang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fserang.jpg?alt=media&token=fe9ce1c8-165f-4989-b4cf-b504abe566b3', 3),
(110, 'Sidoarjo', 'sidoarjo', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsidoarjo.jpg?alt=media&token=fa7cbecd-0cfd-4874-933b-09c1814a4396', 7),
(111, 'Singapore', 'singapore', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsingapore.jpg?alt=media&token=71db6bd2-8026-4447-b98c-d57edd0629cc', 14),
(112, 'Solo Raya', 'solo-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsurakarta.jpg?alt=media&token=b982faee-458d-4299-89ef-8818825ee532', 9),
(113, 'Sukabumi Raya', 'sukabumi-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsukabumi.jpg?alt=media&token=839612e8-d3de-4457-9cd7-bc70b4d2363d', 5),
(114, 'Sumedang', 'sumedang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fsumedang.jpg?alt=media&token=34e221bd-0637-4769-9b90-beddecdc5f3a', 5),
(115, 'Tangerang Raya', 'tangerang-raya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftangerang.jpg?alt=media&token=170fd4b3-ad3d-4ccd-ace8-edb8c330994e', 3),
(116, 'Tanjungpinang', 'tanjungpinang', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftanjung_pinang.jpg?alt=media&token=ec145c09-fae0-426a-b952-1f5b2f7b9447', 12),
(117, 'Tarakan', 'tarakan', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftarakan.png?alt=media&token=d84f61f5-546a-4236-b65e-c12a876f1429', 11),
(118, 'Tasikmalaya', 'tasikmalaya', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftasikmalaya.jpg?alt=media&token=2b85035e-5e01-4cf0-9384-37d011aca78b', 5),
(119, 'Tegal', 'tegal', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftegal.jpg?alt=media&token=2b74a7f5-5407-4318-977e-9e5f4655a0f0', 9),
(120, 'Tojo Una-una', 'tojo-una-una', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftojo-una-una.jpg?alt=media&token=2c239791-d2a4-4b61-b4c6-73095bfb24e6', 16),
(121, 'Trenggalek', 'trenggalek', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2FTrenggalek_coat_of_arms.png?alt=media&token=ca9ddda6-cf4b-4e77-a805-d18bfc01d007', 8),
(122, 'Tuban', 'tuban', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftuban.jpg?alt=media&token=d7aae632-048d-49ce-969c-23993ff8e797', 7),
(123, 'Tulang Bawang Barat', 'tulang-bawang-barat', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Ftulangbawang.jpg?alt=media&token=84fbfd73-d09c-452c-95de-b0cc824816d1', 13),
(124, 'Way Kanan Lampung', 'way-kanan-lampung', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fwaykananlampung.jpg?alt=media&token=2992b75c-35d7-41c0-884a-4141aa9053c6', 13),
(125, 'Wonosobo', 'wonosobo', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2F150px-Kabupaten_Wonosobo.png?alt=media&token=1d0e8718-1359-4a2c-801d-6cc594fd75d2', 9),
(126, 'Yogyakarta', 'yogyakarta', 'https://firebasestorage.googleapis.com/v0/b/passport-1eecf.appspot.com/o/kota%2Fyogyakarta.jpg?alt=media&token=5077dea2-ca23-48ed-96de-6f00e7aa0a68', 9);

-- --------------------------------------------------------

--
-- Table structure for table `t_wilayah_induk`
--

CREATE TABLE `t_wilayah_induk` (
  `id` int(11) NOT NULL,
  `wilayah_induk` varchar(100) NOT NULL,
  `slug` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_wilayah_induk`
--

INSERT INTO `t_wilayah_induk` (`id`, `wilayah_induk`, `slug`) VALUES
(1, 'Aceh & Sumatera Utara', 'aceh-dan-sumatera-utara'),
(2, 'Bali & Nusa Tenggara', 'bali-dan-nusa-tenggara'),
(3, 'Banten', 'banten'),
(4, 'DKI Jakarta', 'dki-jakarta'),
(5, 'Jawa Barat 1', 'jawa-barat-1'),
(6, 'Jawa Barat 2', 'jawa-barat-2'),
(7, 'Jawa Timur 1', 'jawa-timur-1'),
(8, 'Jawa Timur 2', 'jawa-timur-2'),
(9, 'Jogja & Jawa Tengah', 'jogja-dan-jawa-tengah'),
(10, 'Kalimantan Selatan & Utara', 'kalimantan-selatan-dan-utara'),
(11, 'Kalimantan Timur & Barat', 'kalimantan-timur-dan-barat'),
(12, 'Kepulauan Riau & Jambi', 'kepulauan-riau-dan-jambi'),
(13, 'Lampung & Bengkulu', 'lampung-dan-bengkulu'),
(14, 'Luar Negeri', 'luar-negeri'),
(15, 'Riau & Sumatera Barat', 'riau-dan-sumatera-barat'),
(16, 'Sulawesi Selatan, Tenggara & Papua', 'sulawesi-selatan-tenggara-dan-papua'),
(17, 'Sulawesi Utara & Barat', 'sulawesi-utara-dan-barat'),
(18, 'Sumatera Selatan & Bangka Belitung', 'sumatera-selatan-dan-bangka-belitung');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_aff_pw2019`
--
ALTER TABLE `t_aff_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_api_log`
--
ALTER TABLE `t_api_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_berita`
--
ALTER TABLE `t_berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_blog`
--
ALTER TABLE `t_blog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_booth_pw2019`
--
ALTER TABLE `t_booth_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_budget`
--
ALTER TABLE `t_budget`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_budget_item`
--
ALTER TABLE `t_budget_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_device_log`
--
ALTER TABLE `t_device_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_email_comingsoon`
--
ALTER TABLE `t_email_comingsoon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_email_log`
--
ALTER TABLE `t_email_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_email_pw2019_log`
--
ALTER TABLE `t_email_pw2019_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_entrun_peserta`
--
ALTER TABLE `t_entrun_peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_event`
--
ALTER TABLE `t_event`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_event_carousel`
--
ALTER TABLE `t_event_carousel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_event_tiket`
--
ALTER TABLE `t_event_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_kodebayar`
--
ALTER TABLE `t_expo_kodebayar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_loginlog`
--
ALTER TABLE `t_expo_loginlog`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_note_pw2019`
--
ALTER TABLE `t_expo_note_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_pw2019`
--
ALTER TABLE `t_expo_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_tambahan`
--
ALTER TABLE `t_expo_tambahan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_expo_tiket`
--
ALTER TABLE `t_expo_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_fnc_pwd_akun`
--
ALTER TABLE `t_fnc_pwd_akun`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_hotel`
--
ALTER TABLE `t_hotel`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_hotel_kamar`
--
ALTER TABLE `t_hotel_kamar`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_hotel_kode`
--
ALTER TABLE `t_hotel_kode`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_hotel_test`
--
ALTER TABLE `t_hotel_test`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jadwal`
--
ALTER TABLE `t_jadwal`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_jadwal_lokasi`
--
ALTER TABLE `t_jadwal_lokasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_konfirmasi`
--
ALTER TABLE `t_konfirmasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kupon`
--
ALTER TABLE `t_kupon`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_kwitansi`
--
ALTER TABLE `t_kwitansi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_layanan`
--
ALTER TABLE `t_layanan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_nicepay_log`
--
ALTER TABLE `t_nicepay_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_nicepay_sajah`
--
ALTER TABLE `t_nicepay_sajah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_param_pw2019`
--
ALTER TABLE `t_param_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_partner`
--
ALTER TABLE `t_partner`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_peserta`
--
ALTER TABLE `t_peserta`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rekening`
--
ALTER TABLE `t_rekening`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rekening_mutasi`
--
ALTER TABLE `t_rekening_mutasi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_rekening_text`
--
ALTER TABLE `t_rekening_text`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_spoin_pw2019`
--
ALTER TABLE `t_spoin_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sponsor`
--
ALTER TABLE `t_sponsor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_sponsor_level`
--
ALTER TABLE `t_sponsor_level`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiket`
--
ALTER TABLE `t_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiketgroup_pw2019`
--
ALTER TABLE `t_tiketgroup_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiketgroup_trans`
--
ALTER TABLE `t_tiketgroup_trans`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiket_issued`
--
ALTER TABLE `t_tiket_issued`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiket_scanqr`
--
ALTER TABLE `t_tiket_scanqr`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tiket_wilayah`
--
ALTER TABLE `t_tiket_wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_tipebooth_pw2019`
--
ALTER TABLE `t_tipebooth_pw2019`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user`
--
ALTER TABLE `t_user`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user_group`
--
ALTER TABLE `t_user_group`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_user_tiket`
--
ALTER TABLE `t_user_tiket`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_wilayah`
--
ALTER TABLE `t_wilayah`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `t_wilayah_induk`
--
ALTER TABLE `t_wilayah_induk`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_aff_pw2019`
--
ALTER TABLE `t_aff_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_api_log`
--
ALTER TABLE `t_api_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_berita`
--
ALTER TABLE `t_berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_blog`
--
ALTER TABLE `t_blog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_booth_pw2019`
--
ALTER TABLE `t_booth_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=438;

--
-- AUTO_INCREMENT for table `t_budget`
--
ALTER TABLE `t_budget`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_budget_item`
--
ALTER TABLE `t_budget_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_device_log`
--
ALTER TABLE `t_device_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_email_comingsoon`
--
ALTER TABLE `t_email_comingsoon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_email_log`
--
ALTER TABLE `t_email_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_email_pw2019_log`
--
ALTER TABLE `t_email_pw2019_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_entrun_peserta`
--
ALTER TABLE `t_entrun_peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_event`
--
ALTER TABLE `t_event`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `t_event_carousel`
--
ALTER TABLE `t_event_carousel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_event_tiket`
--
ALTER TABLE `t_event_tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_expo_kodebayar`
--
ALTER TABLE `t_expo_kodebayar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1000;

--
-- AUTO_INCREMENT for table `t_expo_loginlog`
--
ALTER TABLE `t_expo_loginlog`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_expo_note_pw2019`
--
ALTER TABLE `t_expo_note_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_expo_pw2019`
--
ALTER TABLE `t_expo_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_expo_tambahan`
--
ALTER TABLE `t_expo_tambahan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_expo_tiket`
--
ALTER TABLE `t_expo_tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_fnc_pwd_akun`
--
ALTER TABLE `t_fnc_pwd_akun`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT for table `t_hotel`
--
ALTER TABLE `t_hotel`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `t_hotel_kamar`
--
ALTER TABLE `t_hotel_kamar`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_hotel_kode`
--
ALTER TABLE `t_hotel_kode`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=401;

--
-- AUTO_INCREMENT for table `t_hotel_test`
--
ALTER TABLE `t_hotel_test`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `t_jadwal`
--
ALTER TABLE `t_jadwal`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `t_jadwal_lokasi`
--
ALTER TABLE `t_jadwal_lokasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_konfirmasi`
--
ALTER TABLE `t_konfirmasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_kupon`
--
ALTER TABLE `t_kupon`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_kwitansi`
--
ALTER TABLE `t_kwitansi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_layanan`
--
ALTER TABLE `t_layanan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `t_nicepay_log`
--
ALTER TABLE `t_nicepay_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_nicepay_sajah`
--
ALTER TABLE `t_nicepay_sajah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_param_pw2019`
--
ALTER TABLE `t_param_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_partner`
--
ALTER TABLE `t_partner`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_peserta`
--
ALTER TABLE `t_peserta`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_rekening`
--
ALTER TABLE `t_rekening`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_rekening_mutasi`
--
ALTER TABLE `t_rekening_mutasi`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_rekening_text`
--
ALTER TABLE `t_rekening_text`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_spoin_pw2019`
--
ALTER TABLE `t_spoin_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_sponsor`
--
ALTER TABLE `t_sponsor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_sponsor_level`
--
ALTER TABLE `t_sponsor_level`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_tiket`
--
ALTER TABLE `t_tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `t_tiketgroup_pw2019`
--
ALTER TABLE `t_tiketgroup_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tiketgroup_trans`
--
ALTER TABLE `t_tiketgroup_trans`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tiket_issued`
--
ALTER TABLE `t_tiket_issued`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tiket_scanqr`
--
ALTER TABLE `t_tiket_scanqr`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tiket_wilayah`
--
ALTER TABLE `t_tiket_wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_tipebooth_pw2019`
--
ALTER TABLE `t_tipebooth_pw2019`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_user`
--
ALTER TABLE `t_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `t_user_group`
--
ALTER TABLE `t_user_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `t_user_tiket`
--
ALTER TABLE `t_user_tiket`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_wilayah`
--
ALTER TABLE `t_wilayah`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=127;

--
-- AUTO_INCREMENT for table `t_wilayah_induk`
--
ALTER TABLE `t_wilayah_induk`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
